import {Builder} from '/data/files/projects/Kode/kwruntime/std/package/compiler/build.ts'
import Path from 'path'
import fs from 'fs'

export class Program{
	static async main(){
		let workingFolder = Path.join(__dirname, "..", "transpiled")
		if(!fs.existsSync(workingFolder)) fs.mkdirSync(workingFolder)


		let builder = new Builder({
			target:'node',

		})
		await builder.compile(Path.join(__dirname, "mod.ts"))
		await builder.writeTo(Path.join(__dirname, "..", "transpiled", "mod.js"))
	}
}