#!/usr/bin/env bash


#!/usr/bin/env bash

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
curl -fsSL https://fnm.vercel.app/install | bash
export PATH="$HOME/.local/share/fnm:$PATH"
fnm install 20
eval "$(fnm env)"
fnm use 20 

npx -y @kwruntime/installer




if [ "$EUID" -eq 0 ]
then
	export _HOME=/usr/local/KwRuntime
	export _APPS=/Applications
else
	export _HOME="$HOME/KwRuntime"
	export _APPS="$HOME/Applications"
fi


if [[ ! -e "$_HOME" ]]; then
    mkdir "$_HOME"
fi

if [[ ! -e "$_HOME/bin" ]]; then
    mkdir "$_HOME/bin"
fi
mkdir -p "$_HOME/src"


curl -L "https://raw.githubusercontent.com/kwruntime/distribution/main/core/mac/KwRuntime.app.tar.gz" -o "$_HOME/src/KwRuntime.app.tar.gz"
cd $_APPS
tar xvf "$_HOME/src/KwRuntime.app.tar.gz"

