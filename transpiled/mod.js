
//KWRUNTIME-DISABLE-TRANSPILATION
var $$Files = {}, $$NPMRequires = null, $$NodeRequire = null, $$filename = null;
try {
  $$NodeRequire = require;
} catch (e) {
}
;
try {
  $$filename = __filename;
} catch (e) {
}
$$Files["/data/files/projects/Kode/kwruntime/installer/src/mod.ts"] = function() {
  var $$modParams = arguments[0];
  var module2 = $$modParams["module"];
  var require2 = $$modParams["require"];
  var global2 = $$modParams["global"];
  var Buffer = $$modParams["Buffer"];
  var importMeta = { "meta": { "url": "file:///data/files/projects/Kode/kwruntime/installer/src/mod.ts" } };
  var KModule = $$modParams["KModule"];
  var asyncRequire = $$modParams["asyncRequire"];
  var exports = $$modParams["exports"];
  var preloadedModules = [];
  if ($$NodeRequire)
    require2 = $$NodeRequire;
  preloadedModules[0] = $$NPMRequires["axios@0.27.2"];
  preloadedModules[1] = $$NPMRequires["tar@6.1.11"];
  preloadedModules[2] = KModule.require("os");
  preloadedModules[3] = KModule.require("path");
  preloadedModules[4] = KModule.require("fs");
  preloadedModules[5] = KModule.require("https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/async.ts");
  preloadedModules[6] = KModule.require("https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/exception.ts");
  preloadedModules[7] = KModule.require("child_process");
  "use strict";
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.Program = exports.langs = void 0;
  var _axios = _interopRequireDefault(preloadedModules[0]);
  var _tar = _interopRequireDefault(preloadedModules[1]);
  var _os = _interopRequireDefault(preloadedModules[2]);
  var _path = _interopRequireDefault(preloadedModules[3]);
  var _fs = _interopRequireDefault(preloadedModules[4]);
  var async = _interopRequireWildcard(preloadedModules[5]);
  var _exception = preloadedModules[6];
  var _child_process = _interopRequireDefault(preloadedModules[7]);
  function _getRequireWildcardCache(nodeInterop) {
    if (typeof WeakMap !== "function")
      return null;
    var cacheBabelInterop = /* @__PURE__ */ new WeakMap();
    var cacheNodeInterop = /* @__PURE__ */ new WeakMap();
    return (_getRequireWildcardCache = function(nodeInterop2) {
      return nodeInterop2 ? cacheNodeInterop : cacheBabelInterop;
    })(nodeInterop);
  }
  function _interopRequireWildcard(obj, nodeInterop) {
    if (!nodeInterop && obj && obj.__esModule) {
      return obj;
    }
    if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
      return { default: obj };
    }
    var cache = _getRequireWildcardCache(nodeInterop);
    if (cache && cache.has(obj)) {
      return cache.get(obj);
    }
    var newObj = {};
    var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;
    for (var key in obj) {
      if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) {
        var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;
        if (desc && (desc.get || desc.set)) {
          Object.defineProperty(newObj, key, desc);
        } else {
          newObj[key] = obj[key];
        }
      }
    }
    newObj.default = obj;
    if (cache) {
      cache.set(obj, newObj);
    }
    return newObj;
  }
  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : { default: obj };
  }
  var langs = {
    "en": {
      "onerror": "> Failed to install",
      "nofiles": "No suitable files found for",
      "notnodejs": "NodeJS cannot be installed. Test installing manually",
      "installing": "Installing KwRuntime, please wait a moment.",
      "finished": {
        text: "Press enter to close.",
        button: "CLOSE"
      }
    },
    "es": {
      "onerror": "> Fall\xF3 la instalaci\xF3n",
      "nofiles": "La instalaci\xF3n no est\xE1 disponible para",
      "notnodejs": "No se pudo instalar NodeJS. Pruebe instalando manualmente",
      "installing": "Instalando KwRuntime, por favor espere un momento.",
      "finished": {
        text: "Presione enter para cerrar",
        button: "CERRAR"
      }
    }
  };
  exports.langs = langs;
  class Program {
    static async uiInstall(lang = "en") {
      process.stdout.write("\x1Bc");
      console.clear();
      process.title = "KwRuntime Installer";
      try {
        this.logo(lang);
        await this.install(lang);
      } catch (e) {
        console.error("\x1B[31m\x1B[1m" + langs[lang].onerror + ":\x1B[0m", e.message);
      } finally {
        let text = langs[lang].finished.text;
        let button = "  " + langs[lang].finished.button + "  ";
        let diff = Math.max(Number(((process.stdout.columns - text.length) / 2).toFixed(0)), 0);
        let diff2 = Math.max(Number(((process.stdout.columns - button.length) / 2).toFixed(0)), 0);
        process.stdout.write(`
${" ".repeat(diff) + text}
${" ".repeat(diff2) + "\x1B[47m\x1B[30m" + button}\x1B[0m`);
        if (process.stdin.setRawMode) {
          process.stdin.setRawMode(true);
          process.stdin.resume();
          process.stdin.on("data", function() {
            console.info();
            process.exit(0);
          });
        }
      }
    }
    static logo(lang = "en") {
      let words = [`
\u2591\u2588\u2500\u2584\u2580 \u2591\u2588\u2500\u2500\u2591\u2588 \u2591\u2588\u2580\u2580\u2588 \u2591\u2588\u2500\u2591\u2588 \u2591\u2588\u2584\u2500\u2591\u2588 \u2580\u2580\u2588\u2580\u2580 \u2580\u2588\u2580 \u2591\u2588\u2580\u2584\u2580\u2588 \u2591\u2588\u2580\u2580\u2580 
\u2591\u2588\u2580\u2584\u2500 \u2591\u2588\u2591\u2588\u2591\u2588 \u2591\u2588\u2584\u2584\u2580 \u2591\u2588\u2500\u2591\u2588 \u2591\u2588\u2591\u2588\u2591\u2588 \u2500\u2591\u2588\u2500\u2500 \u2591\u2588\u2500 \u2591\u2588\u2591\u2588\u2591\u2588 \u2591\u2588\u2580\u2580\u2580 
\u2591\u2588\u2500\u2591\u2588 \u2591\u2588\u2584\u2580\u2584\u2588 \u2591\u2588\u2500\u2591\u2588 \u2500\u2580\u2584\u2584\u2580 \u2591\u2588\u2500\u2500\u2580\u2588 \u2500\u2591\u2588\u2500\u2500 \u2584\u2588\u2584 \u2591\u2588\u2500\u2500\u2591\u2588 \u2591\u2588\u2584\u2584\u2584`, `
 \u2580\u2588\u2580 \u2591\u2588\u2584\u2500\u2591\u2588 \u2591\u2588\u2580\u2580\u2580\u2588 \u2580\u2580\u2588\u2580\u2580 \u2500\u2588\u2580\u2580\u2588 \u2591\u2588\u2500\u2500\u2500 \u2591\u2588\u2500\u2500\u2500 \u2591\u2588\u2580\u2580\u2580 \u2591\u2588\u2580\u2580\u2588 
 \u2591\u2588\u2500 \u2591\u2588\u2591\u2588\u2591\u2588 \u2500\u2580\u2580\u2580\u2584\u2584 \u2500\u2591\u2588\u2500\u2500 \u2591\u2588\u2584\u2584\u2588 \u2591\u2588\u2500\u2500\u2500 \u2591\u2588\u2500\u2500\u2500 \u2591\u2588\u2580\u2580\u2580 \u2591\u2588\u2584\u2584\u2580 
 \u2584\u2588\u2584 \u2591\u2588\u2500\u2500\u2580\u2588 \u2591\u2588\u2584\u2584\u2584\u2588 \u2500\u2591\u2588\u2500\u2500 \u2591\u2588\u2500\u2591\u2588 \u2591\u2588\u2584\u2584\u2588 \u2591\u2588\u2584\u2584\u2588 \u2591\u2588\u2584\u2584\u2584 \u2591\u2588\u2500\u2591\u2588`];
      let logoWords = words.map((a) => a.substring(1).split("\n"));
      let diff = Number(((process.stdout.columns - logoWords[0][0].length) / 2).toFixed(0));
      if (diff < 0) {
        logoWords = [["------------------------", "  KWRUNTIME INSTALLER   ", "------------------------"]];
        diff = Math.max(Number(((process.stdout.columns - logoWords[0][0].length) / 2).toFixed(0)), 0);
      }
      const toWrite = logoWords.map((a) => a.map((b) => " ".repeat(diff) + b).join("\n"));
      console.info(`

\x1B[32m\x1B[1m${toWrite.join("\n\n")}\x1B[0m


\x1B[33m> ${langs[lang].installing}\x1B[0m`);
    }
    static async install(lang = "en") {
      let kwruntimeFolder = _path.default.join(_os.default.homedir(), "KwRuntime");
      if (!_fs.default.existsSync(kwruntimeFolder))
        _fs.default.mkdirSync(kwruntimeFolder);
      let bin = _path.default.join(kwruntimeFolder, "bin");
      if (!_fs.default.existsSync(bin))
        _fs.default.mkdirSync(bin);
      let lib = _path.default.join(kwruntimeFolder, "lib");
      if (!_fs.default.existsSync(lib))
        _fs.default.mkdirSync(lib);
      let runtime = _path.default.join(kwruntimeFolder, "runtime");
      if (!_fs.default.existsSync(runtime))
        _fs.default.mkdirSync(runtime);
      let arch = _os.default.arch();
      if (arch == "ia32")
        arch = "x86";
      let response = await (0, _axios.default)({
        method: "GET",
        url: "https://gitlab.com/kwruntime/installer/-/raw/main/install.info.json"
      });
      let platformData = response.data[_os.default.platform()];
      let files = platformData === null || platformData === void 0 ? void 0 : platformData.files;
      if (!files) {
        throw _exception.Exception.create(`${langs[lang].nofiles}: ${_os.default.platform}-${arch}`).putCode("UNSUPPORTED_PLATFORM");
      }
      for (let file of files) {
        let out = _path.default.join(kwruntimeFolder, file.path);
        let response2 = await (0, _axios.default)({
          method: "GET",
          url: file.href,
          responseType: "arraybuffer"
        });
        let bytes = Buffer.from(response2.data);
        await _fs.default.promises.writeFile(out, bytes);
        if (file.compression == "tar+gz") {
          let def2 = new async.Deferred();
          let sr = _fs.default.createReadStream(out);
          sr.on("error", def2.reject);
          let sw = _tar.default.x({
            C: _path.default.dirname(out)
          });
          sw.on("error", def2.reject);
          sw.on("finish", def2.resolve);
          sr.pipe(sw);
          await def2.promise;
        }
      }
      let nodeversion = process.version.split(".").map((a) => Number(a[0] == "v" ? a.substring(1) : a));
      if (nodeversion[0] < 14) {
        let nodeInfo = platformData.node[arch];
        if (!nodeInfo) {
          throw _exception.Exception.create(`${langs[lang].nofiles}: ${_os.default.platform}-${arch}`).putCode("UNSUPPORTED_PLATFORM");
        }
        if (_os.default.platform() == "win32") {
          let release = _os.default.release().split(".").map(Number);
          if (release[0] <= 6 && release[1] <= 1) {
            nodeInfo = nodeInfo.filter((a) => a.os == "<windows8");
          }
        }
        let installable = nodeInfo[0];
        if (!installable) {
          throw _exception.Exception.create(`${langs[lang].nofiles}: ${_os.default.platform}-${arch}`).putCode("UNSUPPORTED_PLATFORM");
        }
        let nodeTar = _path.default.join(bin, installable.name);
        let response2 = await (0, _axios.default)({
          method: "GET",
          url: installable.href,
          responseType: "arraybuffer"
        });
        let bytes = Buffer.from(response2.data);
        await _fs.default.promises.writeFile(nodeTar, bytes);
        let def2 = new async.Deferred();
        let sr = _fs.default.createReadStream(nodeTar);
        sr.on("error", def2.reject);
        let sw = _tar.default.x({
          C: _path.default.dirname(nodeTar)
        });
        sw.on("error", def2.reject);
        sw.on("finish", def2.resolve);
        sr.pipe(sw);
        await def2.promise;
      } else {
        let nodefolder = _path.default.join(bin, arch);
        if (!_fs.default.existsSync(nodefolder))
          _fs.default.mkdirSync(nodefolder);
        nodefolder = _path.default.join(nodefolder, process.version);
        if (!_fs.default.existsSync(nodefolder))
          _fs.default.mkdirSync(nodefolder);
        let nodeexe2 = _path.default.join(nodefolder, "node");
        if (_os.default.platform() == "win32")
          nodeexe2 += ".exe";
        if (process.execPath != nodeexe2) {
          if (!_fs.default.existsSync(nodeexe2)) {
            try {
              await _fs.default.promises.copyFile(process.execPath, nodeexe2);
            } catch (e) {
            }
          }
        }
      }
      let folders = await _fs.default.promises.readdir(_path.default.join(bin, arch));
      let bynumber = {};
      let convertVersion = function(version) {
        let items = version.split(".").map(Number);
        let factor = 1, value = 0;
        for (let i = items.length - 1; i >= 0; i--) {
          if (isNaN(items[i])) {
            items[i] = 0;
          }
          value += items[i] * factor;
          factor *= 1e4;
        }
        bynumber[value] = version;
        return value;
      };
      let majorversion = folders.map(convertVersion).sort((a, b) => a - b).reverse()[0];
      if (!majorversion) {
        throw _exception.Exception.create(`${langs[lang].notnodejs}`).putCode("NODEJS_NOT_FOUND");
      }
      let nodeexe = _path.default.join(bin, arch, bynumber[majorversion], "node");
      if (_os.default.platform() == "win32")
        nodeexe += ".exe";
      let def = new async.Deferred();
      let p = _child_process.default.spawn(process.execPath, [_path.default.join(runtime, "kwruntime.js"), "--self-install"], {
        stdio: "inherit",
        env: Object.assign({}, process.env, {
          NODE_SKIP_PLATFORM_CHECK: "1"
        })
      });
      p.once("exit", def.resolve);
      p.once("error", def.reject);
      await def.promise;
    }
  }
  exports.Program = Program;
};
$$Files["https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/async.ts"] = function() {
  var $$modParams = arguments[0];
  var module2 = $$modParams["module"];
  var require2 = $$modParams["require"];
  var global2 = $$modParams["global"];
  var Buffer = $$modParams["Buffer"];
  var importMeta = { "meta": { "url": "https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/async.ts", "uri": "https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/async.ts" } };
  var KModule = $$modParams["KModule"];
  var asyncRequire = $$modParams["asyncRequire"];
  var exports = $$modParams["exports"];
  if ($$NodeRequire)
    require2 = $$NodeRequire;
  "use strict";
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.sleep = sleep;
  exports.DelayedTask = exports.Deferred = void 0;
  function _defineProperty2(obj, key, value) {
    if (key in obj) {
      Object.defineProperty(obj, key, { value, enumerable: true, configurable: true, writable: true });
    } else {
      obj[key] = value;
    }
    return obj;
  }
  class Deferred {
    constructor() {
      this._promise = new Promise((resolve, reject) => {
        this.resolve = resolve;
        this.reject = reject;
      });
    }
    get promise() {
      return this._promise;
    }
  }
  exports.Deferred = Deferred;
  class DelayedTask {
    constructor(promise) {
      _defineProperty2(this, "_end", false);
      this._promise = promise;
      this._promise.then((value) => {
        this._value = value;
        this._end = true;
        this._check();
      }).catch((er) => {
        this._error = er;
        this._end = true;
        this._check();
      });
    }
    _check() {
      if (this._generateddeferred && this._end) {
        if (this._error)
          return this._generateddeferred.reject(this._error);
        return this._generateddeferred.resolve(this._value);
      }
    }
    get deferred() {
      if (!this._generateddeferred) {
        this._generateddeferred = new Deferred();
        setImmediate(this._check.bind(this));
      }
      return this._generateddeferred;
    }
    get promise() {
      return this.deferred.promise;
    }
  }
  exports.DelayedTask = DelayedTask;
  function sleep(timeout = 0) {
    return new Promise(function(resolve) {
      setTimeout(resolve, timeout);
    });
  }
};
$$Files["https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/exception.ts"] = function() {
  var $$modParams = arguments[0];
  var module2 = $$modParams["module"];
  var require2 = $$modParams["require"];
  var global2 = $$modParams["global"];
  var Buffer = $$modParams["Buffer"];
  var importMeta = { "meta": { "url": "https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/exception.ts", "uri": "https://raw.githubusercontent.com/kwruntime/std/1.1.19/util/exception.ts" } };
  var KModule = $$modParams["KModule"];
  var asyncRequire = $$modParams["asyncRequire"];
  var exports = $$modParams["exports"];
  if ($$NodeRequire)
    require2 = $$NodeRequire;
  "use strict";
  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = exports.Exception = void 0;
  class Exception extends Error {
    static create(message, innerException = null) {
      let e = new Exception(message);
      if (innerException) {
        e.innerException = innerException;
      }
      return e;
    }
    putCode(code) {
      this.code = code;
      return this;
    }
    putStack(stack) {
      this.stack = stack;
      return this;
    }
    putMessage(message) {
      this.message = message;
      return this;
    }
    raise() {
      throw this;
    }
  }
  exports.Exception = Exception;
  var _default = Exception;
  exports.default = _default;
};
$$Files["/home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/$compiled.ts"] = function() {
  var $$modParams = arguments[0];
  var module2 = $$modParams["module"];
  var require2 = $$modParams["require"];
  var global2 = $$modParams["global"];
  var Buffer = $$modParams["Buffer"];
  var importMeta = { "meta": { "url": "file:///home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/$compiled.ts" } };
  var KModule = $$modParams["KModule"];
  var asyncRequire = $$modParams["asyncRequire"];
  var exports = $$modParams["exports"];
  if ($$NodeRequire)
    require2 = $$NodeRequire;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var require_bind = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/bind.js"(exports2, module22) {
      "use strict";
      module22.exports = function bind(fn, thisArg) {
        return function wrap() {
          var args = new Array(arguments.length);
          for (var i = 0; i < args.length; i++) {
            args[i] = arguments[i];
          }
          return fn.apply(thisArg, args);
        };
      };
    }
  });
  var require_utils = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/utils.js"(exports2, module22) {
      "use strict";
      var bind = require_bind();
      var toString = Object.prototype.toString;
      var kindOf = function(cache) {
        return function(thing) {
          var str = toString.call(thing);
          return cache[str] || (cache[str] = str.slice(8, -1).toLowerCase());
        };
      }(/* @__PURE__ */ Object.create(null));
      function kindOfTest(type) {
        type = type.toLowerCase();
        return function isKindOf(thing) {
          return kindOf(thing) === type;
        };
      }
      function isArray(val) {
        return Array.isArray(val);
      }
      function isUndefined(val) {
        return typeof val === "undefined";
      }
      function isBuffer(val) {
        return val !== null && !isUndefined(val) && val.constructor !== null && !isUndefined(val.constructor) && typeof val.constructor.isBuffer === "function" && val.constructor.isBuffer(val);
      }
      var isArrayBuffer = kindOfTest("ArrayBuffer");
      function isArrayBufferView(val) {
        var result;
        if (typeof ArrayBuffer !== "undefined" && ArrayBuffer.isView) {
          result = ArrayBuffer.isView(val);
        } else {
          result = val && val.buffer && isArrayBuffer(val.buffer);
        }
        return result;
      }
      function isString(val) {
        return typeof val === "string";
      }
      function isNumber(val) {
        return typeof val === "number";
      }
      function isObject(val) {
        return val !== null && typeof val === "object";
      }
      function isPlainObject(val) {
        if (kindOf(val) !== "object") {
          return false;
        }
        var prototype = Object.getPrototypeOf(val);
        return prototype === null || prototype === Object.prototype;
      }
      var isDate = kindOfTest("Date");
      var isFile = kindOfTest("File");
      var isBlob = kindOfTest("Blob");
      var isFileList = kindOfTest("FileList");
      function isFunction(val) {
        return toString.call(val) === "[object Function]";
      }
      function isStream(val) {
        return isObject(val) && isFunction(val.pipe);
      }
      function isFormData(thing) {
        var pattern = "[object FormData]";
        return thing && (typeof FormData === "function" && thing instanceof FormData || toString.call(thing) === pattern || isFunction(thing.toString) && thing.toString() === pattern);
      }
      var isURLSearchParams = kindOfTest("URLSearchParams");
      function trim(str) {
        return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g, "");
      }
      function isStandardBrowserEnv() {
        if (typeof navigator !== "undefined" && (navigator.product === "ReactNative" || navigator.product === "NativeScript" || navigator.product === "NS")) {
          return false;
        }
        return typeof window !== "undefined" && typeof document !== "undefined";
      }
      function forEach(obj, fn) {
        if (obj === null || typeof obj === "undefined") {
          return;
        }
        if (typeof obj !== "object") {
          obj = [obj];
        }
        if (isArray(obj)) {
          for (var i = 0, l = obj.length; i < l; i++) {
            fn.call(null, obj[i], i, obj);
          }
        } else {
          for (var key in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, key)) {
              fn.call(null, obj[key], key, obj);
            }
          }
        }
      }
      function merge() {
        var result = {};
        function assignValue(val, key) {
          if (isPlainObject(result[key]) && isPlainObject(val)) {
            result[key] = merge(result[key], val);
          } else if (isPlainObject(val)) {
            result[key] = merge({}, val);
          } else if (isArray(val)) {
            result[key] = val.slice();
          } else {
            result[key] = val;
          }
        }
        for (var i = 0, l = arguments.length; i < l; i++) {
          forEach(arguments[i], assignValue);
        }
        return result;
      }
      function extend(a, b, thisArg) {
        forEach(b, function assignValue(val, key) {
          if (thisArg && typeof val === "function") {
            a[key] = bind(val, thisArg);
          } else {
            a[key] = val;
          }
        });
        return a;
      }
      function stripBOM(content) {
        if (content.charCodeAt(0) === 65279) {
          content = content.slice(1);
        }
        return content;
      }
      function inherits(constructor, superConstructor, props, descriptors) {
        constructor.prototype = Object.create(superConstructor.prototype, descriptors);
        constructor.prototype.constructor = constructor;
        props && Object.assign(constructor.prototype, props);
      }
      function toFlatObject(sourceObj, destObj, filter) {
        var props;
        var i;
        var prop;
        var merged = {};
        destObj = destObj || {};
        do {
          props = Object.getOwnPropertyNames(sourceObj);
          i = props.length;
          while (i-- > 0) {
            prop = props[i];
            if (!merged[prop]) {
              destObj[prop] = sourceObj[prop];
              merged[prop] = true;
            }
          }
          sourceObj = Object.getPrototypeOf(sourceObj);
        } while (sourceObj && (!filter || filter(sourceObj, destObj)) && sourceObj !== Object.prototype);
        return destObj;
      }
      function endsWith(str, searchString, position) {
        str = String(str);
        if (position === void 0 || position > str.length) {
          position = str.length;
        }
        position -= searchString.length;
        var lastIndex = str.indexOf(searchString, position);
        return lastIndex !== -1 && lastIndex === position;
      }
      function toArray(thing) {
        if (!thing)
          return null;
        var i = thing.length;
        if (isUndefined(i))
          return null;
        var arr = new Array(i);
        while (i-- > 0) {
          arr[i] = thing[i];
        }
        return arr;
      }
      var isTypedArray = function(TypedArray) {
        return function(thing) {
          return TypedArray && thing instanceof TypedArray;
        };
      }(typeof Uint8Array !== "undefined" && Object.getPrototypeOf(Uint8Array));
      module22.exports = {
        isArray,
        isArrayBuffer,
        isBuffer,
        isFormData,
        isArrayBufferView,
        isString,
        isNumber,
        isObject,
        isPlainObject,
        isUndefined,
        isDate,
        isFile,
        isBlob,
        isFunction,
        isStream,
        isURLSearchParams,
        isStandardBrowserEnv,
        forEach,
        merge,
        extend,
        trim,
        stripBOM,
        inherits,
        toFlatObject,
        kindOf,
        kindOfTest,
        endsWith,
        toArray,
        isTypedArray,
        isFileList
      };
    }
  });
  var require_buildURL = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/buildURL.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      function encode(val) {
        return encodeURIComponent(val).replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]");
      }
      module22.exports = function buildURL(url, params, paramsSerializer) {
        if (!params) {
          return url;
        }
        var serializedParams;
        if (paramsSerializer) {
          serializedParams = paramsSerializer(params);
        } else if (utils.isURLSearchParams(params)) {
          serializedParams = params.toString();
        } else {
          var parts = [];
          utils.forEach(params, function serialize(val, key) {
            if (val === null || typeof val === "undefined") {
              return;
            }
            if (utils.isArray(val)) {
              key = key + "[]";
            } else {
              val = [val];
            }
            utils.forEach(val, function parseValue(v) {
              if (utils.isDate(v)) {
                v = v.toISOString();
              } else if (utils.isObject(v)) {
                v = JSON.stringify(v);
              }
              parts.push(encode(key) + "=" + encode(v));
            });
          });
          serializedParams = parts.join("&");
        }
        if (serializedParams) {
          var hashmarkIndex = url.indexOf("#");
          if (hashmarkIndex !== -1) {
            url = url.slice(0, hashmarkIndex);
          }
          url += (url.indexOf("?") === -1 ? "?" : "&") + serializedParams;
        }
        return url;
      };
    }
  });
  var require_InterceptorManager = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/InterceptorManager.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      function InterceptorManager() {
        this.handlers = [];
      }
      InterceptorManager.prototype.use = function use(fulfilled, rejected, options) {
        this.handlers.push({
          fulfilled,
          rejected,
          synchronous: options ? options.synchronous : false,
          runWhen: options ? options.runWhen : null
        });
        return this.handlers.length - 1;
      };
      InterceptorManager.prototype.eject = function eject(id) {
        if (this.handlers[id]) {
          this.handlers[id] = null;
        }
      };
      InterceptorManager.prototype.forEach = function forEach(fn) {
        utils.forEach(this.handlers, function forEachHandler(h) {
          if (h !== null) {
            fn(h);
          }
        });
      };
      module22.exports = InterceptorManager;
    }
  });
  var require_normalizeHeaderName = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/normalizeHeaderName.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      module22.exports = function normalizeHeaderName(headers, normalizedName) {
        utils.forEach(headers, function processHeader(value, name) {
          if (name !== normalizedName && name.toUpperCase() === normalizedName.toUpperCase()) {
            headers[normalizedName] = value;
            delete headers[name];
          }
        });
      };
    }
  });
  var require_AxiosError = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/AxiosError.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      function AxiosError(message, code, config, request, response) {
        Error.call(this);
        this.message = message;
        this.name = "AxiosError";
        code && (this.code = code);
        config && (this.config = config);
        request && (this.request = request);
        response && (this.response = response);
      }
      utils.inherits(AxiosError, Error, {
        toJSON: function toJSON() {
          return {
            // Standard
            message: this.message,
            name: this.name,
            // Microsoft
            description: this.description,
            number: this.number,
            // Mozilla
            fileName: this.fileName,
            lineNumber: this.lineNumber,
            columnNumber: this.columnNumber,
            stack: this.stack,
            // Axios
            config: this.config,
            code: this.code,
            status: this.response && this.response.status ? this.response.status : null
          };
        }
      });
      var prototype = AxiosError.prototype;
      var descriptors = {};
      [
        "ERR_BAD_OPTION_VALUE",
        "ERR_BAD_OPTION",
        "ECONNABORTED",
        "ETIMEDOUT",
        "ERR_NETWORK",
        "ERR_FR_TOO_MANY_REDIRECTS",
        "ERR_DEPRECATED",
        "ERR_BAD_RESPONSE",
        "ERR_BAD_REQUEST",
        "ERR_CANCELED"
        // eslint-disable-next-line func-names
      ].forEach(function(code) {
        descriptors[code] = { value: code };
      });
      Object.defineProperties(AxiosError, descriptors);
      Object.defineProperty(prototype, "isAxiosError", { value: true });
      AxiosError.from = function(error, code, config, request, response, customProps) {
        var axiosError = Object.create(prototype);
        utils.toFlatObject(error, axiosError, function filter(obj) {
          return obj !== Error.prototype;
        });
        AxiosError.call(axiosError, error.message, code, config, request, response);
        axiosError.name = error.name;
        customProps && Object.assign(axiosError, customProps);
        return axiosError;
      };
      module22.exports = AxiosError;
    }
  });
  var require_transitional = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/defaults/transitional.js"(exports2, module22) {
      "use strict";
      module22.exports = {
        silentJSONParsing: true,
        forcedJSONParsing: true,
        clarifyTimeoutError: false
      };
    }
  });
  var require_toFormData = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/toFormData.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      function toFormData(obj, formData) {
        formData = formData || new FormData();
        var stack = [];
        function convertValue(value) {
          if (value === null)
            return "";
          if (utils.isDate(value)) {
            return value.toISOString();
          }
          if (utils.isArrayBuffer(value) || utils.isTypedArray(value)) {
            return typeof Blob === "function" ? new Blob([value]) : Buffer.from(value);
          }
          return value;
        }
        function build(data, parentKey) {
          if (utils.isPlainObject(data) || utils.isArray(data)) {
            if (stack.indexOf(data) !== -1) {
              throw Error("Circular reference detected in " + parentKey);
            }
            stack.push(data);
            utils.forEach(data, function each(value, key) {
              if (utils.isUndefined(value))
                return;
              var fullKey = parentKey ? parentKey + "." + key : key;
              var arr;
              if (value && !parentKey && typeof value === "object") {
                if (utils.endsWith(key, "{}")) {
                  value = JSON.stringify(value);
                } else if (utils.endsWith(key, "[]") && (arr = utils.toArray(value))) {
                  arr.forEach(function(el) {
                    !utils.isUndefined(el) && formData.append(fullKey, convertValue(el));
                  });
                  return;
                }
              }
              build(value, fullKey);
            });
            stack.pop();
          } else {
            formData.append(parentKey, convertValue(data));
          }
        }
        build(obj);
        return formData;
      }
      module22.exports = toFormData;
    }
  });
  var require_settle = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/settle.js"(exports2, module22) {
      "use strict";
      var AxiosError = require_AxiosError();
      module22.exports = function settle(resolve, reject, response) {
        var validateStatus = response.config.validateStatus;
        if (!response.status || !validateStatus || validateStatus(response.status)) {
          resolve(response);
        } else {
          reject(new AxiosError(
            "Request failed with status code " + response.status,
            [AxiosError.ERR_BAD_REQUEST, AxiosError.ERR_BAD_RESPONSE][Math.floor(response.status / 100) - 4],
            response.config,
            response.request,
            response
          ));
        }
      };
    }
  });
  var require_cookies = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/cookies.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      module22.exports = utils.isStandardBrowserEnv() ? (
        // Standard browser envs support document.cookie
        function standardBrowserEnv() {
          return {
            write: function write(name, value, expires, path, domain, secure) {
              var cookie = [];
              cookie.push(name + "=" + encodeURIComponent(value));
              if (utils.isNumber(expires)) {
                cookie.push("expires=" + new Date(expires).toGMTString());
              }
              if (utils.isString(path)) {
                cookie.push("path=" + path);
              }
              if (utils.isString(domain)) {
                cookie.push("domain=" + domain);
              }
              if (secure === true) {
                cookie.push("secure");
              }
              document.cookie = cookie.join("; ");
            },
            read: function read(name) {
              var match = document.cookie.match(new RegExp("(^|;\\s*)(" + name + ")=([^;]*)"));
              return match ? decodeURIComponent(match[3]) : null;
            },
            remove: function remove(name) {
              this.write(name, "", Date.now() - 864e5);
            }
          };
        }()
      ) : (
        // Non standard browser env (web workers, react-native) lack needed support.
        function nonStandardBrowserEnv() {
          return {
            write: function write() {
            },
            read: function read() {
              return null;
            },
            remove: function remove() {
            }
          };
        }()
      );
    }
  });
  var require_isAbsoluteURL = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/isAbsoluteURL.js"(exports2, module22) {
      "use strict";
      module22.exports = function isAbsoluteURL(url) {
        return /^([a-z][a-z\d+\-.]*:)?\/\//i.test(url);
      };
    }
  });
  var require_combineURLs = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/combineURLs.js"(exports2, module22) {
      "use strict";
      module22.exports = function combineURLs(baseURL, relativeURL) {
        return relativeURL ? baseURL.replace(/\/+$/, "") + "/" + relativeURL.replace(/^\/+/, "") : baseURL;
      };
    }
  });
  var require_buildFullPath = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/buildFullPath.js"(exports2, module22) {
      "use strict";
      var isAbsoluteURL = require_isAbsoluteURL();
      var combineURLs = require_combineURLs();
      module22.exports = function buildFullPath(baseURL, requestedURL) {
        if (baseURL && !isAbsoluteURL(requestedURL)) {
          return combineURLs(baseURL, requestedURL);
        }
        return requestedURL;
      };
    }
  });
  var require_parseHeaders = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/parseHeaders.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var ignoreDuplicateOf = [
        "age",
        "authorization",
        "content-length",
        "content-type",
        "etag",
        "expires",
        "from",
        "host",
        "if-modified-since",
        "if-unmodified-since",
        "last-modified",
        "location",
        "max-forwards",
        "proxy-authorization",
        "referer",
        "retry-after",
        "user-agent"
      ];
      module22.exports = function parseHeaders(headers) {
        var parsed = {};
        var key;
        var val;
        var i;
        if (!headers) {
          return parsed;
        }
        utils.forEach(headers.split("\n"), function parser(line) {
          i = line.indexOf(":");
          key = utils.trim(line.substr(0, i)).toLowerCase();
          val = utils.trim(line.substr(i + 1));
          if (key) {
            if (parsed[key] && ignoreDuplicateOf.indexOf(key) >= 0) {
              return;
            }
            if (key === "set-cookie") {
              parsed[key] = (parsed[key] ? parsed[key] : []).concat([val]);
            } else {
              parsed[key] = parsed[key] ? parsed[key] + ", " + val : val;
            }
          }
        });
        return parsed;
      };
    }
  });
  var require_isURLSameOrigin = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/isURLSameOrigin.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      module22.exports = utils.isStandardBrowserEnv() ? (
        // Standard browser envs have full support of the APIs needed to test
        // whether the request URL is of the same origin as current location.
        function standardBrowserEnv() {
          var msie = /(msie|trident)/i.test(navigator.userAgent);
          var urlParsingNode = document.createElement("a");
          var originURL;
          function resolveURL(url) {
            var href = url;
            if (msie) {
              urlParsingNode.setAttribute("href", href);
              href = urlParsingNode.href;
            }
            urlParsingNode.setAttribute("href", href);
            return {
              href: urlParsingNode.href,
              protocol: urlParsingNode.protocol ? urlParsingNode.protocol.replace(/:$/, "") : "",
              host: urlParsingNode.host,
              search: urlParsingNode.search ? urlParsingNode.search.replace(/^\?/, "") : "",
              hash: urlParsingNode.hash ? urlParsingNode.hash.replace(/^#/, "") : "",
              hostname: urlParsingNode.hostname,
              port: urlParsingNode.port,
              pathname: urlParsingNode.pathname.charAt(0) === "/" ? urlParsingNode.pathname : "/" + urlParsingNode.pathname
            };
          }
          originURL = resolveURL(window.location.href);
          return function isURLSameOrigin(requestURL) {
            var parsed = utils.isString(requestURL) ? resolveURL(requestURL) : requestURL;
            return parsed.protocol === originURL.protocol && parsed.host === originURL.host;
          };
        }()
      ) : (
        // Non standard browser envs (web workers, react-native) lack needed support.
        function nonStandardBrowserEnv() {
          return function isURLSameOrigin() {
            return true;
          };
        }()
      );
    }
  });
  var require_CanceledError = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/cancel/CanceledError.js"(exports2, module22) {
      "use strict";
      var AxiosError = require_AxiosError();
      var utils = require_utils();
      function CanceledError(message) {
        AxiosError.call(this, message == null ? "canceled" : message, AxiosError.ERR_CANCELED);
        this.name = "CanceledError";
      }
      utils.inherits(CanceledError, AxiosError, {
        __CANCEL__: true
      });
      module22.exports = CanceledError;
    }
  });
  var require_parseProtocol = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/parseProtocol.js"(exports2, module22) {
      "use strict";
      module22.exports = function parseProtocol(url) {
        var match = /^([-+\w]{1,25})(:?\/\/|:)/.exec(url);
        return match && match[1] || "";
      };
    }
  });
  var require_xhr = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/adapters/xhr.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var settle = require_settle();
      var cookies = require_cookies();
      var buildURL = require_buildURL();
      var buildFullPath = require_buildFullPath();
      var parseHeaders = require_parseHeaders();
      var isURLSameOrigin = require_isURLSameOrigin();
      var transitionalDefaults = require_transitional();
      var AxiosError = require_AxiosError();
      var CanceledError = require_CanceledError();
      var parseProtocol = require_parseProtocol();
      module22.exports = function xhrAdapter(config) {
        return new Promise(function dispatchXhrRequest(resolve, reject) {
          var requestData = config.data;
          var requestHeaders = config.headers;
          var responseType = config.responseType;
          var onCanceled;
          function done() {
            if (config.cancelToken) {
              config.cancelToken.unsubscribe(onCanceled);
            }
            if (config.signal) {
              config.signal.removeEventListener("abort", onCanceled);
            }
          }
          if (utils.isFormData(requestData) && utils.isStandardBrowserEnv()) {
            delete requestHeaders["Content-Type"];
          }
          var request = new XMLHttpRequest();
          if (config.auth) {
            var username = config.auth.username || "";
            var password = config.auth.password ? unescape(encodeURIComponent(config.auth.password)) : "";
            requestHeaders.Authorization = "Basic " + btoa(username + ":" + password);
          }
          var fullPath = buildFullPath(config.baseURL, config.url);
          request.open(config.method.toUpperCase(), buildURL(fullPath, config.params, config.paramsSerializer), true);
          request.timeout = config.timeout;
          function onloadend() {
            if (!request) {
              return;
            }
            var responseHeaders = "getAllResponseHeaders" in request ? parseHeaders(request.getAllResponseHeaders()) : null;
            var responseData = !responseType || responseType === "text" || responseType === "json" ? request.responseText : request.response;
            var response = {
              data: responseData,
              status: request.status,
              statusText: request.statusText,
              headers: responseHeaders,
              config,
              request
            };
            settle(function _resolve(value) {
              resolve(value);
              done();
            }, function _reject(err) {
              reject(err);
              done();
            }, response);
            request = null;
          }
          if ("onloadend" in request) {
            request.onloadend = onloadend;
          } else {
            request.onreadystatechange = function handleLoad() {
              if (!request || request.readyState !== 4) {
                return;
              }
              if (request.status === 0 && !(request.responseURL && request.responseURL.indexOf("file:") === 0)) {
                return;
              }
              setTimeout(onloadend);
            };
          }
          request.onabort = function handleAbort() {
            if (!request) {
              return;
            }
            reject(new AxiosError("Request aborted", AxiosError.ECONNABORTED, config, request));
            request = null;
          };
          request.onerror = function handleError() {
            reject(new AxiosError("Network Error", AxiosError.ERR_NETWORK, config, request, request));
            request = null;
          };
          request.ontimeout = function handleTimeout() {
            var timeoutErrorMessage = config.timeout ? "timeout of " + config.timeout + "ms exceeded" : "timeout exceeded";
            var transitional = config.transitional || transitionalDefaults;
            if (config.timeoutErrorMessage) {
              timeoutErrorMessage = config.timeoutErrorMessage;
            }
            reject(new AxiosError(
              timeoutErrorMessage,
              transitional.clarifyTimeoutError ? AxiosError.ETIMEDOUT : AxiosError.ECONNABORTED,
              config,
              request
            ));
            request = null;
          };
          if (utils.isStandardBrowserEnv()) {
            var xsrfValue = (config.withCredentials || isURLSameOrigin(fullPath)) && config.xsrfCookieName ? cookies.read(config.xsrfCookieName) : void 0;
            if (xsrfValue) {
              requestHeaders[config.xsrfHeaderName] = xsrfValue;
            }
          }
          if ("setRequestHeader" in request) {
            utils.forEach(requestHeaders, function setRequestHeader(val, key) {
              if (typeof requestData === "undefined" && key.toLowerCase() === "content-type") {
                delete requestHeaders[key];
              } else {
                request.setRequestHeader(key, val);
              }
            });
          }
          if (!utils.isUndefined(config.withCredentials)) {
            request.withCredentials = !!config.withCredentials;
          }
          if (responseType && responseType !== "json") {
            request.responseType = config.responseType;
          }
          if (typeof config.onDownloadProgress === "function") {
            request.addEventListener("progress", config.onDownloadProgress);
          }
          if (typeof config.onUploadProgress === "function" && request.upload) {
            request.upload.addEventListener("progress", config.onUploadProgress);
          }
          if (config.cancelToken || config.signal) {
            onCanceled = function(cancel) {
              if (!request) {
                return;
              }
              reject(!cancel || cancel && cancel.type ? new CanceledError() : cancel);
              request.abort();
              request = null;
            };
            config.cancelToken && config.cancelToken.subscribe(onCanceled);
            if (config.signal) {
              config.signal.aborted ? onCanceled() : config.signal.addEventListener("abort", onCanceled);
            }
          }
          if (!requestData) {
            requestData = null;
          }
          var protocol = parseProtocol(fullPath);
          if (protocol && ["http", "https", "file"].indexOf(protocol) === -1) {
            reject(new AxiosError("Unsupported protocol " + protocol + ":", AxiosError.ERR_BAD_REQUEST, config));
            return;
          }
          request.send(requestData);
        });
      };
    }
  });
  var require_debug = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/follow-redirects@1.15.6/node_modules/follow-redirects/debug.js"(exports2, module22) {
      var debug;
      module22.exports = function() {
        if (!debug) {
          try {
            debug = require2("debug")("follow-redirects");
          } catch (error) {
          }
          if (typeof debug !== "function") {
            debug = function() {
            };
          }
        }
        debug.apply(null, arguments);
      };
    }
  });
  var require_follow_redirects = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/follow-redirects@1.15.6/node_modules/follow-redirects/index.js"(exports2, module22) {
      var url = require2("url");
      var URL = url.URL;
      var http = require2("http");
      var https = require2("https");
      var Writable = require2("stream").Writable;
      var assert = require2("assert");
      var debug = require_debug();
      var useNativeURL = false;
      try {
        assert(new URL());
      } catch (error) {
        useNativeURL = error.code === "ERR_INVALID_URL";
      }
      var preservedUrlFields = [
        "auth",
        "host",
        "hostname",
        "href",
        "path",
        "pathname",
        "port",
        "protocol",
        "query",
        "search",
        "hash"
      ];
      var events = ["abort", "aborted", "connect", "error", "socket", "timeout"];
      var eventHandlers = /* @__PURE__ */ Object.create(null);
      events.forEach(function(event) {
        eventHandlers[event] = function(arg1, arg2, arg3) {
          this._redirectable.emit(event, arg1, arg2, arg3);
        };
      });
      var InvalidUrlError = createErrorType(
        "ERR_INVALID_URL",
        "Invalid URL",
        TypeError
      );
      var RedirectionError = createErrorType(
        "ERR_FR_REDIRECTION_FAILURE",
        "Redirected request failed"
      );
      var TooManyRedirectsError = createErrorType(
        "ERR_FR_TOO_MANY_REDIRECTS",
        "Maximum number of redirects exceeded",
        RedirectionError
      );
      var MaxBodyLengthExceededError = createErrorType(
        "ERR_FR_MAX_BODY_LENGTH_EXCEEDED",
        "Request body larger than maxBodyLength limit"
      );
      var WriteAfterEndError = createErrorType(
        "ERR_STREAM_WRITE_AFTER_END",
        "write after end"
      );
      var destroy = Writable.prototype.destroy || noop;
      function RedirectableRequest(options, responseCallback) {
        Writable.call(this);
        this._sanitizeOptions(options);
        this._options = options;
        this._ended = false;
        this._ending = false;
        this._redirectCount = 0;
        this._redirects = [];
        this._requestBodyLength = 0;
        this._requestBodyBuffers = [];
        if (responseCallback) {
          this.on("response", responseCallback);
        }
        var self = this;
        this._onNativeResponse = function(response) {
          try {
            self._processResponse(response);
          } catch (cause) {
            self.emit("error", cause instanceof RedirectionError ? cause : new RedirectionError({ cause }));
          }
        };
        this._performRequest();
      }
      RedirectableRequest.prototype = Object.create(Writable.prototype);
      RedirectableRequest.prototype.abort = function() {
        destroyRequest(this._currentRequest);
        this._currentRequest.abort();
        this.emit("abort");
      };
      RedirectableRequest.prototype.destroy = function(error) {
        destroyRequest(this._currentRequest, error);
        destroy.call(this, error);
        return this;
      };
      RedirectableRequest.prototype.write = function(data, encoding, callback) {
        if (this._ending) {
          throw new WriteAfterEndError();
        }
        if (!isString(data) && !isBuffer(data)) {
          throw new TypeError("data should be a string, Buffer or Uint8Array");
        }
        if (isFunction(encoding)) {
          callback = encoding;
          encoding = null;
        }
        if (data.length === 0) {
          if (callback) {
            callback();
          }
          return;
        }
        if (this._requestBodyLength + data.length <= this._options.maxBodyLength) {
          this._requestBodyLength += data.length;
          this._requestBodyBuffers.push({ data, encoding });
          this._currentRequest.write(data, encoding, callback);
        } else {
          this.emit("error", new MaxBodyLengthExceededError());
          this.abort();
        }
      };
      RedirectableRequest.prototype.end = function(data, encoding, callback) {
        if (isFunction(data)) {
          callback = data;
          data = encoding = null;
        } else if (isFunction(encoding)) {
          callback = encoding;
          encoding = null;
        }
        if (!data) {
          this._ended = this._ending = true;
          this._currentRequest.end(null, null, callback);
        } else {
          var self = this;
          var currentRequest = this._currentRequest;
          this.write(data, encoding, function() {
            self._ended = true;
            currentRequest.end(null, null, callback);
          });
          this._ending = true;
        }
      };
      RedirectableRequest.prototype.setHeader = function(name, value) {
        this._options.headers[name] = value;
        this._currentRequest.setHeader(name, value);
      };
      RedirectableRequest.prototype.removeHeader = function(name) {
        delete this._options.headers[name];
        this._currentRequest.removeHeader(name);
      };
      RedirectableRequest.prototype.setTimeout = function(msecs, callback) {
        var self = this;
        function destroyOnTimeout(socket) {
          socket.setTimeout(msecs);
          socket.removeListener("timeout", socket.destroy);
          socket.addListener("timeout", socket.destroy);
        }
        function startTimer(socket) {
          if (self._timeout) {
            clearTimeout(self._timeout);
          }
          self._timeout = setTimeout(function() {
            self.emit("timeout");
            clearTimer();
          }, msecs);
          destroyOnTimeout(socket);
        }
        function clearTimer() {
          if (self._timeout) {
            clearTimeout(self._timeout);
            self._timeout = null;
          }
          self.removeListener("abort", clearTimer);
          self.removeListener("error", clearTimer);
          self.removeListener("response", clearTimer);
          self.removeListener("close", clearTimer);
          if (callback) {
            self.removeListener("timeout", callback);
          }
          if (!self.socket) {
            self._currentRequest.removeListener("socket", startTimer);
          }
        }
        if (callback) {
          this.on("timeout", callback);
        }
        if (this.socket) {
          startTimer(this.socket);
        } else {
          this._currentRequest.once("socket", startTimer);
        }
        this.on("socket", destroyOnTimeout);
        this.on("abort", clearTimer);
        this.on("error", clearTimer);
        this.on("response", clearTimer);
        this.on("close", clearTimer);
        return this;
      };
      [
        "flushHeaders",
        "getHeader",
        "setNoDelay",
        "setSocketKeepAlive"
      ].forEach(function(method) {
        RedirectableRequest.prototype[method] = function(a, b) {
          return this._currentRequest[method](a, b);
        };
      });
      ["aborted", "connection", "socket"].forEach(function(property) {
        Object.defineProperty(RedirectableRequest.prototype, property, {
          get: function() {
            return this._currentRequest[property];
          }
        });
      });
      RedirectableRequest.prototype._sanitizeOptions = function(options) {
        if (!options.headers) {
          options.headers = {};
        }
        if (options.host) {
          if (!options.hostname) {
            options.hostname = options.host;
          }
          delete options.host;
        }
        if (!options.pathname && options.path) {
          var searchPos = options.path.indexOf("?");
          if (searchPos < 0) {
            options.pathname = options.path;
          } else {
            options.pathname = options.path.substring(0, searchPos);
            options.search = options.path.substring(searchPos);
          }
        }
      };
      RedirectableRequest.prototype._performRequest = function() {
        var protocol = this._options.protocol;
        var nativeProtocol = this._options.nativeProtocols[protocol];
        if (!nativeProtocol) {
          throw new TypeError("Unsupported protocol " + protocol);
        }
        if (this._options.agents) {
          var scheme = protocol.slice(0, -1);
          this._options.agent = this._options.agents[scheme];
        }
        var request = this._currentRequest = nativeProtocol.request(this._options, this._onNativeResponse);
        request._redirectable = this;
        for (var event of events) {
          request.on(event, eventHandlers[event]);
        }
        this._currentUrl = /^\//.test(this._options.path) ? url.format(this._options) : (
          // When making a request to a proxy, […]
          // a client MUST send the target URI in absolute-form […].
          this._options.path
        );
        if (this._isRedirect) {
          var i = 0;
          var self = this;
          var buffers = this._requestBodyBuffers;
          (function writeNext(error) {
            if (request === self._currentRequest) {
              if (error) {
                self.emit("error", error);
              } else if (i < buffers.length) {
                var buffer = buffers[i++];
                if (!request.finished) {
                  request.write(buffer.data, buffer.encoding, writeNext);
                }
              } else if (self._ended) {
                request.end();
              }
            }
          })();
        }
      };
      RedirectableRequest.prototype._processResponse = function(response) {
        var statusCode = response.statusCode;
        if (this._options.trackRedirects) {
          this._redirects.push({
            url: this._currentUrl,
            headers: response.headers,
            statusCode
          });
        }
        var location = response.headers.location;
        if (!location || this._options.followRedirects === false || statusCode < 300 || statusCode >= 400) {
          response.responseUrl = this._currentUrl;
          response.redirects = this._redirects;
          this.emit("response", response);
          this._requestBodyBuffers = [];
          return;
        }
        destroyRequest(this._currentRequest);
        response.destroy();
        if (++this._redirectCount > this._options.maxRedirects) {
          throw new TooManyRedirectsError();
        }
        var requestHeaders;
        var beforeRedirect = this._options.beforeRedirect;
        if (beforeRedirect) {
          requestHeaders = Object.assign({
            // The Host header was set by nativeProtocol.request
            Host: response.req.getHeader("host")
          }, this._options.headers);
        }
        var method = this._options.method;
        if ((statusCode === 301 || statusCode === 302) && this._options.method === "POST" || // RFC7231§6.4.4: The 303 (See Other) status code indicates that
        // the server is redirecting the user agent to a different resource […]
        // A user agent can perform a retrieval request targeting that URI
        // (a GET or HEAD request if using HTTP) […]
        statusCode === 303 && !/^(?:GET|HEAD)$/.test(this._options.method)) {
          this._options.method = "GET";
          this._requestBodyBuffers = [];
          removeMatchingHeaders(/^content-/i, this._options.headers);
        }
        var currentHostHeader = removeMatchingHeaders(/^host$/i, this._options.headers);
        var currentUrlParts = parseUrl(this._currentUrl);
        var currentHost = currentHostHeader || currentUrlParts.host;
        var currentUrl = /^\w+:/.test(location) ? this._currentUrl : url.format(Object.assign(currentUrlParts, { host: currentHost }));
        var redirectUrl = resolveUrl(location, currentUrl);
        debug("redirecting to", redirectUrl.href);
        this._isRedirect = true;
        spreadUrlObject(redirectUrl, this._options);
        if (redirectUrl.protocol !== currentUrlParts.protocol && redirectUrl.protocol !== "https:" || redirectUrl.host !== currentHost && !isSubdomain(redirectUrl.host, currentHost)) {
          removeMatchingHeaders(/^(?:(?:proxy-)?authorization|cookie)$/i, this._options.headers);
        }
        if (isFunction(beforeRedirect)) {
          var responseDetails = {
            headers: response.headers,
            statusCode
          };
          var requestDetails = {
            url: currentUrl,
            method,
            headers: requestHeaders
          };
          beforeRedirect(this._options, responseDetails, requestDetails);
          this._sanitizeOptions(this._options);
        }
        this._performRequest();
      };
      function wrap(protocols) {
        var exports22 = {
          maxRedirects: 21,
          maxBodyLength: 10 * 1024 * 1024
        };
        var nativeProtocols = {};
        Object.keys(protocols).forEach(function(scheme) {
          var protocol = scheme + ":";
          var nativeProtocol = nativeProtocols[protocol] = protocols[scheme];
          var wrappedProtocol = exports22[scheme] = Object.create(nativeProtocol);
          function request(input, options, callback) {
            if (isURL(input)) {
              input = spreadUrlObject(input);
            } else if (isString(input)) {
              input = spreadUrlObject(parseUrl(input));
            } else {
              callback = options;
              options = validateUrl(input);
              input = { protocol };
            }
            if (isFunction(options)) {
              callback = options;
              options = null;
            }
            options = Object.assign({
              maxRedirects: exports22.maxRedirects,
              maxBodyLength: exports22.maxBodyLength
            }, input, options);
            options.nativeProtocols = nativeProtocols;
            if (!isString(options.host) && !isString(options.hostname)) {
              options.hostname = "::1";
            }
            assert.equal(options.protocol, protocol, "protocol mismatch");
            debug("options", options);
            return new RedirectableRequest(options, callback);
          }
          function get(input, options, callback) {
            var wrappedRequest = wrappedProtocol.request(input, options, callback);
            wrappedRequest.end();
            return wrappedRequest;
          }
          Object.defineProperties(wrappedProtocol, {
            request: { value: request, configurable: true, enumerable: true, writable: true },
            get: { value: get, configurable: true, enumerable: true, writable: true }
          });
        });
        return exports22;
      }
      function noop() {
      }
      function parseUrl(input) {
        var parsed;
        if (useNativeURL) {
          parsed = new URL(input);
        } else {
          parsed = validateUrl(url.parse(input));
          if (!isString(parsed.protocol)) {
            throw new InvalidUrlError({ input });
          }
        }
        return parsed;
      }
      function resolveUrl(relative, base) {
        return useNativeURL ? new URL(relative, base) : parseUrl(url.resolve(base, relative));
      }
      function validateUrl(input) {
        if (/^\[/.test(input.hostname) && !/^\[[:0-9a-f]+\]$/i.test(input.hostname)) {
          throw new InvalidUrlError({ input: input.href || input });
        }
        if (/^\[/.test(input.host) && !/^\[[:0-9a-f]+\](:\d+)?$/i.test(input.host)) {
          throw new InvalidUrlError({ input: input.href || input });
        }
        return input;
      }
      function spreadUrlObject(urlObject, target) {
        var spread = target || {};
        for (var key of preservedUrlFields) {
          spread[key] = urlObject[key];
        }
        if (spread.hostname.startsWith("[")) {
          spread.hostname = spread.hostname.slice(1, -1);
        }
        if (spread.port !== "") {
          spread.port = Number(spread.port);
        }
        spread.path = spread.search ? spread.pathname + spread.search : spread.pathname;
        return spread;
      }
      function removeMatchingHeaders(regex, headers) {
        var lastValue;
        for (var header in headers) {
          if (regex.test(header)) {
            lastValue = headers[header];
            delete headers[header];
          }
        }
        return lastValue === null || typeof lastValue === "undefined" ? void 0 : String(lastValue).trim();
      }
      function createErrorType(code, message, baseClass) {
        function CustomError(properties) {
          Error.captureStackTrace(this, this.constructor);
          Object.assign(this, properties || {});
          this.code = code;
          this.message = this.cause ? message + ": " + this.cause.message : message;
        }
        CustomError.prototype = new (baseClass || Error)();
        Object.defineProperties(CustomError.prototype, {
          constructor: {
            value: CustomError,
            enumerable: false
          },
          name: {
            value: "Error [" + code + "]",
            enumerable: false
          }
        });
        return CustomError;
      }
      function destroyRequest(request, error) {
        for (var event of events) {
          request.removeListener(event, eventHandlers[event]);
        }
        request.on("error", noop);
        request.destroy(error);
      }
      function isSubdomain(subdomain, domain) {
        assert(isString(subdomain) && isString(domain));
        var dot = subdomain.length - domain.length - 1;
        return dot > 0 && subdomain[dot] === "." && subdomain.endsWith(domain);
      }
      function isString(value) {
        return typeof value === "string" || value instanceof String;
      }
      function isFunction(value) {
        return typeof value === "function";
      }
      function isBuffer(value) {
        return typeof value === "object" && "length" in value;
      }
      function isURL(value) {
        return URL && value instanceof URL;
      }
      module22.exports = wrap({ http, https });
      module22.exports.wrap = wrap;
    }
  });
  var require_data = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/env/data.js"(exports2, module22) {
      module22.exports = {
        "version": "0.27.2"
      };
    }
  });
  var require_http = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/adapters/http.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var settle = require_settle();
      var buildFullPath = require_buildFullPath();
      var buildURL = require_buildURL();
      var http = require2("http");
      var https = require2("https");
      var httpFollow = require_follow_redirects().http;
      var httpsFollow = require_follow_redirects().https;
      var url = require2("url");
      var zlib = require2("zlib");
      var VERSION = require_data().version;
      var transitionalDefaults = require_transitional();
      var AxiosError = require_AxiosError();
      var CanceledError = require_CanceledError();
      var isHttps = /https:?/;
      var supportedProtocols = ["http:", "https:", "file:"];
      function setProxy(options, proxy, location) {
        options.hostname = proxy.host;
        options.host = proxy.host;
        options.port = proxy.port;
        options.path = location;
        if (proxy.auth) {
          var base64 = Buffer.from(proxy.auth.username + ":" + proxy.auth.password, "utf8").toString("base64");
          options.headers["Proxy-Authorization"] = "Basic " + base64;
        }
        options.beforeRedirect = function beforeRedirect(redirection) {
          redirection.headers.host = redirection.host;
          setProxy(redirection, proxy, redirection.href);
        };
      }
      module22.exports = function httpAdapter(config) {
        return new Promise(function dispatchHttpRequest(resolvePromise, rejectPromise) {
          var onCanceled;
          function done() {
            if (config.cancelToken) {
              config.cancelToken.unsubscribe(onCanceled);
            }
            if (config.signal) {
              config.signal.removeEventListener("abort", onCanceled);
            }
          }
          var resolve = function resolve2(value) {
            done();
            resolvePromise(value);
          };
          var rejected = false;
          var reject = function reject2(value) {
            done();
            rejected = true;
            rejectPromise(value);
          };
          var data = config.data;
          var headers = config.headers;
          var headerNames = {};
          Object.keys(headers).forEach(function storeLowerName(name) {
            headerNames[name.toLowerCase()] = name;
          });
          if ("user-agent" in headerNames) {
            if (!headers[headerNames["user-agent"]]) {
              delete headers[headerNames["user-agent"]];
            }
          } else {
            headers["User-Agent"] = "axios/" + VERSION;
          }
          if (utils.isFormData(data) && utils.isFunction(data.getHeaders)) {
            Object.assign(headers, data.getHeaders());
          } else if (data && !utils.isStream(data)) {
            if (Buffer.isBuffer(data)) {
            } else if (utils.isArrayBuffer(data)) {
              data = Buffer.from(new Uint8Array(data));
            } else if (utils.isString(data)) {
              data = Buffer.from(data, "utf-8");
            } else {
              return reject(new AxiosError(
                "Data after transformation must be a string, an ArrayBuffer, a Buffer, or a Stream",
                AxiosError.ERR_BAD_REQUEST,
                config
              ));
            }
            if (config.maxBodyLength > -1 && data.length > config.maxBodyLength) {
              return reject(new AxiosError(
                "Request body larger than maxBodyLength limit",
                AxiosError.ERR_BAD_REQUEST,
                config
              ));
            }
            if (!headerNames["content-length"]) {
              headers["Content-Length"] = data.length;
            }
          }
          var auth = void 0;
          if (config.auth) {
            var username = config.auth.username || "";
            var password = config.auth.password || "";
            auth = username + ":" + password;
          }
          var fullPath = buildFullPath(config.baseURL, config.url);
          var parsed = url.parse(fullPath);
          var protocol = parsed.protocol || supportedProtocols[0];
          if (supportedProtocols.indexOf(protocol) === -1) {
            return reject(new AxiosError(
              "Unsupported protocol " + protocol,
              AxiosError.ERR_BAD_REQUEST,
              config
            ));
          }
          if (!auth && parsed.auth) {
            var urlAuth = parsed.auth.split(":");
            var urlUsername = urlAuth[0] || "";
            var urlPassword = urlAuth[1] || "";
            auth = urlUsername + ":" + urlPassword;
          }
          if (auth && headerNames.authorization) {
            delete headers[headerNames.authorization];
          }
          var isHttpsRequest = isHttps.test(protocol);
          var agent = isHttpsRequest ? config.httpsAgent : config.httpAgent;
          try {
            buildURL(parsed.path, config.params, config.paramsSerializer).replace(/^\?/, "");
          } catch (err) {
            var customErr = new Error(err.message);
            customErr.config = config;
            customErr.url = config.url;
            customErr.exists = true;
            reject(customErr);
          }
          var options = {
            path: buildURL(parsed.path, config.params, config.paramsSerializer).replace(/^\?/, ""),
            method: config.method.toUpperCase(),
            headers,
            agent,
            agents: { http: config.httpAgent, https: config.httpsAgent },
            auth
          };
          if (config.socketPath) {
            options.socketPath = config.socketPath;
          } else {
            options.hostname = parsed.hostname;
            options.port = parsed.port;
          }
          var proxy = config.proxy;
          if (!proxy && proxy !== false) {
            var proxyEnv = protocol.slice(0, -1) + "_proxy";
            var proxyUrl = process.env[proxyEnv] || process.env[proxyEnv.toUpperCase()];
            if (proxyUrl) {
              var parsedProxyUrl = url.parse(proxyUrl);
              var noProxyEnv = process.env.no_proxy || process.env.NO_PROXY;
              var shouldProxy = true;
              if (noProxyEnv) {
                var noProxy = noProxyEnv.split(",").map(function trim(s) {
                  return s.trim();
                });
                shouldProxy = !noProxy.some(function proxyMatch(proxyElement) {
                  if (!proxyElement) {
                    return false;
                  }
                  if (proxyElement === "*") {
                    return true;
                  }
                  if (proxyElement[0] === "." && parsed.hostname.substr(parsed.hostname.length - proxyElement.length) === proxyElement) {
                    return true;
                  }
                  return parsed.hostname === proxyElement;
                });
              }
              if (shouldProxy) {
                proxy = {
                  host: parsedProxyUrl.hostname,
                  port: parsedProxyUrl.port,
                  protocol: parsedProxyUrl.protocol
                };
                if (parsedProxyUrl.auth) {
                  var proxyUrlAuth = parsedProxyUrl.auth.split(":");
                  proxy.auth = {
                    username: proxyUrlAuth[0],
                    password: proxyUrlAuth[1]
                  };
                }
              }
            }
          }
          if (proxy) {
            options.headers.host = parsed.hostname + (parsed.port ? ":" + parsed.port : "");
            setProxy(options, proxy, protocol + "//" + parsed.hostname + (parsed.port ? ":" + parsed.port : "") + options.path);
          }
          var transport;
          var isHttpsProxy = isHttpsRequest && (proxy ? isHttps.test(proxy.protocol) : true);
          if (config.transport) {
            transport = config.transport;
          } else if (config.maxRedirects === 0) {
            transport = isHttpsProxy ? https : http;
          } else {
            if (config.maxRedirects) {
              options.maxRedirects = config.maxRedirects;
            }
            if (config.beforeRedirect) {
              options.beforeRedirect = config.beforeRedirect;
            }
            transport = isHttpsProxy ? httpsFollow : httpFollow;
          }
          if (config.maxBodyLength > -1) {
            options.maxBodyLength = config.maxBodyLength;
          }
          if (config.insecureHTTPParser) {
            options.insecureHTTPParser = config.insecureHTTPParser;
          }
          var req = transport.request(options, function handleResponse(res) {
            if (req.aborted)
              return;
            var stream = res;
            var lastRequest = res.req || req;
            if (res.statusCode !== 204 && lastRequest.method !== "HEAD" && config.decompress !== false) {
              switch (res.headers["content-encoding"]) {
                case "gzip":
                case "compress":
                case "deflate":
                  stream = stream.pipe(zlib.createUnzip());
                  delete res.headers["content-encoding"];
                  break;
              }
            }
            var response = {
              status: res.statusCode,
              statusText: res.statusMessage,
              headers: res.headers,
              config,
              request: lastRequest
            };
            if (config.responseType === "stream") {
              response.data = stream;
              settle(resolve, reject, response);
            } else {
              var responseBuffer = [];
              var totalResponseBytes = 0;
              stream.on("data", function handleStreamData(chunk) {
                responseBuffer.push(chunk);
                totalResponseBytes += chunk.length;
                if (config.maxContentLength > -1 && totalResponseBytes > config.maxContentLength) {
                  rejected = true;
                  stream.destroy();
                  reject(new AxiosError(
                    "maxContentLength size of " + config.maxContentLength + " exceeded",
                    AxiosError.ERR_BAD_RESPONSE,
                    config,
                    lastRequest
                  ));
                }
              });
              stream.on("aborted", function handlerStreamAborted() {
                if (rejected) {
                  return;
                }
                stream.destroy();
                reject(new AxiosError(
                  "maxContentLength size of " + config.maxContentLength + " exceeded",
                  AxiosError.ERR_BAD_RESPONSE,
                  config,
                  lastRequest
                ));
              });
              stream.on("error", function handleStreamError(err) {
                if (req.aborted)
                  return;
                reject(AxiosError.from(err, null, config, lastRequest));
              });
              stream.on("end", function handleStreamEnd() {
                try {
                  var responseData = responseBuffer.length === 1 ? responseBuffer[0] : Buffer.concat(responseBuffer);
                  if (config.responseType !== "arraybuffer") {
                    responseData = responseData.toString(config.responseEncoding);
                    if (!config.responseEncoding || config.responseEncoding === "utf8") {
                      responseData = utils.stripBOM(responseData);
                    }
                  }
                  response.data = responseData;
                } catch (err) {
                  reject(AxiosError.from(err, null, config, response.request, response));
                }
                settle(resolve, reject, response);
              });
            }
          });
          req.on("error", function handleRequestError(err) {
            reject(AxiosError.from(err, null, config, req));
          });
          req.on("socket", function handleRequestSocket(socket) {
            socket.setKeepAlive(true, 1e3 * 60);
          });
          if (config.timeout) {
            var timeout = parseInt(config.timeout, 10);
            if (isNaN(timeout)) {
              reject(new AxiosError(
                "error trying to parse `config.timeout` to int",
                AxiosError.ERR_BAD_OPTION_VALUE,
                config,
                req
              ));
              return;
            }
            req.setTimeout(timeout, function handleRequestTimeout() {
              req.abort();
              var transitional = config.transitional || transitionalDefaults;
              reject(new AxiosError(
                "timeout of " + timeout + "ms exceeded",
                transitional.clarifyTimeoutError ? AxiosError.ETIMEDOUT : AxiosError.ECONNABORTED,
                config,
                req
              ));
            });
          }
          if (config.cancelToken || config.signal) {
            onCanceled = function(cancel) {
              if (req.aborted)
                return;
              req.abort();
              reject(!cancel || cancel && cancel.type ? new CanceledError() : cancel);
            };
            config.cancelToken && config.cancelToken.subscribe(onCanceled);
            if (config.signal) {
              config.signal.aborted ? onCanceled() : config.signal.addEventListener("abort", onCanceled);
            }
          }
          if (utils.isStream(data)) {
            data.on("error", function handleStreamError(err) {
              reject(AxiosError.from(err, config, null, req));
            }).pipe(req);
          } else {
            req.end(data);
          }
        });
      };
    }
  });
  var require_delayed_stream = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/delayed-stream@1.0.0/node_modules/delayed-stream/lib/delayed_stream.js"(exports2, module22) {
      var Stream = require2("stream").Stream;
      var util = require2("util");
      module22.exports = DelayedStream;
      function DelayedStream() {
        this.source = null;
        this.dataSize = 0;
        this.maxDataSize = 1024 * 1024;
        this.pauseStream = true;
        this._maxDataSizeExceeded = false;
        this._released = false;
        this._bufferedEvents = [];
      }
      util.inherits(DelayedStream, Stream);
      DelayedStream.create = function(source, options) {
        var delayedStream = new this();
        options = options || {};
        for (var option in options) {
          delayedStream[option] = options[option];
        }
        delayedStream.source = source;
        var realEmit = source.emit;
        source.emit = function() {
          delayedStream._handleEmit(arguments);
          return realEmit.apply(source, arguments);
        };
        source.on("error", function() {
        });
        if (delayedStream.pauseStream) {
          source.pause();
        }
        return delayedStream;
      };
      Object.defineProperty(DelayedStream.prototype, "readable", {
        configurable: true,
        enumerable: true,
        get: function() {
          return this.source.readable;
        }
      });
      DelayedStream.prototype.setEncoding = function() {
        return this.source.setEncoding.apply(this.source, arguments);
      };
      DelayedStream.prototype.resume = function() {
        if (!this._released) {
          this.release();
        }
        this.source.resume();
      };
      DelayedStream.prototype.pause = function() {
        this.source.pause();
      };
      DelayedStream.prototype.release = function() {
        this._released = true;
        this._bufferedEvents.forEach(function(args) {
          this.emit.apply(this, args);
        }.bind(this));
        this._bufferedEvents = [];
      };
      DelayedStream.prototype.pipe = function() {
        var r = Stream.prototype.pipe.apply(this, arguments);
        this.resume();
        return r;
      };
      DelayedStream.prototype._handleEmit = function(args) {
        if (this._released) {
          this.emit.apply(this, args);
          return;
        }
        if (args[0] === "data") {
          this.dataSize += args[1].length;
          this._checkIfMaxDataSizeExceeded();
        }
        this._bufferedEvents.push(args);
      };
      DelayedStream.prototype._checkIfMaxDataSizeExceeded = function() {
        if (this._maxDataSizeExceeded) {
          return;
        }
        if (this.dataSize <= this.maxDataSize) {
          return;
        }
        this._maxDataSizeExceeded = true;
        var message = "DelayedStream#maxDataSize of " + this.maxDataSize + " bytes exceeded.";
        this.emit("error", new Error(message));
      };
    }
  });
  var require_combined_stream = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/combined-stream@1.0.8/node_modules/combined-stream/lib/combined_stream.js"(exports2, module22) {
      var util = require2("util");
      var Stream = require2("stream").Stream;
      var DelayedStream = require_delayed_stream();
      module22.exports = CombinedStream;
      function CombinedStream() {
        this.writable = false;
        this.readable = true;
        this.dataSize = 0;
        this.maxDataSize = 2 * 1024 * 1024;
        this.pauseStreams = true;
        this._released = false;
        this._streams = [];
        this._currentStream = null;
        this._insideLoop = false;
        this._pendingNext = false;
      }
      util.inherits(CombinedStream, Stream);
      CombinedStream.create = function(options) {
        var combinedStream = new this();
        options = options || {};
        for (var option in options) {
          combinedStream[option] = options[option];
        }
        return combinedStream;
      };
      CombinedStream.isStreamLike = function(stream) {
        return typeof stream !== "function" && typeof stream !== "string" && typeof stream !== "boolean" && typeof stream !== "number" && !Buffer.isBuffer(stream);
      };
      CombinedStream.prototype.append = function(stream) {
        var isStreamLike = CombinedStream.isStreamLike(stream);
        if (isStreamLike) {
          if (!(stream instanceof DelayedStream)) {
            var newStream = DelayedStream.create(stream, {
              maxDataSize: Infinity,
              pauseStream: this.pauseStreams
            });
            stream.on("data", this._checkDataSize.bind(this));
            stream = newStream;
          }
          this._handleErrors(stream);
          if (this.pauseStreams) {
            stream.pause();
          }
        }
        this._streams.push(stream);
        return this;
      };
      CombinedStream.prototype.pipe = function(dest, options) {
        Stream.prototype.pipe.call(this, dest, options);
        this.resume();
        return dest;
      };
      CombinedStream.prototype._getNext = function() {
        this._currentStream = null;
        if (this._insideLoop) {
          this._pendingNext = true;
          return;
        }
        this._insideLoop = true;
        try {
          do {
            this._pendingNext = false;
            this._realGetNext();
          } while (this._pendingNext);
        } finally {
          this._insideLoop = false;
        }
      };
      CombinedStream.prototype._realGetNext = function() {
        var stream = this._streams.shift();
        if (typeof stream == "undefined") {
          this.end();
          return;
        }
        if (typeof stream !== "function") {
          this._pipeNext(stream);
          return;
        }
        var getStream = stream;
        getStream(function(stream2) {
          var isStreamLike = CombinedStream.isStreamLike(stream2);
          if (isStreamLike) {
            stream2.on("data", this._checkDataSize.bind(this));
            this._handleErrors(stream2);
          }
          this._pipeNext(stream2);
        }.bind(this));
      };
      CombinedStream.prototype._pipeNext = function(stream) {
        this._currentStream = stream;
        var isStreamLike = CombinedStream.isStreamLike(stream);
        if (isStreamLike) {
          stream.on("end", this._getNext.bind(this));
          stream.pipe(this, { end: false });
          return;
        }
        var value = stream;
        this.write(value);
        this._getNext();
      };
      CombinedStream.prototype._handleErrors = function(stream) {
        var self = this;
        stream.on("error", function(err) {
          self._emitError(err);
        });
      };
      CombinedStream.prototype.write = function(data) {
        this.emit("data", data);
      };
      CombinedStream.prototype.pause = function() {
        if (!this.pauseStreams) {
          return;
        }
        if (this.pauseStreams && this._currentStream && typeof this._currentStream.pause == "function")
          this._currentStream.pause();
        this.emit("pause");
      };
      CombinedStream.prototype.resume = function() {
        if (!this._released) {
          this._released = true;
          this.writable = true;
          this._getNext();
        }
        if (this.pauseStreams && this._currentStream && typeof this._currentStream.resume == "function")
          this._currentStream.resume();
        this.emit("resume");
      };
      CombinedStream.prototype.end = function() {
        this._reset();
        this.emit("end");
      };
      CombinedStream.prototype.destroy = function() {
        this._reset();
        this.emit("close");
      };
      CombinedStream.prototype._reset = function() {
        this.writable = false;
        this._streams = [];
        this._currentStream = null;
      };
      CombinedStream.prototype._checkDataSize = function() {
        this._updateDataSize();
        if (this.dataSize <= this.maxDataSize) {
          return;
        }
        var message = "DelayedStream#maxDataSize of " + this.maxDataSize + " bytes exceeded.";
        this._emitError(new Error(message));
      };
      CombinedStream.prototype._updateDataSize = function() {
        this.dataSize = 0;
        var self = this;
        this._streams.forEach(function(stream) {
          if (!stream.dataSize) {
            return;
          }
          self.dataSize += stream.dataSize;
        });
        if (this._currentStream && this._currentStream.dataSize) {
          this.dataSize += this._currentStream.dataSize;
        }
      };
      CombinedStream.prototype._emitError = function(err) {
        this._reset();
        this.emit("error", err);
      };
    }
  });
  var require_db = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mime-db@1.52.0/node_modules/mime-db/db.json"(exports2, module22) {
      module22.exports = {
        "application/1d-interleaved-parityfec": {
          source: "iana"
        },
        "application/3gpdash-qoe-report+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/3gpp-ims+xml": {
          source: "iana",
          compressible: true
        },
        "application/3gpphal+json": {
          source: "iana",
          compressible: true
        },
        "application/3gpphalforms+json": {
          source: "iana",
          compressible: true
        },
        "application/a2l": {
          source: "iana"
        },
        "application/ace+cbor": {
          source: "iana"
        },
        "application/activemessage": {
          source: "iana"
        },
        "application/activity+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-costmap+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-costmapfilter+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-directory+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-endpointcost+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-endpointcostparams+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-endpointprop+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-endpointpropparams+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-error+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-networkmap+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-networkmapfilter+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-updatestreamcontrol+json": {
          source: "iana",
          compressible: true
        },
        "application/alto-updatestreamparams+json": {
          source: "iana",
          compressible: true
        },
        "application/aml": {
          source: "iana"
        },
        "application/andrew-inset": {
          source: "iana",
          extensions: ["ez"]
        },
        "application/applefile": {
          source: "iana"
        },
        "application/applixware": {
          source: "apache",
          extensions: ["aw"]
        },
        "application/at+jwt": {
          source: "iana"
        },
        "application/atf": {
          source: "iana"
        },
        "application/atfx": {
          source: "iana"
        },
        "application/atom+xml": {
          source: "iana",
          compressible: true,
          extensions: ["atom"]
        },
        "application/atomcat+xml": {
          source: "iana",
          compressible: true,
          extensions: ["atomcat"]
        },
        "application/atomdeleted+xml": {
          source: "iana",
          compressible: true,
          extensions: ["atomdeleted"]
        },
        "application/atomicmail": {
          source: "iana"
        },
        "application/atomsvc+xml": {
          source: "iana",
          compressible: true,
          extensions: ["atomsvc"]
        },
        "application/atsc-dwd+xml": {
          source: "iana",
          compressible: true,
          extensions: ["dwd"]
        },
        "application/atsc-dynamic-event-message": {
          source: "iana"
        },
        "application/atsc-held+xml": {
          source: "iana",
          compressible: true,
          extensions: ["held"]
        },
        "application/atsc-rdt+json": {
          source: "iana",
          compressible: true
        },
        "application/atsc-rsat+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rsat"]
        },
        "application/atxml": {
          source: "iana"
        },
        "application/auth-policy+xml": {
          source: "iana",
          compressible: true
        },
        "application/bacnet-xdd+zip": {
          source: "iana",
          compressible: false
        },
        "application/batch-smtp": {
          source: "iana"
        },
        "application/bdoc": {
          compressible: false,
          extensions: ["bdoc"]
        },
        "application/beep+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/calendar+json": {
          source: "iana",
          compressible: true
        },
        "application/calendar+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xcs"]
        },
        "application/call-completion": {
          source: "iana"
        },
        "application/cals-1840": {
          source: "iana"
        },
        "application/captive+json": {
          source: "iana",
          compressible: true
        },
        "application/cbor": {
          source: "iana"
        },
        "application/cbor-seq": {
          source: "iana"
        },
        "application/cccex": {
          source: "iana"
        },
        "application/ccmp+xml": {
          source: "iana",
          compressible: true
        },
        "application/ccxml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["ccxml"]
        },
        "application/cdfx+xml": {
          source: "iana",
          compressible: true,
          extensions: ["cdfx"]
        },
        "application/cdmi-capability": {
          source: "iana",
          extensions: ["cdmia"]
        },
        "application/cdmi-container": {
          source: "iana",
          extensions: ["cdmic"]
        },
        "application/cdmi-domain": {
          source: "iana",
          extensions: ["cdmid"]
        },
        "application/cdmi-object": {
          source: "iana",
          extensions: ["cdmio"]
        },
        "application/cdmi-queue": {
          source: "iana",
          extensions: ["cdmiq"]
        },
        "application/cdni": {
          source: "iana"
        },
        "application/cea": {
          source: "iana"
        },
        "application/cea-2018+xml": {
          source: "iana",
          compressible: true
        },
        "application/cellml+xml": {
          source: "iana",
          compressible: true
        },
        "application/cfw": {
          source: "iana"
        },
        "application/city+json": {
          source: "iana",
          compressible: true
        },
        "application/clr": {
          source: "iana"
        },
        "application/clue+xml": {
          source: "iana",
          compressible: true
        },
        "application/clue_info+xml": {
          source: "iana",
          compressible: true
        },
        "application/cms": {
          source: "iana"
        },
        "application/cnrp+xml": {
          source: "iana",
          compressible: true
        },
        "application/coap-group+json": {
          source: "iana",
          compressible: true
        },
        "application/coap-payload": {
          source: "iana"
        },
        "application/commonground": {
          source: "iana"
        },
        "application/conference-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/cose": {
          source: "iana"
        },
        "application/cose-key": {
          source: "iana"
        },
        "application/cose-key-set": {
          source: "iana"
        },
        "application/cpl+xml": {
          source: "iana",
          compressible: true,
          extensions: ["cpl"]
        },
        "application/csrattrs": {
          source: "iana"
        },
        "application/csta+xml": {
          source: "iana",
          compressible: true
        },
        "application/cstadata+xml": {
          source: "iana",
          compressible: true
        },
        "application/csvm+json": {
          source: "iana",
          compressible: true
        },
        "application/cu-seeme": {
          source: "apache",
          extensions: ["cu"]
        },
        "application/cwt": {
          source: "iana"
        },
        "application/cybercash": {
          source: "iana"
        },
        "application/dart": {
          compressible: true
        },
        "application/dash+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mpd"]
        },
        "application/dash-patch+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mpp"]
        },
        "application/dashdelta": {
          source: "iana"
        },
        "application/davmount+xml": {
          source: "iana",
          compressible: true,
          extensions: ["davmount"]
        },
        "application/dca-rft": {
          source: "iana"
        },
        "application/dcd": {
          source: "iana"
        },
        "application/dec-dx": {
          source: "iana"
        },
        "application/dialog-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/dicom": {
          source: "iana"
        },
        "application/dicom+json": {
          source: "iana",
          compressible: true
        },
        "application/dicom+xml": {
          source: "iana",
          compressible: true
        },
        "application/dii": {
          source: "iana"
        },
        "application/dit": {
          source: "iana"
        },
        "application/dns": {
          source: "iana"
        },
        "application/dns+json": {
          source: "iana",
          compressible: true
        },
        "application/dns-message": {
          source: "iana"
        },
        "application/docbook+xml": {
          source: "apache",
          compressible: true,
          extensions: ["dbk"]
        },
        "application/dots+cbor": {
          source: "iana"
        },
        "application/dskpp+xml": {
          source: "iana",
          compressible: true
        },
        "application/dssc+der": {
          source: "iana",
          extensions: ["dssc"]
        },
        "application/dssc+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xdssc"]
        },
        "application/dvcs": {
          source: "iana"
        },
        "application/ecmascript": {
          source: "iana",
          compressible: true,
          extensions: ["es", "ecma"]
        },
        "application/edi-consent": {
          source: "iana"
        },
        "application/edi-x12": {
          source: "iana",
          compressible: false
        },
        "application/edifact": {
          source: "iana",
          compressible: false
        },
        "application/efi": {
          source: "iana"
        },
        "application/elm+json": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/elm+xml": {
          source: "iana",
          compressible: true
        },
        "application/emergencycalldata.cap+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/emergencycalldata.comment+xml": {
          source: "iana",
          compressible: true
        },
        "application/emergencycalldata.control+xml": {
          source: "iana",
          compressible: true
        },
        "application/emergencycalldata.deviceinfo+xml": {
          source: "iana",
          compressible: true
        },
        "application/emergencycalldata.ecall.msd": {
          source: "iana"
        },
        "application/emergencycalldata.providerinfo+xml": {
          source: "iana",
          compressible: true
        },
        "application/emergencycalldata.serviceinfo+xml": {
          source: "iana",
          compressible: true
        },
        "application/emergencycalldata.subscriberinfo+xml": {
          source: "iana",
          compressible: true
        },
        "application/emergencycalldata.veds+xml": {
          source: "iana",
          compressible: true
        },
        "application/emma+xml": {
          source: "iana",
          compressible: true,
          extensions: ["emma"]
        },
        "application/emotionml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["emotionml"]
        },
        "application/encaprtp": {
          source: "iana"
        },
        "application/epp+xml": {
          source: "iana",
          compressible: true
        },
        "application/epub+zip": {
          source: "iana",
          compressible: false,
          extensions: ["epub"]
        },
        "application/eshop": {
          source: "iana"
        },
        "application/exi": {
          source: "iana",
          extensions: ["exi"]
        },
        "application/expect-ct-report+json": {
          source: "iana",
          compressible: true
        },
        "application/express": {
          source: "iana",
          extensions: ["exp"]
        },
        "application/fastinfoset": {
          source: "iana"
        },
        "application/fastsoap": {
          source: "iana"
        },
        "application/fdt+xml": {
          source: "iana",
          compressible: true,
          extensions: ["fdt"]
        },
        "application/fhir+json": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/fhir+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/fido.trusted-apps+json": {
          compressible: true
        },
        "application/fits": {
          source: "iana"
        },
        "application/flexfec": {
          source: "iana"
        },
        "application/font-sfnt": {
          source: "iana"
        },
        "application/font-tdpfr": {
          source: "iana",
          extensions: ["pfr"]
        },
        "application/font-woff": {
          source: "iana",
          compressible: false
        },
        "application/framework-attributes+xml": {
          source: "iana",
          compressible: true
        },
        "application/geo+json": {
          source: "iana",
          compressible: true,
          extensions: ["geojson"]
        },
        "application/geo+json-seq": {
          source: "iana"
        },
        "application/geopackage+sqlite3": {
          source: "iana"
        },
        "application/geoxacml+xml": {
          source: "iana",
          compressible: true
        },
        "application/gltf-buffer": {
          source: "iana"
        },
        "application/gml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["gml"]
        },
        "application/gpx+xml": {
          source: "apache",
          compressible: true,
          extensions: ["gpx"]
        },
        "application/gxf": {
          source: "apache",
          extensions: ["gxf"]
        },
        "application/gzip": {
          source: "iana",
          compressible: false,
          extensions: ["gz"]
        },
        "application/h224": {
          source: "iana"
        },
        "application/held+xml": {
          source: "iana",
          compressible: true
        },
        "application/hjson": {
          extensions: ["hjson"]
        },
        "application/http": {
          source: "iana"
        },
        "application/hyperstudio": {
          source: "iana",
          extensions: ["stk"]
        },
        "application/ibe-key-request+xml": {
          source: "iana",
          compressible: true
        },
        "application/ibe-pkg-reply+xml": {
          source: "iana",
          compressible: true
        },
        "application/ibe-pp-data": {
          source: "iana"
        },
        "application/iges": {
          source: "iana"
        },
        "application/im-iscomposing+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/index": {
          source: "iana"
        },
        "application/index.cmd": {
          source: "iana"
        },
        "application/index.obj": {
          source: "iana"
        },
        "application/index.response": {
          source: "iana"
        },
        "application/index.vnd": {
          source: "iana"
        },
        "application/inkml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["ink", "inkml"]
        },
        "application/iotp": {
          source: "iana"
        },
        "application/ipfix": {
          source: "iana",
          extensions: ["ipfix"]
        },
        "application/ipp": {
          source: "iana"
        },
        "application/isup": {
          source: "iana"
        },
        "application/its+xml": {
          source: "iana",
          compressible: true,
          extensions: ["its"]
        },
        "application/java-archive": {
          source: "apache",
          compressible: false,
          extensions: ["jar", "war", "ear"]
        },
        "application/java-serialized-object": {
          source: "apache",
          compressible: false,
          extensions: ["ser"]
        },
        "application/java-vm": {
          source: "apache",
          compressible: false,
          extensions: ["class"]
        },
        "application/javascript": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["js", "mjs"]
        },
        "application/jf2feed+json": {
          source: "iana",
          compressible: true
        },
        "application/jose": {
          source: "iana"
        },
        "application/jose+json": {
          source: "iana",
          compressible: true
        },
        "application/jrd+json": {
          source: "iana",
          compressible: true
        },
        "application/jscalendar+json": {
          source: "iana",
          compressible: true
        },
        "application/json": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["json", "map"]
        },
        "application/json-patch+json": {
          source: "iana",
          compressible: true
        },
        "application/json-seq": {
          source: "iana"
        },
        "application/json5": {
          extensions: ["json5"]
        },
        "application/jsonml+json": {
          source: "apache",
          compressible: true,
          extensions: ["jsonml"]
        },
        "application/jwk+json": {
          source: "iana",
          compressible: true
        },
        "application/jwk-set+json": {
          source: "iana",
          compressible: true
        },
        "application/jwt": {
          source: "iana"
        },
        "application/kpml-request+xml": {
          source: "iana",
          compressible: true
        },
        "application/kpml-response+xml": {
          source: "iana",
          compressible: true
        },
        "application/ld+json": {
          source: "iana",
          compressible: true,
          extensions: ["jsonld"]
        },
        "application/lgr+xml": {
          source: "iana",
          compressible: true,
          extensions: ["lgr"]
        },
        "application/link-format": {
          source: "iana"
        },
        "application/load-control+xml": {
          source: "iana",
          compressible: true
        },
        "application/lost+xml": {
          source: "iana",
          compressible: true,
          extensions: ["lostxml"]
        },
        "application/lostsync+xml": {
          source: "iana",
          compressible: true
        },
        "application/lpf+zip": {
          source: "iana",
          compressible: false
        },
        "application/lxf": {
          source: "iana"
        },
        "application/mac-binhex40": {
          source: "iana",
          extensions: ["hqx"]
        },
        "application/mac-compactpro": {
          source: "apache",
          extensions: ["cpt"]
        },
        "application/macwriteii": {
          source: "iana"
        },
        "application/mads+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mads"]
        },
        "application/manifest+json": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["webmanifest"]
        },
        "application/marc": {
          source: "iana",
          extensions: ["mrc"]
        },
        "application/marcxml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mrcx"]
        },
        "application/mathematica": {
          source: "iana",
          extensions: ["ma", "nb", "mb"]
        },
        "application/mathml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mathml"]
        },
        "application/mathml-content+xml": {
          source: "iana",
          compressible: true
        },
        "application/mathml-presentation+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-associated-procedure-description+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-deregister+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-envelope+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-msk+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-msk-response+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-protection-description+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-reception-report+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-register+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-register-response+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-schedule+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbms-user-service-description+xml": {
          source: "iana",
          compressible: true
        },
        "application/mbox": {
          source: "iana",
          extensions: ["mbox"]
        },
        "application/media-policy-dataset+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mpf"]
        },
        "application/media_control+xml": {
          source: "iana",
          compressible: true
        },
        "application/mediaservercontrol+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mscml"]
        },
        "application/merge-patch+json": {
          source: "iana",
          compressible: true
        },
        "application/metalink+xml": {
          source: "apache",
          compressible: true,
          extensions: ["metalink"]
        },
        "application/metalink4+xml": {
          source: "iana",
          compressible: true,
          extensions: ["meta4"]
        },
        "application/mets+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mets"]
        },
        "application/mf4": {
          source: "iana"
        },
        "application/mikey": {
          source: "iana"
        },
        "application/mipc": {
          source: "iana"
        },
        "application/missing-blocks+cbor-seq": {
          source: "iana"
        },
        "application/mmt-aei+xml": {
          source: "iana",
          compressible: true,
          extensions: ["maei"]
        },
        "application/mmt-usd+xml": {
          source: "iana",
          compressible: true,
          extensions: ["musd"]
        },
        "application/mods+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mods"]
        },
        "application/moss-keys": {
          source: "iana"
        },
        "application/moss-signature": {
          source: "iana"
        },
        "application/mosskey-data": {
          source: "iana"
        },
        "application/mosskey-request": {
          source: "iana"
        },
        "application/mp21": {
          source: "iana",
          extensions: ["m21", "mp21"]
        },
        "application/mp4": {
          source: "iana",
          extensions: ["mp4s", "m4p"]
        },
        "application/mpeg4-generic": {
          source: "iana"
        },
        "application/mpeg4-iod": {
          source: "iana"
        },
        "application/mpeg4-iod-xmt": {
          source: "iana"
        },
        "application/mrb-consumer+xml": {
          source: "iana",
          compressible: true
        },
        "application/mrb-publish+xml": {
          source: "iana",
          compressible: true
        },
        "application/msc-ivr+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/msc-mixer+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/msword": {
          source: "iana",
          compressible: false,
          extensions: ["doc", "dot"]
        },
        "application/mud+json": {
          source: "iana",
          compressible: true
        },
        "application/multipart-core": {
          source: "iana"
        },
        "application/mxf": {
          source: "iana",
          extensions: ["mxf"]
        },
        "application/n-quads": {
          source: "iana",
          extensions: ["nq"]
        },
        "application/n-triples": {
          source: "iana",
          extensions: ["nt"]
        },
        "application/nasdata": {
          source: "iana"
        },
        "application/news-checkgroups": {
          source: "iana",
          charset: "US-ASCII"
        },
        "application/news-groupinfo": {
          source: "iana",
          charset: "US-ASCII"
        },
        "application/news-transmission": {
          source: "iana"
        },
        "application/nlsml+xml": {
          source: "iana",
          compressible: true
        },
        "application/node": {
          source: "iana",
          extensions: ["cjs"]
        },
        "application/nss": {
          source: "iana"
        },
        "application/oauth-authz-req+jwt": {
          source: "iana"
        },
        "application/oblivious-dns-message": {
          source: "iana"
        },
        "application/ocsp-request": {
          source: "iana"
        },
        "application/ocsp-response": {
          source: "iana"
        },
        "application/octet-stream": {
          source: "iana",
          compressible: false,
          extensions: ["bin", "dms", "lrf", "mar", "so", "dist", "distz", "pkg", "bpk", "dump", "elc", "deploy", "exe", "dll", "deb", "dmg", "iso", "img", "msi", "msp", "msm", "buffer"]
        },
        "application/oda": {
          source: "iana",
          extensions: ["oda"]
        },
        "application/odm+xml": {
          source: "iana",
          compressible: true
        },
        "application/odx": {
          source: "iana"
        },
        "application/oebps-package+xml": {
          source: "iana",
          compressible: true,
          extensions: ["opf"]
        },
        "application/ogg": {
          source: "iana",
          compressible: false,
          extensions: ["ogx"]
        },
        "application/omdoc+xml": {
          source: "apache",
          compressible: true,
          extensions: ["omdoc"]
        },
        "application/onenote": {
          source: "apache",
          extensions: ["onetoc", "onetoc2", "onetmp", "onepkg"]
        },
        "application/opc-nodeset+xml": {
          source: "iana",
          compressible: true
        },
        "application/oscore": {
          source: "iana"
        },
        "application/oxps": {
          source: "iana",
          extensions: ["oxps"]
        },
        "application/p21": {
          source: "iana"
        },
        "application/p21+zip": {
          source: "iana",
          compressible: false
        },
        "application/p2p-overlay+xml": {
          source: "iana",
          compressible: true,
          extensions: ["relo"]
        },
        "application/parityfec": {
          source: "iana"
        },
        "application/passport": {
          source: "iana"
        },
        "application/patch-ops-error+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xer"]
        },
        "application/pdf": {
          source: "iana",
          compressible: false,
          extensions: ["pdf"]
        },
        "application/pdx": {
          source: "iana"
        },
        "application/pem-certificate-chain": {
          source: "iana"
        },
        "application/pgp-encrypted": {
          source: "iana",
          compressible: false,
          extensions: ["pgp"]
        },
        "application/pgp-keys": {
          source: "iana",
          extensions: ["asc"]
        },
        "application/pgp-signature": {
          source: "iana",
          extensions: ["asc", "sig"]
        },
        "application/pics-rules": {
          source: "apache",
          extensions: ["prf"]
        },
        "application/pidf+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/pidf-diff+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/pkcs10": {
          source: "iana",
          extensions: ["p10"]
        },
        "application/pkcs12": {
          source: "iana"
        },
        "application/pkcs7-mime": {
          source: "iana",
          extensions: ["p7m", "p7c"]
        },
        "application/pkcs7-signature": {
          source: "iana",
          extensions: ["p7s"]
        },
        "application/pkcs8": {
          source: "iana",
          extensions: ["p8"]
        },
        "application/pkcs8-encrypted": {
          source: "iana"
        },
        "application/pkix-attr-cert": {
          source: "iana",
          extensions: ["ac"]
        },
        "application/pkix-cert": {
          source: "iana",
          extensions: ["cer"]
        },
        "application/pkix-crl": {
          source: "iana",
          extensions: ["crl"]
        },
        "application/pkix-pkipath": {
          source: "iana",
          extensions: ["pkipath"]
        },
        "application/pkixcmp": {
          source: "iana",
          extensions: ["pki"]
        },
        "application/pls+xml": {
          source: "iana",
          compressible: true,
          extensions: ["pls"]
        },
        "application/poc-settings+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/postscript": {
          source: "iana",
          compressible: true,
          extensions: ["ai", "eps", "ps"]
        },
        "application/ppsp-tracker+json": {
          source: "iana",
          compressible: true
        },
        "application/problem+json": {
          source: "iana",
          compressible: true
        },
        "application/problem+xml": {
          source: "iana",
          compressible: true
        },
        "application/provenance+xml": {
          source: "iana",
          compressible: true,
          extensions: ["provx"]
        },
        "application/prs.alvestrand.titrax-sheet": {
          source: "iana"
        },
        "application/prs.cww": {
          source: "iana",
          extensions: ["cww"]
        },
        "application/prs.cyn": {
          source: "iana",
          charset: "7-BIT"
        },
        "application/prs.hpub+zip": {
          source: "iana",
          compressible: false
        },
        "application/prs.nprend": {
          source: "iana"
        },
        "application/prs.plucker": {
          source: "iana"
        },
        "application/prs.rdf-xml-crypt": {
          source: "iana"
        },
        "application/prs.xsf+xml": {
          source: "iana",
          compressible: true
        },
        "application/pskc+xml": {
          source: "iana",
          compressible: true,
          extensions: ["pskcxml"]
        },
        "application/pvd+json": {
          source: "iana",
          compressible: true
        },
        "application/qsig": {
          source: "iana"
        },
        "application/raml+yaml": {
          compressible: true,
          extensions: ["raml"]
        },
        "application/raptorfec": {
          source: "iana"
        },
        "application/rdap+json": {
          source: "iana",
          compressible: true
        },
        "application/rdf+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rdf", "owl"]
        },
        "application/reginfo+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rif"]
        },
        "application/relax-ng-compact-syntax": {
          source: "iana",
          extensions: ["rnc"]
        },
        "application/remote-printing": {
          source: "iana"
        },
        "application/reputon+json": {
          source: "iana",
          compressible: true
        },
        "application/resource-lists+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rl"]
        },
        "application/resource-lists-diff+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rld"]
        },
        "application/rfc+xml": {
          source: "iana",
          compressible: true
        },
        "application/riscos": {
          source: "iana"
        },
        "application/rlmi+xml": {
          source: "iana",
          compressible: true
        },
        "application/rls-services+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rs"]
        },
        "application/route-apd+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rapd"]
        },
        "application/route-s-tsid+xml": {
          source: "iana",
          compressible: true,
          extensions: ["sls"]
        },
        "application/route-usd+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rusd"]
        },
        "application/rpki-ghostbusters": {
          source: "iana",
          extensions: ["gbr"]
        },
        "application/rpki-manifest": {
          source: "iana",
          extensions: ["mft"]
        },
        "application/rpki-publication": {
          source: "iana"
        },
        "application/rpki-roa": {
          source: "iana",
          extensions: ["roa"]
        },
        "application/rpki-updown": {
          source: "iana"
        },
        "application/rsd+xml": {
          source: "apache",
          compressible: true,
          extensions: ["rsd"]
        },
        "application/rss+xml": {
          source: "apache",
          compressible: true,
          extensions: ["rss"]
        },
        "application/rtf": {
          source: "iana",
          compressible: true,
          extensions: ["rtf"]
        },
        "application/rtploopback": {
          source: "iana"
        },
        "application/rtx": {
          source: "iana"
        },
        "application/samlassertion+xml": {
          source: "iana",
          compressible: true
        },
        "application/samlmetadata+xml": {
          source: "iana",
          compressible: true
        },
        "application/sarif+json": {
          source: "iana",
          compressible: true
        },
        "application/sarif-external-properties+json": {
          source: "iana",
          compressible: true
        },
        "application/sbe": {
          source: "iana"
        },
        "application/sbml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["sbml"]
        },
        "application/scaip+xml": {
          source: "iana",
          compressible: true
        },
        "application/scim+json": {
          source: "iana",
          compressible: true
        },
        "application/scvp-cv-request": {
          source: "iana",
          extensions: ["scq"]
        },
        "application/scvp-cv-response": {
          source: "iana",
          extensions: ["scs"]
        },
        "application/scvp-vp-request": {
          source: "iana",
          extensions: ["spq"]
        },
        "application/scvp-vp-response": {
          source: "iana",
          extensions: ["spp"]
        },
        "application/sdp": {
          source: "iana",
          extensions: ["sdp"]
        },
        "application/secevent+jwt": {
          source: "iana"
        },
        "application/senml+cbor": {
          source: "iana"
        },
        "application/senml+json": {
          source: "iana",
          compressible: true
        },
        "application/senml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["senmlx"]
        },
        "application/senml-etch+cbor": {
          source: "iana"
        },
        "application/senml-etch+json": {
          source: "iana",
          compressible: true
        },
        "application/senml-exi": {
          source: "iana"
        },
        "application/sensml+cbor": {
          source: "iana"
        },
        "application/sensml+json": {
          source: "iana",
          compressible: true
        },
        "application/sensml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["sensmlx"]
        },
        "application/sensml-exi": {
          source: "iana"
        },
        "application/sep+xml": {
          source: "iana",
          compressible: true
        },
        "application/sep-exi": {
          source: "iana"
        },
        "application/session-info": {
          source: "iana"
        },
        "application/set-payment": {
          source: "iana"
        },
        "application/set-payment-initiation": {
          source: "iana",
          extensions: ["setpay"]
        },
        "application/set-registration": {
          source: "iana"
        },
        "application/set-registration-initiation": {
          source: "iana",
          extensions: ["setreg"]
        },
        "application/sgml": {
          source: "iana"
        },
        "application/sgml-open-catalog": {
          source: "iana"
        },
        "application/shf+xml": {
          source: "iana",
          compressible: true,
          extensions: ["shf"]
        },
        "application/sieve": {
          source: "iana",
          extensions: ["siv", "sieve"]
        },
        "application/simple-filter+xml": {
          source: "iana",
          compressible: true
        },
        "application/simple-message-summary": {
          source: "iana"
        },
        "application/simplesymbolcontainer": {
          source: "iana"
        },
        "application/sipc": {
          source: "iana"
        },
        "application/slate": {
          source: "iana"
        },
        "application/smil": {
          source: "iana"
        },
        "application/smil+xml": {
          source: "iana",
          compressible: true,
          extensions: ["smi", "smil"]
        },
        "application/smpte336m": {
          source: "iana"
        },
        "application/soap+fastinfoset": {
          source: "iana"
        },
        "application/soap+xml": {
          source: "iana",
          compressible: true
        },
        "application/sparql-query": {
          source: "iana",
          extensions: ["rq"]
        },
        "application/sparql-results+xml": {
          source: "iana",
          compressible: true,
          extensions: ["srx"]
        },
        "application/spdx+json": {
          source: "iana",
          compressible: true
        },
        "application/spirits-event+xml": {
          source: "iana",
          compressible: true
        },
        "application/sql": {
          source: "iana"
        },
        "application/srgs": {
          source: "iana",
          extensions: ["gram"]
        },
        "application/srgs+xml": {
          source: "iana",
          compressible: true,
          extensions: ["grxml"]
        },
        "application/sru+xml": {
          source: "iana",
          compressible: true,
          extensions: ["sru"]
        },
        "application/ssdl+xml": {
          source: "apache",
          compressible: true,
          extensions: ["ssdl"]
        },
        "application/ssml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["ssml"]
        },
        "application/stix+json": {
          source: "iana",
          compressible: true
        },
        "application/swid+xml": {
          source: "iana",
          compressible: true,
          extensions: ["swidtag"]
        },
        "application/tamp-apex-update": {
          source: "iana"
        },
        "application/tamp-apex-update-confirm": {
          source: "iana"
        },
        "application/tamp-community-update": {
          source: "iana"
        },
        "application/tamp-community-update-confirm": {
          source: "iana"
        },
        "application/tamp-error": {
          source: "iana"
        },
        "application/tamp-sequence-adjust": {
          source: "iana"
        },
        "application/tamp-sequence-adjust-confirm": {
          source: "iana"
        },
        "application/tamp-status-query": {
          source: "iana"
        },
        "application/tamp-status-response": {
          source: "iana"
        },
        "application/tamp-update": {
          source: "iana"
        },
        "application/tamp-update-confirm": {
          source: "iana"
        },
        "application/tar": {
          compressible: true
        },
        "application/taxii+json": {
          source: "iana",
          compressible: true
        },
        "application/td+json": {
          source: "iana",
          compressible: true
        },
        "application/tei+xml": {
          source: "iana",
          compressible: true,
          extensions: ["tei", "teicorpus"]
        },
        "application/tetra_isi": {
          source: "iana"
        },
        "application/thraud+xml": {
          source: "iana",
          compressible: true,
          extensions: ["tfi"]
        },
        "application/timestamp-query": {
          source: "iana"
        },
        "application/timestamp-reply": {
          source: "iana"
        },
        "application/timestamped-data": {
          source: "iana",
          extensions: ["tsd"]
        },
        "application/tlsrpt+gzip": {
          source: "iana"
        },
        "application/tlsrpt+json": {
          source: "iana",
          compressible: true
        },
        "application/tnauthlist": {
          source: "iana"
        },
        "application/token-introspection+jwt": {
          source: "iana"
        },
        "application/toml": {
          compressible: true,
          extensions: ["toml"]
        },
        "application/trickle-ice-sdpfrag": {
          source: "iana"
        },
        "application/trig": {
          source: "iana",
          extensions: ["trig"]
        },
        "application/ttml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["ttml"]
        },
        "application/tve-trigger": {
          source: "iana"
        },
        "application/tzif": {
          source: "iana"
        },
        "application/tzif-leap": {
          source: "iana"
        },
        "application/ubjson": {
          compressible: false,
          extensions: ["ubj"]
        },
        "application/ulpfec": {
          source: "iana"
        },
        "application/urc-grpsheet+xml": {
          source: "iana",
          compressible: true
        },
        "application/urc-ressheet+xml": {
          source: "iana",
          compressible: true,
          extensions: ["rsheet"]
        },
        "application/urc-targetdesc+xml": {
          source: "iana",
          compressible: true,
          extensions: ["td"]
        },
        "application/urc-uisocketdesc+xml": {
          source: "iana",
          compressible: true
        },
        "application/vcard+json": {
          source: "iana",
          compressible: true
        },
        "application/vcard+xml": {
          source: "iana",
          compressible: true
        },
        "application/vemmi": {
          source: "iana"
        },
        "application/vividence.scriptfile": {
          source: "apache"
        },
        "application/vnd.1000minds.decision-model+xml": {
          source: "iana",
          compressible: true,
          extensions: ["1km"]
        },
        "application/vnd.3gpp-prose+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp-prose-pc3ch+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp-v2x-local-service-information": {
          source: "iana"
        },
        "application/vnd.3gpp.5gnas": {
          source: "iana"
        },
        "application/vnd.3gpp.access-transfer-events+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.bsf+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.gmop+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.gtpc": {
          source: "iana"
        },
        "application/vnd.3gpp.interworking-data": {
          source: "iana"
        },
        "application/vnd.3gpp.lpp": {
          source: "iana"
        },
        "application/vnd.3gpp.mc-signalling-ear": {
          source: "iana"
        },
        "application/vnd.3gpp.mcdata-affiliation-command+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcdata-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcdata-payload": {
          source: "iana"
        },
        "application/vnd.3gpp.mcdata-service-config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcdata-signalling": {
          source: "iana"
        },
        "application/vnd.3gpp.mcdata-ue-config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcdata-user-profile+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-affiliation-command+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-floor-request+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-location-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-mbms-usage-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-service-config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-signed+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-ue-config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-ue-init-config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcptt-user-profile+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-affiliation-command+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-affiliation-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-location-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-mbms-usage-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-service-config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-transmission-request+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-ue-config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mcvideo-user-profile+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.mid-call+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.ngap": {
          source: "iana"
        },
        "application/vnd.3gpp.pfcp": {
          source: "iana"
        },
        "application/vnd.3gpp.pic-bw-large": {
          source: "iana",
          extensions: ["plb"]
        },
        "application/vnd.3gpp.pic-bw-small": {
          source: "iana",
          extensions: ["psb"]
        },
        "application/vnd.3gpp.pic-bw-var": {
          source: "iana",
          extensions: ["pvb"]
        },
        "application/vnd.3gpp.s1ap": {
          source: "iana"
        },
        "application/vnd.3gpp.sms": {
          source: "iana"
        },
        "application/vnd.3gpp.sms+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.srvcc-ext+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.srvcc-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.state-and-event-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp.ussd+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp2.bcmcsinfo+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.3gpp2.sms": {
          source: "iana"
        },
        "application/vnd.3gpp2.tcap": {
          source: "iana",
          extensions: ["tcap"]
        },
        "application/vnd.3lightssoftware.imagescal": {
          source: "iana"
        },
        "application/vnd.3m.post-it-notes": {
          source: "iana",
          extensions: ["pwn"]
        },
        "application/vnd.accpac.simply.aso": {
          source: "iana",
          extensions: ["aso"]
        },
        "application/vnd.accpac.simply.imp": {
          source: "iana",
          extensions: ["imp"]
        },
        "application/vnd.acucobol": {
          source: "iana",
          extensions: ["acu"]
        },
        "application/vnd.acucorp": {
          source: "iana",
          extensions: ["atc", "acutc"]
        },
        "application/vnd.adobe.air-application-installer-package+zip": {
          source: "apache",
          compressible: false,
          extensions: ["air"]
        },
        "application/vnd.adobe.flash.movie": {
          source: "iana"
        },
        "application/vnd.adobe.formscentral.fcdt": {
          source: "iana",
          extensions: ["fcdt"]
        },
        "application/vnd.adobe.fxp": {
          source: "iana",
          extensions: ["fxp", "fxpl"]
        },
        "application/vnd.adobe.partial-upload": {
          source: "iana"
        },
        "application/vnd.adobe.xdp+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xdp"]
        },
        "application/vnd.adobe.xfdf": {
          source: "iana",
          extensions: ["xfdf"]
        },
        "application/vnd.aether.imp": {
          source: "iana"
        },
        "application/vnd.afpc.afplinedata": {
          source: "iana"
        },
        "application/vnd.afpc.afplinedata-pagedef": {
          source: "iana"
        },
        "application/vnd.afpc.cmoca-cmresource": {
          source: "iana"
        },
        "application/vnd.afpc.foca-charset": {
          source: "iana"
        },
        "application/vnd.afpc.foca-codedfont": {
          source: "iana"
        },
        "application/vnd.afpc.foca-codepage": {
          source: "iana"
        },
        "application/vnd.afpc.modca": {
          source: "iana"
        },
        "application/vnd.afpc.modca-cmtable": {
          source: "iana"
        },
        "application/vnd.afpc.modca-formdef": {
          source: "iana"
        },
        "application/vnd.afpc.modca-mediummap": {
          source: "iana"
        },
        "application/vnd.afpc.modca-objectcontainer": {
          source: "iana"
        },
        "application/vnd.afpc.modca-overlay": {
          source: "iana"
        },
        "application/vnd.afpc.modca-pagesegment": {
          source: "iana"
        },
        "application/vnd.age": {
          source: "iana",
          extensions: ["age"]
        },
        "application/vnd.ah-barcode": {
          source: "iana"
        },
        "application/vnd.ahead.space": {
          source: "iana",
          extensions: ["ahead"]
        },
        "application/vnd.airzip.filesecure.azf": {
          source: "iana",
          extensions: ["azf"]
        },
        "application/vnd.airzip.filesecure.azs": {
          source: "iana",
          extensions: ["azs"]
        },
        "application/vnd.amadeus+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.amazon.ebook": {
          source: "apache",
          extensions: ["azw"]
        },
        "application/vnd.amazon.mobi8-ebook": {
          source: "iana"
        },
        "application/vnd.americandynamics.acc": {
          source: "iana",
          extensions: ["acc"]
        },
        "application/vnd.amiga.ami": {
          source: "iana",
          extensions: ["ami"]
        },
        "application/vnd.amundsen.maze+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.android.ota": {
          source: "iana"
        },
        "application/vnd.android.package-archive": {
          source: "apache",
          compressible: false,
          extensions: ["apk"]
        },
        "application/vnd.anki": {
          source: "iana"
        },
        "application/vnd.anser-web-certificate-issue-initiation": {
          source: "iana",
          extensions: ["cii"]
        },
        "application/vnd.anser-web-funds-transfer-initiation": {
          source: "apache",
          extensions: ["fti"]
        },
        "application/vnd.antix.game-component": {
          source: "iana",
          extensions: ["atx"]
        },
        "application/vnd.apache.arrow.file": {
          source: "iana"
        },
        "application/vnd.apache.arrow.stream": {
          source: "iana"
        },
        "application/vnd.apache.thrift.binary": {
          source: "iana"
        },
        "application/vnd.apache.thrift.compact": {
          source: "iana"
        },
        "application/vnd.apache.thrift.json": {
          source: "iana"
        },
        "application/vnd.api+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.aplextor.warrp+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.apothekende.reservation+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.apple.installer+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mpkg"]
        },
        "application/vnd.apple.keynote": {
          source: "iana",
          extensions: ["key"]
        },
        "application/vnd.apple.mpegurl": {
          source: "iana",
          extensions: ["m3u8"]
        },
        "application/vnd.apple.numbers": {
          source: "iana",
          extensions: ["numbers"]
        },
        "application/vnd.apple.pages": {
          source: "iana",
          extensions: ["pages"]
        },
        "application/vnd.apple.pkpass": {
          compressible: false,
          extensions: ["pkpass"]
        },
        "application/vnd.arastra.swi": {
          source: "iana"
        },
        "application/vnd.aristanetworks.swi": {
          source: "iana",
          extensions: ["swi"]
        },
        "application/vnd.artisan+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.artsquare": {
          source: "iana"
        },
        "application/vnd.astraea-software.iota": {
          source: "iana",
          extensions: ["iota"]
        },
        "application/vnd.audiograph": {
          source: "iana",
          extensions: ["aep"]
        },
        "application/vnd.autopackage": {
          source: "iana"
        },
        "application/vnd.avalon+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.avistar+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.balsamiq.bmml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["bmml"]
        },
        "application/vnd.balsamiq.bmpr": {
          source: "iana"
        },
        "application/vnd.banana-accounting": {
          source: "iana"
        },
        "application/vnd.bbf.usp.error": {
          source: "iana"
        },
        "application/vnd.bbf.usp.msg": {
          source: "iana"
        },
        "application/vnd.bbf.usp.msg+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.bekitzur-stech+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.bint.med-content": {
          source: "iana"
        },
        "application/vnd.biopax.rdf+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.blink-idb-value-wrapper": {
          source: "iana"
        },
        "application/vnd.blueice.multipass": {
          source: "iana",
          extensions: ["mpm"]
        },
        "application/vnd.bluetooth.ep.oob": {
          source: "iana"
        },
        "application/vnd.bluetooth.le.oob": {
          source: "iana"
        },
        "application/vnd.bmi": {
          source: "iana",
          extensions: ["bmi"]
        },
        "application/vnd.bpf": {
          source: "iana"
        },
        "application/vnd.bpf3": {
          source: "iana"
        },
        "application/vnd.businessobjects": {
          source: "iana",
          extensions: ["rep"]
        },
        "application/vnd.byu.uapi+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.cab-jscript": {
          source: "iana"
        },
        "application/vnd.canon-cpdl": {
          source: "iana"
        },
        "application/vnd.canon-lips": {
          source: "iana"
        },
        "application/vnd.capasystems-pg+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.cendio.thinlinc.clientconf": {
          source: "iana"
        },
        "application/vnd.century-systems.tcp_stream": {
          source: "iana"
        },
        "application/vnd.chemdraw+xml": {
          source: "iana",
          compressible: true,
          extensions: ["cdxml"]
        },
        "application/vnd.chess-pgn": {
          source: "iana"
        },
        "application/vnd.chipnuts.karaoke-mmd": {
          source: "iana",
          extensions: ["mmd"]
        },
        "application/vnd.ciedi": {
          source: "iana"
        },
        "application/vnd.cinderella": {
          source: "iana",
          extensions: ["cdy"]
        },
        "application/vnd.cirpack.isdn-ext": {
          source: "iana"
        },
        "application/vnd.citationstyles.style+xml": {
          source: "iana",
          compressible: true,
          extensions: ["csl"]
        },
        "application/vnd.claymore": {
          source: "iana",
          extensions: ["cla"]
        },
        "application/vnd.cloanto.rp9": {
          source: "iana",
          extensions: ["rp9"]
        },
        "application/vnd.clonk.c4group": {
          source: "iana",
          extensions: ["c4g", "c4d", "c4f", "c4p", "c4u"]
        },
        "application/vnd.cluetrust.cartomobile-config": {
          source: "iana",
          extensions: ["c11amc"]
        },
        "application/vnd.cluetrust.cartomobile-config-pkg": {
          source: "iana",
          extensions: ["c11amz"]
        },
        "application/vnd.coffeescript": {
          source: "iana"
        },
        "application/vnd.collabio.xodocuments.document": {
          source: "iana"
        },
        "application/vnd.collabio.xodocuments.document-template": {
          source: "iana"
        },
        "application/vnd.collabio.xodocuments.presentation": {
          source: "iana"
        },
        "application/vnd.collabio.xodocuments.presentation-template": {
          source: "iana"
        },
        "application/vnd.collabio.xodocuments.spreadsheet": {
          source: "iana"
        },
        "application/vnd.collabio.xodocuments.spreadsheet-template": {
          source: "iana"
        },
        "application/vnd.collection+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.collection.doc+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.collection.next+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.comicbook+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.comicbook-rar": {
          source: "iana"
        },
        "application/vnd.commerce-battelle": {
          source: "iana"
        },
        "application/vnd.commonspace": {
          source: "iana",
          extensions: ["csp"]
        },
        "application/vnd.contact.cmsg": {
          source: "iana",
          extensions: ["cdbcmsg"]
        },
        "application/vnd.coreos.ignition+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.cosmocaller": {
          source: "iana",
          extensions: ["cmc"]
        },
        "application/vnd.crick.clicker": {
          source: "iana",
          extensions: ["clkx"]
        },
        "application/vnd.crick.clicker.keyboard": {
          source: "iana",
          extensions: ["clkk"]
        },
        "application/vnd.crick.clicker.palette": {
          source: "iana",
          extensions: ["clkp"]
        },
        "application/vnd.crick.clicker.template": {
          source: "iana",
          extensions: ["clkt"]
        },
        "application/vnd.crick.clicker.wordbank": {
          source: "iana",
          extensions: ["clkw"]
        },
        "application/vnd.criticaltools.wbs+xml": {
          source: "iana",
          compressible: true,
          extensions: ["wbs"]
        },
        "application/vnd.cryptii.pipe+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.crypto-shade-file": {
          source: "iana"
        },
        "application/vnd.cryptomator.encrypted": {
          source: "iana"
        },
        "application/vnd.cryptomator.vault": {
          source: "iana"
        },
        "application/vnd.ctc-posml": {
          source: "iana",
          extensions: ["pml"]
        },
        "application/vnd.ctct.ws+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.cups-pdf": {
          source: "iana"
        },
        "application/vnd.cups-postscript": {
          source: "iana"
        },
        "application/vnd.cups-ppd": {
          source: "iana",
          extensions: ["ppd"]
        },
        "application/vnd.cups-raster": {
          source: "iana"
        },
        "application/vnd.cups-raw": {
          source: "iana"
        },
        "application/vnd.curl": {
          source: "iana"
        },
        "application/vnd.curl.car": {
          source: "apache",
          extensions: ["car"]
        },
        "application/vnd.curl.pcurl": {
          source: "apache",
          extensions: ["pcurl"]
        },
        "application/vnd.cyan.dean.root+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.cybank": {
          source: "iana"
        },
        "application/vnd.cyclonedx+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.cyclonedx+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.d2l.coursepackage1p0+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.d3m-dataset": {
          source: "iana"
        },
        "application/vnd.d3m-problem": {
          source: "iana"
        },
        "application/vnd.dart": {
          source: "iana",
          compressible: true,
          extensions: ["dart"]
        },
        "application/vnd.data-vision.rdz": {
          source: "iana",
          extensions: ["rdz"]
        },
        "application/vnd.datapackage+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dataresource+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dbf": {
          source: "iana",
          extensions: ["dbf"]
        },
        "application/vnd.debian.binary-package": {
          source: "iana"
        },
        "application/vnd.dece.data": {
          source: "iana",
          extensions: ["uvf", "uvvf", "uvd", "uvvd"]
        },
        "application/vnd.dece.ttml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["uvt", "uvvt"]
        },
        "application/vnd.dece.unspecified": {
          source: "iana",
          extensions: ["uvx", "uvvx"]
        },
        "application/vnd.dece.zip": {
          source: "iana",
          extensions: ["uvz", "uvvz"]
        },
        "application/vnd.denovo.fcselayout-link": {
          source: "iana",
          extensions: ["fe_launch"]
        },
        "application/vnd.desmume.movie": {
          source: "iana"
        },
        "application/vnd.dir-bi.plate-dl-nosuffix": {
          source: "iana"
        },
        "application/vnd.dm.delegation+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dna": {
          source: "iana",
          extensions: ["dna"]
        },
        "application/vnd.document+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dolby.mlp": {
          source: "apache",
          extensions: ["mlp"]
        },
        "application/vnd.dolby.mobile.1": {
          source: "iana"
        },
        "application/vnd.dolby.mobile.2": {
          source: "iana"
        },
        "application/vnd.doremir.scorecloud-binary-document": {
          source: "iana"
        },
        "application/vnd.dpgraph": {
          source: "iana",
          extensions: ["dpg"]
        },
        "application/vnd.dreamfactory": {
          source: "iana",
          extensions: ["dfac"]
        },
        "application/vnd.drive+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ds-keypoint": {
          source: "apache",
          extensions: ["kpxx"]
        },
        "application/vnd.dtg.local": {
          source: "iana"
        },
        "application/vnd.dtg.local.flash": {
          source: "iana"
        },
        "application/vnd.dtg.local.html": {
          source: "iana"
        },
        "application/vnd.dvb.ait": {
          source: "iana",
          extensions: ["ait"]
        },
        "application/vnd.dvb.dvbisl+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.dvbj": {
          source: "iana"
        },
        "application/vnd.dvb.esgcontainer": {
          source: "iana"
        },
        "application/vnd.dvb.ipdcdftnotifaccess": {
          source: "iana"
        },
        "application/vnd.dvb.ipdcesgaccess": {
          source: "iana"
        },
        "application/vnd.dvb.ipdcesgaccess2": {
          source: "iana"
        },
        "application/vnd.dvb.ipdcesgpdd": {
          source: "iana"
        },
        "application/vnd.dvb.ipdcroaming": {
          source: "iana"
        },
        "application/vnd.dvb.iptv.alfec-base": {
          source: "iana"
        },
        "application/vnd.dvb.iptv.alfec-enhancement": {
          source: "iana"
        },
        "application/vnd.dvb.notif-aggregate-root+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.notif-container+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.notif-generic+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.notif-ia-msglist+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.notif-ia-registration-request+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.notif-ia-registration-response+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.notif-init+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.dvb.pfr": {
          source: "iana"
        },
        "application/vnd.dvb.service": {
          source: "iana",
          extensions: ["svc"]
        },
        "application/vnd.dxr": {
          source: "iana"
        },
        "application/vnd.dynageo": {
          source: "iana",
          extensions: ["geo"]
        },
        "application/vnd.dzr": {
          source: "iana"
        },
        "application/vnd.easykaraoke.cdgdownload": {
          source: "iana"
        },
        "application/vnd.ecdis-update": {
          source: "iana"
        },
        "application/vnd.ecip.rlp": {
          source: "iana"
        },
        "application/vnd.eclipse.ditto+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ecowin.chart": {
          source: "iana",
          extensions: ["mag"]
        },
        "application/vnd.ecowin.filerequest": {
          source: "iana"
        },
        "application/vnd.ecowin.fileupdate": {
          source: "iana"
        },
        "application/vnd.ecowin.series": {
          source: "iana"
        },
        "application/vnd.ecowin.seriesrequest": {
          source: "iana"
        },
        "application/vnd.ecowin.seriesupdate": {
          source: "iana"
        },
        "application/vnd.efi.img": {
          source: "iana"
        },
        "application/vnd.efi.iso": {
          source: "iana"
        },
        "application/vnd.emclient.accessrequest+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.enliven": {
          source: "iana",
          extensions: ["nml"]
        },
        "application/vnd.enphase.envoy": {
          source: "iana"
        },
        "application/vnd.eprints.data+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.epson.esf": {
          source: "iana",
          extensions: ["esf"]
        },
        "application/vnd.epson.msf": {
          source: "iana",
          extensions: ["msf"]
        },
        "application/vnd.epson.quickanime": {
          source: "iana",
          extensions: ["qam"]
        },
        "application/vnd.epson.salt": {
          source: "iana",
          extensions: ["slt"]
        },
        "application/vnd.epson.ssf": {
          source: "iana",
          extensions: ["ssf"]
        },
        "application/vnd.ericsson.quickcall": {
          source: "iana"
        },
        "application/vnd.espass-espass+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.eszigno3+xml": {
          source: "iana",
          compressible: true,
          extensions: ["es3", "et3"]
        },
        "application/vnd.etsi.aoc+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.asic-e+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.etsi.asic-s+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.etsi.cug+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvcommand+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvdiscovery+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvprofile+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvsad-bc+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvsad-cod+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvsad-npvr+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvservice+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvsync+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.iptvueprofile+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.mcid+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.mheg5": {
          source: "iana"
        },
        "application/vnd.etsi.overload-control-policy-dataset+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.pstn+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.sci+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.simservs+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.timestamp-token": {
          source: "iana"
        },
        "application/vnd.etsi.tsl+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.etsi.tsl.der": {
          source: "iana"
        },
        "application/vnd.eu.kasparian.car+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.eudora.data": {
          source: "iana"
        },
        "application/vnd.evolv.ecig.profile": {
          source: "iana"
        },
        "application/vnd.evolv.ecig.settings": {
          source: "iana"
        },
        "application/vnd.evolv.ecig.theme": {
          source: "iana"
        },
        "application/vnd.exstream-empower+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.exstream-package": {
          source: "iana"
        },
        "application/vnd.ezpix-album": {
          source: "iana",
          extensions: ["ez2"]
        },
        "application/vnd.ezpix-package": {
          source: "iana",
          extensions: ["ez3"]
        },
        "application/vnd.f-secure.mobile": {
          source: "iana"
        },
        "application/vnd.familysearch.gedcom+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.fastcopy-disk-image": {
          source: "iana"
        },
        "application/vnd.fdf": {
          source: "iana",
          extensions: ["fdf"]
        },
        "application/vnd.fdsn.mseed": {
          source: "iana",
          extensions: ["mseed"]
        },
        "application/vnd.fdsn.seed": {
          source: "iana",
          extensions: ["seed", "dataless"]
        },
        "application/vnd.ffsns": {
          source: "iana"
        },
        "application/vnd.ficlab.flb+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.filmit.zfc": {
          source: "iana"
        },
        "application/vnd.fints": {
          source: "iana"
        },
        "application/vnd.firemonkeys.cloudcell": {
          source: "iana"
        },
        "application/vnd.flographit": {
          source: "iana",
          extensions: ["gph"]
        },
        "application/vnd.fluxtime.clip": {
          source: "iana",
          extensions: ["ftc"]
        },
        "application/vnd.font-fontforge-sfd": {
          source: "iana"
        },
        "application/vnd.framemaker": {
          source: "iana",
          extensions: ["fm", "frame", "maker", "book"]
        },
        "application/vnd.frogans.fnc": {
          source: "iana",
          extensions: ["fnc"]
        },
        "application/vnd.frogans.ltf": {
          source: "iana",
          extensions: ["ltf"]
        },
        "application/vnd.fsc.weblaunch": {
          source: "iana",
          extensions: ["fsc"]
        },
        "application/vnd.fujifilm.fb.docuworks": {
          source: "iana"
        },
        "application/vnd.fujifilm.fb.docuworks.binder": {
          source: "iana"
        },
        "application/vnd.fujifilm.fb.docuworks.container": {
          source: "iana"
        },
        "application/vnd.fujifilm.fb.jfi+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.fujitsu.oasys": {
          source: "iana",
          extensions: ["oas"]
        },
        "application/vnd.fujitsu.oasys2": {
          source: "iana",
          extensions: ["oa2"]
        },
        "application/vnd.fujitsu.oasys3": {
          source: "iana",
          extensions: ["oa3"]
        },
        "application/vnd.fujitsu.oasysgp": {
          source: "iana",
          extensions: ["fg5"]
        },
        "application/vnd.fujitsu.oasysprs": {
          source: "iana",
          extensions: ["bh2"]
        },
        "application/vnd.fujixerox.art-ex": {
          source: "iana"
        },
        "application/vnd.fujixerox.art4": {
          source: "iana"
        },
        "application/vnd.fujixerox.ddd": {
          source: "iana",
          extensions: ["ddd"]
        },
        "application/vnd.fujixerox.docuworks": {
          source: "iana",
          extensions: ["xdw"]
        },
        "application/vnd.fujixerox.docuworks.binder": {
          source: "iana",
          extensions: ["xbd"]
        },
        "application/vnd.fujixerox.docuworks.container": {
          source: "iana"
        },
        "application/vnd.fujixerox.hbpl": {
          source: "iana"
        },
        "application/vnd.fut-misnet": {
          source: "iana"
        },
        "application/vnd.futoin+cbor": {
          source: "iana"
        },
        "application/vnd.futoin+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.fuzzysheet": {
          source: "iana",
          extensions: ["fzs"]
        },
        "application/vnd.genomatix.tuxedo": {
          source: "iana",
          extensions: ["txd"]
        },
        "application/vnd.gentics.grd+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.geo+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.geocube+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.geogebra.file": {
          source: "iana",
          extensions: ["ggb"]
        },
        "application/vnd.geogebra.slides": {
          source: "iana"
        },
        "application/vnd.geogebra.tool": {
          source: "iana",
          extensions: ["ggt"]
        },
        "application/vnd.geometry-explorer": {
          source: "iana",
          extensions: ["gex", "gre"]
        },
        "application/vnd.geonext": {
          source: "iana",
          extensions: ["gxt"]
        },
        "application/vnd.geoplan": {
          source: "iana",
          extensions: ["g2w"]
        },
        "application/vnd.geospace": {
          source: "iana",
          extensions: ["g3w"]
        },
        "application/vnd.gerber": {
          source: "iana"
        },
        "application/vnd.globalplatform.card-content-mgt": {
          source: "iana"
        },
        "application/vnd.globalplatform.card-content-mgt-response": {
          source: "iana"
        },
        "application/vnd.gmx": {
          source: "iana",
          extensions: ["gmx"]
        },
        "application/vnd.google-apps.document": {
          compressible: false,
          extensions: ["gdoc"]
        },
        "application/vnd.google-apps.presentation": {
          compressible: false,
          extensions: ["gslides"]
        },
        "application/vnd.google-apps.spreadsheet": {
          compressible: false,
          extensions: ["gsheet"]
        },
        "application/vnd.google-earth.kml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["kml"]
        },
        "application/vnd.google-earth.kmz": {
          source: "iana",
          compressible: false,
          extensions: ["kmz"]
        },
        "application/vnd.gov.sk.e-form+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.gov.sk.e-form+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.gov.sk.xmldatacontainer+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.grafeq": {
          source: "iana",
          extensions: ["gqf", "gqs"]
        },
        "application/vnd.gridmp": {
          source: "iana"
        },
        "application/vnd.groove-account": {
          source: "iana",
          extensions: ["gac"]
        },
        "application/vnd.groove-help": {
          source: "iana",
          extensions: ["ghf"]
        },
        "application/vnd.groove-identity-message": {
          source: "iana",
          extensions: ["gim"]
        },
        "application/vnd.groove-injector": {
          source: "iana",
          extensions: ["grv"]
        },
        "application/vnd.groove-tool-message": {
          source: "iana",
          extensions: ["gtm"]
        },
        "application/vnd.groove-tool-template": {
          source: "iana",
          extensions: ["tpl"]
        },
        "application/vnd.groove-vcard": {
          source: "iana",
          extensions: ["vcg"]
        },
        "application/vnd.hal+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.hal+xml": {
          source: "iana",
          compressible: true,
          extensions: ["hal"]
        },
        "application/vnd.handheld-entertainment+xml": {
          source: "iana",
          compressible: true,
          extensions: ["zmm"]
        },
        "application/vnd.hbci": {
          source: "iana",
          extensions: ["hbci"]
        },
        "application/vnd.hc+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.hcl-bireports": {
          source: "iana"
        },
        "application/vnd.hdt": {
          source: "iana"
        },
        "application/vnd.heroku+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.hhe.lesson-player": {
          source: "iana",
          extensions: ["les"]
        },
        "application/vnd.hl7cda+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/vnd.hl7v2+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/vnd.hp-hpgl": {
          source: "iana",
          extensions: ["hpgl"]
        },
        "application/vnd.hp-hpid": {
          source: "iana",
          extensions: ["hpid"]
        },
        "application/vnd.hp-hps": {
          source: "iana",
          extensions: ["hps"]
        },
        "application/vnd.hp-jlyt": {
          source: "iana",
          extensions: ["jlt"]
        },
        "application/vnd.hp-pcl": {
          source: "iana",
          extensions: ["pcl"]
        },
        "application/vnd.hp-pclxl": {
          source: "iana",
          extensions: ["pclxl"]
        },
        "application/vnd.httphone": {
          source: "iana"
        },
        "application/vnd.hydrostatix.sof-data": {
          source: "iana",
          extensions: ["sfd-hdstx"]
        },
        "application/vnd.hyper+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.hyper-item+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.hyperdrive+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.hzn-3d-crossword": {
          source: "iana"
        },
        "application/vnd.ibm.afplinedata": {
          source: "iana"
        },
        "application/vnd.ibm.electronic-media": {
          source: "iana"
        },
        "application/vnd.ibm.minipay": {
          source: "iana",
          extensions: ["mpy"]
        },
        "application/vnd.ibm.modcap": {
          source: "iana",
          extensions: ["afp", "listafp", "list3820"]
        },
        "application/vnd.ibm.rights-management": {
          source: "iana",
          extensions: ["irm"]
        },
        "application/vnd.ibm.secure-container": {
          source: "iana",
          extensions: ["sc"]
        },
        "application/vnd.iccprofile": {
          source: "iana",
          extensions: ["icc", "icm"]
        },
        "application/vnd.ieee.1905": {
          source: "iana"
        },
        "application/vnd.igloader": {
          source: "iana",
          extensions: ["igl"]
        },
        "application/vnd.imagemeter.folder+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.imagemeter.image+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.immervision-ivp": {
          source: "iana",
          extensions: ["ivp"]
        },
        "application/vnd.immervision-ivu": {
          source: "iana",
          extensions: ["ivu"]
        },
        "application/vnd.ims.imsccv1p1": {
          source: "iana"
        },
        "application/vnd.ims.imsccv1p2": {
          source: "iana"
        },
        "application/vnd.ims.imsccv1p3": {
          source: "iana"
        },
        "application/vnd.ims.lis.v2.result+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ims.lti.v2.toolconsumerprofile+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ims.lti.v2.toolproxy+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ims.lti.v2.toolproxy.id+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ims.lti.v2.toolsettings+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ims.lti.v2.toolsettings.simple+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.informedcontrol.rms+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.informix-visionary": {
          source: "iana"
        },
        "application/vnd.infotech.project": {
          source: "iana"
        },
        "application/vnd.infotech.project+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.innopath.wamp.notification": {
          source: "iana"
        },
        "application/vnd.insors.igm": {
          source: "iana",
          extensions: ["igm"]
        },
        "application/vnd.intercon.formnet": {
          source: "iana",
          extensions: ["xpw", "xpx"]
        },
        "application/vnd.intergeo": {
          source: "iana",
          extensions: ["i2g"]
        },
        "application/vnd.intertrust.digibox": {
          source: "iana"
        },
        "application/vnd.intertrust.nncp": {
          source: "iana"
        },
        "application/vnd.intu.qbo": {
          source: "iana",
          extensions: ["qbo"]
        },
        "application/vnd.intu.qfx": {
          source: "iana",
          extensions: ["qfx"]
        },
        "application/vnd.iptc.g2.catalogitem+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.iptc.g2.conceptitem+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.iptc.g2.knowledgeitem+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.iptc.g2.newsitem+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.iptc.g2.newsmessage+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.iptc.g2.packageitem+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.iptc.g2.planningitem+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ipunplugged.rcprofile": {
          source: "iana",
          extensions: ["rcprofile"]
        },
        "application/vnd.irepository.package+xml": {
          source: "iana",
          compressible: true,
          extensions: ["irp"]
        },
        "application/vnd.is-xpr": {
          source: "iana",
          extensions: ["xpr"]
        },
        "application/vnd.isac.fcs": {
          source: "iana",
          extensions: ["fcs"]
        },
        "application/vnd.iso11783-10+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.jam": {
          source: "iana",
          extensions: ["jam"]
        },
        "application/vnd.japannet-directory-service": {
          source: "iana"
        },
        "application/vnd.japannet-jpnstore-wakeup": {
          source: "iana"
        },
        "application/vnd.japannet-payment-wakeup": {
          source: "iana"
        },
        "application/vnd.japannet-registration": {
          source: "iana"
        },
        "application/vnd.japannet-registration-wakeup": {
          source: "iana"
        },
        "application/vnd.japannet-setstore-wakeup": {
          source: "iana"
        },
        "application/vnd.japannet-verification": {
          source: "iana"
        },
        "application/vnd.japannet-verification-wakeup": {
          source: "iana"
        },
        "application/vnd.jcp.javame.midlet-rms": {
          source: "iana",
          extensions: ["rms"]
        },
        "application/vnd.jisp": {
          source: "iana",
          extensions: ["jisp"]
        },
        "application/vnd.joost.joda-archive": {
          source: "iana",
          extensions: ["joda"]
        },
        "application/vnd.jsk.isdn-ngn": {
          source: "iana"
        },
        "application/vnd.kahootz": {
          source: "iana",
          extensions: ["ktz", "ktr"]
        },
        "application/vnd.kde.karbon": {
          source: "iana",
          extensions: ["karbon"]
        },
        "application/vnd.kde.kchart": {
          source: "iana",
          extensions: ["chrt"]
        },
        "application/vnd.kde.kformula": {
          source: "iana",
          extensions: ["kfo"]
        },
        "application/vnd.kde.kivio": {
          source: "iana",
          extensions: ["flw"]
        },
        "application/vnd.kde.kontour": {
          source: "iana",
          extensions: ["kon"]
        },
        "application/vnd.kde.kpresenter": {
          source: "iana",
          extensions: ["kpr", "kpt"]
        },
        "application/vnd.kde.kspread": {
          source: "iana",
          extensions: ["ksp"]
        },
        "application/vnd.kde.kword": {
          source: "iana",
          extensions: ["kwd", "kwt"]
        },
        "application/vnd.kenameaapp": {
          source: "iana",
          extensions: ["htke"]
        },
        "application/vnd.kidspiration": {
          source: "iana",
          extensions: ["kia"]
        },
        "application/vnd.kinar": {
          source: "iana",
          extensions: ["kne", "knp"]
        },
        "application/vnd.koan": {
          source: "iana",
          extensions: ["skp", "skd", "skt", "skm"]
        },
        "application/vnd.kodak-descriptor": {
          source: "iana",
          extensions: ["sse"]
        },
        "application/vnd.las": {
          source: "iana"
        },
        "application/vnd.las.las+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.las.las+xml": {
          source: "iana",
          compressible: true,
          extensions: ["lasxml"]
        },
        "application/vnd.laszip": {
          source: "iana"
        },
        "application/vnd.leap+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.liberty-request+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.llamagraphics.life-balance.desktop": {
          source: "iana",
          extensions: ["lbd"]
        },
        "application/vnd.llamagraphics.life-balance.exchange+xml": {
          source: "iana",
          compressible: true,
          extensions: ["lbe"]
        },
        "application/vnd.logipipe.circuit+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.loom": {
          source: "iana"
        },
        "application/vnd.lotus-1-2-3": {
          source: "iana",
          extensions: ["123"]
        },
        "application/vnd.lotus-approach": {
          source: "iana",
          extensions: ["apr"]
        },
        "application/vnd.lotus-freelance": {
          source: "iana",
          extensions: ["pre"]
        },
        "application/vnd.lotus-notes": {
          source: "iana",
          extensions: ["nsf"]
        },
        "application/vnd.lotus-organizer": {
          source: "iana",
          extensions: ["org"]
        },
        "application/vnd.lotus-screencam": {
          source: "iana",
          extensions: ["scm"]
        },
        "application/vnd.lotus-wordpro": {
          source: "iana",
          extensions: ["lwp"]
        },
        "application/vnd.macports.portpkg": {
          source: "iana",
          extensions: ["portpkg"]
        },
        "application/vnd.mapbox-vector-tile": {
          source: "iana",
          extensions: ["mvt"]
        },
        "application/vnd.marlin.drm.actiontoken+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.marlin.drm.conftoken+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.marlin.drm.license+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.marlin.drm.mdcf": {
          source: "iana"
        },
        "application/vnd.mason+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.maxar.archive.3tz+zip": {
          source: "iana",
          compressible: false
        },
        "application/vnd.maxmind.maxmind-db": {
          source: "iana"
        },
        "application/vnd.mcd": {
          source: "iana",
          extensions: ["mcd"]
        },
        "application/vnd.medcalcdata": {
          source: "iana",
          extensions: ["mc1"]
        },
        "application/vnd.mediastation.cdkey": {
          source: "iana",
          extensions: ["cdkey"]
        },
        "application/vnd.meridian-slingshot": {
          source: "iana"
        },
        "application/vnd.mfer": {
          source: "iana",
          extensions: ["mwf"]
        },
        "application/vnd.mfmp": {
          source: "iana",
          extensions: ["mfm"]
        },
        "application/vnd.micro+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.micrografx.flo": {
          source: "iana",
          extensions: ["flo"]
        },
        "application/vnd.micrografx.igx": {
          source: "iana",
          extensions: ["igx"]
        },
        "application/vnd.microsoft.portable-executable": {
          source: "iana"
        },
        "application/vnd.microsoft.windows.thumbnail-cache": {
          source: "iana"
        },
        "application/vnd.miele+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.mif": {
          source: "iana",
          extensions: ["mif"]
        },
        "application/vnd.minisoft-hp3000-save": {
          source: "iana"
        },
        "application/vnd.mitsubishi.misty-guard.trustweb": {
          source: "iana"
        },
        "application/vnd.mobius.daf": {
          source: "iana",
          extensions: ["daf"]
        },
        "application/vnd.mobius.dis": {
          source: "iana",
          extensions: ["dis"]
        },
        "application/vnd.mobius.mbk": {
          source: "iana",
          extensions: ["mbk"]
        },
        "application/vnd.mobius.mqy": {
          source: "iana",
          extensions: ["mqy"]
        },
        "application/vnd.mobius.msl": {
          source: "iana",
          extensions: ["msl"]
        },
        "application/vnd.mobius.plc": {
          source: "iana",
          extensions: ["plc"]
        },
        "application/vnd.mobius.txf": {
          source: "iana",
          extensions: ["txf"]
        },
        "application/vnd.mophun.application": {
          source: "iana",
          extensions: ["mpn"]
        },
        "application/vnd.mophun.certificate": {
          source: "iana",
          extensions: ["mpc"]
        },
        "application/vnd.motorola.flexsuite": {
          source: "iana"
        },
        "application/vnd.motorola.flexsuite.adsi": {
          source: "iana"
        },
        "application/vnd.motorola.flexsuite.fis": {
          source: "iana"
        },
        "application/vnd.motorola.flexsuite.gotap": {
          source: "iana"
        },
        "application/vnd.motorola.flexsuite.kmr": {
          source: "iana"
        },
        "application/vnd.motorola.flexsuite.ttc": {
          source: "iana"
        },
        "application/vnd.motorola.flexsuite.wem": {
          source: "iana"
        },
        "application/vnd.motorola.iprm": {
          source: "iana"
        },
        "application/vnd.mozilla.xul+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xul"]
        },
        "application/vnd.ms-3mfdocument": {
          source: "iana"
        },
        "application/vnd.ms-artgalry": {
          source: "iana",
          extensions: ["cil"]
        },
        "application/vnd.ms-asf": {
          source: "iana"
        },
        "application/vnd.ms-cab-compressed": {
          source: "iana",
          extensions: ["cab"]
        },
        "application/vnd.ms-color.iccprofile": {
          source: "apache"
        },
        "application/vnd.ms-excel": {
          source: "iana",
          compressible: false,
          extensions: ["xls", "xlm", "xla", "xlc", "xlt", "xlw"]
        },
        "application/vnd.ms-excel.addin.macroenabled.12": {
          source: "iana",
          extensions: ["xlam"]
        },
        "application/vnd.ms-excel.sheet.binary.macroenabled.12": {
          source: "iana",
          extensions: ["xlsb"]
        },
        "application/vnd.ms-excel.sheet.macroenabled.12": {
          source: "iana",
          extensions: ["xlsm"]
        },
        "application/vnd.ms-excel.template.macroenabled.12": {
          source: "iana",
          extensions: ["xltm"]
        },
        "application/vnd.ms-fontobject": {
          source: "iana",
          compressible: true,
          extensions: ["eot"]
        },
        "application/vnd.ms-htmlhelp": {
          source: "iana",
          extensions: ["chm"]
        },
        "application/vnd.ms-ims": {
          source: "iana",
          extensions: ["ims"]
        },
        "application/vnd.ms-lrm": {
          source: "iana",
          extensions: ["lrm"]
        },
        "application/vnd.ms-office.activex+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ms-officetheme": {
          source: "iana",
          extensions: ["thmx"]
        },
        "application/vnd.ms-opentype": {
          source: "apache",
          compressible: true
        },
        "application/vnd.ms-outlook": {
          compressible: false,
          extensions: ["msg"]
        },
        "application/vnd.ms-package.obfuscated-opentype": {
          source: "apache"
        },
        "application/vnd.ms-pki.seccat": {
          source: "apache",
          extensions: ["cat"]
        },
        "application/vnd.ms-pki.stl": {
          source: "apache",
          extensions: ["stl"]
        },
        "application/vnd.ms-playready.initiator+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ms-powerpoint": {
          source: "iana",
          compressible: false,
          extensions: ["ppt", "pps", "pot"]
        },
        "application/vnd.ms-powerpoint.addin.macroenabled.12": {
          source: "iana",
          extensions: ["ppam"]
        },
        "application/vnd.ms-powerpoint.presentation.macroenabled.12": {
          source: "iana",
          extensions: ["pptm"]
        },
        "application/vnd.ms-powerpoint.slide.macroenabled.12": {
          source: "iana",
          extensions: ["sldm"]
        },
        "application/vnd.ms-powerpoint.slideshow.macroenabled.12": {
          source: "iana",
          extensions: ["ppsm"]
        },
        "application/vnd.ms-powerpoint.template.macroenabled.12": {
          source: "iana",
          extensions: ["potm"]
        },
        "application/vnd.ms-printdevicecapabilities+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ms-printing.printticket+xml": {
          source: "apache",
          compressible: true
        },
        "application/vnd.ms-printschematicket+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ms-project": {
          source: "iana",
          extensions: ["mpp", "mpt"]
        },
        "application/vnd.ms-tnef": {
          source: "iana"
        },
        "application/vnd.ms-windows.devicepairing": {
          source: "iana"
        },
        "application/vnd.ms-windows.nwprinting.oob": {
          source: "iana"
        },
        "application/vnd.ms-windows.printerpairing": {
          source: "iana"
        },
        "application/vnd.ms-windows.wsd.oob": {
          source: "iana"
        },
        "application/vnd.ms-wmdrm.lic-chlg-req": {
          source: "iana"
        },
        "application/vnd.ms-wmdrm.lic-resp": {
          source: "iana"
        },
        "application/vnd.ms-wmdrm.meter-chlg-req": {
          source: "iana"
        },
        "application/vnd.ms-wmdrm.meter-resp": {
          source: "iana"
        },
        "application/vnd.ms-word.document.macroenabled.12": {
          source: "iana",
          extensions: ["docm"]
        },
        "application/vnd.ms-word.template.macroenabled.12": {
          source: "iana",
          extensions: ["dotm"]
        },
        "application/vnd.ms-works": {
          source: "iana",
          extensions: ["wps", "wks", "wcm", "wdb"]
        },
        "application/vnd.ms-wpl": {
          source: "iana",
          extensions: ["wpl"]
        },
        "application/vnd.ms-xpsdocument": {
          source: "iana",
          compressible: false,
          extensions: ["xps"]
        },
        "application/vnd.msa-disk-image": {
          source: "iana"
        },
        "application/vnd.mseq": {
          source: "iana",
          extensions: ["mseq"]
        },
        "application/vnd.msign": {
          source: "iana"
        },
        "application/vnd.multiad.creator": {
          source: "iana"
        },
        "application/vnd.multiad.creator.cif": {
          source: "iana"
        },
        "application/vnd.music-niff": {
          source: "iana"
        },
        "application/vnd.musician": {
          source: "iana",
          extensions: ["mus"]
        },
        "application/vnd.muvee.style": {
          source: "iana",
          extensions: ["msty"]
        },
        "application/vnd.mynfc": {
          source: "iana",
          extensions: ["taglet"]
        },
        "application/vnd.nacamar.ybrid+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.ncd.control": {
          source: "iana"
        },
        "application/vnd.ncd.reference": {
          source: "iana"
        },
        "application/vnd.nearst.inv+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.nebumind.line": {
          source: "iana"
        },
        "application/vnd.nervana": {
          source: "iana"
        },
        "application/vnd.netfpx": {
          source: "iana"
        },
        "application/vnd.neurolanguage.nlu": {
          source: "iana",
          extensions: ["nlu"]
        },
        "application/vnd.nimn": {
          source: "iana"
        },
        "application/vnd.nintendo.nitro.rom": {
          source: "iana"
        },
        "application/vnd.nintendo.snes.rom": {
          source: "iana"
        },
        "application/vnd.nitf": {
          source: "iana",
          extensions: ["ntf", "nitf"]
        },
        "application/vnd.noblenet-directory": {
          source: "iana",
          extensions: ["nnd"]
        },
        "application/vnd.noblenet-sealer": {
          source: "iana",
          extensions: ["nns"]
        },
        "application/vnd.noblenet-web": {
          source: "iana",
          extensions: ["nnw"]
        },
        "application/vnd.nokia.catalogs": {
          source: "iana"
        },
        "application/vnd.nokia.conml+wbxml": {
          source: "iana"
        },
        "application/vnd.nokia.conml+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.nokia.iptv.config+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.nokia.isds-radio-presets": {
          source: "iana"
        },
        "application/vnd.nokia.landmark+wbxml": {
          source: "iana"
        },
        "application/vnd.nokia.landmark+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.nokia.landmarkcollection+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.nokia.n-gage.ac+xml": {
          source: "iana",
          compressible: true,
          extensions: ["ac"]
        },
        "application/vnd.nokia.n-gage.data": {
          source: "iana",
          extensions: ["ngdat"]
        },
        "application/vnd.nokia.n-gage.symbian.install": {
          source: "iana",
          extensions: ["n-gage"]
        },
        "application/vnd.nokia.ncd": {
          source: "iana"
        },
        "application/vnd.nokia.pcd+wbxml": {
          source: "iana"
        },
        "application/vnd.nokia.pcd+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.nokia.radio-preset": {
          source: "iana",
          extensions: ["rpst"]
        },
        "application/vnd.nokia.radio-presets": {
          source: "iana",
          extensions: ["rpss"]
        },
        "application/vnd.novadigm.edm": {
          source: "iana",
          extensions: ["edm"]
        },
        "application/vnd.novadigm.edx": {
          source: "iana",
          extensions: ["edx"]
        },
        "application/vnd.novadigm.ext": {
          source: "iana",
          extensions: ["ext"]
        },
        "application/vnd.ntt-local.content-share": {
          source: "iana"
        },
        "application/vnd.ntt-local.file-transfer": {
          source: "iana"
        },
        "application/vnd.ntt-local.ogw_remote-access": {
          source: "iana"
        },
        "application/vnd.ntt-local.sip-ta_remote": {
          source: "iana"
        },
        "application/vnd.ntt-local.sip-ta_tcp_stream": {
          source: "iana"
        },
        "application/vnd.oasis.opendocument.chart": {
          source: "iana",
          extensions: ["odc"]
        },
        "application/vnd.oasis.opendocument.chart-template": {
          source: "iana",
          extensions: ["otc"]
        },
        "application/vnd.oasis.opendocument.database": {
          source: "iana",
          extensions: ["odb"]
        },
        "application/vnd.oasis.opendocument.formula": {
          source: "iana",
          extensions: ["odf"]
        },
        "application/vnd.oasis.opendocument.formula-template": {
          source: "iana",
          extensions: ["odft"]
        },
        "application/vnd.oasis.opendocument.graphics": {
          source: "iana",
          compressible: false,
          extensions: ["odg"]
        },
        "application/vnd.oasis.opendocument.graphics-template": {
          source: "iana",
          extensions: ["otg"]
        },
        "application/vnd.oasis.opendocument.image": {
          source: "iana",
          extensions: ["odi"]
        },
        "application/vnd.oasis.opendocument.image-template": {
          source: "iana",
          extensions: ["oti"]
        },
        "application/vnd.oasis.opendocument.presentation": {
          source: "iana",
          compressible: false,
          extensions: ["odp"]
        },
        "application/vnd.oasis.opendocument.presentation-template": {
          source: "iana",
          extensions: ["otp"]
        },
        "application/vnd.oasis.opendocument.spreadsheet": {
          source: "iana",
          compressible: false,
          extensions: ["ods"]
        },
        "application/vnd.oasis.opendocument.spreadsheet-template": {
          source: "iana",
          extensions: ["ots"]
        },
        "application/vnd.oasis.opendocument.text": {
          source: "iana",
          compressible: false,
          extensions: ["odt"]
        },
        "application/vnd.oasis.opendocument.text-master": {
          source: "iana",
          extensions: ["odm"]
        },
        "application/vnd.oasis.opendocument.text-template": {
          source: "iana",
          extensions: ["ott"]
        },
        "application/vnd.oasis.opendocument.text-web": {
          source: "iana",
          extensions: ["oth"]
        },
        "application/vnd.obn": {
          source: "iana"
        },
        "application/vnd.ocf+cbor": {
          source: "iana"
        },
        "application/vnd.oci.image.manifest.v1+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oftn.l10n+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.contentaccessdownload+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.contentaccessstreaming+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.cspg-hexbinary": {
          source: "iana"
        },
        "application/vnd.oipf.dae.svg+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.dae.xhtml+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.mippvcontrolmessage+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.pae.gem": {
          source: "iana"
        },
        "application/vnd.oipf.spdiscovery+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.spdlist+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.ueprofile+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oipf.userprofile+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.olpc-sugar": {
          source: "iana",
          extensions: ["xo"]
        },
        "application/vnd.oma-scws-config": {
          source: "iana"
        },
        "application/vnd.oma-scws-http-request": {
          source: "iana"
        },
        "application/vnd.oma-scws-http-response": {
          source: "iana"
        },
        "application/vnd.oma.bcast.associated-procedure-parameter+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.bcast.drm-trigger+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.bcast.imd+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.bcast.ltkm": {
          source: "iana"
        },
        "application/vnd.oma.bcast.notification+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.bcast.provisioningtrigger": {
          source: "iana"
        },
        "application/vnd.oma.bcast.sgboot": {
          source: "iana"
        },
        "application/vnd.oma.bcast.sgdd+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.bcast.sgdu": {
          source: "iana"
        },
        "application/vnd.oma.bcast.simple-symbol-container": {
          source: "iana"
        },
        "application/vnd.oma.bcast.smartcard-trigger+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.bcast.sprov+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.bcast.stkm": {
          source: "iana"
        },
        "application/vnd.oma.cab-address-book+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.cab-feature-handler+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.cab-pcc+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.cab-subs-invite+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.cab-user-prefs+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.dcd": {
          source: "iana"
        },
        "application/vnd.oma.dcdc": {
          source: "iana"
        },
        "application/vnd.oma.dd2+xml": {
          source: "iana",
          compressible: true,
          extensions: ["dd2"]
        },
        "application/vnd.oma.drm.risd+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.group-usage-list+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.lwm2m+cbor": {
          source: "iana"
        },
        "application/vnd.oma.lwm2m+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.lwm2m+tlv": {
          source: "iana"
        },
        "application/vnd.oma.pal+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.poc.detailed-progress-report+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.poc.final-report+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.poc.groups+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.poc.invocation-descriptor+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.poc.optimized-progress-report+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.push": {
          source: "iana"
        },
        "application/vnd.oma.scidm.messages+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oma.xcap-directory+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.omads-email+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/vnd.omads-file+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/vnd.omads-folder+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/vnd.omaloc-supl-init": {
          source: "iana"
        },
        "application/vnd.onepager": {
          source: "iana"
        },
        "application/vnd.onepagertamp": {
          source: "iana"
        },
        "application/vnd.onepagertamx": {
          source: "iana"
        },
        "application/vnd.onepagertat": {
          source: "iana"
        },
        "application/vnd.onepagertatp": {
          source: "iana"
        },
        "application/vnd.onepagertatx": {
          source: "iana"
        },
        "application/vnd.openblox.game+xml": {
          source: "iana",
          compressible: true,
          extensions: ["obgx"]
        },
        "application/vnd.openblox.game-binary": {
          source: "iana"
        },
        "application/vnd.openeye.oeb": {
          source: "iana"
        },
        "application/vnd.openofficeorg.extension": {
          source: "apache",
          extensions: ["oxt"]
        },
        "application/vnd.openstreetmap.data+xml": {
          source: "iana",
          compressible: true,
          extensions: ["osm"]
        },
        "application/vnd.opentimestamps.ots": {
          source: "iana"
        },
        "application/vnd.openxmlformats-officedocument.custom-properties+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.customxmlproperties+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.drawing+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.drawingml.chart+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.drawingml.chartshapes+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.drawingml.diagramcolors+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.drawingml.diagramdata+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.drawingml.diagramlayout+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.drawingml.diagramstyle+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.extended-properties+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.commentauthors+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.comments+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.handoutmaster+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.notesmaster+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.notesslide+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.presentation": {
          source: "iana",
          compressible: false,
          extensions: ["pptx"]
        },
        "application/vnd.openxmlformats-officedocument.presentationml.presentation.main+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.presprops+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.slide": {
          source: "iana",
          extensions: ["sldx"]
        },
        "application/vnd.openxmlformats-officedocument.presentationml.slide+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.slidelayout+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.slidemaster+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.slideshow": {
          source: "iana",
          extensions: ["ppsx"]
        },
        "application/vnd.openxmlformats-officedocument.presentationml.slideshow.main+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.slideupdateinfo+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.tablestyles+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.tags+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.template": {
          source: "iana",
          extensions: ["potx"]
        },
        "application/vnd.openxmlformats-officedocument.presentationml.template.main+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.presentationml.viewprops+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.calcchain+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.chartsheet+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.comments+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.connections+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.dialogsheet+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.externallink+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.pivotcachedefinition+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.pivotcacherecords+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.pivottable+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.querytable+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.revisionheaders+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.revisionlog+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedstrings+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": {
          source: "iana",
          compressible: false,
          extensions: ["xlsx"]
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheetmetadata+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.table+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.tablesinglecells+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.template": {
          source: "iana",
          extensions: ["xltx"]
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.template.main+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.usernames+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.volatiledependencies+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.theme+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.themeoverride+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.vmldrawing": {
          source: "iana"
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.comments+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document": {
          source: "iana",
          compressible: false,
          extensions: ["docx"]
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document.glossary+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.endnotes+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.fonttable+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.footer+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.footnotes+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.template": {
          source: "iana",
          extensions: ["dotx"]
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.template.main+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-officedocument.wordprocessingml.websettings+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-package.core-properties+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-package.digital-signature-xmlsignature+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.openxmlformats-package.relationships+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oracle.resource+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.orange.indata": {
          source: "iana"
        },
        "application/vnd.osa.netdeploy": {
          source: "iana"
        },
        "application/vnd.osgeo.mapguide.package": {
          source: "iana",
          extensions: ["mgp"]
        },
        "application/vnd.osgi.bundle": {
          source: "iana"
        },
        "application/vnd.osgi.dp": {
          source: "iana",
          extensions: ["dp"]
        },
        "application/vnd.osgi.subsystem": {
          source: "iana",
          extensions: ["esa"]
        },
        "application/vnd.otps.ct-kip+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.oxli.countgraph": {
          source: "iana"
        },
        "application/vnd.pagerduty+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.palm": {
          source: "iana",
          extensions: ["pdb", "pqa", "oprc"]
        },
        "application/vnd.panoply": {
          source: "iana"
        },
        "application/vnd.paos.xml": {
          source: "iana"
        },
        "application/vnd.patentdive": {
          source: "iana"
        },
        "application/vnd.patientecommsdoc": {
          source: "iana"
        },
        "application/vnd.pawaafile": {
          source: "iana",
          extensions: ["paw"]
        },
        "application/vnd.pcos": {
          source: "iana"
        },
        "application/vnd.pg.format": {
          source: "iana",
          extensions: ["str"]
        },
        "application/vnd.pg.osasli": {
          source: "iana",
          extensions: ["ei6"]
        },
        "application/vnd.piaccess.application-licence": {
          source: "iana"
        },
        "application/vnd.picsel": {
          source: "iana",
          extensions: ["efif"]
        },
        "application/vnd.pmi.widget": {
          source: "iana",
          extensions: ["wg"]
        },
        "application/vnd.poc.group-advertisement+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.pocketlearn": {
          source: "iana",
          extensions: ["plf"]
        },
        "application/vnd.powerbuilder6": {
          source: "iana",
          extensions: ["pbd"]
        },
        "application/vnd.powerbuilder6-s": {
          source: "iana"
        },
        "application/vnd.powerbuilder7": {
          source: "iana"
        },
        "application/vnd.powerbuilder7-s": {
          source: "iana"
        },
        "application/vnd.powerbuilder75": {
          source: "iana"
        },
        "application/vnd.powerbuilder75-s": {
          source: "iana"
        },
        "application/vnd.preminet": {
          source: "iana"
        },
        "application/vnd.previewsystems.box": {
          source: "iana",
          extensions: ["box"]
        },
        "application/vnd.proteus.magazine": {
          source: "iana",
          extensions: ["mgz"]
        },
        "application/vnd.psfs": {
          source: "iana"
        },
        "application/vnd.publishare-delta-tree": {
          source: "iana",
          extensions: ["qps"]
        },
        "application/vnd.pvi.ptid1": {
          source: "iana",
          extensions: ["ptid"]
        },
        "application/vnd.pwg-multiplexed": {
          source: "iana"
        },
        "application/vnd.pwg-xhtml-print+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.qualcomm.brew-app-res": {
          source: "iana"
        },
        "application/vnd.quarantainenet": {
          source: "iana"
        },
        "application/vnd.quark.quarkxpress": {
          source: "iana",
          extensions: ["qxd", "qxt", "qwd", "qwt", "qxl", "qxb"]
        },
        "application/vnd.quobject-quoxdocument": {
          source: "iana"
        },
        "application/vnd.radisys.moml+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-audit+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-audit-conf+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-audit-conn+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-audit-dialog+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-audit-stream+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-conf+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-dialog+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-dialog-base+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-dialog-fax-detect+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-dialog-fax-sendrecv+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-dialog-group+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-dialog-speech+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.radisys.msml-dialog-transform+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.rainstor.data": {
          source: "iana"
        },
        "application/vnd.rapid": {
          source: "iana"
        },
        "application/vnd.rar": {
          source: "iana",
          extensions: ["rar"]
        },
        "application/vnd.realvnc.bed": {
          source: "iana",
          extensions: ["bed"]
        },
        "application/vnd.recordare.musicxml": {
          source: "iana",
          extensions: ["mxl"]
        },
        "application/vnd.recordare.musicxml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["musicxml"]
        },
        "application/vnd.renlearn.rlprint": {
          source: "iana"
        },
        "application/vnd.resilient.logic": {
          source: "iana"
        },
        "application/vnd.restful+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.rig.cryptonote": {
          source: "iana",
          extensions: ["cryptonote"]
        },
        "application/vnd.rim.cod": {
          source: "apache",
          extensions: ["cod"]
        },
        "application/vnd.rn-realmedia": {
          source: "apache",
          extensions: ["rm"]
        },
        "application/vnd.rn-realmedia-vbr": {
          source: "apache",
          extensions: ["rmvb"]
        },
        "application/vnd.route66.link66+xml": {
          source: "iana",
          compressible: true,
          extensions: ["link66"]
        },
        "application/vnd.rs-274x": {
          source: "iana"
        },
        "application/vnd.ruckus.download": {
          source: "iana"
        },
        "application/vnd.s3sms": {
          source: "iana"
        },
        "application/vnd.sailingtracker.track": {
          source: "iana",
          extensions: ["st"]
        },
        "application/vnd.sar": {
          source: "iana"
        },
        "application/vnd.sbm.cid": {
          source: "iana"
        },
        "application/vnd.sbm.mid2": {
          source: "iana"
        },
        "application/vnd.scribus": {
          source: "iana"
        },
        "application/vnd.sealed.3df": {
          source: "iana"
        },
        "application/vnd.sealed.csf": {
          source: "iana"
        },
        "application/vnd.sealed.doc": {
          source: "iana"
        },
        "application/vnd.sealed.eml": {
          source: "iana"
        },
        "application/vnd.sealed.mht": {
          source: "iana"
        },
        "application/vnd.sealed.net": {
          source: "iana"
        },
        "application/vnd.sealed.ppt": {
          source: "iana"
        },
        "application/vnd.sealed.tiff": {
          source: "iana"
        },
        "application/vnd.sealed.xls": {
          source: "iana"
        },
        "application/vnd.sealedmedia.softseal.html": {
          source: "iana"
        },
        "application/vnd.sealedmedia.softseal.pdf": {
          source: "iana"
        },
        "application/vnd.seemail": {
          source: "iana",
          extensions: ["see"]
        },
        "application/vnd.seis+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.sema": {
          source: "iana",
          extensions: ["sema"]
        },
        "application/vnd.semd": {
          source: "iana",
          extensions: ["semd"]
        },
        "application/vnd.semf": {
          source: "iana",
          extensions: ["semf"]
        },
        "application/vnd.shade-save-file": {
          source: "iana"
        },
        "application/vnd.shana.informed.formdata": {
          source: "iana",
          extensions: ["ifm"]
        },
        "application/vnd.shana.informed.formtemplate": {
          source: "iana",
          extensions: ["itp"]
        },
        "application/vnd.shana.informed.interchange": {
          source: "iana",
          extensions: ["iif"]
        },
        "application/vnd.shana.informed.package": {
          source: "iana",
          extensions: ["ipk"]
        },
        "application/vnd.shootproof+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.shopkick+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.shp": {
          source: "iana"
        },
        "application/vnd.shx": {
          source: "iana"
        },
        "application/vnd.sigrok.session": {
          source: "iana"
        },
        "application/vnd.simtech-mindmapper": {
          source: "iana",
          extensions: ["twd", "twds"]
        },
        "application/vnd.siren+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.smaf": {
          source: "iana",
          extensions: ["mmf"]
        },
        "application/vnd.smart.notebook": {
          source: "iana"
        },
        "application/vnd.smart.teacher": {
          source: "iana",
          extensions: ["teacher"]
        },
        "application/vnd.snesdev-page-table": {
          source: "iana"
        },
        "application/vnd.software602.filler.form+xml": {
          source: "iana",
          compressible: true,
          extensions: ["fo"]
        },
        "application/vnd.software602.filler.form-xml-zip": {
          source: "iana"
        },
        "application/vnd.solent.sdkm+xml": {
          source: "iana",
          compressible: true,
          extensions: ["sdkm", "sdkd"]
        },
        "application/vnd.spotfire.dxp": {
          source: "iana",
          extensions: ["dxp"]
        },
        "application/vnd.spotfire.sfs": {
          source: "iana",
          extensions: ["sfs"]
        },
        "application/vnd.sqlite3": {
          source: "iana"
        },
        "application/vnd.sss-cod": {
          source: "iana"
        },
        "application/vnd.sss-dtf": {
          source: "iana"
        },
        "application/vnd.sss-ntf": {
          source: "iana"
        },
        "application/vnd.stardivision.calc": {
          source: "apache",
          extensions: ["sdc"]
        },
        "application/vnd.stardivision.draw": {
          source: "apache",
          extensions: ["sda"]
        },
        "application/vnd.stardivision.impress": {
          source: "apache",
          extensions: ["sdd"]
        },
        "application/vnd.stardivision.math": {
          source: "apache",
          extensions: ["smf"]
        },
        "application/vnd.stardivision.writer": {
          source: "apache",
          extensions: ["sdw", "vor"]
        },
        "application/vnd.stardivision.writer-global": {
          source: "apache",
          extensions: ["sgl"]
        },
        "application/vnd.stepmania.package": {
          source: "iana",
          extensions: ["smzip"]
        },
        "application/vnd.stepmania.stepchart": {
          source: "iana",
          extensions: ["sm"]
        },
        "application/vnd.street-stream": {
          source: "iana"
        },
        "application/vnd.sun.wadl+xml": {
          source: "iana",
          compressible: true,
          extensions: ["wadl"]
        },
        "application/vnd.sun.xml.calc": {
          source: "apache",
          extensions: ["sxc"]
        },
        "application/vnd.sun.xml.calc.template": {
          source: "apache",
          extensions: ["stc"]
        },
        "application/vnd.sun.xml.draw": {
          source: "apache",
          extensions: ["sxd"]
        },
        "application/vnd.sun.xml.draw.template": {
          source: "apache",
          extensions: ["std"]
        },
        "application/vnd.sun.xml.impress": {
          source: "apache",
          extensions: ["sxi"]
        },
        "application/vnd.sun.xml.impress.template": {
          source: "apache",
          extensions: ["sti"]
        },
        "application/vnd.sun.xml.math": {
          source: "apache",
          extensions: ["sxm"]
        },
        "application/vnd.sun.xml.writer": {
          source: "apache",
          extensions: ["sxw"]
        },
        "application/vnd.sun.xml.writer.global": {
          source: "apache",
          extensions: ["sxg"]
        },
        "application/vnd.sun.xml.writer.template": {
          source: "apache",
          extensions: ["stw"]
        },
        "application/vnd.sus-calendar": {
          source: "iana",
          extensions: ["sus", "susp"]
        },
        "application/vnd.svd": {
          source: "iana",
          extensions: ["svd"]
        },
        "application/vnd.swiftview-ics": {
          source: "iana"
        },
        "application/vnd.sycle+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.syft+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.symbian.install": {
          source: "apache",
          extensions: ["sis", "sisx"]
        },
        "application/vnd.syncml+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["xsm"]
        },
        "application/vnd.syncml.dm+wbxml": {
          source: "iana",
          charset: "UTF-8",
          extensions: ["bdm"]
        },
        "application/vnd.syncml.dm+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["xdm"]
        },
        "application/vnd.syncml.dm.notification": {
          source: "iana"
        },
        "application/vnd.syncml.dmddf+wbxml": {
          source: "iana"
        },
        "application/vnd.syncml.dmddf+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["ddf"]
        },
        "application/vnd.syncml.dmtnds+wbxml": {
          source: "iana"
        },
        "application/vnd.syncml.dmtnds+xml": {
          source: "iana",
          charset: "UTF-8",
          compressible: true
        },
        "application/vnd.syncml.ds.notification": {
          source: "iana"
        },
        "application/vnd.tableschema+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.tao.intent-module-archive": {
          source: "iana",
          extensions: ["tao"]
        },
        "application/vnd.tcpdump.pcap": {
          source: "iana",
          extensions: ["pcap", "cap", "dmp"]
        },
        "application/vnd.think-cell.ppttc+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.tmd.mediaflex.api+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.tml": {
          source: "iana"
        },
        "application/vnd.tmobile-livetv": {
          source: "iana",
          extensions: ["tmo"]
        },
        "application/vnd.tri.onesource": {
          source: "iana"
        },
        "application/vnd.trid.tpt": {
          source: "iana",
          extensions: ["tpt"]
        },
        "application/vnd.triscape.mxs": {
          source: "iana",
          extensions: ["mxs"]
        },
        "application/vnd.trueapp": {
          source: "iana",
          extensions: ["tra"]
        },
        "application/vnd.truedoc": {
          source: "iana"
        },
        "application/vnd.ubisoft.webplayer": {
          source: "iana"
        },
        "application/vnd.ufdl": {
          source: "iana",
          extensions: ["ufd", "ufdl"]
        },
        "application/vnd.uiq.theme": {
          source: "iana",
          extensions: ["utz"]
        },
        "application/vnd.umajin": {
          source: "iana",
          extensions: ["umj"]
        },
        "application/vnd.unity": {
          source: "iana",
          extensions: ["unityweb"]
        },
        "application/vnd.uoml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["uoml"]
        },
        "application/vnd.uplanet.alert": {
          source: "iana"
        },
        "application/vnd.uplanet.alert-wbxml": {
          source: "iana"
        },
        "application/vnd.uplanet.bearer-choice": {
          source: "iana"
        },
        "application/vnd.uplanet.bearer-choice-wbxml": {
          source: "iana"
        },
        "application/vnd.uplanet.cacheop": {
          source: "iana"
        },
        "application/vnd.uplanet.cacheop-wbxml": {
          source: "iana"
        },
        "application/vnd.uplanet.channel": {
          source: "iana"
        },
        "application/vnd.uplanet.channel-wbxml": {
          source: "iana"
        },
        "application/vnd.uplanet.list": {
          source: "iana"
        },
        "application/vnd.uplanet.list-wbxml": {
          source: "iana"
        },
        "application/vnd.uplanet.listcmd": {
          source: "iana"
        },
        "application/vnd.uplanet.listcmd-wbxml": {
          source: "iana"
        },
        "application/vnd.uplanet.signal": {
          source: "iana"
        },
        "application/vnd.uri-map": {
          source: "iana"
        },
        "application/vnd.valve.source.material": {
          source: "iana"
        },
        "application/vnd.vcx": {
          source: "iana",
          extensions: ["vcx"]
        },
        "application/vnd.vd-study": {
          source: "iana"
        },
        "application/vnd.vectorworks": {
          source: "iana"
        },
        "application/vnd.vel+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.verimatrix.vcas": {
          source: "iana"
        },
        "application/vnd.veritone.aion+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.veryant.thin": {
          source: "iana"
        },
        "application/vnd.ves.encrypted": {
          source: "iana"
        },
        "application/vnd.vidsoft.vidconference": {
          source: "iana"
        },
        "application/vnd.visio": {
          source: "iana",
          extensions: ["vsd", "vst", "vss", "vsw"]
        },
        "application/vnd.visionary": {
          source: "iana",
          extensions: ["vis"]
        },
        "application/vnd.vividence.scriptfile": {
          source: "iana"
        },
        "application/vnd.vsf": {
          source: "iana",
          extensions: ["vsf"]
        },
        "application/vnd.wap.sic": {
          source: "iana"
        },
        "application/vnd.wap.slc": {
          source: "iana"
        },
        "application/vnd.wap.wbxml": {
          source: "iana",
          charset: "UTF-8",
          extensions: ["wbxml"]
        },
        "application/vnd.wap.wmlc": {
          source: "iana",
          extensions: ["wmlc"]
        },
        "application/vnd.wap.wmlscriptc": {
          source: "iana",
          extensions: ["wmlsc"]
        },
        "application/vnd.webturbo": {
          source: "iana",
          extensions: ["wtb"]
        },
        "application/vnd.wfa.dpp": {
          source: "iana"
        },
        "application/vnd.wfa.p2p": {
          source: "iana"
        },
        "application/vnd.wfa.wsc": {
          source: "iana"
        },
        "application/vnd.windows.devicepairing": {
          source: "iana"
        },
        "application/vnd.wmc": {
          source: "iana"
        },
        "application/vnd.wmf.bootstrap": {
          source: "iana"
        },
        "application/vnd.wolfram.mathematica": {
          source: "iana"
        },
        "application/vnd.wolfram.mathematica.package": {
          source: "iana"
        },
        "application/vnd.wolfram.player": {
          source: "iana",
          extensions: ["nbp"]
        },
        "application/vnd.wordperfect": {
          source: "iana",
          extensions: ["wpd"]
        },
        "application/vnd.wqd": {
          source: "iana",
          extensions: ["wqd"]
        },
        "application/vnd.wrq-hp3000-labelled": {
          source: "iana"
        },
        "application/vnd.wt.stf": {
          source: "iana",
          extensions: ["stf"]
        },
        "application/vnd.wv.csp+wbxml": {
          source: "iana"
        },
        "application/vnd.wv.csp+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.wv.ssp+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.xacml+json": {
          source: "iana",
          compressible: true
        },
        "application/vnd.xara": {
          source: "iana",
          extensions: ["xar"]
        },
        "application/vnd.xfdl": {
          source: "iana",
          extensions: ["xfdl"]
        },
        "application/vnd.xfdl.webform": {
          source: "iana"
        },
        "application/vnd.xmi+xml": {
          source: "iana",
          compressible: true
        },
        "application/vnd.xmpie.cpkg": {
          source: "iana"
        },
        "application/vnd.xmpie.dpkg": {
          source: "iana"
        },
        "application/vnd.xmpie.plan": {
          source: "iana"
        },
        "application/vnd.xmpie.ppkg": {
          source: "iana"
        },
        "application/vnd.xmpie.xlim": {
          source: "iana"
        },
        "application/vnd.yamaha.hv-dic": {
          source: "iana",
          extensions: ["hvd"]
        },
        "application/vnd.yamaha.hv-script": {
          source: "iana",
          extensions: ["hvs"]
        },
        "application/vnd.yamaha.hv-voice": {
          source: "iana",
          extensions: ["hvp"]
        },
        "application/vnd.yamaha.openscoreformat": {
          source: "iana",
          extensions: ["osf"]
        },
        "application/vnd.yamaha.openscoreformat.osfpvg+xml": {
          source: "iana",
          compressible: true,
          extensions: ["osfpvg"]
        },
        "application/vnd.yamaha.remote-setup": {
          source: "iana"
        },
        "application/vnd.yamaha.smaf-audio": {
          source: "iana",
          extensions: ["saf"]
        },
        "application/vnd.yamaha.smaf-phrase": {
          source: "iana",
          extensions: ["spf"]
        },
        "application/vnd.yamaha.through-ngn": {
          source: "iana"
        },
        "application/vnd.yamaha.tunnel-udpencap": {
          source: "iana"
        },
        "application/vnd.yaoweme": {
          source: "iana"
        },
        "application/vnd.yellowriver-custom-menu": {
          source: "iana",
          extensions: ["cmp"]
        },
        "application/vnd.youtube.yt": {
          source: "iana"
        },
        "application/vnd.zul": {
          source: "iana",
          extensions: ["zir", "zirz"]
        },
        "application/vnd.zzazz.deck+xml": {
          source: "iana",
          compressible: true,
          extensions: ["zaz"]
        },
        "application/voicexml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["vxml"]
        },
        "application/voucher-cms+json": {
          source: "iana",
          compressible: true
        },
        "application/vq-rtcpxr": {
          source: "iana"
        },
        "application/wasm": {
          source: "iana",
          compressible: true,
          extensions: ["wasm"]
        },
        "application/watcherinfo+xml": {
          source: "iana",
          compressible: true,
          extensions: ["wif"]
        },
        "application/webpush-options+json": {
          source: "iana",
          compressible: true
        },
        "application/whoispp-query": {
          source: "iana"
        },
        "application/whoispp-response": {
          source: "iana"
        },
        "application/widget": {
          source: "iana",
          extensions: ["wgt"]
        },
        "application/winhlp": {
          source: "apache",
          extensions: ["hlp"]
        },
        "application/wita": {
          source: "iana"
        },
        "application/wordperfect5.1": {
          source: "iana"
        },
        "application/wsdl+xml": {
          source: "iana",
          compressible: true,
          extensions: ["wsdl"]
        },
        "application/wspolicy+xml": {
          source: "iana",
          compressible: true,
          extensions: ["wspolicy"]
        },
        "application/x-7z-compressed": {
          source: "apache",
          compressible: false,
          extensions: ["7z"]
        },
        "application/x-abiword": {
          source: "apache",
          extensions: ["abw"]
        },
        "application/x-ace-compressed": {
          source: "apache",
          extensions: ["ace"]
        },
        "application/x-amf": {
          source: "apache"
        },
        "application/x-apple-diskimage": {
          source: "apache",
          extensions: ["dmg"]
        },
        "application/x-arj": {
          compressible: false,
          extensions: ["arj"]
        },
        "application/x-authorware-bin": {
          source: "apache",
          extensions: ["aab", "x32", "u32", "vox"]
        },
        "application/x-authorware-map": {
          source: "apache",
          extensions: ["aam"]
        },
        "application/x-authorware-seg": {
          source: "apache",
          extensions: ["aas"]
        },
        "application/x-bcpio": {
          source: "apache",
          extensions: ["bcpio"]
        },
        "application/x-bdoc": {
          compressible: false,
          extensions: ["bdoc"]
        },
        "application/x-bittorrent": {
          source: "apache",
          extensions: ["torrent"]
        },
        "application/x-blorb": {
          source: "apache",
          extensions: ["blb", "blorb"]
        },
        "application/x-bzip": {
          source: "apache",
          compressible: false,
          extensions: ["bz"]
        },
        "application/x-bzip2": {
          source: "apache",
          compressible: false,
          extensions: ["bz2", "boz"]
        },
        "application/x-cbr": {
          source: "apache",
          extensions: ["cbr", "cba", "cbt", "cbz", "cb7"]
        },
        "application/x-cdlink": {
          source: "apache",
          extensions: ["vcd"]
        },
        "application/x-cfs-compressed": {
          source: "apache",
          extensions: ["cfs"]
        },
        "application/x-chat": {
          source: "apache",
          extensions: ["chat"]
        },
        "application/x-chess-pgn": {
          source: "apache",
          extensions: ["pgn"]
        },
        "application/x-chrome-extension": {
          extensions: ["crx"]
        },
        "application/x-cocoa": {
          source: "nginx",
          extensions: ["cco"]
        },
        "application/x-compress": {
          source: "apache"
        },
        "application/x-conference": {
          source: "apache",
          extensions: ["nsc"]
        },
        "application/x-cpio": {
          source: "apache",
          extensions: ["cpio"]
        },
        "application/x-csh": {
          source: "apache",
          extensions: ["csh"]
        },
        "application/x-deb": {
          compressible: false
        },
        "application/x-debian-package": {
          source: "apache",
          extensions: ["deb", "udeb"]
        },
        "application/x-dgc-compressed": {
          source: "apache",
          extensions: ["dgc"]
        },
        "application/x-director": {
          source: "apache",
          extensions: ["dir", "dcr", "dxr", "cst", "cct", "cxt", "w3d", "fgd", "swa"]
        },
        "application/x-doom": {
          source: "apache",
          extensions: ["wad"]
        },
        "application/x-dtbncx+xml": {
          source: "apache",
          compressible: true,
          extensions: ["ncx"]
        },
        "application/x-dtbook+xml": {
          source: "apache",
          compressible: true,
          extensions: ["dtb"]
        },
        "application/x-dtbresource+xml": {
          source: "apache",
          compressible: true,
          extensions: ["res"]
        },
        "application/x-dvi": {
          source: "apache",
          compressible: false,
          extensions: ["dvi"]
        },
        "application/x-envoy": {
          source: "apache",
          extensions: ["evy"]
        },
        "application/x-eva": {
          source: "apache",
          extensions: ["eva"]
        },
        "application/x-font-bdf": {
          source: "apache",
          extensions: ["bdf"]
        },
        "application/x-font-dos": {
          source: "apache"
        },
        "application/x-font-framemaker": {
          source: "apache"
        },
        "application/x-font-ghostscript": {
          source: "apache",
          extensions: ["gsf"]
        },
        "application/x-font-libgrx": {
          source: "apache"
        },
        "application/x-font-linux-psf": {
          source: "apache",
          extensions: ["psf"]
        },
        "application/x-font-pcf": {
          source: "apache",
          extensions: ["pcf"]
        },
        "application/x-font-snf": {
          source: "apache",
          extensions: ["snf"]
        },
        "application/x-font-speedo": {
          source: "apache"
        },
        "application/x-font-sunos-news": {
          source: "apache"
        },
        "application/x-font-type1": {
          source: "apache",
          extensions: ["pfa", "pfb", "pfm", "afm"]
        },
        "application/x-font-vfont": {
          source: "apache"
        },
        "application/x-freearc": {
          source: "apache",
          extensions: ["arc"]
        },
        "application/x-futuresplash": {
          source: "apache",
          extensions: ["spl"]
        },
        "application/x-gca-compressed": {
          source: "apache",
          extensions: ["gca"]
        },
        "application/x-glulx": {
          source: "apache",
          extensions: ["ulx"]
        },
        "application/x-gnumeric": {
          source: "apache",
          extensions: ["gnumeric"]
        },
        "application/x-gramps-xml": {
          source: "apache",
          extensions: ["gramps"]
        },
        "application/x-gtar": {
          source: "apache",
          extensions: ["gtar"]
        },
        "application/x-gzip": {
          source: "apache"
        },
        "application/x-hdf": {
          source: "apache",
          extensions: ["hdf"]
        },
        "application/x-httpd-php": {
          compressible: true,
          extensions: ["php"]
        },
        "application/x-install-instructions": {
          source: "apache",
          extensions: ["install"]
        },
        "application/x-iso9660-image": {
          source: "apache",
          extensions: ["iso"]
        },
        "application/x-iwork-keynote-sffkey": {
          extensions: ["key"]
        },
        "application/x-iwork-numbers-sffnumbers": {
          extensions: ["numbers"]
        },
        "application/x-iwork-pages-sffpages": {
          extensions: ["pages"]
        },
        "application/x-java-archive-diff": {
          source: "nginx",
          extensions: ["jardiff"]
        },
        "application/x-java-jnlp-file": {
          source: "apache",
          compressible: false,
          extensions: ["jnlp"]
        },
        "application/x-javascript": {
          compressible: true
        },
        "application/x-keepass2": {
          extensions: ["kdbx"]
        },
        "application/x-latex": {
          source: "apache",
          compressible: false,
          extensions: ["latex"]
        },
        "application/x-lua-bytecode": {
          extensions: ["luac"]
        },
        "application/x-lzh-compressed": {
          source: "apache",
          extensions: ["lzh", "lha"]
        },
        "application/x-makeself": {
          source: "nginx",
          extensions: ["run"]
        },
        "application/x-mie": {
          source: "apache",
          extensions: ["mie"]
        },
        "application/x-mobipocket-ebook": {
          source: "apache",
          extensions: ["prc", "mobi"]
        },
        "application/x-mpegurl": {
          compressible: false
        },
        "application/x-ms-application": {
          source: "apache",
          extensions: ["application"]
        },
        "application/x-ms-shortcut": {
          source: "apache",
          extensions: ["lnk"]
        },
        "application/x-ms-wmd": {
          source: "apache",
          extensions: ["wmd"]
        },
        "application/x-ms-wmz": {
          source: "apache",
          extensions: ["wmz"]
        },
        "application/x-ms-xbap": {
          source: "apache",
          extensions: ["xbap"]
        },
        "application/x-msaccess": {
          source: "apache",
          extensions: ["mdb"]
        },
        "application/x-msbinder": {
          source: "apache",
          extensions: ["obd"]
        },
        "application/x-mscardfile": {
          source: "apache",
          extensions: ["crd"]
        },
        "application/x-msclip": {
          source: "apache",
          extensions: ["clp"]
        },
        "application/x-msdos-program": {
          extensions: ["exe"]
        },
        "application/x-msdownload": {
          source: "apache",
          extensions: ["exe", "dll", "com", "bat", "msi"]
        },
        "application/x-msmediaview": {
          source: "apache",
          extensions: ["mvb", "m13", "m14"]
        },
        "application/x-msmetafile": {
          source: "apache",
          extensions: ["wmf", "wmz", "emf", "emz"]
        },
        "application/x-msmoney": {
          source: "apache",
          extensions: ["mny"]
        },
        "application/x-mspublisher": {
          source: "apache",
          extensions: ["pub"]
        },
        "application/x-msschedule": {
          source: "apache",
          extensions: ["scd"]
        },
        "application/x-msterminal": {
          source: "apache",
          extensions: ["trm"]
        },
        "application/x-mswrite": {
          source: "apache",
          extensions: ["wri"]
        },
        "application/x-netcdf": {
          source: "apache",
          extensions: ["nc", "cdf"]
        },
        "application/x-ns-proxy-autoconfig": {
          compressible: true,
          extensions: ["pac"]
        },
        "application/x-nzb": {
          source: "apache",
          extensions: ["nzb"]
        },
        "application/x-perl": {
          source: "nginx",
          extensions: ["pl", "pm"]
        },
        "application/x-pilot": {
          source: "nginx",
          extensions: ["prc", "pdb"]
        },
        "application/x-pkcs12": {
          source: "apache",
          compressible: false,
          extensions: ["p12", "pfx"]
        },
        "application/x-pkcs7-certificates": {
          source: "apache",
          extensions: ["p7b", "spc"]
        },
        "application/x-pkcs7-certreqresp": {
          source: "apache",
          extensions: ["p7r"]
        },
        "application/x-pki-message": {
          source: "iana"
        },
        "application/x-rar-compressed": {
          source: "apache",
          compressible: false,
          extensions: ["rar"]
        },
        "application/x-redhat-package-manager": {
          source: "nginx",
          extensions: ["rpm"]
        },
        "application/x-research-info-systems": {
          source: "apache",
          extensions: ["ris"]
        },
        "application/x-sea": {
          source: "nginx",
          extensions: ["sea"]
        },
        "application/x-sh": {
          source: "apache",
          compressible: true,
          extensions: ["sh"]
        },
        "application/x-shar": {
          source: "apache",
          extensions: ["shar"]
        },
        "application/x-shockwave-flash": {
          source: "apache",
          compressible: false,
          extensions: ["swf"]
        },
        "application/x-silverlight-app": {
          source: "apache",
          extensions: ["xap"]
        },
        "application/x-sql": {
          source: "apache",
          extensions: ["sql"]
        },
        "application/x-stuffit": {
          source: "apache",
          compressible: false,
          extensions: ["sit"]
        },
        "application/x-stuffitx": {
          source: "apache",
          extensions: ["sitx"]
        },
        "application/x-subrip": {
          source: "apache",
          extensions: ["srt"]
        },
        "application/x-sv4cpio": {
          source: "apache",
          extensions: ["sv4cpio"]
        },
        "application/x-sv4crc": {
          source: "apache",
          extensions: ["sv4crc"]
        },
        "application/x-t3vm-image": {
          source: "apache",
          extensions: ["t3"]
        },
        "application/x-tads": {
          source: "apache",
          extensions: ["gam"]
        },
        "application/x-tar": {
          source: "apache",
          compressible: true,
          extensions: ["tar"]
        },
        "application/x-tcl": {
          source: "apache",
          extensions: ["tcl", "tk"]
        },
        "application/x-tex": {
          source: "apache",
          extensions: ["tex"]
        },
        "application/x-tex-tfm": {
          source: "apache",
          extensions: ["tfm"]
        },
        "application/x-texinfo": {
          source: "apache",
          extensions: ["texinfo", "texi"]
        },
        "application/x-tgif": {
          source: "apache",
          extensions: ["obj"]
        },
        "application/x-ustar": {
          source: "apache",
          extensions: ["ustar"]
        },
        "application/x-virtualbox-hdd": {
          compressible: true,
          extensions: ["hdd"]
        },
        "application/x-virtualbox-ova": {
          compressible: true,
          extensions: ["ova"]
        },
        "application/x-virtualbox-ovf": {
          compressible: true,
          extensions: ["ovf"]
        },
        "application/x-virtualbox-vbox": {
          compressible: true,
          extensions: ["vbox"]
        },
        "application/x-virtualbox-vbox-extpack": {
          compressible: false,
          extensions: ["vbox-extpack"]
        },
        "application/x-virtualbox-vdi": {
          compressible: true,
          extensions: ["vdi"]
        },
        "application/x-virtualbox-vhd": {
          compressible: true,
          extensions: ["vhd"]
        },
        "application/x-virtualbox-vmdk": {
          compressible: true,
          extensions: ["vmdk"]
        },
        "application/x-wais-source": {
          source: "apache",
          extensions: ["src"]
        },
        "application/x-web-app-manifest+json": {
          compressible: true,
          extensions: ["webapp"]
        },
        "application/x-www-form-urlencoded": {
          source: "iana",
          compressible: true
        },
        "application/x-x509-ca-cert": {
          source: "iana",
          extensions: ["der", "crt", "pem"]
        },
        "application/x-x509-ca-ra-cert": {
          source: "iana"
        },
        "application/x-x509-next-ca-cert": {
          source: "iana"
        },
        "application/x-xfig": {
          source: "apache",
          extensions: ["fig"]
        },
        "application/x-xliff+xml": {
          source: "apache",
          compressible: true,
          extensions: ["xlf"]
        },
        "application/x-xpinstall": {
          source: "apache",
          compressible: false,
          extensions: ["xpi"]
        },
        "application/x-xz": {
          source: "apache",
          extensions: ["xz"]
        },
        "application/x-zmachine": {
          source: "apache",
          extensions: ["z1", "z2", "z3", "z4", "z5", "z6", "z7", "z8"]
        },
        "application/x400-bp": {
          source: "iana"
        },
        "application/xacml+xml": {
          source: "iana",
          compressible: true
        },
        "application/xaml+xml": {
          source: "apache",
          compressible: true,
          extensions: ["xaml"]
        },
        "application/xcap-att+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xav"]
        },
        "application/xcap-caps+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xca"]
        },
        "application/xcap-diff+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xdf"]
        },
        "application/xcap-el+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xel"]
        },
        "application/xcap-error+xml": {
          source: "iana",
          compressible: true
        },
        "application/xcap-ns+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xns"]
        },
        "application/xcon-conference-info+xml": {
          source: "iana",
          compressible: true
        },
        "application/xcon-conference-info-diff+xml": {
          source: "iana",
          compressible: true
        },
        "application/xenc+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xenc"]
        },
        "application/xhtml+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xhtml", "xht"]
        },
        "application/xhtml-voice+xml": {
          source: "apache",
          compressible: true
        },
        "application/xliff+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xlf"]
        },
        "application/xml": {
          source: "iana",
          compressible: true,
          extensions: ["xml", "xsl", "xsd", "rng"]
        },
        "application/xml-dtd": {
          source: "iana",
          compressible: true,
          extensions: ["dtd"]
        },
        "application/xml-external-parsed-entity": {
          source: "iana"
        },
        "application/xml-patch+xml": {
          source: "iana",
          compressible: true
        },
        "application/xmpp+xml": {
          source: "iana",
          compressible: true
        },
        "application/xop+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xop"]
        },
        "application/xproc+xml": {
          source: "apache",
          compressible: true,
          extensions: ["xpl"]
        },
        "application/xslt+xml": {
          source: "iana",
          compressible: true,
          extensions: ["xsl", "xslt"]
        },
        "application/xspf+xml": {
          source: "apache",
          compressible: true,
          extensions: ["xspf"]
        },
        "application/xv+xml": {
          source: "iana",
          compressible: true,
          extensions: ["mxml", "xhvml", "xvml", "xvm"]
        },
        "application/yang": {
          source: "iana",
          extensions: ["yang"]
        },
        "application/yang-data+json": {
          source: "iana",
          compressible: true
        },
        "application/yang-data+xml": {
          source: "iana",
          compressible: true
        },
        "application/yang-patch+json": {
          source: "iana",
          compressible: true
        },
        "application/yang-patch+xml": {
          source: "iana",
          compressible: true
        },
        "application/yin+xml": {
          source: "iana",
          compressible: true,
          extensions: ["yin"]
        },
        "application/zip": {
          source: "iana",
          compressible: false,
          extensions: ["zip"]
        },
        "application/zlib": {
          source: "iana"
        },
        "application/zstd": {
          source: "iana"
        },
        "audio/1d-interleaved-parityfec": {
          source: "iana"
        },
        "audio/32kadpcm": {
          source: "iana"
        },
        "audio/3gpp": {
          source: "iana",
          compressible: false,
          extensions: ["3gpp"]
        },
        "audio/3gpp2": {
          source: "iana"
        },
        "audio/aac": {
          source: "iana"
        },
        "audio/ac3": {
          source: "iana"
        },
        "audio/adpcm": {
          source: "apache",
          extensions: ["adp"]
        },
        "audio/amr": {
          source: "iana",
          extensions: ["amr"]
        },
        "audio/amr-wb": {
          source: "iana"
        },
        "audio/amr-wb+": {
          source: "iana"
        },
        "audio/aptx": {
          source: "iana"
        },
        "audio/asc": {
          source: "iana"
        },
        "audio/atrac-advanced-lossless": {
          source: "iana"
        },
        "audio/atrac-x": {
          source: "iana"
        },
        "audio/atrac3": {
          source: "iana"
        },
        "audio/basic": {
          source: "iana",
          compressible: false,
          extensions: ["au", "snd"]
        },
        "audio/bv16": {
          source: "iana"
        },
        "audio/bv32": {
          source: "iana"
        },
        "audio/clearmode": {
          source: "iana"
        },
        "audio/cn": {
          source: "iana"
        },
        "audio/dat12": {
          source: "iana"
        },
        "audio/dls": {
          source: "iana"
        },
        "audio/dsr-es201108": {
          source: "iana"
        },
        "audio/dsr-es202050": {
          source: "iana"
        },
        "audio/dsr-es202211": {
          source: "iana"
        },
        "audio/dsr-es202212": {
          source: "iana"
        },
        "audio/dv": {
          source: "iana"
        },
        "audio/dvi4": {
          source: "iana"
        },
        "audio/eac3": {
          source: "iana"
        },
        "audio/encaprtp": {
          source: "iana"
        },
        "audio/evrc": {
          source: "iana"
        },
        "audio/evrc-qcp": {
          source: "iana"
        },
        "audio/evrc0": {
          source: "iana"
        },
        "audio/evrc1": {
          source: "iana"
        },
        "audio/evrcb": {
          source: "iana"
        },
        "audio/evrcb0": {
          source: "iana"
        },
        "audio/evrcb1": {
          source: "iana"
        },
        "audio/evrcnw": {
          source: "iana"
        },
        "audio/evrcnw0": {
          source: "iana"
        },
        "audio/evrcnw1": {
          source: "iana"
        },
        "audio/evrcwb": {
          source: "iana"
        },
        "audio/evrcwb0": {
          source: "iana"
        },
        "audio/evrcwb1": {
          source: "iana"
        },
        "audio/evs": {
          source: "iana"
        },
        "audio/flexfec": {
          source: "iana"
        },
        "audio/fwdred": {
          source: "iana"
        },
        "audio/g711-0": {
          source: "iana"
        },
        "audio/g719": {
          source: "iana"
        },
        "audio/g722": {
          source: "iana"
        },
        "audio/g7221": {
          source: "iana"
        },
        "audio/g723": {
          source: "iana"
        },
        "audio/g726-16": {
          source: "iana"
        },
        "audio/g726-24": {
          source: "iana"
        },
        "audio/g726-32": {
          source: "iana"
        },
        "audio/g726-40": {
          source: "iana"
        },
        "audio/g728": {
          source: "iana"
        },
        "audio/g729": {
          source: "iana"
        },
        "audio/g7291": {
          source: "iana"
        },
        "audio/g729d": {
          source: "iana"
        },
        "audio/g729e": {
          source: "iana"
        },
        "audio/gsm": {
          source: "iana"
        },
        "audio/gsm-efr": {
          source: "iana"
        },
        "audio/gsm-hr-08": {
          source: "iana"
        },
        "audio/ilbc": {
          source: "iana"
        },
        "audio/ip-mr_v2.5": {
          source: "iana"
        },
        "audio/isac": {
          source: "apache"
        },
        "audio/l16": {
          source: "iana"
        },
        "audio/l20": {
          source: "iana"
        },
        "audio/l24": {
          source: "iana",
          compressible: false
        },
        "audio/l8": {
          source: "iana"
        },
        "audio/lpc": {
          source: "iana"
        },
        "audio/melp": {
          source: "iana"
        },
        "audio/melp1200": {
          source: "iana"
        },
        "audio/melp2400": {
          source: "iana"
        },
        "audio/melp600": {
          source: "iana"
        },
        "audio/mhas": {
          source: "iana"
        },
        "audio/midi": {
          source: "apache",
          extensions: ["mid", "midi", "kar", "rmi"]
        },
        "audio/mobile-xmf": {
          source: "iana",
          extensions: ["mxmf"]
        },
        "audio/mp3": {
          compressible: false,
          extensions: ["mp3"]
        },
        "audio/mp4": {
          source: "iana",
          compressible: false,
          extensions: ["m4a", "mp4a"]
        },
        "audio/mp4a-latm": {
          source: "iana"
        },
        "audio/mpa": {
          source: "iana"
        },
        "audio/mpa-robust": {
          source: "iana"
        },
        "audio/mpeg": {
          source: "iana",
          compressible: false,
          extensions: ["mpga", "mp2", "mp2a", "mp3", "m2a", "m3a"]
        },
        "audio/mpeg4-generic": {
          source: "iana"
        },
        "audio/musepack": {
          source: "apache"
        },
        "audio/ogg": {
          source: "iana",
          compressible: false,
          extensions: ["oga", "ogg", "spx", "opus"]
        },
        "audio/opus": {
          source: "iana"
        },
        "audio/parityfec": {
          source: "iana"
        },
        "audio/pcma": {
          source: "iana"
        },
        "audio/pcma-wb": {
          source: "iana"
        },
        "audio/pcmu": {
          source: "iana"
        },
        "audio/pcmu-wb": {
          source: "iana"
        },
        "audio/prs.sid": {
          source: "iana"
        },
        "audio/qcelp": {
          source: "iana"
        },
        "audio/raptorfec": {
          source: "iana"
        },
        "audio/red": {
          source: "iana"
        },
        "audio/rtp-enc-aescm128": {
          source: "iana"
        },
        "audio/rtp-midi": {
          source: "iana"
        },
        "audio/rtploopback": {
          source: "iana"
        },
        "audio/rtx": {
          source: "iana"
        },
        "audio/s3m": {
          source: "apache",
          extensions: ["s3m"]
        },
        "audio/scip": {
          source: "iana"
        },
        "audio/silk": {
          source: "apache",
          extensions: ["sil"]
        },
        "audio/smv": {
          source: "iana"
        },
        "audio/smv-qcp": {
          source: "iana"
        },
        "audio/smv0": {
          source: "iana"
        },
        "audio/sofa": {
          source: "iana"
        },
        "audio/sp-midi": {
          source: "iana"
        },
        "audio/speex": {
          source: "iana"
        },
        "audio/t140c": {
          source: "iana"
        },
        "audio/t38": {
          source: "iana"
        },
        "audio/telephone-event": {
          source: "iana"
        },
        "audio/tetra_acelp": {
          source: "iana"
        },
        "audio/tetra_acelp_bb": {
          source: "iana"
        },
        "audio/tone": {
          source: "iana"
        },
        "audio/tsvcis": {
          source: "iana"
        },
        "audio/uemclip": {
          source: "iana"
        },
        "audio/ulpfec": {
          source: "iana"
        },
        "audio/usac": {
          source: "iana"
        },
        "audio/vdvi": {
          source: "iana"
        },
        "audio/vmr-wb": {
          source: "iana"
        },
        "audio/vnd.3gpp.iufp": {
          source: "iana"
        },
        "audio/vnd.4sb": {
          source: "iana"
        },
        "audio/vnd.audiokoz": {
          source: "iana"
        },
        "audio/vnd.celp": {
          source: "iana"
        },
        "audio/vnd.cisco.nse": {
          source: "iana"
        },
        "audio/vnd.cmles.radio-events": {
          source: "iana"
        },
        "audio/vnd.cns.anp1": {
          source: "iana"
        },
        "audio/vnd.cns.inf1": {
          source: "iana"
        },
        "audio/vnd.dece.audio": {
          source: "iana",
          extensions: ["uva", "uvva"]
        },
        "audio/vnd.digital-winds": {
          source: "iana",
          extensions: ["eol"]
        },
        "audio/vnd.dlna.adts": {
          source: "iana"
        },
        "audio/vnd.dolby.heaac.1": {
          source: "iana"
        },
        "audio/vnd.dolby.heaac.2": {
          source: "iana"
        },
        "audio/vnd.dolby.mlp": {
          source: "iana"
        },
        "audio/vnd.dolby.mps": {
          source: "iana"
        },
        "audio/vnd.dolby.pl2": {
          source: "iana"
        },
        "audio/vnd.dolby.pl2x": {
          source: "iana"
        },
        "audio/vnd.dolby.pl2z": {
          source: "iana"
        },
        "audio/vnd.dolby.pulse.1": {
          source: "iana"
        },
        "audio/vnd.dra": {
          source: "iana",
          extensions: ["dra"]
        },
        "audio/vnd.dts": {
          source: "iana",
          extensions: ["dts"]
        },
        "audio/vnd.dts.hd": {
          source: "iana",
          extensions: ["dtshd"]
        },
        "audio/vnd.dts.uhd": {
          source: "iana"
        },
        "audio/vnd.dvb.file": {
          source: "iana"
        },
        "audio/vnd.everad.plj": {
          source: "iana"
        },
        "audio/vnd.hns.audio": {
          source: "iana"
        },
        "audio/vnd.lucent.voice": {
          source: "iana",
          extensions: ["lvp"]
        },
        "audio/vnd.ms-playready.media.pya": {
          source: "iana",
          extensions: ["pya"]
        },
        "audio/vnd.nokia.mobile-xmf": {
          source: "iana"
        },
        "audio/vnd.nortel.vbk": {
          source: "iana"
        },
        "audio/vnd.nuera.ecelp4800": {
          source: "iana",
          extensions: ["ecelp4800"]
        },
        "audio/vnd.nuera.ecelp7470": {
          source: "iana",
          extensions: ["ecelp7470"]
        },
        "audio/vnd.nuera.ecelp9600": {
          source: "iana",
          extensions: ["ecelp9600"]
        },
        "audio/vnd.octel.sbc": {
          source: "iana"
        },
        "audio/vnd.presonus.multitrack": {
          source: "iana"
        },
        "audio/vnd.qcelp": {
          source: "iana"
        },
        "audio/vnd.rhetorex.32kadpcm": {
          source: "iana"
        },
        "audio/vnd.rip": {
          source: "iana",
          extensions: ["rip"]
        },
        "audio/vnd.rn-realaudio": {
          compressible: false
        },
        "audio/vnd.sealedmedia.softseal.mpeg": {
          source: "iana"
        },
        "audio/vnd.vmx.cvsd": {
          source: "iana"
        },
        "audio/vnd.wave": {
          compressible: false
        },
        "audio/vorbis": {
          source: "iana",
          compressible: false
        },
        "audio/vorbis-config": {
          source: "iana"
        },
        "audio/wav": {
          compressible: false,
          extensions: ["wav"]
        },
        "audio/wave": {
          compressible: false,
          extensions: ["wav"]
        },
        "audio/webm": {
          source: "apache",
          compressible: false,
          extensions: ["weba"]
        },
        "audio/x-aac": {
          source: "apache",
          compressible: false,
          extensions: ["aac"]
        },
        "audio/x-aiff": {
          source: "apache",
          extensions: ["aif", "aiff", "aifc"]
        },
        "audio/x-caf": {
          source: "apache",
          compressible: false,
          extensions: ["caf"]
        },
        "audio/x-flac": {
          source: "apache",
          extensions: ["flac"]
        },
        "audio/x-m4a": {
          source: "nginx",
          extensions: ["m4a"]
        },
        "audio/x-matroska": {
          source: "apache",
          extensions: ["mka"]
        },
        "audio/x-mpegurl": {
          source: "apache",
          extensions: ["m3u"]
        },
        "audio/x-ms-wax": {
          source: "apache",
          extensions: ["wax"]
        },
        "audio/x-ms-wma": {
          source: "apache",
          extensions: ["wma"]
        },
        "audio/x-pn-realaudio": {
          source: "apache",
          extensions: ["ram", "ra"]
        },
        "audio/x-pn-realaudio-plugin": {
          source: "apache",
          extensions: ["rmp"]
        },
        "audio/x-realaudio": {
          source: "nginx",
          extensions: ["ra"]
        },
        "audio/x-tta": {
          source: "apache"
        },
        "audio/x-wav": {
          source: "apache",
          extensions: ["wav"]
        },
        "audio/xm": {
          source: "apache",
          extensions: ["xm"]
        },
        "chemical/x-cdx": {
          source: "apache",
          extensions: ["cdx"]
        },
        "chemical/x-cif": {
          source: "apache",
          extensions: ["cif"]
        },
        "chemical/x-cmdf": {
          source: "apache",
          extensions: ["cmdf"]
        },
        "chemical/x-cml": {
          source: "apache",
          extensions: ["cml"]
        },
        "chemical/x-csml": {
          source: "apache",
          extensions: ["csml"]
        },
        "chemical/x-pdb": {
          source: "apache"
        },
        "chemical/x-xyz": {
          source: "apache",
          extensions: ["xyz"]
        },
        "font/collection": {
          source: "iana",
          extensions: ["ttc"]
        },
        "font/otf": {
          source: "iana",
          compressible: true,
          extensions: ["otf"]
        },
        "font/sfnt": {
          source: "iana"
        },
        "font/ttf": {
          source: "iana",
          compressible: true,
          extensions: ["ttf"]
        },
        "font/woff": {
          source: "iana",
          extensions: ["woff"]
        },
        "font/woff2": {
          source: "iana",
          extensions: ["woff2"]
        },
        "image/aces": {
          source: "iana",
          extensions: ["exr"]
        },
        "image/apng": {
          compressible: false,
          extensions: ["apng"]
        },
        "image/avci": {
          source: "iana",
          extensions: ["avci"]
        },
        "image/avcs": {
          source: "iana",
          extensions: ["avcs"]
        },
        "image/avif": {
          source: "iana",
          compressible: false,
          extensions: ["avif"]
        },
        "image/bmp": {
          source: "iana",
          compressible: true,
          extensions: ["bmp"]
        },
        "image/cgm": {
          source: "iana",
          extensions: ["cgm"]
        },
        "image/dicom-rle": {
          source: "iana",
          extensions: ["drle"]
        },
        "image/emf": {
          source: "iana",
          extensions: ["emf"]
        },
        "image/fits": {
          source: "iana",
          extensions: ["fits"]
        },
        "image/g3fax": {
          source: "iana",
          extensions: ["g3"]
        },
        "image/gif": {
          source: "iana",
          compressible: false,
          extensions: ["gif"]
        },
        "image/heic": {
          source: "iana",
          extensions: ["heic"]
        },
        "image/heic-sequence": {
          source: "iana",
          extensions: ["heics"]
        },
        "image/heif": {
          source: "iana",
          extensions: ["heif"]
        },
        "image/heif-sequence": {
          source: "iana",
          extensions: ["heifs"]
        },
        "image/hej2k": {
          source: "iana",
          extensions: ["hej2"]
        },
        "image/hsj2": {
          source: "iana",
          extensions: ["hsj2"]
        },
        "image/ief": {
          source: "iana",
          extensions: ["ief"]
        },
        "image/jls": {
          source: "iana",
          extensions: ["jls"]
        },
        "image/jp2": {
          source: "iana",
          compressible: false,
          extensions: ["jp2", "jpg2"]
        },
        "image/jpeg": {
          source: "iana",
          compressible: false,
          extensions: ["jpeg", "jpg", "jpe"]
        },
        "image/jph": {
          source: "iana",
          extensions: ["jph"]
        },
        "image/jphc": {
          source: "iana",
          extensions: ["jhc"]
        },
        "image/jpm": {
          source: "iana",
          compressible: false,
          extensions: ["jpm"]
        },
        "image/jpx": {
          source: "iana",
          compressible: false,
          extensions: ["jpx", "jpf"]
        },
        "image/jxr": {
          source: "iana",
          extensions: ["jxr"]
        },
        "image/jxra": {
          source: "iana",
          extensions: ["jxra"]
        },
        "image/jxrs": {
          source: "iana",
          extensions: ["jxrs"]
        },
        "image/jxs": {
          source: "iana",
          extensions: ["jxs"]
        },
        "image/jxsc": {
          source: "iana",
          extensions: ["jxsc"]
        },
        "image/jxsi": {
          source: "iana",
          extensions: ["jxsi"]
        },
        "image/jxss": {
          source: "iana",
          extensions: ["jxss"]
        },
        "image/ktx": {
          source: "iana",
          extensions: ["ktx"]
        },
        "image/ktx2": {
          source: "iana",
          extensions: ["ktx2"]
        },
        "image/naplps": {
          source: "iana"
        },
        "image/pjpeg": {
          compressible: false
        },
        "image/png": {
          source: "iana",
          compressible: false,
          extensions: ["png"]
        },
        "image/prs.btif": {
          source: "iana",
          extensions: ["btif"]
        },
        "image/prs.pti": {
          source: "iana",
          extensions: ["pti"]
        },
        "image/pwg-raster": {
          source: "iana"
        },
        "image/sgi": {
          source: "apache",
          extensions: ["sgi"]
        },
        "image/svg+xml": {
          source: "iana",
          compressible: true,
          extensions: ["svg", "svgz"]
        },
        "image/t38": {
          source: "iana",
          extensions: ["t38"]
        },
        "image/tiff": {
          source: "iana",
          compressible: false,
          extensions: ["tif", "tiff"]
        },
        "image/tiff-fx": {
          source: "iana",
          extensions: ["tfx"]
        },
        "image/vnd.adobe.photoshop": {
          source: "iana",
          compressible: true,
          extensions: ["psd"]
        },
        "image/vnd.airzip.accelerator.azv": {
          source: "iana",
          extensions: ["azv"]
        },
        "image/vnd.cns.inf2": {
          source: "iana"
        },
        "image/vnd.dece.graphic": {
          source: "iana",
          extensions: ["uvi", "uvvi", "uvg", "uvvg"]
        },
        "image/vnd.djvu": {
          source: "iana",
          extensions: ["djvu", "djv"]
        },
        "image/vnd.dvb.subtitle": {
          source: "iana",
          extensions: ["sub"]
        },
        "image/vnd.dwg": {
          source: "iana",
          extensions: ["dwg"]
        },
        "image/vnd.dxf": {
          source: "iana",
          extensions: ["dxf"]
        },
        "image/vnd.fastbidsheet": {
          source: "iana",
          extensions: ["fbs"]
        },
        "image/vnd.fpx": {
          source: "iana",
          extensions: ["fpx"]
        },
        "image/vnd.fst": {
          source: "iana",
          extensions: ["fst"]
        },
        "image/vnd.fujixerox.edmics-mmr": {
          source: "iana",
          extensions: ["mmr"]
        },
        "image/vnd.fujixerox.edmics-rlc": {
          source: "iana",
          extensions: ["rlc"]
        },
        "image/vnd.globalgraphics.pgb": {
          source: "iana"
        },
        "image/vnd.microsoft.icon": {
          source: "iana",
          compressible: true,
          extensions: ["ico"]
        },
        "image/vnd.mix": {
          source: "iana"
        },
        "image/vnd.mozilla.apng": {
          source: "iana"
        },
        "image/vnd.ms-dds": {
          compressible: true,
          extensions: ["dds"]
        },
        "image/vnd.ms-modi": {
          source: "iana",
          extensions: ["mdi"]
        },
        "image/vnd.ms-photo": {
          source: "apache",
          extensions: ["wdp"]
        },
        "image/vnd.net-fpx": {
          source: "iana",
          extensions: ["npx"]
        },
        "image/vnd.pco.b16": {
          source: "iana",
          extensions: ["b16"]
        },
        "image/vnd.radiance": {
          source: "iana"
        },
        "image/vnd.sealed.png": {
          source: "iana"
        },
        "image/vnd.sealedmedia.softseal.gif": {
          source: "iana"
        },
        "image/vnd.sealedmedia.softseal.jpg": {
          source: "iana"
        },
        "image/vnd.svf": {
          source: "iana"
        },
        "image/vnd.tencent.tap": {
          source: "iana",
          extensions: ["tap"]
        },
        "image/vnd.valve.source.texture": {
          source: "iana",
          extensions: ["vtf"]
        },
        "image/vnd.wap.wbmp": {
          source: "iana",
          extensions: ["wbmp"]
        },
        "image/vnd.xiff": {
          source: "iana",
          extensions: ["xif"]
        },
        "image/vnd.zbrush.pcx": {
          source: "iana",
          extensions: ["pcx"]
        },
        "image/webp": {
          source: "apache",
          extensions: ["webp"]
        },
        "image/wmf": {
          source: "iana",
          extensions: ["wmf"]
        },
        "image/x-3ds": {
          source: "apache",
          extensions: ["3ds"]
        },
        "image/x-cmu-raster": {
          source: "apache",
          extensions: ["ras"]
        },
        "image/x-cmx": {
          source: "apache",
          extensions: ["cmx"]
        },
        "image/x-freehand": {
          source: "apache",
          extensions: ["fh", "fhc", "fh4", "fh5", "fh7"]
        },
        "image/x-icon": {
          source: "apache",
          compressible: true,
          extensions: ["ico"]
        },
        "image/x-jng": {
          source: "nginx",
          extensions: ["jng"]
        },
        "image/x-mrsid-image": {
          source: "apache",
          extensions: ["sid"]
        },
        "image/x-ms-bmp": {
          source: "nginx",
          compressible: true,
          extensions: ["bmp"]
        },
        "image/x-pcx": {
          source: "apache",
          extensions: ["pcx"]
        },
        "image/x-pict": {
          source: "apache",
          extensions: ["pic", "pct"]
        },
        "image/x-portable-anymap": {
          source: "apache",
          extensions: ["pnm"]
        },
        "image/x-portable-bitmap": {
          source: "apache",
          extensions: ["pbm"]
        },
        "image/x-portable-graymap": {
          source: "apache",
          extensions: ["pgm"]
        },
        "image/x-portable-pixmap": {
          source: "apache",
          extensions: ["ppm"]
        },
        "image/x-rgb": {
          source: "apache",
          extensions: ["rgb"]
        },
        "image/x-tga": {
          source: "apache",
          extensions: ["tga"]
        },
        "image/x-xbitmap": {
          source: "apache",
          extensions: ["xbm"]
        },
        "image/x-xcf": {
          compressible: false
        },
        "image/x-xpixmap": {
          source: "apache",
          extensions: ["xpm"]
        },
        "image/x-xwindowdump": {
          source: "apache",
          extensions: ["xwd"]
        },
        "message/cpim": {
          source: "iana"
        },
        "message/delivery-status": {
          source: "iana"
        },
        "message/disposition-notification": {
          source: "iana",
          extensions: [
            "disposition-notification"
          ]
        },
        "message/external-body": {
          source: "iana"
        },
        "message/feedback-report": {
          source: "iana"
        },
        "message/global": {
          source: "iana",
          extensions: ["u8msg"]
        },
        "message/global-delivery-status": {
          source: "iana",
          extensions: ["u8dsn"]
        },
        "message/global-disposition-notification": {
          source: "iana",
          extensions: ["u8mdn"]
        },
        "message/global-headers": {
          source: "iana",
          extensions: ["u8hdr"]
        },
        "message/http": {
          source: "iana",
          compressible: false
        },
        "message/imdn+xml": {
          source: "iana",
          compressible: true
        },
        "message/news": {
          source: "iana"
        },
        "message/partial": {
          source: "iana",
          compressible: false
        },
        "message/rfc822": {
          source: "iana",
          compressible: true,
          extensions: ["eml", "mime"]
        },
        "message/s-http": {
          source: "iana"
        },
        "message/sip": {
          source: "iana"
        },
        "message/sipfrag": {
          source: "iana"
        },
        "message/tracking-status": {
          source: "iana"
        },
        "message/vnd.si.simp": {
          source: "iana"
        },
        "message/vnd.wfa.wsc": {
          source: "iana",
          extensions: ["wsc"]
        },
        "model/3mf": {
          source: "iana",
          extensions: ["3mf"]
        },
        "model/e57": {
          source: "iana"
        },
        "model/gltf+json": {
          source: "iana",
          compressible: true,
          extensions: ["gltf"]
        },
        "model/gltf-binary": {
          source: "iana",
          compressible: true,
          extensions: ["glb"]
        },
        "model/iges": {
          source: "iana",
          compressible: false,
          extensions: ["igs", "iges"]
        },
        "model/mesh": {
          source: "iana",
          compressible: false,
          extensions: ["msh", "mesh", "silo"]
        },
        "model/mtl": {
          source: "iana",
          extensions: ["mtl"]
        },
        "model/obj": {
          source: "iana",
          extensions: ["obj"]
        },
        "model/step": {
          source: "iana"
        },
        "model/step+xml": {
          source: "iana",
          compressible: true,
          extensions: ["stpx"]
        },
        "model/step+zip": {
          source: "iana",
          compressible: false,
          extensions: ["stpz"]
        },
        "model/step-xml+zip": {
          source: "iana",
          compressible: false,
          extensions: ["stpxz"]
        },
        "model/stl": {
          source: "iana",
          extensions: ["stl"]
        },
        "model/vnd.collada+xml": {
          source: "iana",
          compressible: true,
          extensions: ["dae"]
        },
        "model/vnd.dwf": {
          source: "iana",
          extensions: ["dwf"]
        },
        "model/vnd.flatland.3dml": {
          source: "iana"
        },
        "model/vnd.gdl": {
          source: "iana",
          extensions: ["gdl"]
        },
        "model/vnd.gs-gdl": {
          source: "apache"
        },
        "model/vnd.gs.gdl": {
          source: "iana"
        },
        "model/vnd.gtw": {
          source: "iana",
          extensions: ["gtw"]
        },
        "model/vnd.moml+xml": {
          source: "iana",
          compressible: true
        },
        "model/vnd.mts": {
          source: "iana",
          extensions: ["mts"]
        },
        "model/vnd.opengex": {
          source: "iana",
          extensions: ["ogex"]
        },
        "model/vnd.parasolid.transmit.binary": {
          source: "iana",
          extensions: ["x_b"]
        },
        "model/vnd.parasolid.transmit.text": {
          source: "iana",
          extensions: ["x_t"]
        },
        "model/vnd.pytha.pyox": {
          source: "iana"
        },
        "model/vnd.rosette.annotated-data-model": {
          source: "iana"
        },
        "model/vnd.sap.vds": {
          source: "iana",
          extensions: ["vds"]
        },
        "model/vnd.usdz+zip": {
          source: "iana",
          compressible: false,
          extensions: ["usdz"]
        },
        "model/vnd.valve.source.compiled-map": {
          source: "iana",
          extensions: ["bsp"]
        },
        "model/vnd.vtu": {
          source: "iana",
          extensions: ["vtu"]
        },
        "model/vrml": {
          source: "iana",
          compressible: false,
          extensions: ["wrl", "vrml"]
        },
        "model/x3d+binary": {
          source: "apache",
          compressible: false,
          extensions: ["x3db", "x3dbz"]
        },
        "model/x3d+fastinfoset": {
          source: "iana",
          extensions: ["x3db"]
        },
        "model/x3d+vrml": {
          source: "apache",
          compressible: false,
          extensions: ["x3dv", "x3dvz"]
        },
        "model/x3d+xml": {
          source: "iana",
          compressible: true,
          extensions: ["x3d", "x3dz"]
        },
        "model/x3d-vrml": {
          source: "iana",
          extensions: ["x3dv"]
        },
        "multipart/alternative": {
          source: "iana",
          compressible: false
        },
        "multipart/appledouble": {
          source: "iana"
        },
        "multipart/byteranges": {
          source: "iana"
        },
        "multipart/digest": {
          source: "iana"
        },
        "multipart/encrypted": {
          source: "iana",
          compressible: false
        },
        "multipart/form-data": {
          source: "iana",
          compressible: false
        },
        "multipart/header-set": {
          source: "iana"
        },
        "multipart/mixed": {
          source: "iana"
        },
        "multipart/multilingual": {
          source: "iana"
        },
        "multipart/parallel": {
          source: "iana"
        },
        "multipart/related": {
          source: "iana",
          compressible: false
        },
        "multipart/report": {
          source: "iana"
        },
        "multipart/signed": {
          source: "iana",
          compressible: false
        },
        "multipart/vnd.bint.med-plus": {
          source: "iana"
        },
        "multipart/voice-message": {
          source: "iana"
        },
        "multipart/x-mixed-replace": {
          source: "iana"
        },
        "text/1d-interleaved-parityfec": {
          source: "iana"
        },
        "text/cache-manifest": {
          source: "iana",
          compressible: true,
          extensions: ["appcache", "manifest"]
        },
        "text/calendar": {
          source: "iana",
          extensions: ["ics", "ifb"]
        },
        "text/calender": {
          compressible: true
        },
        "text/cmd": {
          compressible: true
        },
        "text/coffeescript": {
          extensions: ["coffee", "litcoffee"]
        },
        "text/cql": {
          source: "iana"
        },
        "text/cql-expression": {
          source: "iana"
        },
        "text/cql-identifier": {
          source: "iana"
        },
        "text/css": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["css"]
        },
        "text/csv": {
          source: "iana",
          compressible: true,
          extensions: ["csv"]
        },
        "text/csv-schema": {
          source: "iana"
        },
        "text/directory": {
          source: "iana"
        },
        "text/dns": {
          source: "iana"
        },
        "text/ecmascript": {
          source: "iana"
        },
        "text/encaprtp": {
          source: "iana"
        },
        "text/enriched": {
          source: "iana"
        },
        "text/fhirpath": {
          source: "iana"
        },
        "text/flexfec": {
          source: "iana"
        },
        "text/fwdred": {
          source: "iana"
        },
        "text/gff3": {
          source: "iana"
        },
        "text/grammar-ref-list": {
          source: "iana"
        },
        "text/html": {
          source: "iana",
          compressible: true,
          extensions: ["html", "htm", "shtml"]
        },
        "text/jade": {
          extensions: ["jade"]
        },
        "text/javascript": {
          source: "iana",
          compressible: true
        },
        "text/jcr-cnd": {
          source: "iana"
        },
        "text/jsx": {
          compressible: true,
          extensions: ["jsx"]
        },
        "text/less": {
          compressible: true,
          extensions: ["less"]
        },
        "text/markdown": {
          source: "iana",
          compressible: true,
          extensions: ["markdown", "md"]
        },
        "text/mathml": {
          source: "nginx",
          extensions: ["mml"]
        },
        "text/mdx": {
          compressible: true,
          extensions: ["mdx"]
        },
        "text/mizar": {
          source: "iana"
        },
        "text/n3": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["n3"]
        },
        "text/parameters": {
          source: "iana",
          charset: "UTF-8"
        },
        "text/parityfec": {
          source: "iana"
        },
        "text/plain": {
          source: "iana",
          compressible: true,
          extensions: ["txt", "text", "conf", "def", "list", "log", "in", "ini"]
        },
        "text/provenance-notation": {
          source: "iana",
          charset: "UTF-8"
        },
        "text/prs.fallenstein.rst": {
          source: "iana"
        },
        "text/prs.lines.tag": {
          source: "iana",
          extensions: ["dsc"]
        },
        "text/prs.prop.logic": {
          source: "iana"
        },
        "text/raptorfec": {
          source: "iana"
        },
        "text/red": {
          source: "iana"
        },
        "text/rfc822-headers": {
          source: "iana"
        },
        "text/richtext": {
          source: "iana",
          compressible: true,
          extensions: ["rtx"]
        },
        "text/rtf": {
          source: "iana",
          compressible: true,
          extensions: ["rtf"]
        },
        "text/rtp-enc-aescm128": {
          source: "iana"
        },
        "text/rtploopback": {
          source: "iana"
        },
        "text/rtx": {
          source: "iana"
        },
        "text/sgml": {
          source: "iana",
          extensions: ["sgml", "sgm"]
        },
        "text/shaclc": {
          source: "iana"
        },
        "text/shex": {
          source: "iana",
          extensions: ["shex"]
        },
        "text/slim": {
          extensions: ["slim", "slm"]
        },
        "text/spdx": {
          source: "iana",
          extensions: ["spdx"]
        },
        "text/strings": {
          source: "iana"
        },
        "text/stylus": {
          extensions: ["stylus", "styl"]
        },
        "text/t140": {
          source: "iana"
        },
        "text/tab-separated-values": {
          source: "iana",
          compressible: true,
          extensions: ["tsv"]
        },
        "text/troff": {
          source: "iana",
          extensions: ["t", "tr", "roff", "man", "me", "ms"]
        },
        "text/turtle": {
          source: "iana",
          charset: "UTF-8",
          extensions: ["ttl"]
        },
        "text/ulpfec": {
          source: "iana"
        },
        "text/uri-list": {
          source: "iana",
          compressible: true,
          extensions: ["uri", "uris", "urls"]
        },
        "text/vcard": {
          source: "iana",
          compressible: true,
          extensions: ["vcard"]
        },
        "text/vnd.a": {
          source: "iana"
        },
        "text/vnd.abc": {
          source: "iana"
        },
        "text/vnd.ascii-art": {
          source: "iana"
        },
        "text/vnd.curl": {
          source: "iana",
          extensions: ["curl"]
        },
        "text/vnd.curl.dcurl": {
          source: "apache",
          extensions: ["dcurl"]
        },
        "text/vnd.curl.mcurl": {
          source: "apache",
          extensions: ["mcurl"]
        },
        "text/vnd.curl.scurl": {
          source: "apache",
          extensions: ["scurl"]
        },
        "text/vnd.debian.copyright": {
          source: "iana",
          charset: "UTF-8"
        },
        "text/vnd.dmclientscript": {
          source: "iana"
        },
        "text/vnd.dvb.subtitle": {
          source: "iana",
          extensions: ["sub"]
        },
        "text/vnd.esmertec.theme-descriptor": {
          source: "iana",
          charset: "UTF-8"
        },
        "text/vnd.familysearch.gedcom": {
          source: "iana",
          extensions: ["ged"]
        },
        "text/vnd.ficlab.flt": {
          source: "iana"
        },
        "text/vnd.fly": {
          source: "iana",
          extensions: ["fly"]
        },
        "text/vnd.fmi.flexstor": {
          source: "iana",
          extensions: ["flx"]
        },
        "text/vnd.gml": {
          source: "iana"
        },
        "text/vnd.graphviz": {
          source: "iana",
          extensions: ["gv"]
        },
        "text/vnd.hans": {
          source: "iana"
        },
        "text/vnd.hgl": {
          source: "iana"
        },
        "text/vnd.in3d.3dml": {
          source: "iana",
          extensions: ["3dml"]
        },
        "text/vnd.in3d.spot": {
          source: "iana",
          extensions: ["spot"]
        },
        "text/vnd.iptc.newsml": {
          source: "iana"
        },
        "text/vnd.iptc.nitf": {
          source: "iana"
        },
        "text/vnd.latex-z": {
          source: "iana"
        },
        "text/vnd.motorola.reflex": {
          source: "iana"
        },
        "text/vnd.ms-mediapackage": {
          source: "iana"
        },
        "text/vnd.net2phone.commcenter.command": {
          source: "iana"
        },
        "text/vnd.radisys.msml-basic-layout": {
          source: "iana"
        },
        "text/vnd.senx.warpscript": {
          source: "iana"
        },
        "text/vnd.si.uricatalogue": {
          source: "iana"
        },
        "text/vnd.sosi": {
          source: "iana"
        },
        "text/vnd.sun.j2me.app-descriptor": {
          source: "iana",
          charset: "UTF-8",
          extensions: ["jad"]
        },
        "text/vnd.trolltech.linguist": {
          source: "iana",
          charset: "UTF-8"
        },
        "text/vnd.wap.si": {
          source: "iana"
        },
        "text/vnd.wap.sl": {
          source: "iana"
        },
        "text/vnd.wap.wml": {
          source: "iana",
          extensions: ["wml"]
        },
        "text/vnd.wap.wmlscript": {
          source: "iana",
          extensions: ["wmls"]
        },
        "text/vtt": {
          source: "iana",
          charset: "UTF-8",
          compressible: true,
          extensions: ["vtt"]
        },
        "text/x-asm": {
          source: "apache",
          extensions: ["s", "asm"]
        },
        "text/x-c": {
          source: "apache",
          extensions: ["c", "cc", "cxx", "cpp", "h", "hh", "dic"]
        },
        "text/x-component": {
          source: "nginx",
          extensions: ["htc"]
        },
        "text/x-fortran": {
          source: "apache",
          extensions: ["f", "for", "f77", "f90"]
        },
        "text/x-gwt-rpc": {
          compressible: true
        },
        "text/x-handlebars-template": {
          extensions: ["hbs"]
        },
        "text/x-java-source": {
          source: "apache",
          extensions: ["java"]
        },
        "text/x-jquery-tmpl": {
          compressible: true
        },
        "text/x-lua": {
          extensions: ["lua"]
        },
        "text/x-markdown": {
          compressible: true,
          extensions: ["mkd"]
        },
        "text/x-nfo": {
          source: "apache",
          extensions: ["nfo"]
        },
        "text/x-opml": {
          source: "apache",
          extensions: ["opml"]
        },
        "text/x-org": {
          compressible: true,
          extensions: ["org"]
        },
        "text/x-pascal": {
          source: "apache",
          extensions: ["p", "pas"]
        },
        "text/x-processing": {
          compressible: true,
          extensions: ["pde"]
        },
        "text/x-sass": {
          extensions: ["sass"]
        },
        "text/x-scss": {
          extensions: ["scss"]
        },
        "text/x-setext": {
          source: "apache",
          extensions: ["etx"]
        },
        "text/x-sfv": {
          source: "apache",
          extensions: ["sfv"]
        },
        "text/x-suse-ymp": {
          compressible: true,
          extensions: ["ymp"]
        },
        "text/x-uuencode": {
          source: "apache",
          extensions: ["uu"]
        },
        "text/x-vcalendar": {
          source: "apache",
          extensions: ["vcs"]
        },
        "text/x-vcard": {
          source: "apache",
          extensions: ["vcf"]
        },
        "text/xml": {
          source: "iana",
          compressible: true,
          extensions: ["xml"]
        },
        "text/xml-external-parsed-entity": {
          source: "iana"
        },
        "text/yaml": {
          compressible: true,
          extensions: ["yaml", "yml"]
        },
        "video/1d-interleaved-parityfec": {
          source: "iana"
        },
        "video/3gpp": {
          source: "iana",
          extensions: ["3gp", "3gpp"]
        },
        "video/3gpp-tt": {
          source: "iana"
        },
        "video/3gpp2": {
          source: "iana",
          extensions: ["3g2"]
        },
        "video/av1": {
          source: "iana"
        },
        "video/bmpeg": {
          source: "iana"
        },
        "video/bt656": {
          source: "iana"
        },
        "video/celb": {
          source: "iana"
        },
        "video/dv": {
          source: "iana"
        },
        "video/encaprtp": {
          source: "iana"
        },
        "video/ffv1": {
          source: "iana"
        },
        "video/flexfec": {
          source: "iana"
        },
        "video/h261": {
          source: "iana",
          extensions: ["h261"]
        },
        "video/h263": {
          source: "iana",
          extensions: ["h263"]
        },
        "video/h263-1998": {
          source: "iana"
        },
        "video/h263-2000": {
          source: "iana"
        },
        "video/h264": {
          source: "iana",
          extensions: ["h264"]
        },
        "video/h264-rcdo": {
          source: "iana"
        },
        "video/h264-svc": {
          source: "iana"
        },
        "video/h265": {
          source: "iana"
        },
        "video/iso.segment": {
          source: "iana",
          extensions: ["m4s"]
        },
        "video/jpeg": {
          source: "iana",
          extensions: ["jpgv"]
        },
        "video/jpeg2000": {
          source: "iana"
        },
        "video/jpm": {
          source: "apache",
          extensions: ["jpm", "jpgm"]
        },
        "video/jxsv": {
          source: "iana"
        },
        "video/mj2": {
          source: "iana",
          extensions: ["mj2", "mjp2"]
        },
        "video/mp1s": {
          source: "iana"
        },
        "video/mp2p": {
          source: "iana"
        },
        "video/mp2t": {
          source: "iana",
          extensions: ["ts"]
        },
        "video/mp4": {
          source: "iana",
          compressible: false,
          extensions: ["mp4", "mp4v", "mpg4"]
        },
        "video/mp4v-es": {
          source: "iana"
        },
        "video/mpeg": {
          source: "iana",
          compressible: false,
          extensions: ["mpeg", "mpg", "mpe", "m1v", "m2v"]
        },
        "video/mpeg4-generic": {
          source: "iana"
        },
        "video/mpv": {
          source: "iana"
        },
        "video/nv": {
          source: "iana"
        },
        "video/ogg": {
          source: "iana",
          compressible: false,
          extensions: ["ogv"]
        },
        "video/parityfec": {
          source: "iana"
        },
        "video/pointer": {
          source: "iana"
        },
        "video/quicktime": {
          source: "iana",
          compressible: false,
          extensions: ["qt", "mov"]
        },
        "video/raptorfec": {
          source: "iana"
        },
        "video/raw": {
          source: "iana"
        },
        "video/rtp-enc-aescm128": {
          source: "iana"
        },
        "video/rtploopback": {
          source: "iana"
        },
        "video/rtx": {
          source: "iana"
        },
        "video/scip": {
          source: "iana"
        },
        "video/smpte291": {
          source: "iana"
        },
        "video/smpte292m": {
          source: "iana"
        },
        "video/ulpfec": {
          source: "iana"
        },
        "video/vc1": {
          source: "iana"
        },
        "video/vc2": {
          source: "iana"
        },
        "video/vnd.cctv": {
          source: "iana"
        },
        "video/vnd.dece.hd": {
          source: "iana",
          extensions: ["uvh", "uvvh"]
        },
        "video/vnd.dece.mobile": {
          source: "iana",
          extensions: ["uvm", "uvvm"]
        },
        "video/vnd.dece.mp4": {
          source: "iana"
        },
        "video/vnd.dece.pd": {
          source: "iana",
          extensions: ["uvp", "uvvp"]
        },
        "video/vnd.dece.sd": {
          source: "iana",
          extensions: ["uvs", "uvvs"]
        },
        "video/vnd.dece.video": {
          source: "iana",
          extensions: ["uvv", "uvvv"]
        },
        "video/vnd.directv.mpeg": {
          source: "iana"
        },
        "video/vnd.directv.mpeg-tts": {
          source: "iana"
        },
        "video/vnd.dlna.mpeg-tts": {
          source: "iana"
        },
        "video/vnd.dvb.file": {
          source: "iana",
          extensions: ["dvb"]
        },
        "video/vnd.fvt": {
          source: "iana",
          extensions: ["fvt"]
        },
        "video/vnd.hns.video": {
          source: "iana"
        },
        "video/vnd.iptvforum.1dparityfec-1010": {
          source: "iana"
        },
        "video/vnd.iptvforum.1dparityfec-2005": {
          source: "iana"
        },
        "video/vnd.iptvforum.2dparityfec-1010": {
          source: "iana"
        },
        "video/vnd.iptvforum.2dparityfec-2005": {
          source: "iana"
        },
        "video/vnd.iptvforum.ttsavc": {
          source: "iana"
        },
        "video/vnd.iptvforum.ttsmpeg2": {
          source: "iana"
        },
        "video/vnd.motorola.video": {
          source: "iana"
        },
        "video/vnd.motorola.videop": {
          source: "iana"
        },
        "video/vnd.mpegurl": {
          source: "iana",
          extensions: ["mxu", "m4u"]
        },
        "video/vnd.ms-playready.media.pyv": {
          source: "iana",
          extensions: ["pyv"]
        },
        "video/vnd.nokia.interleaved-multimedia": {
          source: "iana"
        },
        "video/vnd.nokia.mp4vr": {
          source: "iana"
        },
        "video/vnd.nokia.videovoip": {
          source: "iana"
        },
        "video/vnd.objectvideo": {
          source: "iana"
        },
        "video/vnd.radgamettools.bink": {
          source: "iana"
        },
        "video/vnd.radgamettools.smacker": {
          source: "iana"
        },
        "video/vnd.sealed.mpeg1": {
          source: "iana"
        },
        "video/vnd.sealed.mpeg4": {
          source: "iana"
        },
        "video/vnd.sealed.swf": {
          source: "iana"
        },
        "video/vnd.sealedmedia.softseal.mov": {
          source: "iana"
        },
        "video/vnd.uvvu.mp4": {
          source: "iana",
          extensions: ["uvu", "uvvu"]
        },
        "video/vnd.vivo": {
          source: "iana",
          extensions: ["viv"]
        },
        "video/vnd.youtube.yt": {
          source: "iana"
        },
        "video/vp8": {
          source: "iana"
        },
        "video/vp9": {
          source: "iana"
        },
        "video/webm": {
          source: "apache",
          compressible: false,
          extensions: ["webm"]
        },
        "video/x-f4v": {
          source: "apache",
          extensions: ["f4v"]
        },
        "video/x-fli": {
          source: "apache",
          extensions: ["fli"]
        },
        "video/x-flv": {
          source: "apache",
          compressible: false,
          extensions: ["flv"]
        },
        "video/x-m4v": {
          source: "apache",
          extensions: ["m4v"]
        },
        "video/x-matroska": {
          source: "apache",
          compressible: false,
          extensions: ["mkv", "mk3d", "mks"]
        },
        "video/x-mng": {
          source: "apache",
          extensions: ["mng"]
        },
        "video/x-ms-asf": {
          source: "apache",
          extensions: ["asf", "asx"]
        },
        "video/x-ms-vob": {
          source: "apache",
          extensions: ["vob"]
        },
        "video/x-ms-wm": {
          source: "apache",
          extensions: ["wm"]
        },
        "video/x-ms-wmv": {
          source: "apache",
          compressible: false,
          extensions: ["wmv"]
        },
        "video/x-ms-wmx": {
          source: "apache",
          extensions: ["wmx"]
        },
        "video/x-ms-wvx": {
          source: "apache",
          extensions: ["wvx"]
        },
        "video/x-msvideo": {
          source: "apache",
          extensions: ["avi"]
        },
        "video/x-sgi-movie": {
          source: "apache",
          extensions: ["movie"]
        },
        "video/x-smv": {
          source: "apache",
          extensions: ["smv"]
        },
        "x-conference/x-cooltalk": {
          source: "apache",
          extensions: ["ice"]
        },
        "x-shader/x-fragment": {
          compressible: true
        },
        "x-shader/x-vertex": {
          compressible: true
        }
      };
    }
  });
  var require_mime_db = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mime-db@1.52.0/node_modules/mime-db/index.js"(exports2, module22) {
      module22.exports = require_db();
    }
  });
  var require_mime_types = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mime-types@2.1.35/node_modules/mime-types/index.js"(exports2) {
      "use strict";
      var db = require_mime_db();
      var extname = require2("path").extname;
      var EXTRACT_TYPE_REGEXP = /^\s*([^;\s]*)(?:;|\s|$)/;
      var TEXT_TYPE_REGEXP = /^text\//i;
      exports2.charset = charset;
      exports2.charsets = { lookup: charset };
      exports2.contentType = contentType;
      exports2.extension = extension;
      exports2.extensions = /* @__PURE__ */ Object.create(null);
      exports2.lookup = lookup;
      exports2.types = /* @__PURE__ */ Object.create(null);
      populateMaps(exports2.extensions, exports2.types);
      function charset(type) {
        if (!type || typeof type !== "string") {
          return false;
        }
        var match = EXTRACT_TYPE_REGEXP.exec(type);
        var mime = match && db[match[1].toLowerCase()];
        if (mime && mime.charset) {
          return mime.charset;
        }
        if (match && TEXT_TYPE_REGEXP.test(match[1])) {
          return "UTF-8";
        }
        return false;
      }
      function contentType(str) {
        if (!str || typeof str !== "string") {
          return false;
        }
        var mime = str.indexOf("/") === -1 ? exports2.lookup(str) : str;
        if (!mime) {
          return false;
        }
        if (mime.indexOf("charset") === -1) {
          var charset2 = exports2.charset(mime);
          if (charset2)
            mime += "; charset=" + charset2.toLowerCase();
        }
        return mime;
      }
      function extension(type) {
        if (!type || typeof type !== "string") {
          return false;
        }
        var match = EXTRACT_TYPE_REGEXP.exec(type);
        var exts = match && exports2.extensions[match[1].toLowerCase()];
        if (!exts || !exts.length) {
          return false;
        }
        return exts[0];
      }
      function lookup(path) {
        if (!path || typeof path !== "string") {
          return false;
        }
        var extension2 = extname("x." + path).toLowerCase().substr(1);
        if (!extension2) {
          return false;
        }
        return exports2.types[extension2] || false;
      }
      function populateMaps(extensions, types) {
        var preference = ["nginx", "apache", void 0, "iana"];
        Object.keys(db).forEach(function forEachMimeType(type) {
          var mime = db[type];
          var exts = mime.extensions;
          if (!exts || !exts.length) {
            return;
          }
          extensions[type] = exts;
          for (var i = 0; i < exts.length; i++) {
            var extension2 = exts[i];
            if (types[extension2]) {
              var from = preference.indexOf(db[types[extension2]].source);
              var to = preference.indexOf(mime.source);
              if (types[extension2] !== "application/octet-stream" && (from > to || from === to && types[extension2].substr(0, 12) === "application/")) {
                continue;
              }
            }
            types[extension2] = type;
          }
        });
      }
    }
  });
  var require_defer = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/lib/defer.js"(exports2, module22) {
      module22.exports = defer;
      function defer(fn) {
        var nextTick = typeof setImmediate == "function" ? setImmediate : typeof process == "object" && typeof process.nextTick == "function" ? process.nextTick : null;
        if (nextTick) {
          nextTick(fn);
        } else {
          setTimeout(fn, 0);
        }
      }
    }
  });
  var require_async = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/lib/async.js"(exports2, module22) {
      var defer = require_defer();
      module22.exports = async;
      function async(callback) {
        var isAsync = false;
        defer(function() {
          isAsync = true;
        });
        return function async_callback(err, result) {
          if (isAsync) {
            callback(err, result);
          } else {
            defer(function nextTick_callback() {
              callback(err, result);
            });
          }
        };
      }
    }
  });
  var require_abort = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/lib/abort.js"(exports2, module22) {
      module22.exports = abort;
      function abort(state) {
        Object.keys(state.jobs).forEach(clean.bind(state));
        state.jobs = {};
      }
      function clean(key) {
        if (typeof this.jobs[key] == "function") {
          this.jobs[key]();
        }
      }
    }
  });
  var require_iterate = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/lib/iterate.js"(exports2, module22) {
      var async = require_async();
      var abort = require_abort();
      module22.exports = iterate;
      function iterate(list, iterator, state, callback) {
        var key = state["keyedList"] ? state["keyedList"][state.index] : state.index;
        state.jobs[key] = runJob(iterator, key, list[key], function(error, output) {
          if (!(key in state.jobs)) {
            return;
          }
          delete state.jobs[key];
          if (error) {
            abort(state);
          } else {
            state.results[key] = output;
          }
          callback(error, state.results);
        });
      }
      function runJob(iterator, key, item, callback) {
        var aborter;
        if (iterator.length == 2) {
          aborter = iterator(item, async(callback));
        } else {
          aborter = iterator(item, key, async(callback));
        }
        return aborter;
      }
    }
  });
  var require_state = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/lib/state.js"(exports2, module22) {
      module22.exports = state;
      function state(list, sortMethod) {
        var isNamedList = !Array.isArray(list), initState = {
          index: 0,
          keyedList: isNamedList || sortMethod ? Object.keys(list) : null,
          jobs: {},
          results: isNamedList ? {} : [],
          size: isNamedList ? Object.keys(list).length : list.length
        };
        if (sortMethod) {
          initState.keyedList.sort(isNamedList ? sortMethod : function(a, b) {
            return sortMethod(list[a], list[b]);
          });
        }
        return initState;
      }
    }
  });
  var require_terminator = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/lib/terminator.js"(exports2, module22) {
      var abort = require_abort();
      var async = require_async();
      module22.exports = terminator;
      function terminator(callback) {
        if (!Object.keys(this.jobs).length) {
          return;
        }
        this.index = this.size;
        abort(this);
        async(callback)(null, this.results);
      }
    }
  });
  var require_parallel = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/parallel.js"(exports2, module22) {
      var iterate = require_iterate();
      var initState = require_state();
      var terminator = require_terminator();
      module22.exports = parallel;
      function parallel(list, iterator, callback) {
        var state = initState(list);
        while (state.index < (state["keyedList"] || list).length) {
          iterate(list, iterator, state, function(error, result) {
            if (error) {
              callback(error, result);
              return;
            }
            if (Object.keys(state.jobs).length === 0) {
              callback(null, state.results);
              return;
            }
          });
          state.index++;
        }
        return terminator.bind(state, callback);
      }
    }
  });
  var require_serialOrdered = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/serialOrdered.js"(exports2, module22) {
      var iterate = require_iterate();
      var initState = require_state();
      var terminator = require_terminator();
      module22.exports = serialOrdered;
      module22.exports.ascending = ascending;
      module22.exports.descending = descending;
      function serialOrdered(list, iterator, sortMethod, callback) {
        var state = initState(list, sortMethod);
        iterate(list, iterator, state, function iteratorHandler(error, result) {
          if (error) {
            callback(error, result);
            return;
          }
          state.index++;
          if (state.index < (state["keyedList"] || list).length) {
            iterate(list, iterator, state, iteratorHandler);
            return;
          }
          callback(null, state.results);
        });
        return terminator.bind(state, callback);
      }
      function ascending(a, b) {
        return a < b ? -1 : a > b ? 1 : 0;
      }
      function descending(a, b) {
        return -1 * ascending(a, b);
      }
    }
  });
  var require_serial = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/serial.js"(exports2, module22) {
      var serialOrdered = require_serialOrdered();
      module22.exports = serial;
      function serial(list, iterator, callback) {
        return serialOrdered(list, iterator, null, callback);
      }
    }
  });
  var require_asynckit = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/asynckit@0.4.0/node_modules/asynckit/index.js"(exports2, module22) {
      module22.exports = {
        parallel: require_parallel(),
        serial: require_serial(),
        serialOrdered: require_serialOrdered()
      };
    }
  });
  var require_populate = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/form-data@4.0.0/node_modules/form-data/lib/populate.js"(exports2, module22) {
      module22.exports = function(dst, src) {
        Object.keys(src).forEach(function(prop) {
          dst[prop] = dst[prop] || src[prop];
        });
        return dst;
      };
    }
  });
  var require_form_data = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/form-data@4.0.0/node_modules/form-data/lib/form_data.js"(exports2, module22) {
      var CombinedStream = require_combined_stream();
      var util = require2("util");
      var path = require2("path");
      var http = require2("http");
      var https = require2("https");
      var parseUrl = require2("url").parse;
      var fs = require2("fs");
      var Stream = require2("stream").Stream;
      var mime = require_mime_types();
      var asynckit = require_asynckit();
      var populate = require_populate();
      module22.exports = FormData2;
      util.inherits(FormData2, CombinedStream);
      function FormData2(options) {
        if (!(this instanceof FormData2)) {
          return new FormData2(options);
        }
        this._overheadLength = 0;
        this._valueLength = 0;
        this._valuesToMeasure = [];
        CombinedStream.call(this);
        options = options || {};
        for (var option in options) {
          this[option] = options[option];
        }
      }
      FormData2.LINE_BREAK = "\r\n";
      FormData2.DEFAULT_CONTENT_TYPE = "application/octet-stream";
      FormData2.prototype.append = function(field, value, options) {
        options = options || {};
        if (typeof options == "string") {
          options = { filename: options };
        }
        var append = CombinedStream.prototype.append.bind(this);
        if (typeof value == "number") {
          value = "" + value;
        }
        if (util.isArray(value)) {
          this._error(new Error("Arrays are not supported."));
          return;
        }
        var header = this._multiPartHeader(field, value, options);
        var footer = this._multiPartFooter();
        append(header);
        append(value);
        append(footer);
        this._trackLength(header, value, options);
      };
      FormData2.prototype._trackLength = function(header, value, options) {
        var valueLength = 0;
        if (options.knownLength != null) {
          valueLength += +options.knownLength;
        } else if (Buffer.isBuffer(value)) {
          valueLength = value.length;
        } else if (typeof value === "string") {
          valueLength = Buffer.byteLength(value);
        }
        this._valueLength += valueLength;
        this._overheadLength += Buffer.byteLength(header) + FormData2.LINE_BREAK.length;
        if (!value || !value.path && !(value.readable && value.hasOwnProperty("httpVersion")) && !(value instanceof Stream)) {
          return;
        }
        if (!options.knownLength) {
          this._valuesToMeasure.push(value);
        }
      };
      FormData2.prototype._lengthRetriever = function(value, callback) {
        if (value.hasOwnProperty("fd")) {
          if (value.end != void 0 && value.end != Infinity && value.start != void 0) {
            callback(null, value.end + 1 - (value.start ? value.start : 0));
          } else {
            fs.stat(value.path, function(err, stat) {
              var fileSize;
              if (err) {
                callback(err);
                return;
              }
              fileSize = stat.size - (value.start ? value.start : 0);
              callback(null, fileSize);
            });
          }
        } else if (value.hasOwnProperty("httpVersion")) {
          callback(null, +value.headers["content-length"]);
        } else if (value.hasOwnProperty("httpModule")) {
          value.on("response", function(response) {
            value.pause();
            callback(null, +response.headers["content-length"]);
          });
          value.resume();
        } else {
          callback("Unknown stream");
        }
      };
      FormData2.prototype._multiPartHeader = function(field, value, options) {
        if (typeof options.header == "string") {
          return options.header;
        }
        var contentDisposition = this._getContentDisposition(value, options);
        var contentType = this._getContentType(value, options);
        var contents = "";
        var headers = {
          // add custom disposition as third element or keep it two elements if not
          "Content-Disposition": ["form-data", 'name="' + field + '"'].concat(contentDisposition || []),
          // if no content type. allow it to be empty array
          "Content-Type": [].concat(contentType || [])
        };
        if (typeof options.header == "object") {
          populate(headers, options.header);
        }
        var header;
        for (var prop in headers) {
          if (!headers.hasOwnProperty(prop))
            continue;
          header = headers[prop];
          if (header == null) {
            continue;
          }
          if (!Array.isArray(header)) {
            header = [header];
          }
          if (header.length) {
            contents += prop + ": " + header.join("; ") + FormData2.LINE_BREAK;
          }
        }
        return "--" + this.getBoundary() + FormData2.LINE_BREAK + contents + FormData2.LINE_BREAK;
      };
      FormData2.prototype._getContentDisposition = function(value, options) {
        var filename, contentDisposition;
        if (typeof options.filepath === "string") {
          filename = path.normalize(options.filepath).replace(/\\/g, "/");
        } else if (options.filename || value.name || value.path) {
          filename = path.basename(options.filename || value.name || value.path);
        } else if (value.readable && value.hasOwnProperty("httpVersion")) {
          filename = path.basename(value.client._httpMessage.path || "");
        }
        if (filename) {
          contentDisposition = 'filename="' + filename + '"';
        }
        return contentDisposition;
      };
      FormData2.prototype._getContentType = function(value, options) {
        var contentType = options.contentType;
        if (!contentType && value.name) {
          contentType = mime.lookup(value.name);
        }
        if (!contentType && value.path) {
          contentType = mime.lookup(value.path);
        }
        if (!contentType && value.readable && value.hasOwnProperty("httpVersion")) {
          contentType = value.headers["content-type"];
        }
        if (!contentType && (options.filepath || options.filename)) {
          contentType = mime.lookup(options.filepath || options.filename);
        }
        if (!contentType && typeof value == "object") {
          contentType = FormData2.DEFAULT_CONTENT_TYPE;
        }
        return contentType;
      };
      FormData2.prototype._multiPartFooter = function() {
        return function(next) {
          var footer = FormData2.LINE_BREAK;
          var lastPart = this._streams.length === 0;
          if (lastPart) {
            footer += this._lastBoundary();
          }
          next(footer);
        }.bind(this);
      };
      FormData2.prototype._lastBoundary = function() {
        return "--" + this.getBoundary() + "--" + FormData2.LINE_BREAK;
      };
      FormData2.prototype.getHeaders = function(userHeaders) {
        var header;
        var formHeaders = {
          "content-type": "multipart/form-data; boundary=" + this.getBoundary()
        };
        for (header in userHeaders) {
          if (userHeaders.hasOwnProperty(header)) {
            formHeaders[header.toLowerCase()] = userHeaders[header];
          }
        }
        return formHeaders;
      };
      FormData2.prototype.setBoundary = function(boundary) {
        this._boundary = boundary;
      };
      FormData2.prototype.getBoundary = function() {
        if (!this._boundary) {
          this._generateBoundary();
        }
        return this._boundary;
      };
      FormData2.prototype.getBuffer = function() {
        var dataBuffer = new Buffer.alloc(0);
        var boundary = this.getBoundary();
        for (var i = 0, len = this._streams.length; i < len; i++) {
          if (typeof this._streams[i] !== "function") {
            if (Buffer.isBuffer(this._streams[i])) {
              dataBuffer = Buffer.concat([dataBuffer, this._streams[i]]);
            } else {
              dataBuffer = Buffer.concat([dataBuffer, Buffer.from(this._streams[i])]);
            }
            if (typeof this._streams[i] !== "string" || this._streams[i].substring(2, boundary.length + 2) !== boundary) {
              dataBuffer = Buffer.concat([dataBuffer, Buffer.from(FormData2.LINE_BREAK)]);
            }
          }
        }
        return Buffer.concat([dataBuffer, Buffer.from(this._lastBoundary())]);
      };
      FormData2.prototype._generateBoundary = function() {
        var boundary = "--------------------------";
        for (var i = 0; i < 24; i++) {
          boundary += Math.floor(Math.random() * 10).toString(16);
        }
        this._boundary = boundary;
      };
      FormData2.prototype.getLengthSync = function() {
        var knownLength = this._overheadLength + this._valueLength;
        if (this._streams.length) {
          knownLength += this._lastBoundary().length;
        }
        if (!this.hasKnownLength()) {
          this._error(new Error("Cannot calculate proper length in synchronous way."));
        }
        return knownLength;
      };
      FormData2.prototype.hasKnownLength = function() {
        var hasKnownLength = true;
        if (this._valuesToMeasure.length) {
          hasKnownLength = false;
        }
        return hasKnownLength;
      };
      FormData2.prototype.getLength = function(cb) {
        var knownLength = this._overheadLength + this._valueLength;
        if (this._streams.length) {
          knownLength += this._lastBoundary().length;
        }
        if (!this._valuesToMeasure.length) {
          process.nextTick(cb.bind(this, null, knownLength));
          return;
        }
        asynckit.parallel(this._valuesToMeasure, this._lengthRetriever, function(err, values) {
          if (err) {
            cb(err);
            return;
          }
          values.forEach(function(length) {
            knownLength += length;
          });
          cb(null, knownLength);
        });
      };
      FormData2.prototype.submit = function(params, cb) {
        var request, options, defaults = { method: "post" };
        if (typeof params == "string") {
          params = parseUrl(params);
          options = populate({
            port: params.port,
            path: params.pathname,
            host: params.hostname,
            protocol: params.protocol
          }, defaults);
        } else {
          options = populate(params, defaults);
          if (!options.port) {
            options.port = options.protocol == "https:" ? 443 : 80;
          }
        }
        options.headers = this.getHeaders(params.headers);
        if (options.protocol == "https:") {
          request = https.request(options);
        } else {
          request = http.request(options);
        }
        this.getLength(function(err, length) {
          if (err && err !== "Unknown stream") {
            this._error(err);
            return;
          }
          if (length) {
            request.setHeader("Content-Length", length);
          }
          this.pipe(request);
          if (cb) {
            var onResponse;
            var callback = function(error, responce) {
              request.removeListener("error", callback);
              request.removeListener("response", onResponse);
              return cb.call(this, error, responce);
            };
            onResponse = callback.bind(this, null);
            request.on("error", callback);
            request.on("response", onResponse);
          }
        }.bind(this));
        return request;
      };
      FormData2.prototype._error = function(err) {
        if (!this.error) {
          this.error = err;
          this.pause();
          this.emit("error", err);
        }
      };
      FormData2.prototype.toString = function() {
        return "[object FormData]";
      };
    }
  });
  var require_FormData = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/defaults/env/FormData.js"(exports2, module22) {
      module22.exports = require_form_data();
    }
  });
  var require_defaults = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/defaults/index.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var normalizeHeaderName = require_normalizeHeaderName();
      var AxiosError = require_AxiosError();
      var transitionalDefaults = require_transitional();
      var toFormData = require_toFormData();
      var DEFAULT_CONTENT_TYPE = {
        "Content-Type": "application/x-www-form-urlencoded"
      };
      function setContentTypeIfUnset(headers, value) {
        if (!utils.isUndefined(headers) && utils.isUndefined(headers["Content-Type"])) {
          headers["Content-Type"] = value;
        }
      }
      function getDefaultAdapter() {
        var adapter;
        if (typeof XMLHttpRequest !== "undefined") {
          adapter = require_xhr();
        } else if (typeof process !== "undefined" && Object.prototype.toString.call(process) === "[object process]") {
          adapter = require_http();
        }
        return adapter;
      }
      function stringifySafely(rawValue, parser, encoder) {
        if (utils.isString(rawValue)) {
          try {
            (parser || JSON.parse)(rawValue);
            return utils.trim(rawValue);
          } catch (e) {
            if (e.name !== "SyntaxError") {
              throw e;
            }
          }
        }
        return (encoder || JSON.stringify)(rawValue);
      }
      var defaults = {
        transitional: transitionalDefaults,
        adapter: getDefaultAdapter(),
        transformRequest: [function transformRequest(data, headers) {
          normalizeHeaderName(headers, "Accept");
          normalizeHeaderName(headers, "Content-Type");
          if (utils.isFormData(data) || utils.isArrayBuffer(data) || utils.isBuffer(data) || utils.isStream(data) || utils.isFile(data) || utils.isBlob(data)) {
            return data;
          }
          if (utils.isArrayBufferView(data)) {
            return data.buffer;
          }
          if (utils.isURLSearchParams(data)) {
            setContentTypeIfUnset(headers, "application/x-www-form-urlencoded;charset=utf-8");
            return data.toString();
          }
          var isObjectPayload = utils.isObject(data);
          var contentType = headers && headers["Content-Type"];
          var isFileList;
          if ((isFileList = utils.isFileList(data)) || isObjectPayload && contentType === "multipart/form-data") {
            var _FormData = this.env && this.env.FormData;
            return toFormData(isFileList ? { "files[]": data } : data, _FormData && new _FormData());
          } else if (isObjectPayload || contentType === "application/json") {
            setContentTypeIfUnset(headers, "application/json");
            return stringifySafely(data);
          }
          return data;
        }],
        transformResponse: [function transformResponse(data) {
          var transitional = this.transitional || defaults.transitional;
          var silentJSONParsing = transitional && transitional.silentJSONParsing;
          var forcedJSONParsing = transitional && transitional.forcedJSONParsing;
          var strictJSONParsing = !silentJSONParsing && this.responseType === "json";
          if (strictJSONParsing || forcedJSONParsing && utils.isString(data) && data.length) {
            try {
              return JSON.parse(data);
            } catch (e) {
              if (strictJSONParsing) {
                if (e.name === "SyntaxError") {
                  throw AxiosError.from(e, AxiosError.ERR_BAD_RESPONSE, this, null, this.response);
                }
                throw e;
              }
            }
          }
          return data;
        }],
        /**
         * A timeout in milliseconds to abort a request. If set to 0 (default) a
         * timeout is not created.
         */
        timeout: 0,
        xsrfCookieName: "XSRF-TOKEN",
        xsrfHeaderName: "X-XSRF-TOKEN",
        maxContentLength: -1,
        maxBodyLength: -1,
        env: {
          FormData: require_FormData()
        },
        validateStatus: function validateStatus(status) {
          return status >= 200 && status < 300;
        },
        headers: {
          common: {
            "Accept": "application/json, text/plain, */*"
          }
        }
      };
      utils.forEach(["delete", "get", "head"], function forEachMethodNoData(method) {
        defaults.headers[method] = {};
      });
      utils.forEach(["post", "put", "patch"], function forEachMethodWithData(method) {
        defaults.headers[method] = utils.merge(DEFAULT_CONTENT_TYPE);
      });
      module22.exports = defaults;
    }
  });
  var require_transformData = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/transformData.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var defaults = require_defaults();
      module22.exports = function transformData(data, headers, fns) {
        var context = this || defaults;
        utils.forEach(fns, function transform(fn) {
          data = fn.call(context, data, headers);
        });
        return data;
      };
    }
  });
  var require_isCancel = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/cancel/isCancel.js"(exports2, module22) {
      "use strict";
      module22.exports = function isCancel(value) {
        return !!(value && value.__CANCEL__);
      };
    }
  });
  var require_dispatchRequest = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/dispatchRequest.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var transformData = require_transformData();
      var isCancel = require_isCancel();
      var defaults = require_defaults();
      var CanceledError = require_CanceledError();
      function throwIfCancellationRequested(config) {
        if (config.cancelToken) {
          config.cancelToken.throwIfRequested();
        }
        if (config.signal && config.signal.aborted) {
          throw new CanceledError();
        }
      }
      module22.exports = function dispatchRequest(config) {
        throwIfCancellationRequested(config);
        config.headers = config.headers || {};
        config.data = transformData.call(
          config,
          config.data,
          config.headers,
          config.transformRequest
        );
        config.headers = utils.merge(
          config.headers.common || {},
          config.headers[config.method] || {},
          config.headers
        );
        utils.forEach(
          ["delete", "get", "head", "post", "put", "patch", "common"],
          function cleanHeaderConfig(method) {
            delete config.headers[method];
          }
        );
        var adapter = config.adapter || defaults.adapter;
        return adapter(config).then(function onAdapterResolution(response) {
          throwIfCancellationRequested(config);
          response.data = transformData.call(
            config,
            response.data,
            response.headers,
            config.transformResponse
          );
          return response;
        }, function onAdapterRejection(reason) {
          if (!isCancel(reason)) {
            throwIfCancellationRequested(config);
            if (reason && reason.response) {
              reason.response.data = transformData.call(
                config,
                reason.response.data,
                reason.response.headers,
                config.transformResponse
              );
            }
          }
          return Promise.reject(reason);
        });
      };
    }
  });
  var require_mergeConfig = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/mergeConfig.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      module22.exports = function mergeConfig(config1, config2) {
        config2 = config2 || {};
        var config = {};
        function getMergedValue(target, source) {
          if (utils.isPlainObject(target) && utils.isPlainObject(source)) {
            return utils.merge(target, source);
          } else if (utils.isPlainObject(source)) {
            return utils.merge({}, source);
          } else if (utils.isArray(source)) {
            return source.slice();
          }
          return source;
        }
        function mergeDeepProperties(prop) {
          if (!utils.isUndefined(config2[prop])) {
            return getMergedValue(config1[prop], config2[prop]);
          } else if (!utils.isUndefined(config1[prop])) {
            return getMergedValue(void 0, config1[prop]);
          }
        }
        function valueFromConfig2(prop) {
          if (!utils.isUndefined(config2[prop])) {
            return getMergedValue(void 0, config2[prop]);
          }
        }
        function defaultToConfig2(prop) {
          if (!utils.isUndefined(config2[prop])) {
            return getMergedValue(void 0, config2[prop]);
          } else if (!utils.isUndefined(config1[prop])) {
            return getMergedValue(void 0, config1[prop]);
          }
        }
        function mergeDirectKeys(prop) {
          if (prop in config2) {
            return getMergedValue(config1[prop], config2[prop]);
          } else if (prop in config1) {
            return getMergedValue(void 0, config1[prop]);
          }
        }
        var mergeMap = {
          "url": valueFromConfig2,
          "method": valueFromConfig2,
          "data": valueFromConfig2,
          "baseURL": defaultToConfig2,
          "transformRequest": defaultToConfig2,
          "transformResponse": defaultToConfig2,
          "paramsSerializer": defaultToConfig2,
          "timeout": defaultToConfig2,
          "timeoutMessage": defaultToConfig2,
          "withCredentials": defaultToConfig2,
          "adapter": defaultToConfig2,
          "responseType": defaultToConfig2,
          "xsrfCookieName": defaultToConfig2,
          "xsrfHeaderName": defaultToConfig2,
          "onUploadProgress": defaultToConfig2,
          "onDownloadProgress": defaultToConfig2,
          "decompress": defaultToConfig2,
          "maxContentLength": defaultToConfig2,
          "maxBodyLength": defaultToConfig2,
          "beforeRedirect": defaultToConfig2,
          "transport": defaultToConfig2,
          "httpAgent": defaultToConfig2,
          "httpsAgent": defaultToConfig2,
          "cancelToken": defaultToConfig2,
          "socketPath": defaultToConfig2,
          "responseEncoding": defaultToConfig2,
          "validateStatus": mergeDirectKeys
        };
        utils.forEach(Object.keys(config1).concat(Object.keys(config2)), function computeConfigValue(prop) {
          var merge = mergeMap[prop] || mergeDeepProperties;
          var configValue = merge(prop);
          utils.isUndefined(configValue) && merge !== mergeDirectKeys || (config[prop] = configValue);
        });
        return config;
      };
    }
  });
  var require_validator = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/validator.js"(exports2, module22) {
      "use strict";
      var VERSION = require_data().version;
      var AxiosError = require_AxiosError();
      var validators = {};
      ["object", "boolean", "number", "function", "string", "symbol"].forEach(function(type, i) {
        validators[type] = function validator(thing) {
          return typeof thing === type || "a" + (i < 1 ? "n " : " ") + type;
        };
      });
      var deprecatedWarnings = {};
      validators.transitional = function transitional(validator, version, message) {
        function formatMessage(opt, desc) {
          return "[Axios v" + VERSION + "] Transitional option '" + opt + "'" + desc + (message ? ". " + message : "");
        }
        return function(value, opt, opts) {
          if (validator === false) {
            throw new AxiosError(
              formatMessage(opt, " has been removed" + (version ? " in " + version : "")),
              AxiosError.ERR_DEPRECATED
            );
          }
          if (version && !deprecatedWarnings[opt]) {
            deprecatedWarnings[opt] = true;
            console.warn(
              formatMessage(
                opt,
                " has been deprecated since v" + version + " and will be removed in the near future"
              )
            );
          }
          return validator ? validator(value, opt, opts) : true;
        };
      };
      function assertOptions(options, schema, allowUnknown) {
        if (typeof options !== "object") {
          throw new AxiosError("options must be an object", AxiosError.ERR_BAD_OPTION_VALUE);
        }
        var keys = Object.keys(options);
        var i = keys.length;
        while (i-- > 0) {
          var opt = keys[i];
          var validator = schema[opt];
          if (validator) {
            var value = options[opt];
            var result = value === void 0 || validator(value, opt, options);
            if (result !== true) {
              throw new AxiosError("option " + opt + " must be " + result, AxiosError.ERR_BAD_OPTION_VALUE);
            }
            continue;
          }
          if (allowUnknown !== true) {
            throw new AxiosError("Unknown option " + opt, AxiosError.ERR_BAD_OPTION);
          }
        }
      }
      module22.exports = {
        assertOptions,
        validators
      };
    }
  });
  var require_Axios = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/core/Axios.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var buildURL = require_buildURL();
      var InterceptorManager = require_InterceptorManager();
      var dispatchRequest = require_dispatchRequest();
      var mergeConfig = require_mergeConfig();
      var buildFullPath = require_buildFullPath();
      var validator = require_validator();
      var validators = validator.validators;
      function Axios(instanceConfig) {
        this.defaults = instanceConfig;
        this.interceptors = {
          request: new InterceptorManager(),
          response: new InterceptorManager()
        };
      }
      Axios.prototype.request = function request(configOrUrl, config) {
        if (typeof configOrUrl === "string") {
          config = config || {};
          config.url = configOrUrl;
        } else {
          config = configOrUrl || {};
        }
        config = mergeConfig(this.defaults, config);
        if (config.method) {
          config.method = config.method.toLowerCase();
        } else if (this.defaults.method) {
          config.method = this.defaults.method.toLowerCase();
        } else {
          config.method = "get";
        }
        var transitional = config.transitional;
        if (transitional !== void 0) {
          validator.assertOptions(transitional, {
            silentJSONParsing: validators.transitional(validators.boolean),
            forcedJSONParsing: validators.transitional(validators.boolean),
            clarifyTimeoutError: validators.transitional(validators.boolean)
          }, false);
        }
        var requestInterceptorChain = [];
        var synchronousRequestInterceptors = true;
        this.interceptors.request.forEach(function unshiftRequestInterceptors(interceptor) {
          if (typeof interceptor.runWhen === "function" && interceptor.runWhen(config) === false) {
            return;
          }
          synchronousRequestInterceptors = synchronousRequestInterceptors && interceptor.synchronous;
          requestInterceptorChain.unshift(interceptor.fulfilled, interceptor.rejected);
        });
        var responseInterceptorChain = [];
        this.interceptors.response.forEach(function pushResponseInterceptors(interceptor) {
          responseInterceptorChain.push(interceptor.fulfilled, interceptor.rejected);
        });
        var promise;
        if (!synchronousRequestInterceptors) {
          var chain = [dispatchRequest, void 0];
          Array.prototype.unshift.apply(chain, requestInterceptorChain);
          chain = chain.concat(responseInterceptorChain);
          promise = Promise.resolve(config);
          while (chain.length) {
            promise = promise.then(chain.shift(), chain.shift());
          }
          return promise;
        }
        var newConfig = config;
        while (requestInterceptorChain.length) {
          var onFulfilled = requestInterceptorChain.shift();
          var onRejected = requestInterceptorChain.shift();
          try {
            newConfig = onFulfilled(newConfig);
          } catch (error) {
            onRejected(error);
            break;
          }
        }
        try {
          promise = dispatchRequest(newConfig);
        } catch (error) {
          return Promise.reject(error);
        }
        while (responseInterceptorChain.length) {
          promise = promise.then(responseInterceptorChain.shift(), responseInterceptorChain.shift());
        }
        return promise;
      };
      Axios.prototype.getUri = function getUri(config) {
        config = mergeConfig(this.defaults, config);
        var fullPath = buildFullPath(config.baseURL, config.url);
        return buildURL(fullPath, config.params, config.paramsSerializer);
      };
      utils.forEach(["delete", "get", "head", "options"], function forEachMethodNoData(method) {
        Axios.prototype[method] = function(url, config) {
          return this.request(mergeConfig(config || {}, {
            method,
            url,
            data: (config || {}).data
          }));
        };
      });
      utils.forEach(["post", "put", "patch"], function forEachMethodWithData(method) {
        function generateHTTPMethod(isForm) {
          return function httpMethod(url, data, config) {
            return this.request(mergeConfig(config || {}, {
              method,
              headers: isForm ? {
                "Content-Type": "multipart/form-data"
              } : {},
              url,
              data
            }));
          };
        }
        Axios.prototype[method] = generateHTTPMethod();
        Axios.prototype[method + "Form"] = generateHTTPMethod(true);
      });
      module22.exports = Axios;
    }
  });
  var require_CancelToken = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/cancel/CancelToken.js"(exports2, module22) {
      "use strict";
      var CanceledError = require_CanceledError();
      function CancelToken(executor) {
        if (typeof executor !== "function") {
          throw new TypeError("executor must be a function.");
        }
        var resolvePromise;
        this.promise = new Promise(function promiseExecutor(resolve) {
          resolvePromise = resolve;
        });
        var token = this;
        this.promise.then(function(cancel) {
          if (!token._listeners)
            return;
          var i;
          var l = token._listeners.length;
          for (i = 0; i < l; i++) {
            token._listeners[i](cancel);
          }
          token._listeners = null;
        });
        this.promise.then = function(onfulfilled) {
          var _resolve;
          var promise = new Promise(function(resolve) {
            token.subscribe(resolve);
            _resolve = resolve;
          }).then(onfulfilled);
          promise.cancel = function reject() {
            token.unsubscribe(_resolve);
          };
          return promise;
        };
        executor(function cancel(message) {
          if (token.reason) {
            return;
          }
          token.reason = new CanceledError(message);
          resolvePromise(token.reason);
        });
      }
      CancelToken.prototype.throwIfRequested = function throwIfRequested() {
        if (this.reason) {
          throw this.reason;
        }
      };
      CancelToken.prototype.subscribe = function subscribe(listener) {
        if (this.reason) {
          listener(this.reason);
          return;
        }
        if (this._listeners) {
          this._listeners.push(listener);
        } else {
          this._listeners = [listener];
        }
      };
      CancelToken.prototype.unsubscribe = function unsubscribe(listener) {
        if (!this._listeners) {
          return;
        }
        var index = this._listeners.indexOf(listener);
        if (index !== -1) {
          this._listeners.splice(index, 1);
        }
      };
      CancelToken.source = function source() {
        var cancel;
        var token = new CancelToken(function executor(c) {
          cancel = c;
        });
        return {
          token,
          cancel
        };
      };
      module22.exports = CancelToken;
    }
  });
  var require_spread = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/spread.js"(exports2, module22) {
      "use strict";
      module22.exports = function spread(callback) {
        return function wrap(arr) {
          return callback.apply(null, arr);
        };
      };
    }
  });
  var require_isAxiosError = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/helpers/isAxiosError.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      module22.exports = function isAxiosError(payload) {
        return utils.isObject(payload) && payload.isAxiosError === true;
      };
    }
  });
  var require_axios = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/lib/axios.js"(exports2, module22) {
      "use strict";
      var utils = require_utils();
      var bind = require_bind();
      var Axios = require_Axios();
      var mergeConfig = require_mergeConfig();
      var defaults = require_defaults();
      function createInstance(defaultConfig) {
        var context = new Axios(defaultConfig);
        var instance = bind(Axios.prototype.request, context);
        utils.extend(instance, Axios.prototype, context);
        utils.extend(instance, context);
        instance.create = function create(instanceConfig) {
          return createInstance(mergeConfig(defaultConfig, instanceConfig));
        };
        return instance;
      }
      var axios = createInstance(defaults);
      axios.Axios = Axios;
      axios.CanceledError = require_CanceledError();
      axios.CancelToken = require_CancelToken();
      axios.isCancel = require_isCancel();
      axios.VERSION = require_data().version;
      axios.toFormData = require_toFormData();
      axios.AxiosError = require_AxiosError();
      axios.Cancel = axios.CanceledError;
      axios.all = function all(promises) {
        return Promise.all(promises);
      };
      axios.spread = require_spread();
      axios.isAxiosError = require_isAxiosError();
      module22.exports = axios;
      module22.exports.default = axios;
    }
  });
  var require_axios2 = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/axios@0.27.2/node_modules/axios/index.js"(exports2, module22) {
      module22.exports = require_axios();
    }
  });
  var require_high_level_opt = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/high-level-opt.js"(exports2, module22) {
      "use strict";
      var argmap = /* @__PURE__ */ new Map([
        ["C", "cwd"],
        ["f", "file"],
        ["z", "gzip"],
        ["P", "preservePaths"],
        ["U", "unlink"],
        ["strip-components", "strip"],
        ["stripComponents", "strip"],
        ["keep-newer", "newer"],
        ["keepNewer", "newer"],
        ["keep-newer-files", "newer"],
        ["keepNewerFiles", "newer"],
        ["k", "keep"],
        ["keep-existing", "keep"],
        ["keepExisting", "keep"],
        ["m", "noMtime"],
        ["no-mtime", "noMtime"],
        ["p", "preserveOwner"],
        ["L", "follow"],
        ["h", "follow"]
      ]);
      module22.exports = (opt) => opt ? Object.keys(opt).map((k) => [
        argmap.has(k) ? argmap.get(k) : k,
        opt[k]
      ]).reduce((set, kv) => (set[kv[0]] = kv[1], set), /* @__PURE__ */ Object.create(null)) : {};
    }
  });
  var require_minipass = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/minipass@3.3.6/node_modules/minipass/index.js"(exports2, module22) {
      "use strict";
      var proc = typeof process === "object" && process ? process : {
        stdout: null,
        stderr: null
      };
      var EE = require2("events");
      var Stream = require2("stream");
      var SD = require2("string_decoder").StringDecoder;
      var EOF = Symbol("EOF");
      var MAYBE_EMIT_END = Symbol("maybeEmitEnd");
      var EMITTED_END = Symbol("emittedEnd");
      var EMITTING_END = Symbol("emittingEnd");
      var EMITTED_ERROR = Symbol("emittedError");
      var CLOSED = Symbol("closed");
      var READ = Symbol("read");
      var FLUSH = Symbol("flush");
      var FLUSHCHUNK = Symbol("flushChunk");
      var ENCODING = Symbol("encoding");
      var DECODER = Symbol("decoder");
      var FLOWING = Symbol("flowing");
      var PAUSED = Symbol("paused");
      var RESUME = Symbol("resume");
      var BUFFERLENGTH = Symbol("bufferLength");
      var BUFFERPUSH = Symbol("bufferPush");
      var BUFFERSHIFT = Symbol("bufferShift");
      var OBJECTMODE = Symbol("objectMode");
      var DESTROYED = Symbol("destroyed");
      var EMITDATA = Symbol("emitData");
      var EMITEND = Symbol("emitEnd");
      var EMITEND2 = Symbol("emitEnd2");
      var ASYNC = Symbol("async");
      var defer = (fn) => Promise.resolve().then(fn);
      var doIter = global2._MP_NO_ITERATOR_SYMBOLS_ !== "1";
      var ASYNCITERATOR = doIter && Symbol.asyncIterator || Symbol("asyncIterator not implemented");
      var ITERATOR = doIter && Symbol.iterator || Symbol("iterator not implemented");
      var isEndish = (ev) => ev === "end" || ev === "finish" || ev === "prefinish";
      var isArrayBuffer = (b) => b instanceof ArrayBuffer || typeof b === "object" && b.constructor && b.constructor.name === "ArrayBuffer" && b.byteLength >= 0;
      var isArrayBufferView = (b) => !Buffer.isBuffer(b) && ArrayBuffer.isView(b);
      var Pipe = class {
        constructor(src, dest, opts) {
          this.src = src;
          this.dest = dest;
          this.opts = opts;
          this.ondrain = () => src[RESUME]();
          dest.on("drain", this.ondrain);
        }
        unpipe() {
          this.dest.removeListener("drain", this.ondrain);
        }
        // istanbul ignore next - only here for the prototype
        proxyErrors() {
        }
        end() {
          this.unpipe();
          if (this.opts.end)
            this.dest.end();
        }
      };
      var PipeProxyErrors = class extends Pipe {
        unpipe() {
          this.src.removeListener("error", this.proxyErrors);
          super.unpipe();
        }
        constructor(src, dest, opts) {
          super(src, dest, opts);
          this.proxyErrors = (er) => dest.emit("error", er);
          src.on("error", this.proxyErrors);
        }
      };
      module22.exports = class Minipass extends Stream {
        constructor(options) {
          super();
          this[FLOWING] = false;
          this[PAUSED] = false;
          this.pipes = [];
          this.buffer = [];
          this[OBJECTMODE] = options && options.objectMode || false;
          if (this[OBJECTMODE])
            this[ENCODING] = null;
          else
            this[ENCODING] = options && options.encoding || null;
          if (this[ENCODING] === "buffer")
            this[ENCODING] = null;
          this[ASYNC] = options && !!options.async || false;
          this[DECODER] = this[ENCODING] ? new SD(this[ENCODING]) : null;
          this[EOF] = false;
          this[EMITTED_END] = false;
          this[EMITTING_END] = false;
          this[CLOSED] = false;
          this[EMITTED_ERROR] = null;
          this.writable = true;
          this.readable = true;
          this[BUFFERLENGTH] = 0;
          this[DESTROYED] = false;
        }
        get bufferLength() {
          return this[BUFFERLENGTH];
        }
        get encoding() {
          return this[ENCODING];
        }
        set encoding(enc) {
          if (this[OBJECTMODE])
            throw new Error("cannot set encoding in objectMode");
          if (this[ENCODING] && enc !== this[ENCODING] && (this[DECODER] && this[DECODER].lastNeed || this[BUFFERLENGTH]))
            throw new Error("cannot change encoding");
          if (this[ENCODING] !== enc) {
            this[DECODER] = enc ? new SD(enc) : null;
            if (this.buffer.length)
              this.buffer = this.buffer.map((chunk) => this[DECODER].write(chunk));
          }
          this[ENCODING] = enc;
        }
        setEncoding(enc) {
          this.encoding = enc;
        }
        get objectMode() {
          return this[OBJECTMODE];
        }
        set objectMode(om) {
          this[OBJECTMODE] = this[OBJECTMODE] || !!om;
        }
        get ["async"]() {
          return this[ASYNC];
        }
        set ["async"](a) {
          this[ASYNC] = this[ASYNC] || !!a;
        }
        write(chunk, encoding, cb) {
          if (this[EOF])
            throw new Error("write after end");
          if (this[DESTROYED]) {
            this.emit("error", Object.assign(
              new Error("Cannot call write after a stream was destroyed"),
              { code: "ERR_STREAM_DESTROYED" }
            ));
            return true;
          }
          if (typeof encoding === "function")
            cb = encoding, encoding = "utf8";
          if (!encoding)
            encoding = "utf8";
          const fn = this[ASYNC] ? defer : (f) => f();
          if (!this[OBJECTMODE] && !Buffer.isBuffer(chunk)) {
            if (isArrayBufferView(chunk))
              chunk = Buffer.from(chunk.buffer, chunk.byteOffset, chunk.byteLength);
            else if (isArrayBuffer(chunk))
              chunk = Buffer.from(chunk);
            else if (typeof chunk !== "string")
              this.objectMode = true;
          }
          if (this[OBJECTMODE]) {
            if (this.flowing && this[BUFFERLENGTH] !== 0)
              this[FLUSH](true);
            if (this.flowing)
              this.emit("data", chunk);
            else
              this[BUFFERPUSH](chunk);
            if (this[BUFFERLENGTH] !== 0)
              this.emit("readable");
            if (cb)
              fn(cb);
            return this.flowing;
          }
          if (!chunk.length) {
            if (this[BUFFERLENGTH] !== 0)
              this.emit("readable");
            if (cb)
              fn(cb);
            return this.flowing;
          }
          if (typeof chunk === "string" && // unless it is a string already ready for us to use
          !(encoding === this[ENCODING] && !this[DECODER].lastNeed)) {
            chunk = Buffer.from(chunk, encoding);
          }
          if (Buffer.isBuffer(chunk) && this[ENCODING])
            chunk = this[DECODER].write(chunk);
          if (this.flowing && this[BUFFERLENGTH] !== 0)
            this[FLUSH](true);
          if (this.flowing)
            this.emit("data", chunk);
          else
            this[BUFFERPUSH](chunk);
          if (this[BUFFERLENGTH] !== 0)
            this.emit("readable");
          if (cb)
            fn(cb);
          return this.flowing;
        }
        read(n) {
          if (this[DESTROYED])
            return null;
          if (this[BUFFERLENGTH] === 0 || n === 0 || n > this[BUFFERLENGTH]) {
            this[MAYBE_EMIT_END]();
            return null;
          }
          if (this[OBJECTMODE])
            n = null;
          if (this.buffer.length > 1 && !this[OBJECTMODE]) {
            if (this.encoding)
              this.buffer = [this.buffer.join("")];
            else
              this.buffer = [Buffer.concat(this.buffer, this[BUFFERLENGTH])];
          }
          const ret = this[READ](n || null, this.buffer[0]);
          this[MAYBE_EMIT_END]();
          return ret;
        }
        [READ](n, chunk) {
          if (n === chunk.length || n === null)
            this[BUFFERSHIFT]();
          else {
            this.buffer[0] = chunk.slice(n);
            chunk = chunk.slice(0, n);
            this[BUFFERLENGTH] -= n;
          }
          this.emit("data", chunk);
          if (!this.buffer.length && !this[EOF])
            this.emit("drain");
          return chunk;
        }
        end(chunk, encoding, cb) {
          if (typeof chunk === "function")
            cb = chunk, chunk = null;
          if (typeof encoding === "function")
            cb = encoding, encoding = "utf8";
          if (chunk)
            this.write(chunk, encoding);
          if (cb)
            this.once("end", cb);
          this[EOF] = true;
          this.writable = false;
          if (this.flowing || !this[PAUSED])
            this[MAYBE_EMIT_END]();
          return this;
        }
        // don't let the internal resume be overwritten
        [RESUME]() {
          if (this[DESTROYED])
            return;
          this[PAUSED] = false;
          this[FLOWING] = true;
          this.emit("resume");
          if (this.buffer.length)
            this[FLUSH]();
          else if (this[EOF])
            this[MAYBE_EMIT_END]();
          else
            this.emit("drain");
        }
        resume() {
          return this[RESUME]();
        }
        pause() {
          this[FLOWING] = false;
          this[PAUSED] = true;
        }
        get destroyed() {
          return this[DESTROYED];
        }
        get flowing() {
          return this[FLOWING];
        }
        get paused() {
          return this[PAUSED];
        }
        [BUFFERPUSH](chunk) {
          if (this[OBJECTMODE])
            this[BUFFERLENGTH] += 1;
          else
            this[BUFFERLENGTH] += chunk.length;
          this.buffer.push(chunk);
        }
        [BUFFERSHIFT]() {
          if (this.buffer.length) {
            if (this[OBJECTMODE])
              this[BUFFERLENGTH] -= 1;
            else
              this[BUFFERLENGTH] -= this.buffer[0].length;
          }
          return this.buffer.shift();
        }
        [FLUSH](noDrain) {
          do {
          } while (this[FLUSHCHUNK](this[BUFFERSHIFT]()));
          if (!noDrain && !this.buffer.length && !this[EOF])
            this.emit("drain");
        }
        [FLUSHCHUNK](chunk) {
          return chunk ? (this.emit("data", chunk), this.flowing) : false;
        }
        pipe(dest, opts) {
          if (this[DESTROYED])
            return;
          const ended = this[EMITTED_END];
          opts = opts || {};
          if (dest === proc.stdout || dest === proc.stderr)
            opts.end = false;
          else
            opts.end = opts.end !== false;
          opts.proxyErrors = !!opts.proxyErrors;
          if (ended) {
            if (opts.end)
              dest.end();
          } else {
            this.pipes.push(!opts.proxyErrors ? new Pipe(this, dest, opts) : new PipeProxyErrors(this, dest, opts));
            if (this[ASYNC])
              defer(() => this[RESUME]());
            else
              this[RESUME]();
          }
          return dest;
        }
        unpipe(dest) {
          const p = this.pipes.find((p2) => p2.dest === dest);
          if (p) {
            this.pipes.splice(this.pipes.indexOf(p), 1);
            p.unpipe();
          }
        }
        addListener(ev, fn) {
          return this.on(ev, fn);
        }
        on(ev, fn) {
          const ret = super.on(ev, fn);
          if (ev === "data" && !this.pipes.length && !this.flowing)
            this[RESUME]();
          else if (ev === "readable" && this[BUFFERLENGTH] !== 0)
            super.emit("readable");
          else if (isEndish(ev) && this[EMITTED_END]) {
            super.emit(ev);
            this.removeAllListeners(ev);
          } else if (ev === "error" && this[EMITTED_ERROR]) {
            if (this[ASYNC])
              defer(() => fn.call(this, this[EMITTED_ERROR]));
            else
              fn.call(this, this[EMITTED_ERROR]);
          }
          return ret;
        }
        get emittedEnd() {
          return this[EMITTED_END];
        }
        [MAYBE_EMIT_END]() {
          if (!this[EMITTING_END] && !this[EMITTED_END] && !this[DESTROYED] && this.buffer.length === 0 && this[EOF]) {
            this[EMITTING_END] = true;
            this.emit("end");
            this.emit("prefinish");
            this.emit("finish");
            if (this[CLOSED])
              this.emit("close");
            this[EMITTING_END] = false;
          }
        }
        emit(ev, data, ...extra) {
          if (ev !== "error" && ev !== "close" && ev !== DESTROYED && this[DESTROYED])
            return;
          else if (ev === "data") {
            return !data ? false : this[ASYNC] ? defer(() => this[EMITDATA](data)) : this[EMITDATA](data);
          } else if (ev === "end") {
            return this[EMITEND]();
          } else if (ev === "close") {
            this[CLOSED] = true;
            if (!this[EMITTED_END] && !this[DESTROYED])
              return;
            const ret2 = super.emit("close");
            this.removeAllListeners("close");
            return ret2;
          } else if (ev === "error") {
            this[EMITTED_ERROR] = data;
            const ret2 = super.emit("error", data);
            this[MAYBE_EMIT_END]();
            return ret2;
          } else if (ev === "resume") {
            const ret2 = super.emit("resume");
            this[MAYBE_EMIT_END]();
            return ret2;
          } else if (ev === "finish" || ev === "prefinish") {
            const ret2 = super.emit(ev);
            this.removeAllListeners(ev);
            return ret2;
          }
          const ret = super.emit(ev, data, ...extra);
          this[MAYBE_EMIT_END]();
          return ret;
        }
        [EMITDATA](data) {
          for (const p of this.pipes) {
            if (p.dest.write(data) === false)
              this.pause();
          }
          const ret = super.emit("data", data);
          this[MAYBE_EMIT_END]();
          return ret;
        }
        [EMITEND]() {
          if (this[EMITTED_END])
            return;
          this[EMITTED_END] = true;
          this.readable = false;
          if (this[ASYNC])
            defer(() => this[EMITEND2]());
          else
            this[EMITEND2]();
        }
        [EMITEND2]() {
          if (this[DECODER]) {
            const data = this[DECODER].end();
            if (data) {
              for (const p of this.pipes) {
                p.dest.write(data);
              }
              super.emit("data", data);
            }
          }
          for (const p of this.pipes) {
            p.end();
          }
          const ret = super.emit("end");
          this.removeAllListeners("end");
          return ret;
        }
        // const all = await stream.collect()
        collect() {
          const buf = [];
          if (!this[OBJECTMODE])
            buf.dataLength = 0;
          const p = this.promise();
          this.on("data", (c) => {
            buf.push(c);
            if (!this[OBJECTMODE])
              buf.dataLength += c.length;
          });
          return p.then(() => buf);
        }
        // const data = await stream.concat()
        concat() {
          return this[OBJECTMODE] ? Promise.reject(new Error("cannot concat in objectMode")) : this.collect().then((buf) => this[OBJECTMODE] ? Promise.reject(new Error("cannot concat in objectMode")) : this[ENCODING] ? buf.join("") : Buffer.concat(buf, buf.dataLength));
        }
        // stream.promise().then(() => done, er => emitted error)
        promise() {
          return new Promise((resolve, reject) => {
            this.on(DESTROYED, () => reject(new Error("stream destroyed")));
            this.on("error", (er) => reject(er));
            this.on("end", () => resolve());
          });
        }
        // for await (let chunk of stream)
        [ASYNCITERATOR]() {
          const next = () => {
            const res = this.read();
            if (res !== null)
              return Promise.resolve({ done: false, value: res });
            if (this[EOF])
              return Promise.resolve({ done: true });
            let resolve = null;
            let reject = null;
            const onerr = (er) => {
              this.removeListener("data", ondata);
              this.removeListener("end", onend);
              reject(er);
            };
            const ondata = (value) => {
              this.removeListener("error", onerr);
              this.removeListener("end", onend);
              this.pause();
              resolve({ value, done: !!this[EOF] });
            };
            const onend = () => {
              this.removeListener("error", onerr);
              this.removeListener("data", ondata);
              resolve({ done: true });
            };
            const ondestroy = () => onerr(new Error("stream destroyed"));
            return new Promise((res2, rej) => {
              reject = rej;
              resolve = res2;
              this.once(DESTROYED, ondestroy);
              this.once("error", onerr);
              this.once("end", onend);
              this.once("data", ondata);
            });
          };
          return { next };
        }
        // for (let chunk of stream)
        [ITERATOR]() {
          const next = () => {
            const value = this.read();
            const done = value === null;
            return { value, done };
          };
          return { next };
        }
        destroy(er) {
          if (this[DESTROYED]) {
            if (er)
              this.emit("error", er);
            else
              this.emit(DESTROYED);
            return this;
          }
          this[DESTROYED] = true;
          this.buffer.length = 0;
          this[BUFFERLENGTH] = 0;
          if (typeof this.close === "function" && !this[CLOSED])
            this.close();
          if (er)
            this.emit("error", er);
          else
            this.emit(DESTROYED);
          return this;
        }
        static isStream(s) {
          return !!s && (s instanceof Minipass || s instanceof Stream || s instanceof EE && (typeof s.pipe === "function" || // readable
          typeof s.write === "function" && typeof s.end === "function"));
        }
      };
    }
  });
  var require_constants = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/minizlib@2.1.2/node_modules/minizlib/constants.js"(exports2, module22) {
      var realZlibConstants = require2("zlib").constants || /* istanbul ignore next */
      { ZLIB_VERNUM: 4736 };
      module22.exports = Object.freeze(Object.assign(/* @__PURE__ */ Object.create(null), {
        Z_NO_FLUSH: 0,
        Z_PARTIAL_FLUSH: 1,
        Z_SYNC_FLUSH: 2,
        Z_FULL_FLUSH: 3,
        Z_FINISH: 4,
        Z_BLOCK: 5,
        Z_OK: 0,
        Z_STREAM_END: 1,
        Z_NEED_DICT: 2,
        Z_ERRNO: -1,
        Z_STREAM_ERROR: -2,
        Z_DATA_ERROR: -3,
        Z_MEM_ERROR: -4,
        Z_BUF_ERROR: -5,
        Z_VERSION_ERROR: -6,
        Z_NO_COMPRESSION: 0,
        Z_BEST_SPEED: 1,
        Z_BEST_COMPRESSION: 9,
        Z_DEFAULT_COMPRESSION: -1,
        Z_FILTERED: 1,
        Z_HUFFMAN_ONLY: 2,
        Z_RLE: 3,
        Z_FIXED: 4,
        Z_DEFAULT_STRATEGY: 0,
        DEFLATE: 1,
        INFLATE: 2,
        GZIP: 3,
        GUNZIP: 4,
        DEFLATERAW: 5,
        INFLATERAW: 6,
        UNZIP: 7,
        BROTLI_DECODE: 8,
        BROTLI_ENCODE: 9,
        Z_MIN_WINDOWBITS: 8,
        Z_MAX_WINDOWBITS: 15,
        Z_DEFAULT_WINDOWBITS: 15,
        Z_MIN_CHUNK: 64,
        Z_MAX_CHUNK: Infinity,
        Z_DEFAULT_CHUNK: 16384,
        Z_MIN_MEMLEVEL: 1,
        Z_MAX_MEMLEVEL: 9,
        Z_DEFAULT_MEMLEVEL: 8,
        Z_MIN_LEVEL: -1,
        Z_MAX_LEVEL: 9,
        Z_DEFAULT_LEVEL: -1,
        BROTLI_OPERATION_PROCESS: 0,
        BROTLI_OPERATION_FLUSH: 1,
        BROTLI_OPERATION_FINISH: 2,
        BROTLI_OPERATION_EMIT_METADATA: 3,
        BROTLI_MODE_GENERIC: 0,
        BROTLI_MODE_TEXT: 1,
        BROTLI_MODE_FONT: 2,
        BROTLI_DEFAULT_MODE: 0,
        BROTLI_MIN_QUALITY: 0,
        BROTLI_MAX_QUALITY: 11,
        BROTLI_DEFAULT_QUALITY: 11,
        BROTLI_MIN_WINDOW_BITS: 10,
        BROTLI_MAX_WINDOW_BITS: 24,
        BROTLI_LARGE_MAX_WINDOW_BITS: 30,
        BROTLI_DEFAULT_WINDOW: 22,
        BROTLI_MIN_INPUT_BLOCK_BITS: 16,
        BROTLI_MAX_INPUT_BLOCK_BITS: 24,
        BROTLI_PARAM_MODE: 0,
        BROTLI_PARAM_QUALITY: 1,
        BROTLI_PARAM_LGWIN: 2,
        BROTLI_PARAM_LGBLOCK: 3,
        BROTLI_PARAM_DISABLE_LITERAL_CONTEXT_MODELING: 4,
        BROTLI_PARAM_SIZE_HINT: 5,
        BROTLI_PARAM_LARGE_WINDOW: 6,
        BROTLI_PARAM_NPOSTFIX: 7,
        BROTLI_PARAM_NDIRECT: 8,
        BROTLI_DECODER_RESULT_ERROR: 0,
        BROTLI_DECODER_RESULT_SUCCESS: 1,
        BROTLI_DECODER_RESULT_NEEDS_MORE_INPUT: 2,
        BROTLI_DECODER_RESULT_NEEDS_MORE_OUTPUT: 3,
        BROTLI_DECODER_PARAM_DISABLE_RING_BUFFER_REALLOCATION: 0,
        BROTLI_DECODER_PARAM_LARGE_WINDOW: 1,
        BROTLI_DECODER_NO_ERROR: 0,
        BROTLI_DECODER_SUCCESS: 1,
        BROTLI_DECODER_NEEDS_MORE_INPUT: 2,
        BROTLI_DECODER_NEEDS_MORE_OUTPUT: 3,
        BROTLI_DECODER_ERROR_FORMAT_EXUBERANT_NIBBLE: -1,
        BROTLI_DECODER_ERROR_FORMAT_RESERVED: -2,
        BROTLI_DECODER_ERROR_FORMAT_EXUBERANT_META_NIBBLE: -3,
        BROTLI_DECODER_ERROR_FORMAT_SIMPLE_HUFFMAN_ALPHABET: -4,
        BROTLI_DECODER_ERROR_FORMAT_SIMPLE_HUFFMAN_SAME: -5,
        BROTLI_DECODER_ERROR_FORMAT_CL_SPACE: -6,
        BROTLI_DECODER_ERROR_FORMAT_HUFFMAN_SPACE: -7,
        BROTLI_DECODER_ERROR_FORMAT_CONTEXT_MAP_REPEAT: -8,
        BROTLI_DECODER_ERROR_FORMAT_BLOCK_LENGTH_1: -9,
        BROTLI_DECODER_ERROR_FORMAT_BLOCK_LENGTH_2: -10,
        BROTLI_DECODER_ERROR_FORMAT_TRANSFORM: -11,
        BROTLI_DECODER_ERROR_FORMAT_DICTIONARY: -12,
        BROTLI_DECODER_ERROR_FORMAT_WINDOW_BITS: -13,
        BROTLI_DECODER_ERROR_FORMAT_PADDING_1: -14,
        BROTLI_DECODER_ERROR_FORMAT_PADDING_2: -15,
        BROTLI_DECODER_ERROR_FORMAT_DISTANCE: -16,
        BROTLI_DECODER_ERROR_DICTIONARY_NOT_SET: -19,
        BROTLI_DECODER_ERROR_INVALID_ARGUMENTS: -20,
        BROTLI_DECODER_ERROR_ALLOC_CONTEXT_MODES: -21,
        BROTLI_DECODER_ERROR_ALLOC_TREE_GROUPS: -22,
        BROTLI_DECODER_ERROR_ALLOC_CONTEXT_MAP: -25,
        BROTLI_DECODER_ERROR_ALLOC_RING_BUFFER_1: -26,
        BROTLI_DECODER_ERROR_ALLOC_RING_BUFFER_2: -27,
        BROTLI_DECODER_ERROR_ALLOC_BLOCK_TYPE_TREES: -30,
        BROTLI_DECODER_ERROR_UNREACHABLE: -31
      }, realZlibConstants));
    }
  });
  var require_minizlib = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/minizlib@2.1.2/node_modules/minizlib/index.js"(exports2) {
      "use strict";
      var assert = require2("assert");
      var Buffer2 = require2("buffer").Buffer;
      var realZlib = require2("zlib");
      var constants = exports2.constants = require_constants();
      var Minipass = require_minipass();
      var OriginalBufferConcat = Buffer2.concat;
      var _superWrite = Symbol("_superWrite");
      var ZlibError = class extends Error {
        constructor(err) {
          super("zlib: " + err.message);
          this.code = err.code;
          this.errno = err.errno;
          if (!this.code)
            this.code = "ZLIB_ERROR";
          this.message = "zlib: " + err.message;
          Error.captureStackTrace(this, this.constructor);
        }
        get name() {
          return "ZlibError";
        }
      };
      var _opts = Symbol("opts");
      var _flushFlag = Symbol("flushFlag");
      var _finishFlushFlag = Symbol("finishFlushFlag");
      var _fullFlushFlag = Symbol("fullFlushFlag");
      var _handle = Symbol("handle");
      var _onError = Symbol("onError");
      var _sawError = Symbol("sawError");
      var _level = Symbol("level");
      var _strategy = Symbol("strategy");
      var _ended = Symbol("ended");
      var _defaultFullFlush = Symbol("_defaultFullFlush");
      var ZlibBase = class extends Minipass {
        constructor(opts, mode) {
          if (!opts || typeof opts !== "object")
            throw new TypeError("invalid options for ZlibBase constructor");
          super(opts);
          this[_sawError] = false;
          this[_ended] = false;
          this[_opts] = opts;
          this[_flushFlag] = opts.flush;
          this[_finishFlushFlag] = opts.finishFlush;
          try {
            this[_handle] = new realZlib[mode](opts);
          } catch (er) {
            throw new ZlibError(er);
          }
          this[_onError] = (err) => {
            if (this[_sawError])
              return;
            this[_sawError] = true;
            this.close();
            this.emit("error", err);
          };
          this[_handle].on("error", (er) => this[_onError](new ZlibError(er)));
          this.once("end", () => this.close);
        }
        close() {
          if (this[_handle]) {
            this[_handle].close();
            this[_handle] = null;
            this.emit("close");
          }
        }
        reset() {
          if (!this[_sawError]) {
            assert(this[_handle], "zlib binding closed");
            return this[_handle].reset();
          }
        }
        flush(flushFlag) {
          if (this.ended)
            return;
          if (typeof flushFlag !== "number")
            flushFlag = this[_fullFlushFlag];
          this.write(Object.assign(Buffer2.alloc(0), { [_flushFlag]: flushFlag }));
        }
        end(chunk, encoding, cb) {
          if (chunk)
            this.write(chunk, encoding);
          this.flush(this[_finishFlushFlag]);
          this[_ended] = true;
          return super.end(null, null, cb);
        }
        get ended() {
          return this[_ended];
        }
        write(chunk, encoding, cb) {
          if (typeof encoding === "function")
            cb = encoding, encoding = "utf8";
          if (typeof chunk === "string")
            chunk = Buffer2.from(chunk, encoding);
          if (this[_sawError])
            return;
          assert(this[_handle], "zlib binding closed");
          const nativeHandle = this[_handle]._handle;
          const originalNativeClose = nativeHandle.close;
          nativeHandle.close = () => {
          };
          const originalClose = this[_handle].close;
          this[_handle].close = () => {
          };
          Buffer2.concat = (args) => args;
          let result;
          try {
            const flushFlag = typeof chunk[_flushFlag] === "number" ? chunk[_flushFlag] : this[_flushFlag];
            result = this[_handle]._processChunk(chunk, flushFlag);
            Buffer2.concat = OriginalBufferConcat;
          } catch (err) {
            Buffer2.concat = OriginalBufferConcat;
            this[_onError](new ZlibError(err));
          } finally {
            if (this[_handle]) {
              this[_handle]._handle = nativeHandle;
              nativeHandle.close = originalNativeClose;
              this[_handle].close = originalClose;
              this[_handle].removeAllListeners("error");
            }
          }
          if (this[_handle])
            this[_handle].on("error", (er) => this[_onError](new ZlibError(er)));
          let writeReturn;
          if (result) {
            if (Array.isArray(result) && result.length > 0) {
              writeReturn = this[_superWrite](Buffer2.from(result[0]));
              for (let i = 1; i < result.length; i++) {
                writeReturn = this[_superWrite](result[i]);
              }
            } else {
              writeReturn = this[_superWrite](Buffer2.from(result));
            }
          }
          if (cb)
            cb();
          return writeReturn;
        }
        [_superWrite](data) {
          return super.write(data);
        }
      };
      var Zlib = class extends ZlibBase {
        constructor(opts, mode) {
          opts = opts || {};
          opts.flush = opts.flush || constants.Z_NO_FLUSH;
          opts.finishFlush = opts.finishFlush || constants.Z_FINISH;
          super(opts, mode);
          this[_fullFlushFlag] = constants.Z_FULL_FLUSH;
          this[_level] = opts.level;
          this[_strategy] = opts.strategy;
        }
        params(level, strategy) {
          if (this[_sawError])
            return;
          if (!this[_handle])
            throw new Error("cannot switch params when binding is closed");
          if (!this[_handle].params)
            throw new Error("not supported in this implementation");
          if (this[_level] !== level || this[_strategy] !== strategy) {
            this.flush(constants.Z_SYNC_FLUSH);
            assert(this[_handle], "zlib binding closed");
            const origFlush = this[_handle].flush;
            this[_handle].flush = (flushFlag, cb) => {
              this.flush(flushFlag);
              cb();
            };
            try {
              this[_handle].params(level, strategy);
            } finally {
              this[_handle].flush = origFlush;
            }
            if (this[_handle]) {
              this[_level] = level;
              this[_strategy] = strategy;
            }
          }
        }
      };
      var Deflate = class extends Zlib {
        constructor(opts) {
          super(opts, "Deflate");
        }
      };
      var Inflate = class extends Zlib {
        constructor(opts) {
          super(opts, "Inflate");
        }
      };
      var _portable = Symbol("_portable");
      var Gzip = class extends Zlib {
        constructor(opts) {
          super(opts, "Gzip");
          this[_portable] = opts && !!opts.portable;
        }
        [_superWrite](data) {
          if (!this[_portable])
            return super[_superWrite](data);
          this[_portable] = false;
          data[9] = 255;
          return super[_superWrite](data);
        }
      };
      var Gunzip = class extends Zlib {
        constructor(opts) {
          super(opts, "Gunzip");
        }
      };
      var DeflateRaw = class extends Zlib {
        constructor(opts) {
          super(opts, "DeflateRaw");
        }
      };
      var InflateRaw = class extends Zlib {
        constructor(opts) {
          super(opts, "InflateRaw");
        }
      };
      var Unzip = class extends Zlib {
        constructor(opts) {
          super(opts, "Unzip");
        }
      };
      var Brotli = class extends ZlibBase {
        constructor(opts, mode) {
          opts = opts || {};
          opts.flush = opts.flush || constants.BROTLI_OPERATION_PROCESS;
          opts.finishFlush = opts.finishFlush || constants.BROTLI_OPERATION_FINISH;
          super(opts, mode);
          this[_fullFlushFlag] = constants.BROTLI_OPERATION_FLUSH;
        }
      };
      var BrotliCompress = class extends Brotli {
        constructor(opts) {
          super(opts, "BrotliCompress");
        }
      };
      var BrotliDecompress = class extends Brotli {
        constructor(opts) {
          super(opts, "BrotliDecompress");
        }
      };
      exports2.Deflate = Deflate;
      exports2.Inflate = Inflate;
      exports2.Gzip = Gzip;
      exports2.Gunzip = Gunzip;
      exports2.DeflateRaw = DeflateRaw;
      exports2.InflateRaw = InflateRaw;
      exports2.Unzip = Unzip;
      if (typeof realZlib.BrotliCompress === "function") {
        exports2.BrotliCompress = BrotliCompress;
        exports2.BrotliDecompress = BrotliDecompress;
      } else {
        exports2.BrotliCompress = exports2.BrotliDecompress = class {
          constructor() {
            throw new Error("Brotli is not supported in this version of Node.js");
          }
        };
      }
    }
  });
  var require_normalize_windows_path = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/normalize-windows-path.js"(exports2, module22) {
      var platform = process.env.TESTING_TAR_FAKE_PLATFORM || process.platform;
      module22.exports = platform !== "win32" ? (p) => p : (p) => p && p.replace(/\\/g, "/");
    }
  });
  var require_read_entry = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/read-entry.js"(exports2, module22) {
      "use strict";
      var MiniPass = require_minipass();
      var normPath = require_normalize_windows_path();
      var SLURP = Symbol("slurp");
      module22.exports = class ReadEntry extends MiniPass {
        constructor(header, ex, gex) {
          super();
          this.pause();
          this.extended = ex;
          this.globalExtended = gex;
          this.header = header;
          this.startBlockSize = 512 * Math.ceil(header.size / 512);
          this.blockRemain = this.startBlockSize;
          this.remain = header.size;
          this.type = header.type;
          this.meta = false;
          this.ignore = false;
          switch (this.type) {
            case "File":
            case "OldFile":
            case "Link":
            case "SymbolicLink":
            case "CharacterDevice":
            case "BlockDevice":
            case "Directory":
            case "FIFO":
            case "ContiguousFile":
            case "GNUDumpDir":
              break;
            case "NextFileHasLongLinkpath":
            case "NextFileHasLongPath":
            case "OldGnuLongPath":
            case "GlobalExtendedHeader":
            case "ExtendedHeader":
            case "OldExtendedHeader":
              this.meta = true;
              break;
            default:
              this.ignore = true;
          }
          this.path = normPath(header.path);
          this.mode = header.mode;
          if (this.mode)
            this.mode = this.mode & 4095;
          this.uid = header.uid;
          this.gid = header.gid;
          this.uname = header.uname;
          this.gname = header.gname;
          this.size = header.size;
          this.mtime = header.mtime;
          this.atime = header.atime;
          this.ctime = header.ctime;
          this.linkpath = normPath(header.linkpath);
          this.uname = header.uname;
          this.gname = header.gname;
          if (ex)
            this[SLURP](ex);
          if (gex)
            this[SLURP](gex, true);
        }
        write(data) {
          const writeLen = data.length;
          if (writeLen > this.blockRemain)
            throw new Error("writing more to entry than is appropriate");
          const r = this.remain;
          const br = this.blockRemain;
          this.remain = Math.max(0, r - writeLen);
          this.blockRemain = Math.max(0, br - writeLen);
          if (this.ignore)
            return true;
          if (r >= writeLen)
            return super.write(data);
          return super.write(data.slice(0, r));
        }
        [SLURP](ex, global22) {
          for (const k in ex) {
            if (ex[k] !== null && ex[k] !== void 0 && !(global22 && k === "path"))
              this[k] = k === "path" || k === "linkpath" ? normPath(ex[k]) : ex[k];
          }
        }
      };
    }
  });
  var require_types = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/types.js"(exports2) {
      "use strict";
      exports2.name = /* @__PURE__ */ new Map([
        ["0", "File"],
        // same as File
        ["", "OldFile"],
        ["1", "Link"],
        ["2", "SymbolicLink"],
        // Devices and FIFOs aren't fully supported
        // they are parsed, but skipped when unpacking
        ["3", "CharacterDevice"],
        ["4", "BlockDevice"],
        ["5", "Directory"],
        ["6", "FIFO"],
        // same as File
        ["7", "ContiguousFile"],
        // pax headers
        ["g", "GlobalExtendedHeader"],
        ["x", "ExtendedHeader"],
        // vendor-specific stuff
        // skip
        ["A", "SolarisACL"],
        // like 5, but with data, which should be skipped
        ["D", "GNUDumpDir"],
        // metadata only, skip
        ["I", "Inode"],
        // data = link path of next file
        ["K", "NextFileHasLongLinkpath"],
        // data = path of next file
        ["L", "NextFileHasLongPath"],
        // skip
        ["M", "ContinuationFile"],
        // like L
        ["N", "OldGnuLongPath"],
        // skip
        ["S", "SparseFile"],
        // skip
        ["V", "TapeVolumeHeader"],
        // like x
        ["X", "OldExtendedHeader"]
      ]);
      exports2.code = new Map(Array.from(exports2.name).map((kv) => [kv[1], kv[0]]));
    }
  });
  var require_large_numbers = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/large-numbers.js"(exports2, module22) {
      "use strict";
      var encode = (num, buf) => {
        if (!Number.isSafeInteger(num))
          throw Error("cannot encode number outside of javascript safe integer range");
        else if (num < 0)
          encodeNegative(num, buf);
        else
          encodePositive(num, buf);
        return buf;
      };
      var encodePositive = (num, buf) => {
        buf[0] = 128;
        for (var i = buf.length; i > 1; i--) {
          buf[i - 1] = num & 255;
          num = Math.floor(num / 256);
        }
      };
      var encodeNegative = (num, buf) => {
        buf[0] = 255;
        var flipped = false;
        num = num * -1;
        for (var i = buf.length; i > 1; i--) {
          var byte = num & 255;
          num = Math.floor(num / 256);
          if (flipped)
            buf[i - 1] = onesComp(byte);
          else if (byte === 0)
            buf[i - 1] = 0;
          else {
            flipped = true;
            buf[i - 1] = twosComp(byte);
          }
        }
      };
      var parse = (buf) => {
        const pre = buf[0];
        const value = pre === 128 ? pos(buf.slice(1, buf.length)) : pre === 255 ? twos(buf) : null;
        if (value === null)
          throw Error("invalid base256 encoding");
        if (!Number.isSafeInteger(value))
          throw Error("parsed number outside of javascript safe integer range");
        return value;
      };
      var twos = (buf) => {
        var len = buf.length;
        var sum = 0;
        var flipped = false;
        for (var i = len - 1; i > -1; i--) {
          var byte = buf[i];
          var f;
          if (flipped)
            f = onesComp(byte);
          else if (byte === 0)
            f = byte;
          else {
            flipped = true;
            f = twosComp(byte);
          }
          if (f !== 0)
            sum -= f * Math.pow(256, len - i - 1);
        }
        return sum;
      };
      var pos = (buf) => {
        var len = buf.length;
        var sum = 0;
        for (var i = len - 1; i > -1; i--) {
          var byte = buf[i];
          if (byte !== 0)
            sum += byte * Math.pow(256, len - i - 1);
        }
        return sum;
      };
      var onesComp = (byte) => (255 ^ byte) & 255;
      var twosComp = (byte) => (255 ^ byte) + 1 & 255;
      module22.exports = {
        encode,
        parse
      };
    }
  });
  var require_header = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/header.js"(exports2, module22) {
      "use strict";
      var types = require_types();
      var pathModule = require2("path").posix;
      var large = require_large_numbers();
      var SLURP = Symbol("slurp");
      var TYPE = Symbol("type");
      var Header = class {
        constructor(data, off, ex, gex) {
          this.cksumValid = false;
          this.needPax = false;
          this.nullBlock = false;
          this.block = null;
          this.path = null;
          this.mode = null;
          this.uid = null;
          this.gid = null;
          this.size = null;
          this.mtime = null;
          this.cksum = null;
          this[TYPE] = "0";
          this.linkpath = null;
          this.uname = null;
          this.gname = null;
          this.devmaj = 0;
          this.devmin = 0;
          this.atime = null;
          this.ctime = null;
          if (Buffer.isBuffer(data))
            this.decode(data, off || 0, ex, gex);
          else if (data)
            this.set(data);
        }
        decode(buf, off, ex, gex) {
          if (!off)
            off = 0;
          if (!buf || !(buf.length >= off + 512))
            throw new Error("need 512 bytes for header");
          this.path = decString(buf, off, 100);
          this.mode = decNumber(buf, off + 100, 8);
          this.uid = decNumber(buf, off + 108, 8);
          this.gid = decNumber(buf, off + 116, 8);
          this.size = decNumber(buf, off + 124, 12);
          this.mtime = decDate(buf, off + 136, 12);
          this.cksum = decNumber(buf, off + 148, 12);
          this[SLURP](ex);
          this[SLURP](gex, true);
          this[TYPE] = decString(buf, off + 156, 1);
          if (this[TYPE] === "")
            this[TYPE] = "0";
          if (this[TYPE] === "0" && this.path.substr(-1) === "/")
            this[TYPE] = "5";
          if (this[TYPE] === "5")
            this.size = 0;
          this.linkpath = decString(buf, off + 157, 100);
          if (buf.slice(off + 257, off + 265).toString() === "ustar\x0000") {
            this.uname = decString(buf, off + 265, 32);
            this.gname = decString(buf, off + 297, 32);
            this.devmaj = decNumber(buf, off + 329, 8);
            this.devmin = decNumber(buf, off + 337, 8);
            if (buf[off + 475] !== 0) {
              const prefix = decString(buf, off + 345, 155);
              this.path = prefix + "/" + this.path;
            } else {
              const prefix = decString(buf, off + 345, 130);
              if (prefix)
                this.path = prefix + "/" + this.path;
              this.atime = decDate(buf, off + 476, 12);
              this.ctime = decDate(buf, off + 488, 12);
            }
          }
          let sum = 8 * 32;
          for (let i = off; i < off + 148; i++)
            sum += buf[i];
          for (let i = off + 156; i < off + 512; i++)
            sum += buf[i];
          this.cksumValid = sum === this.cksum;
          if (this.cksum === null && sum === 8 * 32)
            this.nullBlock = true;
        }
        [SLURP](ex, global22) {
          for (const k in ex) {
            if (ex[k] !== null && ex[k] !== void 0 && !(global22 && k === "path"))
              this[k] = ex[k];
          }
        }
        encode(buf, off) {
          if (!buf) {
            buf = this.block = Buffer.alloc(512);
            off = 0;
          }
          if (!off)
            off = 0;
          if (!(buf.length >= off + 512))
            throw new Error("need 512 bytes for header");
          const prefixSize = this.ctime || this.atime ? 130 : 155;
          const split = splitPrefix(this.path || "", prefixSize);
          const path = split[0];
          const prefix = split[1];
          this.needPax = split[2];
          this.needPax = encString(buf, off, 100, path) || this.needPax;
          this.needPax = encNumber(buf, off + 100, 8, this.mode) || this.needPax;
          this.needPax = encNumber(buf, off + 108, 8, this.uid) || this.needPax;
          this.needPax = encNumber(buf, off + 116, 8, this.gid) || this.needPax;
          this.needPax = encNumber(buf, off + 124, 12, this.size) || this.needPax;
          this.needPax = encDate(buf, off + 136, 12, this.mtime) || this.needPax;
          buf[off + 156] = this[TYPE].charCodeAt(0);
          this.needPax = encString(buf, off + 157, 100, this.linkpath) || this.needPax;
          buf.write("ustar\x0000", off + 257, 8);
          this.needPax = encString(buf, off + 265, 32, this.uname) || this.needPax;
          this.needPax = encString(buf, off + 297, 32, this.gname) || this.needPax;
          this.needPax = encNumber(buf, off + 329, 8, this.devmaj) || this.needPax;
          this.needPax = encNumber(buf, off + 337, 8, this.devmin) || this.needPax;
          this.needPax = encString(buf, off + 345, prefixSize, prefix) || this.needPax;
          if (buf[off + 475] !== 0)
            this.needPax = encString(buf, off + 345, 155, prefix) || this.needPax;
          else {
            this.needPax = encString(buf, off + 345, 130, prefix) || this.needPax;
            this.needPax = encDate(buf, off + 476, 12, this.atime) || this.needPax;
            this.needPax = encDate(buf, off + 488, 12, this.ctime) || this.needPax;
          }
          let sum = 8 * 32;
          for (let i = off; i < off + 148; i++)
            sum += buf[i];
          for (let i = off + 156; i < off + 512; i++)
            sum += buf[i];
          this.cksum = sum;
          encNumber(buf, off + 148, 8, this.cksum);
          this.cksumValid = true;
          return this.needPax;
        }
        set(data) {
          for (const i in data) {
            if (data[i] !== null && data[i] !== void 0)
              this[i] = data[i];
          }
        }
        get type() {
          return types.name.get(this[TYPE]) || this[TYPE];
        }
        get typeKey() {
          return this[TYPE];
        }
        set type(type) {
          if (types.code.has(type))
            this[TYPE] = types.code.get(type);
          else
            this[TYPE] = type;
        }
      };
      var splitPrefix = (p, prefixSize) => {
        const pathSize = 100;
        let pp = p;
        let prefix = "";
        let ret;
        const root = pathModule.parse(p).root || ".";
        if (Buffer.byteLength(pp) < pathSize)
          ret = [pp, prefix, false];
        else {
          prefix = pathModule.dirname(pp);
          pp = pathModule.basename(pp);
          do {
            if (Buffer.byteLength(pp) <= pathSize && Buffer.byteLength(prefix) <= prefixSize)
              ret = [pp, prefix, false];
            else if (Buffer.byteLength(pp) > pathSize && Buffer.byteLength(prefix) <= prefixSize)
              ret = [pp.substr(0, pathSize - 1), prefix, true];
            else {
              pp = pathModule.join(pathModule.basename(prefix), pp);
              prefix = pathModule.dirname(prefix);
            }
          } while (prefix !== root && !ret);
          if (!ret)
            ret = [p.substr(0, pathSize - 1), "", true];
        }
        return ret;
      };
      var decString = (buf, off, size) => buf.slice(off, off + size).toString("utf8").replace(/\0.*/, "");
      var decDate = (buf, off, size) => numToDate(decNumber(buf, off, size));
      var numToDate = (num) => num === null ? null : new Date(num * 1e3);
      var decNumber = (buf, off, size) => buf[off] & 128 ? large.parse(buf.slice(off, off + size)) : decSmallNumber(buf, off, size);
      var nanNull = (value) => isNaN(value) ? null : value;
      var decSmallNumber = (buf, off, size) => nanNull(parseInt(
        buf.slice(off, off + size).toString("utf8").replace(/\0.*$/, "").trim(),
        8
      ));
      var MAXNUM = {
        12: 8589934591,
        8: 2097151
      };
      var encNumber = (buf, off, size, number) => number === null ? false : number > MAXNUM[size] || number < 0 ? (large.encode(number, buf.slice(off, off + size)), true) : (encSmallNumber(buf, off, size, number), false);
      var encSmallNumber = (buf, off, size, number) => buf.write(octalString(number, size), off, size, "ascii");
      var octalString = (number, size) => padOctal(Math.floor(number).toString(8), size);
      var padOctal = (string, size) => (string.length === size - 1 ? string : new Array(size - string.length - 1).join("0") + string + " ") + "\0";
      var encDate = (buf, off, size, date) => date === null ? false : encNumber(buf, off, size, date.getTime() / 1e3);
      var NULLS = new Array(156).join("\0");
      var encString = (buf, off, size, string) => string === null ? false : (buf.write(string + NULLS, off, size, "utf8"), string.length !== Buffer.byteLength(string) || string.length > size);
      module22.exports = Header;
    }
  });
  var require_pax = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/pax.js"(exports2, module22) {
      "use strict";
      var Header = require_header();
      var path = require2("path");
      var Pax = class {
        constructor(obj, global22) {
          this.atime = obj.atime || null;
          this.charset = obj.charset || null;
          this.comment = obj.comment || null;
          this.ctime = obj.ctime || null;
          this.gid = obj.gid || null;
          this.gname = obj.gname || null;
          this.linkpath = obj.linkpath || null;
          this.mtime = obj.mtime || null;
          this.path = obj.path || null;
          this.size = obj.size || null;
          this.uid = obj.uid || null;
          this.uname = obj.uname || null;
          this.dev = obj.dev || null;
          this.ino = obj.ino || null;
          this.nlink = obj.nlink || null;
          this.global = global22 || false;
        }
        encode() {
          const body = this.encodeBody();
          if (body === "")
            return null;
          const bodyLen = Buffer.byteLength(body);
          const bufLen = 512 * Math.ceil(1 + bodyLen / 512);
          const buf = Buffer.allocUnsafe(bufLen);
          for (let i = 0; i < 512; i++)
            buf[i] = 0;
          new Header({
            // XXX split the path
            // then the path should be PaxHeader + basename, but less than 99,
            // prepend with the dirname
            path: ("PaxHeader/" + path.basename(this.path)).slice(0, 99),
            mode: this.mode || 420,
            uid: this.uid || null,
            gid: this.gid || null,
            size: bodyLen,
            mtime: this.mtime || null,
            type: this.global ? "GlobalExtendedHeader" : "ExtendedHeader",
            linkpath: "",
            uname: this.uname || "",
            gname: this.gname || "",
            devmaj: 0,
            devmin: 0,
            atime: this.atime || null,
            ctime: this.ctime || null
          }).encode(buf);
          buf.write(body, 512, bodyLen, "utf8");
          for (let i = bodyLen + 512; i < buf.length; i++)
            buf[i] = 0;
          return buf;
        }
        encodeBody() {
          return this.encodeField("path") + this.encodeField("ctime") + this.encodeField("atime") + this.encodeField("dev") + this.encodeField("ino") + this.encodeField("nlink") + this.encodeField("charset") + this.encodeField("comment") + this.encodeField("gid") + this.encodeField("gname") + this.encodeField("linkpath") + this.encodeField("mtime") + this.encodeField("size") + this.encodeField("uid") + this.encodeField("uname");
        }
        encodeField(field) {
          if (this[field] === null || this[field] === void 0)
            return "";
          const v = this[field] instanceof Date ? this[field].getTime() / 1e3 : this[field];
          const s = " " + (field === "dev" || field === "ino" || field === "nlink" ? "SCHILY." : "") + field + "=" + v + "\n";
          const byteLen = Buffer.byteLength(s);
          let digits = Math.floor(Math.log(byteLen) / Math.log(10)) + 1;
          if (byteLen + digits >= Math.pow(10, digits))
            digits += 1;
          const len = digits + byteLen;
          return len + s;
        }
      };
      Pax.parse = (string, ex, g) => new Pax(merge(parseKV(string), ex), g);
      var merge = (a, b) => b ? Object.keys(a).reduce((s, k) => (s[k] = a[k], s), b) : a;
      var parseKV = (string) => string.replace(/\n$/, "").split("\n").reduce(parseKVLine, /* @__PURE__ */ Object.create(null));
      var parseKVLine = (set, line) => {
        const n = parseInt(line, 10);
        if (n !== Buffer.byteLength(line) + 1)
          return set;
        line = line.substr((n + " ").length);
        const kv = line.split("=");
        const k = kv.shift().replace(/^SCHILY\.(dev|ino|nlink)/, "$1");
        if (!k)
          return set;
        const v = kv.join("=");
        set[k] = /^([A-Z]+\.)?([mac]|birth|creation)time$/.test(k) ? new Date(v * 1e3) : /^[0-9]+$/.test(v) ? +v : v;
        return set;
      };
      module22.exports = Pax;
    }
  });
  var require_strip_trailing_slashes = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/strip-trailing-slashes.js"(exports2, module22) {
      module22.exports = (str) => {
        let i = str.length - 1;
        let slashesStart = -1;
        while (i > -1 && str.charAt(i) === "/") {
          slashesStart = i;
          i--;
        }
        return slashesStart === -1 ? str : str.slice(0, slashesStart);
      };
    }
  });
  var require_warn_mixin = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/warn-mixin.js"(exports2, module22) {
      "use strict";
      module22.exports = (Base) => class extends Base {
        warn(code, message, data = {}) {
          if (this.file)
            data.file = this.file;
          if (this.cwd)
            data.cwd = this.cwd;
          data.code = message instanceof Error && message.code || code;
          data.tarCode = code;
          if (!this.strict && data.recoverable !== false) {
            if (message instanceof Error) {
              data = Object.assign(message, data);
              message = message.message;
            }
            this.emit("warn", data.tarCode, message, data);
          } else if (message instanceof Error)
            this.emit("error", Object.assign(message, data));
          else
            this.emit("error", Object.assign(new Error(`${code}: ${message}`), data));
        }
      };
    }
  });
  var require_winchars = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/winchars.js"(exports2, module22) {
      "use strict";
      var raw = [
        "|",
        "<",
        ">",
        "?",
        ":"
      ];
      var win = raw.map((char) => String.fromCharCode(61440 + char.charCodeAt(0)));
      var toWin = new Map(raw.map((char, i) => [char, win[i]]));
      var toRaw = new Map(win.map((char, i) => [char, raw[i]]));
      module22.exports = {
        encode: (s) => raw.reduce((s2, c) => s2.split(c).join(toWin.get(c)), s),
        decode: (s) => win.reduce((s2, c) => s2.split(c).join(toRaw.get(c)), s)
      };
    }
  });
  var require_strip_absolute_path = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/strip-absolute-path.js"(exports2, module22) {
      var { isAbsolute, parse } = require2("path").win32;
      module22.exports = (path) => {
        let r = "";
        let parsed = parse(path);
        while (isAbsolute(path) || parsed.root) {
          const root = path.charAt(0) === "/" && path.slice(0, 4) !== "//?/" ? "/" : parsed.root;
          path = path.substr(root.length);
          r += root;
          parsed = parse(path);
        }
        return [r, path];
      };
    }
  });
  var require_mode_fix = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/mode-fix.js"(exports2, module22) {
      "use strict";
      module22.exports = (mode, isDir, portable) => {
        mode &= 4095;
        if (portable)
          mode = (mode | 384) & ~18;
        if (isDir) {
          if (mode & 256)
            mode |= 64;
          if (mode & 32)
            mode |= 8;
          if (mode & 4)
            mode |= 1;
        }
        return mode;
      };
    }
  });
  var require_write_entry = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/write-entry.js"(exports2, module22) {
      "use strict";
      var MiniPass = require_minipass();
      var Pax = require_pax();
      var Header = require_header();
      var fs = require2("fs");
      var path = require2("path");
      var normPath = require_normalize_windows_path();
      var stripSlash = require_strip_trailing_slashes();
      var prefixPath = (path2, prefix) => {
        if (!prefix)
          return normPath(path2);
        path2 = normPath(path2).replace(/^\.(\/|$)/, "");
        return stripSlash(prefix) + "/" + path2;
      };
      var maxReadSize = 16 * 1024 * 1024;
      var PROCESS = Symbol("process");
      var FILE = Symbol("file");
      var DIRECTORY = Symbol("directory");
      var SYMLINK = Symbol("symlink");
      var HARDLINK = Symbol("hardlink");
      var HEADER = Symbol("header");
      var READ = Symbol("read");
      var LSTAT = Symbol("lstat");
      var ONLSTAT = Symbol("onlstat");
      var ONREAD = Symbol("onread");
      var ONREADLINK = Symbol("onreadlink");
      var OPENFILE = Symbol("openfile");
      var ONOPENFILE = Symbol("onopenfile");
      var CLOSE = Symbol("close");
      var MODE = Symbol("mode");
      var AWAITDRAIN = Symbol("awaitDrain");
      var ONDRAIN = Symbol("ondrain");
      var PREFIX = Symbol("prefix");
      var HAD_ERROR = Symbol("hadError");
      var warner = require_warn_mixin();
      var winchars = require_winchars();
      var stripAbsolutePath = require_strip_absolute_path();
      var modeFix = require_mode_fix();
      var WriteEntry = warner(class WriteEntry extends MiniPass {
        constructor(p, opt) {
          opt = opt || {};
          super(opt);
          if (typeof p !== "string")
            throw new TypeError("path is required");
          this.path = normPath(p);
          this.portable = !!opt.portable;
          this.myuid = process.getuid && process.getuid() || 0;
          this.myuser = process.env.USER || "";
          this.maxReadSize = opt.maxReadSize || maxReadSize;
          this.linkCache = opt.linkCache || /* @__PURE__ */ new Map();
          this.statCache = opt.statCache || /* @__PURE__ */ new Map();
          this.preservePaths = !!opt.preservePaths;
          this.cwd = normPath(opt.cwd || process.cwd());
          this.strict = !!opt.strict;
          this.noPax = !!opt.noPax;
          this.noMtime = !!opt.noMtime;
          this.mtime = opt.mtime || null;
          this.prefix = opt.prefix ? normPath(opt.prefix) : null;
          this.fd = null;
          this.blockLen = null;
          this.blockRemain = null;
          this.buf = null;
          this.offset = null;
          this.length = null;
          this.pos = null;
          this.remain = null;
          if (typeof opt.onwarn === "function")
            this.on("warn", opt.onwarn);
          let pathWarn = false;
          if (!this.preservePaths) {
            const [root, stripped] = stripAbsolutePath(this.path);
            if (root) {
              this.path = stripped;
              pathWarn = root;
            }
          }
          this.win32 = !!opt.win32 || process.platform === "win32";
          if (this.win32) {
            this.path = winchars.decode(this.path.replace(/\\/g, "/"));
            p = p.replace(/\\/g, "/");
          }
          this.absolute = normPath(opt.absolute || path.resolve(this.cwd, p));
          if (this.path === "")
            this.path = "./";
          if (pathWarn) {
            this.warn("TAR_ENTRY_INFO", `stripping ${pathWarn} from absolute path`, {
              entry: this,
              path: pathWarn + this.path
            });
          }
          if (this.statCache.has(this.absolute))
            this[ONLSTAT](this.statCache.get(this.absolute));
          else
            this[LSTAT]();
        }
        emit(ev, ...data) {
          if (ev === "error")
            this[HAD_ERROR] = true;
          return super.emit(ev, ...data);
        }
        [LSTAT]() {
          fs.lstat(this.absolute, (er, stat) => {
            if (er)
              return this.emit("error", er);
            this[ONLSTAT](stat);
          });
        }
        [ONLSTAT](stat) {
          this.statCache.set(this.absolute, stat);
          this.stat = stat;
          if (!stat.isFile())
            stat.size = 0;
          this.type = getType(stat);
          this.emit("stat", stat);
          this[PROCESS]();
        }
        [PROCESS]() {
          switch (this.type) {
            case "File":
              return this[FILE]();
            case "Directory":
              return this[DIRECTORY]();
            case "SymbolicLink":
              return this[SYMLINK]();
            default:
              return this.end();
          }
        }
        [MODE](mode) {
          return modeFix(mode, this.type === "Directory", this.portable);
        }
        [PREFIX](path2) {
          return prefixPath(path2, this.prefix);
        }
        [HEADER]() {
          if (this.type === "Directory" && this.portable)
            this.noMtime = true;
          this.header = new Header({
            path: this[PREFIX](this.path),
            // only apply the prefix to hard links.
            linkpath: this.type === "Link" ? this[PREFIX](this.linkpath) : this.linkpath,
            // only the permissions and setuid/setgid/sticky bitflags
            // not the higher-order bits that specify file type
            mode: this[MODE](this.stat.mode),
            uid: this.portable ? null : this.stat.uid,
            gid: this.portable ? null : this.stat.gid,
            size: this.stat.size,
            mtime: this.noMtime ? null : this.mtime || this.stat.mtime,
            type: this.type,
            uname: this.portable ? null : this.stat.uid === this.myuid ? this.myuser : "",
            atime: this.portable ? null : this.stat.atime,
            ctime: this.portable ? null : this.stat.ctime
          });
          if (this.header.encode() && !this.noPax) {
            super.write(new Pax({
              atime: this.portable ? null : this.header.atime,
              ctime: this.portable ? null : this.header.ctime,
              gid: this.portable ? null : this.header.gid,
              mtime: this.noMtime ? null : this.mtime || this.header.mtime,
              path: this[PREFIX](this.path),
              linkpath: this.type === "Link" ? this[PREFIX](this.linkpath) : this.linkpath,
              size: this.header.size,
              uid: this.portable ? null : this.header.uid,
              uname: this.portable ? null : this.header.uname,
              dev: this.portable ? null : this.stat.dev,
              ino: this.portable ? null : this.stat.ino,
              nlink: this.portable ? null : this.stat.nlink
            }).encode());
          }
          super.write(this.header.block);
        }
        [DIRECTORY]() {
          if (this.path.substr(-1) !== "/")
            this.path += "/";
          this.stat.size = 0;
          this[HEADER]();
          this.end();
        }
        [SYMLINK]() {
          fs.readlink(this.absolute, (er, linkpath) => {
            if (er)
              return this.emit("error", er);
            this[ONREADLINK](linkpath);
          });
        }
        [ONREADLINK](linkpath) {
          this.linkpath = normPath(linkpath);
          this[HEADER]();
          this.end();
        }
        [HARDLINK](linkpath) {
          this.type = "Link";
          this.linkpath = normPath(path.relative(this.cwd, linkpath));
          this.stat.size = 0;
          this[HEADER]();
          this.end();
        }
        [FILE]() {
          if (this.stat.nlink > 1) {
            const linkKey = this.stat.dev + ":" + this.stat.ino;
            if (this.linkCache.has(linkKey)) {
              const linkpath = this.linkCache.get(linkKey);
              if (linkpath.indexOf(this.cwd) === 0)
                return this[HARDLINK](linkpath);
            }
            this.linkCache.set(linkKey, this.absolute);
          }
          this[HEADER]();
          if (this.stat.size === 0)
            return this.end();
          this[OPENFILE]();
        }
        [OPENFILE]() {
          fs.open(this.absolute, "r", (er, fd) => {
            if (er)
              return this.emit("error", er);
            this[ONOPENFILE](fd);
          });
        }
        [ONOPENFILE](fd) {
          this.fd = fd;
          if (this[HAD_ERROR])
            return this[CLOSE]();
          this.blockLen = 512 * Math.ceil(this.stat.size / 512);
          this.blockRemain = this.blockLen;
          const bufLen = Math.min(this.blockLen, this.maxReadSize);
          this.buf = Buffer.allocUnsafe(bufLen);
          this.offset = 0;
          this.pos = 0;
          this.remain = this.stat.size;
          this.length = this.buf.length;
          this[READ]();
        }
        [READ]() {
          const { fd, buf, offset, length, pos } = this;
          fs.read(fd, buf, offset, length, pos, (er, bytesRead) => {
            if (er) {
              return this[CLOSE](() => this.emit("error", er));
            }
            this[ONREAD](bytesRead);
          });
        }
        [CLOSE](cb) {
          fs.close(this.fd, cb);
        }
        [ONREAD](bytesRead) {
          if (bytesRead <= 0 && this.remain > 0) {
            const er = new Error("encountered unexpected EOF");
            er.path = this.absolute;
            er.syscall = "read";
            er.code = "EOF";
            return this[CLOSE](() => this.emit("error", er));
          }
          if (bytesRead > this.remain) {
            const er = new Error("did not encounter expected EOF");
            er.path = this.absolute;
            er.syscall = "read";
            er.code = "EOF";
            return this[CLOSE](() => this.emit("error", er));
          }
          if (bytesRead === this.remain) {
            for (let i = bytesRead; i < this.length && bytesRead < this.blockRemain; i++) {
              this.buf[i + this.offset] = 0;
              bytesRead++;
              this.remain++;
            }
          }
          const writeBuf = this.offset === 0 && bytesRead === this.buf.length ? this.buf : this.buf.slice(this.offset, this.offset + bytesRead);
          const flushed = this.write(writeBuf);
          if (!flushed)
            this[AWAITDRAIN](() => this[ONDRAIN]());
          else
            this[ONDRAIN]();
        }
        [AWAITDRAIN](cb) {
          this.once("drain", cb);
        }
        write(writeBuf) {
          if (this.blockRemain < writeBuf.length) {
            const er = new Error("writing more data than expected");
            er.path = this.absolute;
            return this.emit("error", er);
          }
          this.remain -= writeBuf.length;
          this.blockRemain -= writeBuf.length;
          this.pos += writeBuf.length;
          this.offset += writeBuf.length;
          return super.write(writeBuf);
        }
        [ONDRAIN]() {
          if (!this.remain) {
            if (this.blockRemain)
              super.write(Buffer.alloc(this.blockRemain));
            return this[CLOSE]((er) => er ? this.emit("error", er) : this.end());
          }
          if (this.offset >= this.length) {
            this.buf = Buffer.allocUnsafe(Math.min(this.blockRemain, this.buf.length));
            this.offset = 0;
          }
          this.length = this.buf.length - this.offset;
          this[READ]();
        }
      });
      var WriteEntrySync = class extends WriteEntry {
        [LSTAT]() {
          this[ONLSTAT](fs.lstatSync(this.absolute));
        }
        [SYMLINK]() {
          this[ONREADLINK](fs.readlinkSync(this.absolute));
        }
        [OPENFILE]() {
          this[ONOPENFILE](fs.openSync(this.absolute, "r"));
        }
        [READ]() {
          let threw = true;
          try {
            const { fd, buf, offset, length, pos } = this;
            const bytesRead = fs.readSync(fd, buf, offset, length, pos);
            this[ONREAD](bytesRead);
            threw = false;
          } finally {
            if (threw) {
              try {
                this[CLOSE](() => {
                });
              } catch (er) {
              }
            }
          }
        }
        [AWAITDRAIN](cb) {
          cb();
        }
        [CLOSE](cb) {
          fs.closeSync(this.fd);
          cb();
        }
      };
      var WriteEntryTar = warner(class WriteEntryTar extends MiniPass {
        constructor(readEntry, opt) {
          opt = opt || {};
          super(opt);
          this.preservePaths = !!opt.preservePaths;
          this.portable = !!opt.portable;
          this.strict = !!opt.strict;
          this.noPax = !!opt.noPax;
          this.noMtime = !!opt.noMtime;
          this.readEntry = readEntry;
          this.type = readEntry.type;
          if (this.type === "Directory" && this.portable)
            this.noMtime = true;
          this.prefix = opt.prefix || null;
          this.path = normPath(readEntry.path);
          this.mode = this[MODE](readEntry.mode);
          this.uid = this.portable ? null : readEntry.uid;
          this.gid = this.portable ? null : readEntry.gid;
          this.uname = this.portable ? null : readEntry.uname;
          this.gname = this.portable ? null : readEntry.gname;
          this.size = readEntry.size;
          this.mtime = this.noMtime ? null : opt.mtime || readEntry.mtime;
          this.atime = this.portable ? null : readEntry.atime;
          this.ctime = this.portable ? null : readEntry.ctime;
          this.linkpath = normPath(readEntry.linkpath);
          if (typeof opt.onwarn === "function")
            this.on("warn", opt.onwarn);
          let pathWarn = false;
          if (!this.preservePaths) {
            const [root, stripped] = stripAbsolutePath(this.path);
            if (root) {
              this.path = stripped;
              pathWarn = root;
            }
          }
          this.remain = readEntry.size;
          this.blockRemain = readEntry.startBlockSize;
          this.header = new Header({
            path: this[PREFIX](this.path),
            linkpath: this.type === "Link" ? this[PREFIX](this.linkpath) : this.linkpath,
            // only the permissions and setuid/setgid/sticky bitflags
            // not the higher-order bits that specify file type
            mode: this.mode,
            uid: this.portable ? null : this.uid,
            gid: this.portable ? null : this.gid,
            size: this.size,
            mtime: this.noMtime ? null : this.mtime,
            type: this.type,
            uname: this.portable ? null : this.uname,
            atime: this.portable ? null : this.atime,
            ctime: this.portable ? null : this.ctime
          });
          if (pathWarn) {
            this.warn("TAR_ENTRY_INFO", `stripping ${pathWarn} from absolute path`, {
              entry: this,
              path: pathWarn + this.path
            });
          }
          if (this.header.encode() && !this.noPax) {
            super.write(new Pax({
              atime: this.portable ? null : this.atime,
              ctime: this.portable ? null : this.ctime,
              gid: this.portable ? null : this.gid,
              mtime: this.noMtime ? null : this.mtime,
              path: this[PREFIX](this.path),
              linkpath: this.type === "Link" ? this[PREFIX](this.linkpath) : this.linkpath,
              size: this.size,
              uid: this.portable ? null : this.uid,
              uname: this.portable ? null : this.uname,
              dev: this.portable ? null : this.readEntry.dev,
              ino: this.portable ? null : this.readEntry.ino,
              nlink: this.portable ? null : this.readEntry.nlink
            }).encode());
          }
          super.write(this.header.block);
          readEntry.pipe(this);
        }
        [PREFIX](path2) {
          return prefixPath(path2, this.prefix);
        }
        [MODE](mode) {
          return modeFix(mode, this.type === "Directory", this.portable);
        }
        write(data) {
          const writeLen = data.length;
          if (writeLen > this.blockRemain)
            throw new Error("writing more to entry than is appropriate");
          this.blockRemain -= writeLen;
          return super.write(data);
        }
        end() {
          if (this.blockRemain)
            super.write(Buffer.alloc(this.blockRemain));
          return super.end();
        }
      });
      WriteEntry.Sync = WriteEntrySync;
      WriteEntry.Tar = WriteEntryTar;
      var getType = (stat) => stat.isFile() ? "File" : stat.isDirectory() ? "Directory" : stat.isSymbolicLink() ? "SymbolicLink" : "Unsupported";
      module22.exports = WriteEntry;
    }
  });
  var require_iterator = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/yallist@4.0.0/node_modules/yallist/iterator.js"(exports2, module22) {
      "use strict";
      module22.exports = function(Yallist) {
        Yallist.prototype[Symbol.iterator] = function* () {
          for (let walker = this.head; walker; walker = walker.next) {
            yield walker.value;
          }
        };
      };
    }
  });
  var require_yallist = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/yallist@4.0.0/node_modules/yallist/yallist.js"(exports2, module22) {
      "use strict";
      module22.exports = Yallist;
      Yallist.Node = Node;
      Yallist.create = Yallist;
      function Yallist(list) {
        var self = this;
        if (!(self instanceof Yallist)) {
          self = new Yallist();
        }
        self.tail = null;
        self.head = null;
        self.length = 0;
        if (list && typeof list.forEach === "function") {
          list.forEach(function(item) {
            self.push(item);
          });
        } else if (arguments.length > 0) {
          for (var i = 0, l = arguments.length; i < l; i++) {
            self.push(arguments[i]);
          }
        }
        return self;
      }
      Yallist.prototype.removeNode = function(node) {
        if (node.list !== this) {
          throw new Error("removing node which does not belong to this list");
        }
        var next = node.next;
        var prev = node.prev;
        if (next) {
          next.prev = prev;
        }
        if (prev) {
          prev.next = next;
        }
        if (node === this.head) {
          this.head = next;
        }
        if (node === this.tail) {
          this.tail = prev;
        }
        node.list.length--;
        node.next = null;
        node.prev = null;
        node.list = null;
        return next;
      };
      Yallist.prototype.unshiftNode = function(node) {
        if (node === this.head) {
          return;
        }
        if (node.list) {
          node.list.removeNode(node);
        }
        var head = this.head;
        node.list = this;
        node.next = head;
        if (head) {
          head.prev = node;
        }
        this.head = node;
        if (!this.tail) {
          this.tail = node;
        }
        this.length++;
      };
      Yallist.prototype.pushNode = function(node) {
        if (node === this.tail) {
          return;
        }
        if (node.list) {
          node.list.removeNode(node);
        }
        var tail = this.tail;
        node.list = this;
        node.prev = tail;
        if (tail) {
          tail.next = node;
        }
        this.tail = node;
        if (!this.head) {
          this.head = node;
        }
        this.length++;
      };
      Yallist.prototype.push = function() {
        for (var i = 0, l = arguments.length; i < l; i++) {
          push(this, arguments[i]);
        }
        return this.length;
      };
      Yallist.prototype.unshift = function() {
        for (var i = 0, l = arguments.length; i < l; i++) {
          unshift(this, arguments[i]);
        }
        return this.length;
      };
      Yallist.prototype.pop = function() {
        if (!this.tail) {
          return void 0;
        }
        var res = this.tail.value;
        this.tail = this.tail.prev;
        if (this.tail) {
          this.tail.next = null;
        } else {
          this.head = null;
        }
        this.length--;
        return res;
      };
      Yallist.prototype.shift = function() {
        if (!this.head) {
          return void 0;
        }
        var res = this.head.value;
        this.head = this.head.next;
        if (this.head) {
          this.head.prev = null;
        } else {
          this.tail = null;
        }
        this.length--;
        return res;
      };
      Yallist.prototype.forEach = function(fn, thisp) {
        thisp = thisp || this;
        for (var walker = this.head, i = 0; walker !== null; i++) {
          fn.call(thisp, walker.value, i, this);
          walker = walker.next;
        }
      };
      Yallist.prototype.forEachReverse = function(fn, thisp) {
        thisp = thisp || this;
        for (var walker = this.tail, i = this.length - 1; walker !== null; i--) {
          fn.call(thisp, walker.value, i, this);
          walker = walker.prev;
        }
      };
      Yallist.prototype.get = function(n) {
        for (var i = 0, walker = this.head; walker !== null && i < n; i++) {
          walker = walker.next;
        }
        if (i === n && walker !== null) {
          return walker.value;
        }
      };
      Yallist.prototype.getReverse = function(n) {
        for (var i = 0, walker = this.tail; walker !== null && i < n; i++) {
          walker = walker.prev;
        }
        if (i === n && walker !== null) {
          return walker.value;
        }
      };
      Yallist.prototype.map = function(fn, thisp) {
        thisp = thisp || this;
        var res = new Yallist();
        for (var walker = this.head; walker !== null; ) {
          res.push(fn.call(thisp, walker.value, this));
          walker = walker.next;
        }
        return res;
      };
      Yallist.prototype.mapReverse = function(fn, thisp) {
        thisp = thisp || this;
        var res = new Yallist();
        for (var walker = this.tail; walker !== null; ) {
          res.push(fn.call(thisp, walker.value, this));
          walker = walker.prev;
        }
        return res;
      };
      Yallist.prototype.reduce = function(fn, initial) {
        var acc;
        var walker = this.head;
        if (arguments.length > 1) {
          acc = initial;
        } else if (this.head) {
          walker = this.head.next;
          acc = this.head.value;
        } else {
          throw new TypeError("Reduce of empty list with no initial value");
        }
        for (var i = 0; walker !== null; i++) {
          acc = fn(acc, walker.value, i);
          walker = walker.next;
        }
        return acc;
      };
      Yallist.prototype.reduceReverse = function(fn, initial) {
        var acc;
        var walker = this.tail;
        if (arguments.length > 1) {
          acc = initial;
        } else if (this.tail) {
          walker = this.tail.prev;
          acc = this.tail.value;
        } else {
          throw new TypeError("Reduce of empty list with no initial value");
        }
        for (var i = this.length - 1; walker !== null; i--) {
          acc = fn(acc, walker.value, i);
          walker = walker.prev;
        }
        return acc;
      };
      Yallist.prototype.toArray = function() {
        var arr = new Array(this.length);
        for (var i = 0, walker = this.head; walker !== null; i++) {
          arr[i] = walker.value;
          walker = walker.next;
        }
        return arr;
      };
      Yallist.prototype.toArrayReverse = function() {
        var arr = new Array(this.length);
        for (var i = 0, walker = this.tail; walker !== null; i++) {
          arr[i] = walker.value;
          walker = walker.prev;
        }
        return arr;
      };
      Yallist.prototype.slice = function(from, to) {
        to = to || this.length;
        if (to < 0) {
          to += this.length;
        }
        from = from || 0;
        if (from < 0) {
          from += this.length;
        }
        var ret = new Yallist();
        if (to < from || to < 0) {
          return ret;
        }
        if (from < 0) {
          from = 0;
        }
        if (to > this.length) {
          to = this.length;
        }
        for (var i = 0, walker = this.head; walker !== null && i < from; i++) {
          walker = walker.next;
        }
        for (; walker !== null && i < to; i++, walker = walker.next) {
          ret.push(walker.value);
        }
        return ret;
      };
      Yallist.prototype.sliceReverse = function(from, to) {
        to = to || this.length;
        if (to < 0) {
          to += this.length;
        }
        from = from || 0;
        if (from < 0) {
          from += this.length;
        }
        var ret = new Yallist();
        if (to < from || to < 0) {
          return ret;
        }
        if (from < 0) {
          from = 0;
        }
        if (to > this.length) {
          to = this.length;
        }
        for (var i = this.length, walker = this.tail; walker !== null && i > to; i--) {
          walker = walker.prev;
        }
        for (; walker !== null && i > from; i--, walker = walker.prev) {
          ret.push(walker.value);
        }
        return ret;
      };
      Yallist.prototype.splice = function(start, deleteCount, ...nodes) {
        if (start > this.length) {
          start = this.length - 1;
        }
        if (start < 0) {
          start = this.length + start;
        }
        for (var i = 0, walker = this.head; walker !== null && i < start; i++) {
          walker = walker.next;
        }
        var ret = [];
        for (var i = 0; walker && i < deleteCount; i++) {
          ret.push(walker.value);
          walker = this.removeNode(walker);
        }
        if (walker === null) {
          walker = this.tail;
        }
        if (walker !== this.head && walker !== this.tail) {
          walker = walker.prev;
        }
        for (var i = 0; i < nodes.length; i++) {
          walker = insert(this, walker, nodes[i]);
        }
        return ret;
      };
      Yallist.prototype.reverse = function() {
        var head = this.head;
        var tail = this.tail;
        for (var walker = head; walker !== null; walker = walker.prev) {
          var p = walker.prev;
          walker.prev = walker.next;
          walker.next = p;
        }
        this.head = tail;
        this.tail = head;
        return this;
      };
      function insert(self, node, value) {
        var inserted = node === self.head ? new Node(value, null, node, self) : new Node(value, node, node.next, self);
        if (inserted.next === null) {
          self.tail = inserted;
        }
        if (inserted.prev === null) {
          self.head = inserted;
        }
        self.length++;
        return inserted;
      }
      function push(self, item) {
        self.tail = new Node(item, self.tail, null, self);
        if (!self.head) {
          self.head = self.tail;
        }
        self.length++;
      }
      function unshift(self, item) {
        self.head = new Node(item, null, self.head, self);
        if (!self.tail) {
          self.tail = self.head;
        }
        self.length++;
      }
      function Node(value, prev, next, list) {
        if (!(this instanceof Node)) {
          return new Node(value, prev, next, list);
        }
        this.list = list;
        this.value = value;
        if (prev) {
          prev.next = this;
          this.prev = prev;
        } else {
          this.prev = null;
        }
        if (next) {
          next.prev = this;
          this.next = next;
        } else {
          this.next = null;
        }
      }
      try {
        require_iterator()(Yallist);
      } catch (er) {
      }
    }
  });
  var require_pack = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/pack.js"(exports2, module22) {
      "use strict";
      var PackJob = class {
        constructor(path2, absolute) {
          this.path = path2 || "./";
          this.absolute = absolute;
          this.entry = null;
          this.stat = null;
          this.readdir = null;
          this.pending = false;
          this.ignore = false;
          this.piped = false;
        }
      };
      var MiniPass = require_minipass();
      var zlib = require_minizlib();
      var ReadEntry = require_read_entry();
      var WriteEntry = require_write_entry();
      var WriteEntrySync = WriteEntry.Sync;
      var WriteEntryTar = WriteEntry.Tar;
      var Yallist = require_yallist();
      var EOF = Buffer.alloc(1024);
      var ONSTAT = Symbol("onStat");
      var ENDED = Symbol("ended");
      var QUEUE = Symbol("queue");
      var CURRENT = Symbol("current");
      var PROCESS = Symbol("process");
      var PROCESSING = Symbol("processing");
      var PROCESSJOB = Symbol("processJob");
      var JOBS = Symbol("jobs");
      var JOBDONE = Symbol("jobDone");
      var ADDFSENTRY = Symbol("addFSEntry");
      var ADDTARENTRY = Symbol("addTarEntry");
      var STAT = Symbol("stat");
      var READDIR = Symbol("readdir");
      var ONREADDIR = Symbol("onreaddir");
      var PIPE = Symbol("pipe");
      var ENTRY = Symbol("entry");
      var ENTRYOPT = Symbol("entryOpt");
      var WRITEENTRYCLASS = Symbol("writeEntryClass");
      var WRITE = Symbol("write");
      var ONDRAIN = Symbol("ondrain");
      var fs = require2("fs");
      var path = require2("path");
      var warner = require_warn_mixin();
      var normPath = require_normalize_windows_path();
      var Pack = warner(class Pack extends MiniPass {
        constructor(opt) {
          super(opt);
          opt = opt || /* @__PURE__ */ Object.create(null);
          this.opt = opt;
          this.file = opt.file || "";
          this.cwd = opt.cwd || process.cwd();
          this.maxReadSize = opt.maxReadSize;
          this.preservePaths = !!opt.preservePaths;
          this.strict = !!opt.strict;
          this.noPax = !!opt.noPax;
          this.prefix = normPath(opt.prefix || "");
          this.linkCache = opt.linkCache || /* @__PURE__ */ new Map();
          this.statCache = opt.statCache || /* @__PURE__ */ new Map();
          this.readdirCache = opt.readdirCache || /* @__PURE__ */ new Map();
          this[WRITEENTRYCLASS] = WriteEntry;
          if (typeof opt.onwarn === "function")
            this.on("warn", opt.onwarn);
          this.portable = !!opt.portable;
          this.zip = null;
          if (opt.gzip) {
            if (typeof opt.gzip !== "object")
              opt.gzip = {};
            if (this.portable)
              opt.gzip.portable = true;
            this.zip = new zlib.Gzip(opt.gzip);
            this.zip.on("data", (chunk) => super.write(chunk));
            this.zip.on("end", (_) => super.end());
            this.zip.on("drain", (_) => this[ONDRAIN]());
            this.on("resume", (_) => this.zip.resume());
          } else
            this.on("drain", this[ONDRAIN]);
          this.noDirRecurse = !!opt.noDirRecurse;
          this.follow = !!opt.follow;
          this.noMtime = !!opt.noMtime;
          this.mtime = opt.mtime || null;
          this.filter = typeof opt.filter === "function" ? opt.filter : (_) => true;
          this[QUEUE] = new Yallist();
          this[JOBS] = 0;
          this.jobs = +opt.jobs || 4;
          this[PROCESSING] = false;
          this[ENDED] = false;
        }
        [WRITE](chunk) {
          return super.write(chunk);
        }
        add(path2) {
          this.write(path2);
          return this;
        }
        end(path2) {
          if (path2)
            this.write(path2);
          this[ENDED] = true;
          this[PROCESS]();
          return this;
        }
        write(path2) {
          if (this[ENDED])
            throw new Error("write after end");
          if (path2 instanceof ReadEntry)
            this[ADDTARENTRY](path2);
          else
            this[ADDFSENTRY](path2);
          return this.flowing;
        }
        [ADDTARENTRY](p) {
          const absolute = normPath(path.resolve(this.cwd, p.path));
          if (!this.filter(p.path, p))
            p.resume();
          else {
            const job = new PackJob(p.path, absolute, false);
            job.entry = new WriteEntryTar(p, this[ENTRYOPT](job));
            job.entry.on("end", (_) => this[JOBDONE](job));
            this[JOBS] += 1;
            this[QUEUE].push(job);
          }
          this[PROCESS]();
        }
        [ADDFSENTRY](p) {
          const absolute = normPath(path.resolve(this.cwd, p));
          this[QUEUE].push(new PackJob(p, absolute));
          this[PROCESS]();
        }
        [STAT](job) {
          job.pending = true;
          this[JOBS] += 1;
          const stat = this.follow ? "stat" : "lstat";
          fs[stat](job.absolute, (er, stat2) => {
            job.pending = false;
            this[JOBS] -= 1;
            if (er)
              this.emit("error", er);
            else
              this[ONSTAT](job, stat2);
          });
        }
        [ONSTAT](job, stat) {
          this.statCache.set(job.absolute, stat);
          job.stat = stat;
          if (!this.filter(job.path, stat))
            job.ignore = true;
          this[PROCESS]();
        }
        [READDIR](job) {
          job.pending = true;
          this[JOBS] += 1;
          fs.readdir(job.absolute, (er, entries) => {
            job.pending = false;
            this[JOBS] -= 1;
            if (er)
              return this.emit("error", er);
            this[ONREADDIR](job, entries);
          });
        }
        [ONREADDIR](job, entries) {
          this.readdirCache.set(job.absolute, entries);
          job.readdir = entries;
          this[PROCESS]();
        }
        [PROCESS]() {
          if (this[PROCESSING])
            return;
          this[PROCESSING] = true;
          for (let w = this[QUEUE].head; w !== null && this[JOBS] < this.jobs; w = w.next) {
            this[PROCESSJOB](w.value);
            if (w.value.ignore) {
              const p = w.next;
              this[QUEUE].removeNode(w);
              w.next = p;
            }
          }
          this[PROCESSING] = false;
          if (this[ENDED] && !this[QUEUE].length && this[JOBS] === 0) {
            if (this.zip)
              this.zip.end(EOF);
            else {
              super.write(EOF);
              super.end();
            }
          }
        }
        get [CURRENT]() {
          return this[QUEUE] && this[QUEUE].head && this[QUEUE].head.value;
        }
        [JOBDONE](job) {
          this[QUEUE].shift();
          this[JOBS] -= 1;
          this[PROCESS]();
        }
        [PROCESSJOB](job) {
          if (job.pending)
            return;
          if (job.entry) {
            if (job === this[CURRENT] && !job.piped)
              this[PIPE](job);
            return;
          }
          if (!job.stat) {
            if (this.statCache.has(job.absolute))
              this[ONSTAT](job, this.statCache.get(job.absolute));
            else
              this[STAT](job);
          }
          if (!job.stat)
            return;
          if (job.ignore)
            return;
          if (!this.noDirRecurse && job.stat.isDirectory() && !job.readdir) {
            if (this.readdirCache.has(job.absolute))
              this[ONREADDIR](job, this.readdirCache.get(job.absolute));
            else
              this[READDIR](job);
            if (!job.readdir)
              return;
          }
          job.entry = this[ENTRY](job);
          if (!job.entry) {
            job.ignore = true;
            return;
          }
          if (job === this[CURRENT] && !job.piped)
            this[PIPE](job);
        }
        [ENTRYOPT](job) {
          return {
            onwarn: (code, msg, data) => this.warn(code, msg, data),
            noPax: this.noPax,
            cwd: this.cwd,
            absolute: job.absolute,
            preservePaths: this.preservePaths,
            maxReadSize: this.maxReadSize,
            strict: this.strict,
            portable: this.portable,
            linkCache: this.linkCache,
            statCache: this.statCache,
            noMtime: this.noMtime,
            mtime: this.mtime,
            prefix: this.prefix
          };
        }
        [ENTRY](job) {
          this[JOBS] += 1;
          try {
            return new this[WRITEENTRYCLASS](job.path, this[ENTRYOPT](job)).on("end", () => this[JOBDONE](job)).on("error", (er) => this.emit("error", er));
          } catch (er) {
            this.emit("error", er);
          }
        }
        [ONDRAIN]() {
          if (this[CURRENT] && this[CURRENT].entry)
            this[CURRENT].entry.resume();
        }
        // like .pipe() but using super, because our write() is special
        [PIPE](job) {
          job.piped = true;
          if (job.readdir) {
            job.readdir.forEach((entry) => {
              const p = job.path;
              const base = p === "./" ? "" : p.replace(/\/*$/, "/");
              this[ADDFSENTRY](base + entry);
            });
          }
          const source = job.entry;
          const zip = this.zip;
          if (zip) {
            source.on("data", (chunk) => {
              if (!zip.write(chunk))
                source.pause();
            });
          } else {
            source.on("data", (chunk) => {
              if (!super.write(chunk))
                source.pause();
            });
          }
        }
        pause() {
          if (this.zip)
            this.zip.pause();
          return super.pause();
        }
      });
      var PackSync = class extends Pack {
        constructor(opt) {
          super(opt);
          this[WRITEENTRYCLASS] = WriteEntrySync;
        }
        // pause/resume are no-ops in sync streams.
        pause() {
        }
        resume() {
        }
        [STAT](job) {
          const stat = this.follow ? "statSync" : "lstatSync";
          this[ONSTAT](job, fs[stat](job.absolute));
        }
        [READDIR](job, stat) {
          this[ONREADDIR](job, fs.readdirSync(job.absolute));
        }
        // gotta get it all in this tick
        [PIPE](job) {
          const source = job.entry;
          const zip = this.zip;
          if (job.readdir) {
            job.readdir.forEach((entry) => {
              const p = job.path;
              const base = p === "./" ? "" : p.replace(/\/*$/, "/");
              this[ADDFSENTRY](base + entry);
            });
          }
          if (zip) {
            source.on("data", (chunk) => {
              zip.write(chunk);
            });
          } else {
            source.on("data", (chunk) => {
              super[WRITE](chunk);
            });
          }
        }
      };
      Pack.Sync = PackSync;
      module22.exports = Pack;
    }
  });
  var require_fs_minipass = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/fs-minipass@2.1.0/node_modules/fs-minipass/index.js"(exports2) {
      "use strict";
      var MiniPass = require_minipass();
      var EE = require2("events").EventEmitter;
      var fs = require2("fs");
      var writev = fs.writev;
      if (!writev) {
        const binding = process.binding("fs");
        const FSReqWrap = binding.FSReqWrap || binding.FSReqCallback;
        writev = (fd, iovec, pos, cb) => {
          const done = (er, bw) => cb(er, bw, iovec);
          const req = new FSReqWrap();
          req.oncomplete = done;
          binding.writeBuffers(fd, iovec, pos, req);
        };
      }
      var _autoClose = Symbol("_autoClose");
      var _close = Symbol("_close");
      var _ended = Symbol("_ended");
      var _fd = Symbol("_fd");
      var _finished = Symbol("_finished");
      var _flags = Symbol("_flags");
      var _flush = Symbol("_flush");
      var _handleChunk = Symbol("_handleChunk");
      var _makeBuf = Symbol("_makeBuf");
      var _mode = Symbol("_mode");
      var _needDrain = Symbol("_needDrain");
      var _onerror = Symbol("_onerror");
      var _onopen = Symbol("_onopen");
      var _onread = Symbol("_onread");
      var _onwrite = Symbol("_onwrite");
      var _open = Symbol("_open");
      var _path = Symbol("_path");
      var _pos = Symbol("_pos");
      var _queue = Symbol("_queue");
      var _read = Symbol("_read");
      var _readSize = Symbol("_readSize");
      var _reading = Symbol("_reading");
      var _remain = Symbol("_remain");
      var _size = Symbol("_size");
      var _write = Symbol("_write");
      var _writing = Symbol("_writing");
      var _defaultFlag = Symbol("_defaultFlag");
      var _errored = Symbol("_errored");
      var ReadStream = class extends MiniPass {
        constructor(path, opt) {
          opt = opt || {};
          super(opt);
          this.readable = true;
          this.writable = false;
          if (typeof path !== "string")
            throw new TypeError("path must be a string");
          this[_errored] = false;
          this[_fd] = typeof opt.fd === "number" ? opt.fd : null;
          this[_path] = path;
          this[_readSize] = opt.readSize || 16 * 1024 * 1024;
          this[_reading] = false;
          this[_size] = typeof opt.size === "number" ? opt.size : Infinity;
          this[_remain] = this[_size];
          this[_autoClose] = typeof opt.autoClose === "boolean" ? opt.autoClose : true;
          if (typeof this[_fd] === "number")
            this[_read]();
          else
            this[_open]();
        }
        get fd() {
          return this[_fd];
        }
        get path() {
          return this[_path];
        }
        write() {
          throw new TypeError("this is a readable stream");
        }
        end() {
          throw new TypeError("this is a readable stream");
        }
        [_open]() {
          fs.open(this[_path], "r", (er, fd) => this[_onopen](er, fd));
        }
        [_onopen](er, fd) {
          if (er)
            this[_onerror](er);
          else {
            this[_fd] = fd;
            this.emit("open", fd);
            this[_read]();
          }
        }
        [_makeBuf]() {
          return Buffer.allocUnsafe(Math.min(this[_readSize], this[_remain]));
        }
        [_read]() {
          if (!this[_reading]) {
            this[_reading] = true;
            const buf = this[_makeBuf]();
            if (buf.length === 0)
              return process.nextTick(() => this[_onread](null, 0, buf));
            fs.read(this[_fd], buf, 0, buf.length, null, (er, br, buf2) => this[_onread](er, br, buf2));
          }
        }
        [_onread](er, br, buf) {
          this[_reading] = false;
          if (er)
            this[_onerror](er);
          else if (this[_handleChunk](br, buf))
            this[_read]();
        }
        [_close]() {
          if (this[_autoClose] && typeof this[_fd] === "number") {
            const fd = this[_fd];
            this[_fd] = null;
            fs.close(fd, (er) => er ? this.emit("error", er) : this.emit("close"));
          }
        }
        [_onerror](er) {
          this[_reading] = true;
          this[_close]();
          this.emit("error", er);
        }
        [_handleChunk](br, buf) {
          let ret = false;
          this[_remain] -= br;
          if (br > 0)
            ret = super.write(br < buf.length ? buf.slice(0, br) : buf);
          if (br === 0 || this[_remain] <= 0) {
            ret = false;
            this[_close]();
            super.end();
          }
          return ret;
        }
        emit(ev, data) {
          switch (ev) {
            case "prefinish":
            case "finish":
              break;
            case "drain":
              if (typeof this[_fd] === "number")
                this[_read]();
              break;
            case "error":
              if (this[_errored])
                return;
              this[_errored] = true;
              return super.emit(ev, data);
            default:
              return super.emit(ev, data);
          }
        }
      };
      var ReadStreamSync = class extends ReadStream {
        [_open]() {
          let threw = true;
          try {
            this[_onopen](null, fs.openSync(this[_path], "r"));
            threw = false;
          } finally {
            if (threw)
              this[_close]();
          }
        }
        [_read]() {
          let threw = true;
          try {
            if (!this[_reading]) {
              this[_reading] = true;
              do {
                const buf = this[_makeBuf]();
                const br = buf.length === 0 ? 0 : fs.readSync(this[_fd], buf, 0, buf.length, null);
                if (!this[_handleChunk](br, buf))
                  break;
              } while (true);
              this[_reading] = false;
            }
            threw = false;
          } finally {
            if (threw)
              this[_close]();
          }
        }
        [_close]() {
          if (this[_autoClose] && typeof this[_fd] === "number") {
            const fd = this[_fd];
            this[_fd] = null;
            fs.closeSync(fd);
            this.emit("close");
          }
        }
      };
      var WriteStream = class extends EE {
        constructor(path, opt) {
          opt = opt || {};
          super(opt);
          this.readable = false;
          this.writable = true;
          this[_errored] = false;
          this[_writing] = false;
          this[_ended] = false;
          this[_needDrain] = false;
          this[_queue] = [];
          this[_path] = path;
          this[_fd] = typeof opt.fd === "number" ? opt.fd : null;
          this[_mode] = opt.mode === void 0 ? 438 : opt.mode;
          this[_pos] = typeof opt.start === "number" ? opt.start : null;
          this[_autoClose] = typeof opt.autoClose === "boolean" ? opt.autoClose : true;
          const defaultFlag = this[_pos] !== null ? "r+" : "w";
          this[_defaultFlag] = opt.flags === void 0;
          this[_flags] = this[_defaultFlag] ? defaultFlag : opt.flags;
          if (this[_fd] === null)
            this[_open]();
        }
        emit(ev, data) {
          if (ev === "error") {
            if (this[_errored])
              return;
            this[_errored] = true;
          }
          return super.emit(ev, data);
        }
        get fd() {
          return this[_fd];
        }
        get path() {
          return this[_path];
        }
        [_onerror](er) {
          this[_close]();
          this[_writing] = true;
          this.emit("error", er);
        }
        [_open]() {
          fs.open(
            this[_path],
            this[_flags],
            this[_mode],
            (er, fd) => this[_onopen](er, fd)
          );
        }
        [_onopen](er, fd) {
          if (this[_defaultFlag] && this[_flags] === "r+" && er && er.code === "ENOENT") {
            this[_flags] = "w";
            this[_open]();
          } else if (er)
            this[_onerror](er);
          else {
            this[_fd] = fd;
            this.emit("open", fd);
            this[_flush]();
          }
        }
        end(buf, enc) {
          if (buf)
            this.write(buf, enc);
          this[_ended] = true;
          if (!this[_writing] && !this[_queue].length && typeof this[_fd] === "number")
            this[_onwrite](null, 0);
          return this;
        }
        write(buf, enc) {
          if (typeof buf === "string")
            buf = Buffer.from(buf, enc);
          if (this[_ended]) {
            this.emit("error", new Error("write() after end()"));
            return false;
          }
          if (this[_fd] === null || this[_writing] || this[_queue].length) {
            this[_queue].push(buf);
            this[_needDrain] = true;
            return false;
          }
          this[_writing] = true;
          this[_write](buf);
          return true;
        }
        [_write](buf) {
          fs.write(this[_fd], buf, 0, buf.length, this[_pos], (er, bw) => this[_onwrite](er, bw));
        }
        [_onwrite](er, bw) {
          if (er)
            this[_onerror](er);
          else {
            if (this[_pos] !== null)
              this[_pos] += bw;
            if (this[_queue].length)
              this[_flush]();
            else {
              this[_writing] = false;
              if (this[_ended] && !this[_finished]) {
                this[_finished] = true;
                this[_close]();
                this.emit("finish");
              } else if (this[_needDrain]) {
                this[_needDrain] = false;
                this.emit("drain");
              }
            }
          }
        }
        [_flush]() {
          if (this[_queue].length === 0) {
            if (this[_ended])
              this[_onwrite](null, 0);
          } else if (this[_queue].length === 1)
            this[_write](this[_queue].pop());
          else {
            const iovec = this[_queue];
            this[_queue] = [];
            writev(
              this[_fd],
              iovec,
              this[_pos],
              (er, bw) => this[_onwrite](er, bw)
            );
          }
        }
        [_close]() {
          if (this[_autoClose] && typeof this[_fd] === "number") {
            const fd = this[_fd];
            this[_fd] = null;
            fs.close(fd, (er) => er ? this.emit("error", er) : this.emit("close"));
          }
        }
      };
      var WriteStreamSync = class extends WriteStream {
        [_open]() {
          let fd;
          if (this[_defaultFlag] && this[_flags] === "r+") {
            try {
              fd = fs.openSync(this[_path], this[_flags], this[_mode]);
            } catch (er) {
              if (er.code === "ENOENT") {
                this[_flags] = "w";
                return this[_open]();
              } else
                throw er;
            }
          } else
            fd = fs.openSync(this[_path], this[_flags], this[_mode]);
          this[_onopen](null, fd);
        }
        [_close]() {
          if (this[_autoClose] && typeof this[_fd] === "number") {
            const fd = this[_fd];
            this[_fd] = null;
            fs.closeSync(fd);
            this.emit("close");
          }
        }
        [_write](buf) {
          let threw = true;
          try {
            this[_onwrite](
              null,
              fs.writeSync(this[_fd], buf, 0, buf.length, this[_pos])
            );
            threw = false;
          } finally {
            if (threw)
              try {
                this[_close]();
              } catch (_) {
              }
          }
        }
      };
      exports2.ReadStream = ReadStream;
      exports2.ReadStreamSync = ReadStreamSync;
      exports2.WriteStream = WriteStream;
      exports2.WriteStreamSync = WriteStreamSync;
    }
  });
  var require_parse = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/parse.js"(exports2, module22) {
      "use strict";
      var warner = require_warn_mixin();
      var Header = require_header();
      var EE = require2("events");
      var Yallist = require_yallist();
      var maxMetaEntrySize = 1024 * 1024;
      var Entry = require_read_entry();
      var Pax = require_pax();
      var zlib = require_minizlib();
      var gzipHeader = Buffer.from([31, 139]);
      var STATE = Symbol("state");
      var WRITEENTRY = Symbol("writeEntry");
      var READENTRY = Symbol("readEntry");
      var NEXTENTRY = Symbol("nextEntry");
      var PROCESSENTRY = Symbol("processEntry");
      var EX = Symbol("extendedHeader");
      var GEX = Symbol("globalExtendedHeader");
      var META = Symbol("meta");
      var EMITMETA = Symbol("emitMeta");
      var BUFFER = Symbol("buffer");
      var QUEUE = Symbol("queue");
      var ENDED = Symbol("ended");
      var EMITTEDEND = Symbol("emittedEnd");
      var EMIT = Symbol("emit");
      var UNZIP = Symbol("unzip");
      var CONSUMECHUNK = Symbol("consumeChunk");
      var CONSUMECHUNKSUB = Symbol("consumeChunkSub");
      var CONSUMEBODY = Symbol("consumeBody");
      var CONSUMEMETA = Symbol("consumeMeta");
      var CONSUMEHEADER = Symbol("consumeHeader");
      var CONSUMING = Symbol("consuming");
      var BUFFERCONCAT = Symbol("bufferConcat");
      var MAYBEEND = Symbol("maybeEnd");
      var WRITING = Symbol("writing");
      var ABORTED = Symbol("aborted");
      var DONE = Symbol("onDone");
      var SAW_VALID_ENTRY = Symbol("sawValidEntry");
      var SAW_NULL_BLOCK = Symbol("sawNullBlock");
      var SAW_EOF = Symbol("sawEOF");
      var noop = (_) => true;
      module22.exports = warner(class Parser extends EE {
        constructor(opt) {
          opt = opt || {};
          super(opt);
          this.file = opt.file || "";
          this[SAW_VALID_ENTRY] = null;
          this.on(DONE, (_) => {
            if (this[STATE] === "begin" || this[SAW_VALID_ENTRY] === false) {
              this.warn("TAR_BAD_ARCHIVE", "Unrecognized archive format");
            }
          });
          if (opt.ondone)
            this.on(DONE, opt.ondone);
          else {
            this.on(DONE, (_) => {
              this.emit("prefinish");
              this.emit("finish");
              this.emit("end");
              this.emit("close");
            });
          }
          this.strict = !!opt.strict;
          this.maxMetaEntrySize = opt.maxMetaEntrySize || maxMetaEntrySize;
          this.filter = typeof opt.filter === "function" ? opt.filter : noop;
          this.writable = true;
          this.readable = false;
          this[QUEUE] = new Yallist();
          this[BUFFER] = null;
          this[READENTRY] = null;
          this[WRITEENTRY] = null;
          this[STATE] = "begin";
          this[META] = "";
          this[EX] = null;
          this[GEX] = null;
          this[ENDED] = false;
          this[UNZIP] = null;
          this[ABORTED] = false;
          this[SAW_NULL_BLOCK] = false;
          this[SAW_EOF] = false;
          if (typeof opt.onwarn === "function")
            this.on("warn", opt.onwarn);
          if (typeof opt.onentry === "function")
            this.on("entry", opt.onentry);
        }
        [CONSUMEHEADER](chunk, position) {
          if (this[SAW_VALID_ENTRY] === null)
            this[SAW_VALID_ENTRY] = false;
          let header;
          try {
            header = new Header(chunk, position, this[EX], this[GEX]);
          } catch (er) {
            return this.warn("TAR_ENTRY_INVALID", er);
          }
          if (header.nullBlock) {
            if (this[SAW_NULL_BLOCK]) {
              this[SAW_EOF] = true;
              if (this[STATE] === "begin")
                this[STATE] = "header";
              this[EMIT]("eof");
            } else {
              this[SAW_NULL_BLOCK] = true;
              this[EMIT]("nullBlock");
            }
          } else {
            this[SAW_NULL_BLOCK] = false;
            if (!header.cksumValid)
              this.warn("TAR_ENTRY_INVALID", "checksum failure", { header });
            else if (!header.path)
              this.warn("TAR_ENTRY_INVALID", "path is required", { header });
            else {
              const type = header.type;
              if (/^(Symbolic)?Link$/.test(type) && !header.linkpath)
                this.warn("TAR_ENTRY_INVALID", "linkpath required", { header });
              else if (!/^(Symbolic)?Link$/.test(type) && header.linkpath)
                this.warn("TAR_ENTRY_INVALID", "linkpath forbidden", { header });
              else {
                const entry = this[WRITEENTRY] = new Entry(header, this[EX], this[GEX]);
                if (!this[SAW_VALID_ENTRY]) {
                  if (entry.remain) {
                    const onend = () => {
                      if (!entry.invalid)
                        this[SAW_VALID_ENTRY] = true;
                    };
                    entry.on("end", onend);
                  } else
                    this[SAW_VALID_ENTRY] = true;
                }
                if (entry.meta) {
                  if (entry.size > this.maxMetaEntrySize) {
                    entry.ignore = true;
                    this[EMIT]("ignoredEntry", entry);
                    this[STATE] = "ignore";
                    entry.resume();
                  } else if (entry.size > 0) {
                    this[META] = "";
                    entry.on("data", (c) => this[META] += c);
                    this[STATE] = "meta";
                  }
                } else {
                  this[EX] = null;
                  entry.ignore = entry.ignore || !this.filter(entry.path, entry);
                  if (entry.ignore) {
                    this[EMIT]("ignoredEntry", entry);
                    this[STATE] = entry.remain ? "ignore" : "header";
                    entry.resume();
                  } else {
                    if (entry.remain)
                      this[STATE] = "body";
                    else {
                      this[STATE] = "header";
                      entry.end();
                    }
                    if (!this[READENTRY]) {
                      this[QUEUE].push(entry);
                      this[NEXTENTRY]();
                    } else
                      this[QUEUE].push(entry);
                  }
                }
              }
            }
          }
        }
        [PROCESSENTRY](entry) {
          let go = true;
          if (!entry) {
            this[READENTRY] = null;
            go = false;
          } else if (Array.isArray(entry))
            this.emit.apply(this, entry);
          else {
            this[READENTRY] = entry;
            this.emit("entry", entry);
            if (!entry.emittedEnd) {
              entry.on("end", (_) => this[NEXTENTRY]());
              go = false;
            }
          }
          return go;
        }
        [NEXTENTRY]() {
          do {
          } while (this[PROCESSENTRY](this[QUEUE].shift()));
          if (!this[QUEUE].length) {
            const re = this[READENTRY];
            const drainNow = !re || re.flowing || re.size === re.remain;
            if (drainNow) {
              if (!this[WRITING])
                this.emit("drain");
            } else
              re.once("drain", (_) => this.emit("drain"));
          }
        }
        [CONSUMEBODY](chunk, position) {
          const entry = this[WRITEENTRY];
          const br = entry.blockRemain;
          const c = br >= chunk.length && position === 0 ? chunk : chunk.slice(position, position + br);
          entry.write(c);
          if (!entry.blockRemain) {
            this[STATE] = "header";
            this[WRITEENTRY] = null;
            entry.end();
          }
          return c.length;
        }
        [CONSUMEMETA](chunk, position) {
          const entry = this[WRITEENTRY];
          const ret = this[CONSUMEBODY](chunk, position);
          if (!this[WRITEENTRY])
            this[EMITMETA](entry);
          return ret;
        }
        [EMIT](ev, data, extra) {
          if (!this[QUEUE].length && !this[READENTRY])
            this.emit(ev, data, extra);
          else
            this[QUEUE].push([ev, data, extra]);
        }
        [EMITMETA](entry) {
          this[EMIT]("meta", this[META]);
          switch (entry.type) {
            case "ExtendedHeader":
            case "OldExtendedHeader":
              this[EX] = Pax.parse(this[META], this[EX], false);
              break;
            case "GlobalExtendedHeader":
              this[GEX] = Pax.parse(this[META], this[GEX], true);
              break;
            case "NextFileHasLongPath":
            case "OldGnuLongPath":
              this[EX] = this[EX] || /* @__PURE__ */ Object.create(null);
              this[EX].path = this[META].replace(/\0.*/, "");
              break;
            case "NextFileHasLongLinkpath":
              this[EX] = this[EX] || /* @__PURE__ */ Object.create(null);
              this[EX].linkpath = this[META].replace(/\0.*/, "");
              break;
            default:
              throw new Error("unknown meta: " + entry.type);
          }
        }
        abort(error) {
          this[ABORTED] = true;
          this.emit("abort", error);
          this.warn("TAR_ABORT", error, { recoverable: false });
        }
        write(chunk) {
          if (this[ABORTED])
            return;
          if (this[UNZIP] === null && chunk) {
            if (this[BUFFER]) {
              chunk = Buffer.concat([this[BUFFER], chunk]);
              this[BUFFER] = null;
            }
            if (chunk.length < gzipHeader.length) {
              this[BUFFER] = chunk;
              return true;
            }
            for (let i = 0; this[UNZIP] === null && i < gzipHeader.length; i++) {
              if (chunk[i] !== gzipHeader[i])
                this[UNZIP] = false;
            }
            if (this[UNZIP] === null) {
              const ended = this[ENDED];
              this[ENDED] = false;
              this[UNZIP] = new zlib.Unzip();
              this[UNZIP].on("data", (chunk2) => this[CONSUMECHUNK](chunk2));
              this[UNZIP].on("error", (er) => this.abort(er));
              this[UNZIP].on("end", (_) => {
                this[ENDED] = true;
                this[CONSUMECHUNK]();
              });
              this[WRITING] = true;
              const ret2 = this[UNZIP][ended ? "end" : "write"](chunk);
              this[WRITING] = false;
              return ret2;
            }
          }
          this[WRITING] = true;
          if (this[UNZIP])
            this[UNZIP].write(chunk);
          else
            this[CONSUMECHUNK](chunk);
          this[WRITING] = false;
          const ret = this[QUEUE].length ? false : this[READENTRY] ? this[READENTRY].flowing : true;
          if (!ret && !this[QUEUE].length)
            this[READENTRY].once("drain", (_) => this.emit("drain"));
          return ret;
        }
        [BUFFERCONCAT](c) {
          if (c && !this[ABORTED])
            this[BUFFER] = this[BUFFER] ? Buffer.concat([this[BUFFER], c]) : c;
        }
        [MAYBEEND]() {
          if (this[ENDED] && !this[EMITTEDEND] && !this[ABORTED] && !this[CONSUMING]) {
            this[EMITTEDEND] = true;
            const entry = this[WRITEENTRY];
            if (entry && entry.blockRemain) {
              const have = this[BUFFER] ? this[BUFFER].length : 0;
              this.warn("TAR_BAD_ARCHIVE", `Truncated input (needed ${entry.blockRemain} more bytes, only ${have} available)`, { entry });
              if (this[BUFFER])
                entry.write(this[BUFFER]);
              entry.end();
            }
            this[EMIT](DONE);
          }
        }
        [CONSUMECHUNK](chunk) {
          if (this[CONSUMING])
            this[BUFFERCONCAT](chunk);
          else if (!chunk && !this[BUFFER])
            this[MAYBEEND]();
          else {
            this[CONSUMING] = true;
            if (this[BUFFER]) {
              this[BUFFERCONCAT](chunk);
              const c = this[BUFFER];
              this[BUFFER] = null;
              this[CONSUMECHUNKSUB](c);
            } else
              this[CONSUMECHUNKSUB](chunk);
            while (this[BUFFER] && this[BUFFER].length >= 512 && !this[ABORTED] && !this[SAW_EOF]) {
              const c = this[BUFFER];
              this[BUFFER] = null;
              this[CONSUMECHUNKSUB](c);
            }
            this[CONSUMING] = false;
          }
          if (!this[BUFFER] || this[ENDED])
            this[MAYBEEND]();
        }
        [CONSUMECHUNKSUB](chunk) {
          let position = 0;
          const length = chunk.length;
          while (position + 512 <= length && !this[ABORTED] && !this[SAW_EOF]) {
            switch (this[STATE]) {
              case "begin":
              case "header":
                this[CONSUMEHEADER](chunk, position);
                position += 512;
                break;
              case "ignore":
              case "body":
                position += this[CONSUMEBODY](chunk, position);
                break;
              case "meta":
                position += this[CONSUMEMETA](chunk, position);
                break;
              default:
                throw new Error("invalid state: " + this[STATE]);
            }
          }
          if (position < length) {
            if (this[BUFFER])
              this[BUFFER] = Buffer.concat([chunk.slice(position), this[BUFFER]]);
            else
              this[BUFFER] = chunk.slice(position);
          }
        }
        end(chunk) {
          if (!this[ABORTED]) {
            if (this[UNZIP])
              this[UNZIP].end(chunk);
            else {
              this[ENDED] = true;
              this.write(chunk);
            }
          }
        }
      });
    }
  });
  var require_list = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/list.js"(exports2, module22) {
      "use strict";
      var hlo = require_high_level_opt();
      var Parser = require_parse();
      var fs = require2("fs");
      var fsm = require_fs_minipass();
      var path = require2("path");
      var stripSlash = require_strip_trailing_slashes();
      module22.exports = (opt_, files, cb) => {
        if (typeof opt_ === "function")
          cb = opt_, files = null, opt_ = {};
        else if (Array.isArray(opt_))
          files = opt_, opt_ = {};
        if (typeof files === "function")
          cb = files, files = null;
        if (!files)
          files = [];
        else
          files = Array.from(files);
        const opt = hlo(opt_);
        if (opt.sync && typeof cb === "function")
          throw new TypeError("callback not supported for sync tar functions");
        if (!opt.file && typeof cb === "function")
          throw new TypeError("callback only supported with file option");
        if (files.length)
          filesFilter(opt, files);
        if (!opt.noResume)
          onentryFunction(opt);
        return opt.file && opt.sync ? listFileSync(opt) : opt.file ? listFile(opt, cb) : list(opt);
      };
      var onentryFunction = (opt) => {
        const onentry = opt.onentry;
        opt.onentry = onentry ? (e) => {
          onentry(e);
          e.resume();
        } : (e) => e.resume();
      };
      var filesFilter = (opt, files) => {
        const map = new Map(files.map((f) => [stripSlash(f), true]));
        const filter = opt.filter;
        const mapHas = (file, r) => {
          const root = r || path.parse(file).root || ".";
          const ret = file === root ? false : map.has(file) ? map.get(file) : mapHas(path.dirname(file), root);
          map.set(file, ret);
          return ret;
        };
        opt.filter = filter ? (file, entry) => filter(file, entry) && mapHas(stripSlash(file)) : (file) => mapHas(stripSlash(file));
      };
      var listFileSync = (opt) => {
        const p = list(opt);
        const file = opt.file;
        let threw = true;
        let fd;
        try {
          const stat = fs.statSync(file);
          const readSize = opt.maxReadSize || 16 * 1024 * 1024;
          if (stat.size < readSize)
            p.end(fs.readFileSync(file));
          else {
            let pos = 0;
            const buf = Buffer.allocUnsafe(readSize);
            fd = fs.openSync(file, "r");
            while (pos < stat.size) {
              const bytesRead = fs.readSync(fd, buf, 0, readSize, pos);
              pos += bytesRead;
              p.write(buf.slice(0, bytesRead));
            }
            p.end();
          }
          threw = false;
        } finally {
          if (threw && fd) {
            try {
              fs.closeSync(fd);
            } catch (er) {
            }
          }
        }
      };
      var listFile = (opt, cb) => {
        const parse = new Parser(opt);
        const readSize = opt.maxReadSize || 16 * 1024 * 1024;
        const file = opt.file;
        const p = new Promise((resolve, reject) => {
          parse.on("error", reject);
          parse.on("end", resolve);
          fs.stat(file, (er, stat) => {
            if (er)
              reject(er);
            else {
              const stream = new fsm.ReadStream(file, {
                readSize,
                size: stat.size
              });
              stream.on("error", reject);
              stream.pipe(parse);
            }
          });
        });
        return cb ? p.then(cb, cb) : p;
      };
      var list = (opt) => new Parser(opt);
    }
  });
  var require_create = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/create.js"(exports2, module22) {
      "use strict";
      var hlo = require_high_level_opt();
      var Pack = require_pack();
      var fsm = require_fs_minipass();
      var t = require_list();
      var path = require2("path");
      module22.exports = (opt_, files, cb) => {
        if (typeof files === "function")
          cb = files;
        if (Array.isArray(opt_))
          files = opt_, opt_ = {};
        if (!files || !Array.isArray(files) || !files.length)
          throw new TypeError("no files or directories specified");
        files = Array.from(files);
        const opt = hlo(opt_);
        if (opt.sync && typeof cb === "function")
          throw new TypeError("callback not supported for sync tar functions");
        if (!opt.file && typeof cb === "function")
          throw new TypeError("callback only supported with file option");
        return opt.file && opt.sync ? createFileSync(opt, files) : opt.file ? createFile(opt, files, cb) : opt.sync ? createSync(opt, files) : create(opt, files);
      };
      var createFileSync = (opt, files) => {
        const p = new Pack.Sync(opt);
        const stream = new fsm.WriteStreamSync(opt.file, {
          mode: opt.mode || 438
        });
        p.pipe(stream);
        addFilesSync(p, files);
      };
      var createFile = (opt, files, cb) => {
        const p = new Pack(opt);
        const stream = new fsm.WriteStream(opt.file, {
          mode: opt.mode || 438
        });
        p.pipe(stream);
        const promise = new Promise((res, rej) => {
          stream.on("error", rej);
          stream.on("close", res);
          p.on("error", rej);
        });
        addFilesAsync(p, files);
        return cb ? promise.then(cb, cb) : promise;
      };
      var addFilesSync = (p, files) => {
        files.forEach((file) => {
          if (file.charAt(0) === "@") {
            t({
              file: path.resolve(p.cwd, file.substr(1)),
              sync: true,
              noResume: true,
              onentry: (entry) => p.add(entry)
            });
          } else
            p.add(file);
        });
        p.end();
      };
      var addFilesAsync = (p, files) => {
        while (files.length) {
          const file = files.shift();
          if (file.charAt(0) === "@") {
            return t({
              file: path.resolve(p.cwd, file.substr(1)),
              noResume: true,
              onentry: (entry) => p.add(entry)
            }).then((_) => addFilesAsync(p, files));
          } else
            p.add(file);
        }
        p.end();
      };
      var createSync = (opt, files) => {
        const p = new Pack.Sync(opt);
        addFilesSync(p, files);
        return p;
      };
      var create = (opt, files) => {
        const p = new Pack(opt);
        addFilesAsync(p, files);
        return p;
      };
    }
  });
  var require_replace = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/replace.js"(exports2, module22) {
      "use strict";
      var hlo = require_high_level_opt();
      var Pack = require_pack();
      var fs = require2("fs");
      var fsm = require_fs_minipass();
      var t = require_list();
      var path = require2("path");
      var Header = require_header();
      module22.exports = (opt_, files, cb) => {
        const opt = hlo(opt_);
        if (!opt.file)
          throw new TypeError("file is required");
        if (opt.gzip)
          throw new TypeError("cannot append to compressed archives");
        if (!files || !Array.isArray(files) || !files.length)
          throw new TypeError("no files or directories specified");
        files = Array.from(files);
        return opt.sync ? replaceSync(opt, files) : replace(opt, files, cb);
      };
      var replaceSync = (opt, files) => {
        const p = new Pack.Sync(opt);
        let threw = true;
        let fd;
        let position;
        try {
          try {
            fd = fs.openSync(opt.file, "r+");
          } catch (er) {
            if (er.code === "ENOENT")
              fd = fs.openSync(opt.file, "w+");
            else
              throw er;
          }
          const st = fs.fstatSync(fd);
          const headBuf = Buffer.alloc(512);
          POSITION:
            for (position = 0; position < st.size; position += 512) {
              for (let bufPos = 0, bytes = 0; bufPos < 512; bufPos += bytes) {
                bytes = fs.readSync(
                  fd,
                  headBuf,
                  bufPos,
                  headBuf.length - bufPos,
                  position + bufPos
                );
                if (position === 0 && headBuf[0] === 31 && headBuf[1] === 139)
                  throw new Error("cannot append to compressed archives");
                if (!bytes)
                  break POSITION;
              }
              const h = new Header(headBuf);
              if (!h.cksumValid)
                break;
              const entryBlockSize = 512 * Math.ceil(h.size / 512);
              if (position + entryBlockSize + 512 > st.size)
                break;
              position += entryBlockSize;
              if (opt.mtimeCache)
                opt.mtimeCache.set(h.path, h.mtime);
            }
          threw = false;
          streamSync(opt, p, position, fd, files);
        } finally {
          if (threw) {
            try {
              fs.closeSync(fd);
            } catch (er) {
            }
          }
        }
      };
      var streamSync = (opt, p, position, fd, files) => {
        const stream = new fsm.WriteStreamSync(opt.file, {
          fd,
          start: position
        });
        p.pipe(stream);
        addFilesSync(p, files);
      };
      var replace = (opt, files, cb) => {
        files = Array.from(files);
        const p = new Pack(opt);
        const getPos = (fd, size, cb_) => {
          const cb2 = (er, pos) => {
            if (er)
              fs.close(fd, (_) => cb_(er));
            else
              cb_(null, pos);
          };
          let position = 0;
          if (size === 0)
            return cb2(null, 0);
          let bufPos = 0;
          const headBuf = Buffer.alloc(512);
          const onread = (er, bytes) => {
            if (er)
              return cb2(er);
            bufPos += bytes;
            if (bufPos < 512 && bytes) {
              return fs.read(
                fd,
                headBuf,
                bufPos,
                headBuf.length - bufPos,
                position + bufPos,
                onread
              );
            }
            if (position === 0 && headBuf[0] === 31 && headBuf[1] === 139)
              return cb2(new Error("cannot append to compressed archives"));
            if (bufPos < 512)
              return cb2(null, position);
            const h = new Header(headBuf);
            if (!h.cksumValid)
              return cb2(null, position);
            const entryBlockSize = 512 * Math.ceil(h.size / 512);
            if (position + entryBlockSize + 512 > size)
              return cb2(null, position);
            position += entryBlockSize + 512;
            if (position >= size)
              return cb2(null, position);
            if (opt.mtimeCache)
              opt.mtimeCache.set(h.path, h.mtime);
            bufPos = 0;
            fs.read(fd, headBuf, 0, 512, position, onread);
          };
          fs.read(fd, headBuf, 0, 512, position, onread);
        };
        const promise = new Promise((resolve, reject) => {
          p.on("error", reject);
          let flag = "r+";
          const onopen = (er, fd) => {
            if (er && er.code === "ENOENT" && flag === "r+") {
              flag = "w+";
              return fs.open(opt.file, flag, onopen);
            }
            if (er)
              return reject(er);
            fs.fstat(fd, (er2, st) => {
              if (er2)
                return fs.close(fd, () => reject(er2));
              getPos(fd, st.size, (er3, position) => {
                if (er3)
                  return reject(er3);
                const stream = new fsm.WriteStream(opt.file, {
                  fd,
                  start: position
                });
                p.pipe(stream);
                stream.on("error", reject);
                stream.on("close", resolve);
                addFilesAsync(p, files);
              });
            });
          };
          fs.open(opt.file, flag, onopen);
        });
        return cb ? promise.then(cb, cb) : promise;
      };
      var addFilesSync = (p, files) => {
        files.forEach((file) => {
          if (file.charAt(0) === "@") {
            t({
              file: path.resolve(p.cwd, file.substr(1)),
              sync: true,
              noResume: true,
              onentry: (entry) => p.add(entry)
            });
          } else
            p.add(file);
        });
        p.end();
      };
      var addFilesAsync = (p, files) => {
        while (files.length) {
          const file = files.shift();
          if (file.charAt(0) === "@") {
            return t({
              file: path.resolve(p.cwd, file.substr(1)),
              noResume: true,
              onentry: (entry) => p.add(entry)
            }).then((_) => addFilesAsync(p, files));
          } else
            p.add(file);
        }
        p.end();
      };
    }
  });
  var require_update = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/update.js"(exports2, module22) {
      "use strict";
      var hlo = require_high_level_opt();
      var r = require_replace();
      module22.exports = (opt_, files, cb) => {
        const opt = hlo(opt_);
        if (!opt.file)
          throw new TypeError("file is required");
        if (opt.gzip)
          throw new TypeError("cannot append to compressed archives");
        if (!files || !Array.isArray(files) || !files.length)
          throw new TypeError("no files or directories specified");
        files = Array.from(files);
        mtimeFilter(opt);
        return r(opt, files, cb);
      };
      var mtimeFilter = (opt) => {
        const filter = opt.filter;
        if (!opt.mtimeCache)
          opt.mtimeCache = /* @__PURE__ */ new Map();
        opt.filter = filter ? (path, stat) => filter(path, stat) && !(opt.mtimeCache.get(path) > stat.mtime) : (path, stat) => !(opt.mtimeCache.get(path) > stat.mtime);
      };
    }
  });
  var require_opts_arg = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mkdirp@1.0.4/node_modules/mkdirp/lib/opts-arg.js"(exports2, module22) {
      var { promisify } = require2("util");
      var fs = require2("fs");
      var optsArg = (opts) => {
        if (!opts)
          opts = { mode: 511, fs };
        else if (typeof opts === "object")
          opts = { mode: 511, fs, ...opts };
        else if (typeof opts === "number")
          opts = { mode: opts, fs };
        else if (typeof opts === "string")
          opts = { mode: parseInt(opts, 8), fs };
        else
          throw new TypeError("invalid options argument");
        opts.mkdir = opts.mkdir || opts.fs.mkdir || fs.mkdir;
        opts.mkdirAsync = promisify(opts.mkdir);
        opts.stat = opts.stat || opts.fs.stat || fs.stat;
        opts.statAsync = promisify(opts.stat);
        opts.statSync = opts.statSync || opts.fs.statSync || fs.statSync;
        opts.mkdirSync = opts.mkdirSync || opts.fs.mkdirSync || fs.mkdirSync;
        return opts;
      };
      module22.exports = optsArg;
    }
  });
  var require_path_arg = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mkdirp@1.0.4/node_modules/mkdirp/lib/path-arg.js"(exports2, module22) {
      var platform = process.env.__TESTING_MKDIRP_PLATFORM__ || process.platform;
      var { resolve, parse } = require2("path");
      var pathArg = (path) => {
        if (/\0/.test(path)) {
          throw Object.assign(
            new TypeError("path must be a string without null bytes"),
            {
              path,
              code: "ERR_INVALID_ARG_VALUE"
            }
          );
        }
        path = resolve(path);
        if (platform === "win32") {
          const badWinChars = /[*|"<>?:]/;
          const { root } = parse(path);
          if (badWinChars.test(path.substr(root.length))) {
            throw Object.assign(new Error("Illegal characters in path."), {
              path,
              code: "EINVAL"
            });
          }
        }
        return path;
      };
      module22.exports = pathArg;
    }
  });
  var require_find_made = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mkdirp@1.0.4/node_modules/mkdirp/lib/find-made.js"(exports2, module22) {
      var { dirname } = require2("path");
      var findMade = (opts, parent, path = void 0) => {
        if (path === parent)
          return Promise.resolve();
        return opts.statAsync(parent).then(
          (st) => st.isDirectory() ? path : void 0,
          // will fail later
          (er) => er.code === "ENOENT" ? findMade(opts, dirname(parent), parent) : void 0
        );
      };
      var findMadeSync = (opts, parent, path = void 0) => {
        if (path === parent)
          return void 0;
        try {
          return opts.statSync(parent).isDirectory() ? path : void 0;
        } catch (er) {
          return er.code === "ENOENT" ? findMadeSync(opts, dirname(parent), parent) : void 0;
        }
      };
      module22.exports = { findMade, findMadeSync };
    }
  });
  var require_mkdirp_manual = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mkdirp@1.0.4/node_modules/mkdirp/lib/mkdirp-manual.js"(exports2, module22) {
      var { dirname } = require2("path");
      var mkdirpManual = (path, opts, made) => {
        opts.recursive = false;
        const parent = dirname(path);
        if (parent === path) {
          return opts.mkdirAsync(path, opts).catch((er) => {
            if (er.code !== "EISDIR")
              throw er;
          });
        }
        return opts.mkdirAsync(path, opts).then(() => made || path, (er) => {
          if (er.code === "ENOENT")
            return mkdirpManual(parent, opts).then((made2) => mkdirpManual(path, opts, made2));
          if (er.code !== "EEXIST" && er.code !== "EROFS")
            throw er;
          return opts.statAsync(path).then((st) => {
            if (st.isDirectory())
              return made;
            else
              throw er;
          }, () => {
            throw er;
          });
        });
      };
      var mkdirpManualSync = (path, opts, made) => {
        const parent = dirname(path);
        opts.recursive = false;
        if (parent === path) {
          try {
            return opts.mkdirSync(path, opts);
          } catch (er) {
            if (er.code !== "EISDIR")
              throw er;
            else
              return;
          }
        }
        try {
          opts.mkdirSync(path, opts);
          return made || path;
        } catch (er) {
          if (er.code === "ENOENT")
            return mkdirpManualSync(path, opts, mkdirpManualSync(parent, opts, made));
          if (er.code !== "EEXIST" && er.code !== "EROFS")
            throw er;
          try {
            if (!opts.statSync(path).isDirectory())
              throw er;
          } catch (_) {
            throw er;
          }
        }
      };
      module22.exports = { mkdirpManual, mkdirpManualSync };
    }
  });
  var require_mkdirp_native = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mkdirp@1.0.4/node_modules/mkdirp/lib/mkdirp-native.js"(exports2, module22) {
      var { dirname } = require2("path");
      var { findMade, findMadeSync } = require_find_made();
      var { mkdirpManual, mkdirpManualSync } = require_mkdirp_manual();
      var mkdirpNative = (path, opts) => {
        opts.recursive = true;
        const parent = dirname(path);
        if (parent === path)
          return opts.mkdirAsync(path, opts);
        return findMade(opts, path).then((made) => opts.mkdirAsync(path, opts).then(() => made).catch((er) => {
          if (er.code === "ENOENT")
            return mkdirpManual(path, opts);
          else
            throw er;
        }));
      };
      var mkdirpNativeSync = (path, opts) => {
        opts.recursive = true;
        const parent = dirname(path);
        if (parent === path)
          return opts.mkdirSync(path, opts);
        const made = findMadeSync(opts, path);
        try {
          opts.mkdirSync(path, opts);
          return made;
        } catch (er) {
          if (er.code === "ENOENT")
            return mkdirpManualSync(path, opts);
          else
            throw er;
        }
      };
      module22.exports = { mkdirpNative, mkdirpNativeSync };
    }
  });
  var require_use_native = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mkdirp@1.0.4/node_modules/mkdirp/lib/use-native.js"(exports2, module22) {
      var fs = require2("fs");
      var version = process.env.__TESTING_MKDIRP_NODE_VERSION__ || process.version;
      var versArr = version.replace(/^v/, "").split(".");
      var hasNative = +versArr[0] > 10 || +versArr[0] === 10 && +versArr[1] >= 12;
      var useNative = !hasNative ? () => false : (opts) => opts.mkdir === fs.mkdir;
      var useNativeSync = !hasNative ? () => false : (opts) => opts.mkdirSync === fs.mkdirSync;
      module22.exports = { useNative, useNativeSync };
    }
  });
  var require_mkdirp = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/mkdirp@1.0.4/node_modules/mkdirp/index.js"(exports2, module22) {
      var optsArg = require_opts_arg();
      var pathArg = require_path_arg();
      var { mkdirpNative, mkdirpNativeSync } = require_mkdirp_native();
      var { mkdirpManual, mkdirpManualSync } = require_mkdirp_manual();
      var { useNative, useNativeSync } = require_use_native();
      var mkdirp = (path, opts) => {
        path = pathArg(path);
        opts = optsArg(opts);
        return useNative(opts) ? mkdirpNative(path, opts) : mkdirpManual(path, opts);
      };
      var mkdirpSync = (path, opts) => {
        path = pathArg(path);
        opts = optsArg(opts);
        return useNativeSync(opts) ? mkdirpNativeSync(path, opts) : mkdirpManualSync(path, opts);
      };
      mkdirp.sync = mkdirpSync;
      mkdirp.native = (path, opts) => mkdirpNative(pathArg(path), optsArg(opts));
      mkdirp.manual = (path, opts) => mkdirpManual(pathArg(path), optsArg(opts));
      mkdirp.nativeSync = (path, opts) => mkdirpNativeSync(pathArg(path), optsArg(opts));
      mkdirp.manualSync = (path, opts) => mkdirpManualSync(pathArg(path), optsArg(opts));
      module22.exports = mkdirp;
    }
  });
  var require_chownr = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/chownr@2.0.0/node_modules/chownr/chownr.js"(exports2, module22) {
      "use strict";
      var fs = require2("fs");
      var path = require2("path");
      var LCHOWN = fs.lchown ? "lchown" : "chown";
      var LCHOWNSYNC = fs.lchownSync ? "lchownSync" : "chownSync";
      var needEISDIRHandled = fs.lchown && !process.version.match(/v1[1-9]+\./) && !process.version.match(/v10\.[6-9]/);
      var lchownSync = (path2, uid, gid) => {
        try {
          return fs[LCHOWNSYNC](path2, uid, gid);
        } catch (er) {
          if (er.code !== "ENOENT")
            throw er;
        }
      };
      var chownSync = (path2, uid, gid) => {
        try {
          return fs.chownSync(path2, uid, gid);
        } catch (er) {
          if (er.code !== "ENOENT")
            throw er;
        }
      };
      var handleEISDIR = needEISDIRHandled ? (path2, uid, gid, cb) => (er) => {
        if (!er || er.code !== "EISDIR")
          cb(er);
        else
          fs.chown(path2, uid, gid, cb);
      } : (_, __, ___, cb) => cb;
      var handleEISDirSync = needEISDIRHandled ? (path2, uid, gid) => {
        try {
          return lchownSync(path2, uid, gid);
        } catch (er) {
          if (er.code !== "EISDIR")
            throw er;
          chownSync(path2, uid, gid);
        }
      } : (path2, uid, gid) => lchownSync(path2, uid, gid);
      var nodeVersion = process.version;
      var readdir = (path2, options, cb) => fs.readdir(path2, options, cb);
      var readdirSync = (path2, options) => fs.readdirSync(path2, options);
      if (/^v4\./.test(nodeVersion))
        readdir = (path2, options, cb) => fs.readdir(path2, cb);
      var chown = (cpath, uid, gid, cb) => {
        fs[LCHOWN](cpath, uid, gid, handleEISDIR(cpath, uid, gid, (er) => {
          cb(er && er.code !== "ENOENT" ? er : null);
        }));
      };
      var chownrKid = (p, child, uid, gid, cb) => {
        if (typeof child === "string")
          return fs.lstat(path.resolve(p, child), (er, stats) => {
            if (er)
              return cb(er.code !== "ENOENT" ? er : null);
            stats.name = child;
            chownrKid(p, stats, uid, gid, cb);
          });
        if (child.isDirectory()) {
          chownr(path.resolve(p, child.name), uid, gid, (er) => {
            if (er)
              return cb(er);
            const cpath = path.resolve(p, child.name);
            chown(cpath, uid, gid, cb);
          });
        } else {
          const cpath = path.resolve(p, child.name);
          chown(cpath, uid, gid, cb);
        }
      };
      var chownr = (p, uid, gid, cb) => {
        readdir(p, { withFileTypes: true }, (er, children) => {
          if (er) {
            if (er.code === "ENOENT")
              return cb();
            else if (er.code !== "ENOTDIR" && er.code !== "ENOTSUP")
              return cb(er);
          }
          if (er || !children.length)
            return chown(p, uid, gid, cb);
          let len = children.length;
          let errState = null;
          const then = (er2) => {
            if (errState)
              return;
            if (er2)
              return cb(errState = er2);
            if (--len === 0)
              return chown(p, uid, gid, cb);
          };
          children.forEach((child) => chownrKid(p, child, uid, gid, then));
        });
      };
      var chownrKidSync = (p, child, uid, gid) => {
        if (typeof child === "string") {
          try {
            const stats = fs.lstatSync(path.resolve(p, child));
            stats.name = child;
            child = stats;
          } catch (er) {
            if (er.code === "ENOENT")
              return;
            else
              throw er;
          }
        }
        if (child.isDirectory())
          chownrSync(path.resolve(p, child.name), uid, gid);
        handleEISDirSync(path.resolve(p, child.name), uid, gid);
      };
      var chownrSync = (p, uid, gid) => {
        let children;
        try {
          children = readdirSync(p, { withFileTypes: true });
        } catch (er) {
          if (er.code === "ENOENT")
            return;
          else if (er.code === "ENOTDIR" || er.code === "ENOTSUP")
            return handleEISDirSync(p, uid, gid);
          else
            throw er;
        }
        if (children && children.length)
          children.forEach((child) => chownrKidSync(p, child, uid, gid));
        return handleEISDirSync(p, uid, gid);
      };
      module22.exports = chownr;
      chownr.sync = chownrSync;
    }
  });
  var require_mkdir = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/mkdir.js"(exports2, module22) {
      "use strict";
      var mkdirp = require_mkdirp();
      var fs = require2("fs");
      var path = require2("path");
      var chownr = require_chownr();
      var normPath = require_normalize_windows_path();
      var SymlinkError = class extends Error {
        constructor(symlink, path2) {
          super("Cannot extract through symbolic link");
          this.path = path2;
          this.symlink = symlink;
        }
        get name() {
          return "SylinkError";
        }
      };
      var CwdError = class extends Error {
        constructor(path2, code) {
          super(code + ": Cannot cd into '" + path2 + "'");
          this.path = path2;
          this.code = code;
        }
        get name() {
          return "CwdError";
        }
      };
      var cGet = (cache, key) => cache.get(normPath(key));
      var cSet = (cache, key, val) => cache.set(normPath(key), val);
      var checkCwd = (dir, cb) => {
        fs.stat(dir, (er, st) => {
          if (er || !st.isDirectory())
            er = new CwdError(dir, er && er.code || "ENOTDIR");
          cb(er);
        });
      };
      module22.exports = (dir, opt, cb) => {
        dir = normPath(dir);
        const umask = opt.umask;
        const mode = opt.mode | 448;
        const needChmod = (mode & umask) !== 0;
        const uid = opt.uid;
        const gid = opt.gid;
        const doChown = typeof uid === "number" && typeof gid === "number" && (uid !== opt.processUid || gid !== opt.processGid);
        const preserve = opt.preserve;
        const unlink = opt.unlink;
        const cache = opt.cache;
        const cwd = normPath(opt.cwd);
        const done = (er, created) => {
          if (er)
            cb(er);
          else {
            cSet(cache, dir, true);
            if (created && doChown)
              chownr(created, uid, gid, (er2) => done(er2));
            else if (needChmod)
              fs.chmod(dir, mode, cb);
            else
              cb();
          }
        };
        if (cache && cGet(cache, dir) === true)
          return done();
        if (dir === cwd)
          return checkCwd(dir, done);
        if (preserve)
          return mkdirp(dir, { mode }).then((made) => done(null, made), done);
        const sub = normPath(path.relative(cwd, dir));
        const parts = sub.split("/");
        mkdir_(cwd, parts, mode, cache, unlink, cwd, null, done);
      };
      var mkdir_ = (base, parts, mode, cache, unlink, cwd, created, cb) => {
        if (!parts.length)
          return cb(null, created);
        const p = parts.shift();
        const part = normPath(path.resolve(base + "/" + p));
        if (cGet(cache, part))
          return mkdir_(part, parts, mode, cache, unlink, cwd, created, cb);
        fs.mkdir(part, mode, onmkdir(part, parts, mode, cache, unlink, cwd, created, cb));
      };
      var onmkdir = (part, parts, mode, cache, unlink, cwd, created, cb) => (er) => {
        if (er) {
          fs.lstat(part, (statEr, st) => {
            if (statEr) {
              statEr.path = statEr.path && normPath(statEr.path);
              cb(statEr);
            } else if (st.isDirectory())
              mkdir_(part, parts, mode, cache, unlink, cwd, created, cb);
            else if (unlink) {
              fs.unlink(part, (er2) => {
                if (er2)
                  return cb(er2);
                fs.mkdir(part, mode, onmkdir(part, parts, mode, cache, unlink, cwd, created, cb));
              });
            } else if (st.isSymbolicLink())
              return cb(new SymlinkError(part, part + "/" + parts.join("/")));
            else
              cb(er);
          });
        } else {
          created = created || part;
          mkdir_(part, parts, mode, cache, unlink, cwd, created, cb);
        }
      };
      var checkCwdSync = (dir) => {
        let ok = false;
        let code = "ENOTDIR";
        try {
          ok = fs.statSync(dir).isDirectory();
        } catch (er) {
          code = er.code;
        } finally {
          if (!ok)
            throw new CwdError(dir, code);
        }
      };
      module22.exports.sync = (dir, opt) => {
        dir = normPath(dir);
        const umask = opt.umask;
        const mode = opt.mode | 448;
        const needChmod = (mode & umask) !== 0;
        const uid = opt.uid;
        const gid = opt.gid;
        const doChown = typeof uid === "number" && typeof gid === "number" && (uid !== opt.processUid || gid !== opt.processGid);
        const preserve = opt.preserve;
        const unlink = opt.unlink;
        const cache = opt.cache;
        const cwd = normPath(opt.cwd);
        const done = (created2) => {
          cSet(cache, dir, true);
          if (created2 && doChown)
            chownr.sync(created2, uid, gid);
          if (needChmod)
            fs.chmodSync(dir, mode);
        };
        if (cache && cGet(cache, dir) === true)
          return done();
        if (dir === cwd) {
          checkCwdSync(cwd);
          return done();
        }
        if (preserve)
          return done(mkdirp.sync(dir, mode));
        const sub = normPath(path.relative(cwd, dir));
        const parts = sub.split("/");
        let created = null;
        for (let p = parts.shift(), part = cwd; p && (part += "/" + p); p = parts.shift()) {
          part = normPath(path.resolve(part));
          if (cGet(cache, part))
            continue;
          try {
            fs.mkdirSync(part, mode);
            created = created || part;
            cSet(cache, part, true);
          } catch (er) {
            const st = fs.lstatSync(part);
            if (st.isDirectory()) {
              cSet(cache, part, true);
              continue;
            } else if (unlink) {
              fs.unlinkSync(part);
              fs.mkdirSync(part, mode);
              created = created || part;
              cSet(cache, part, true);
              continue;
            } else if (st.isSymbolicLink())
              return new SymlinkError(part, part + "/" + parts.join("/"));
          }
        }
        return done(created);
      };
    }
  });
  var require_normalize_unicode = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/normalize-unicode.js"(exports2, module22) {
      var normalizeCache = /* @__PURE__ */ Object.create(null);
      var { hasOwnProperty } = Object.prototype;
      module22.exports = (s) => {
        if (!hasOwnProperty.call(normalizeCache, s))
          normalizeCache[s] = s.normalize("NFKD");
        return normalizeCache[s];
      };
    }
  });
  var require_path_reservations = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/path-reservations.js"(exports2, module22) {
      var assert = require2("assert");
      var normalize = require_normalize_unicode();
      var stripSlashes = require_strip_trailing_slashes();
      var { join } = require2("path");
      var platform = process.env.TESTING_TAR_FAKE_PLATFORM || process.platform;
      var isWindows = platform === "win32";
      module22.exports = () => {
        const queues = /* @__PURE__ */ new Map();
        const reservations = /* @__PURE__ */ new Map();
        const getDirs = (path) => {
          const dirs = path.split("/").slice(0, -1).reduce((set, path2) => {
            if (set.length)
              path2 = join(set[set.length - 1], path2);
            set.push(path2 || "/");
            return set;
          }, []);
          return dirs;
        };
        const running = /* @__PURE__ */ new Set();
        const getQueues = (fn) => {
          const res = reservations.get(fn);
          if (!res)
            throw new Error("function does not have any path reservations");
          return {
            paths: res.paths.map((path) => queues.get(path)),
            dirs: [...res.dirs].map((path) => queues.get(path))
          };
        };
        const check = (fn) => {
          const { paths, dirs } = getQueues(fn);
          return paths.every((q) => q[0] === fn) && dirs.every((q) => q[0] instanceof Set && q[0].has(fn));
        };
        const run = (fn) => {
          if (running.has(fn) || !check(fn))
            return false;
          running.add(fn);
          fn(() => clear(fn));
          return true;
        };
        const clear = (fn) => {
          if (!running.has(fn))
            return false;
          const { paths, dirs } = reservations.get(fn);
          const next = /* @__PURE__ */ new Set();
          paths.forEach((path) => {
            const q = queues.get(path);
            assert.equal(q[0], fn);
            if (q.length === 1)
              queues.delete(path);
            else {
              q.shift();
              if (typeof q[0] === "function")
                next.add(q[0]);
              else
                q[0].forEach((fn2) => next.add(fn2));
            }
          });
          dirs.forEach((dir) => {
            const q = queues.get(dir);
            assert(q[0] instanceof Set);
            if (q[0].size === 1 && q.length === 1)
              queues.delete(dir);
            else if (q[0].size === 1) {
              q.shift();
              next.add(q[0]);
            } else
              q[0].delete(fn);
          });
          running.delete(fn);
          next.forEach((fn2) => run(fn2));
          return true;
        };
        const reserve = (paths, fn) => {
          paths = isWindows ? ["win32 parallelization disabled"] : paths.map((p) => {
            return normalize(stripSlashes(join(p))).toLowerCase();
          });
          const dirs = new Set(
            paths.map((path) => getDirs(path)).reduce((a, b) => a.concat(b))
          );
          reservations.set(fn, { dirs, paths });
          paths.forEach((path) => {
            const q = queues.get(path);
            if (!q)
              queues.set(path, [fn]);
            else
              q.push(fn);
          });
          dirs.forEach((dir) => {
            const q = queues.get(dir);
            if (!q)
              queues.set(dir, [/* @__PURE__ */ new Set([fn])]);
            else if (q[q.length - 1] instanceof Set)
              q[q.length - 1].add(fn);
            else
              q.push(/* @__PURE__ */ new Set([fn]));
          });
          return run(fn);
        };
        return { check, reserve };
      };
    }
  });
  var require_get_write_flag = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/get-write-flag.js"(exports2, module22) {
      var platform = process.env.__FAKE_PLATFORM__ || process.platform;
      var isWindows = platform === "win32";
      var fs = global2.__FAKE_TESTING_FS__ || require2("fs");
      var { O_CREAT, O_TRUNC, O_WRONLY, UV_FS_O_FILEMAP = 0 } = fs.constants;
      var fMapEnabled = isWindows && !!UV_FS_O_FILEMAP;
      var fMapLimit = 512 * 1024;
      var fMapFlag = UV_FS_O_FILEMAP | O_TRUNC | O_CREAT | O_WRONLY;
      module22.exports = !fMapEnabled ? () => "w" : (size) => size < fMapLimit ? fMapFlag : "w";
    }
  });
  var require_unpack = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/unpack.js"(exports2, module22) {
      "use strict";
      var assert = require2("assert");
      var Parser = require_parse();
      var fs = require2("fs");
      var fsm = require_fs_minipass();
      var path = require2("path");
      var mkdir = require_mkdir();
      var wc = require_winchars();
      var pathReservations = require_path_reservations();
      var stripAbsolutePath = require_strip_absolute_path();
      var normPath = require_normalize_windows_path();
      var stripSlash = require_strip_trailing_slashes();
      var normalize = require_normalize_unicode();
      var ONENTRY = Symbol("onEntry");
      var CHECKFS = Symbol("checkFs");
      var CHECKFS2 = Symbol("checkFs2");
      var PRUNECACHE = Symbol("pruneCache");
      var ISREUSABLE = Symbol("isReusable");
      var MAKEFS = Symbol("makeFs");
      var FILE = Symbol("file");
      var DIRECTORY = Symbol("directory");
      var LINK = Symbol("link");
      var SYMLINK = Symbol("symlink");
      var HARDLINK = Symbol("hardlink");
      var UNSUPPORTED = Symbol("unsupported");
      var CHECKPATH = Symbol("checkPath");
      var MKDIR = Symbol("mkdir");
      var ONERROR = Symbol("onError");
      var PENDING = Symbol("pending");
      var PEND = Symbol("pend");
      var UNPEND = Symbol("unpend");
      var ENDED = Symbol("ended");
      var MAYBECLOSE = Symbol("maybeClose");
      var SKIP = Symbol("skip");
      var DOCHOWN = Symbol("doChown");
      var UID = Symbol("uid");
      var GID = Symbol("gid");
      var CHECKED_CWD = Symbol("checkedCwd");
      var crypto = require2("crypto");
      var getFlag = require_get_write_flag();
      var platform = process.env.TESTING_TAR_FAKE_PLATFORM || process.platform;
      var isWindows = platform === "win32";
      var unlinkFile = (path2, cb) => {
        if (!isWindows)
          return fs.unlink(path2, cb);
        const name = path2 + ".DELETE." + crypto.randomBytes(16).toString("hex");
        fs.rename(path2, name, (er) => {
          if (er)
            return cb(er);
          fs.unlink(name, cb);
        });
      };
      var unlinkFileSync = (path2) => {
        if (!isWindows)
          return fs.unlinkSync(path2);
        const name = path2 + ".DELETE." + crypto.randomBytes(16).toString("hex");
        fs.renameSync(path2, name);
        fs.unlinkSync(name);
      };
      var uint32 = (a, b, c) => a === a >>> 0 ? a : b === b >>> 0 ? b : c;
      var cacheKeyNormalize = (path2) => normalize(stripSlash(normPath(path2))).toLowerCase();
      var pruneCache = (cache, abs) => {
        abs = cacheKeyNormalize(abs);
        for (const path2 of cache.keys()) {
          const pnorm = cacheKeyNormalize(path2);
          if (pnorm === abs || pnorm.indexOf(abs + "/") === 0)
            cache.delete(path2);
        }
      };
      var dropCache = (cache) => {
        for (const key of cache.keys())
          cache.delete(key);
      };
      var Unpack = class extends Parser {
        constructor(opt) {
          if (!opt)
            opt = {};
          opt.ondone = (_) => {
            this[ENDED] = true;
            this[MAYBECLOSE]();
          };
          super(opt);
          this[CHECKED_CWD] = false;
          this.reservations = pathReservations();
          this.transform = typeof opt.transform === "function" ? opt.transform : null;
          this.writable = true;
          this.readable = false;
          this[PENDING] = 0;
          this[ENDED] = false;
          this.dirCache = opt.dirCache || /* @__PURE__ */ new Map();
          if (typeof opt.uid === "number" || typeof opt.gid === "number") {
            if (typeof opt.uid !== "number" || typeof opt.gid !== "number")
              throw new TypeError("cannot set owner without number uid and gid");
            if (opt.preserveOwner) {
              throw new TypeError(
                "cannot preserve owner in archive and also set owner explicitly"
              );
            }
            this.uid = opt.uid;
            this.gid = opt.gid;
            this.setOwner = true;
          } else {
            this.uid = null;
            this.gid = null;
            this.setOwner = false;
          }
          if (opt.preserveOwner === void 0 && typeof opt.uid !== "number")
            this.preserveOwner = process.getuid && process.getuid() === 0;
          else
            this.preserveOwner = !!opt.preserveOwner;
          this.processUid = (this.preserveOwner || this.setOwner) && process.getuid ? process.getuid() : null;
          this.processGid = (this.preserveOwner || this.setOwner) && process.getgid ? process.getgid() : null;
          this.forceChown = opt.forceChown === true;
          this.win32 = !!opt.win32 || isWindows;
          this.newer = !!opt.newer;
          this.keep = !!opt.keep;
          this.noMtime = !!opt.noMtime;
          this.preservePaths = !!opt.preservePaths;
          this.unlink = !!opt.unlink;
          this.cwd = normPath(path.resolve(opt.cwd || process.cwd()));
          this.strip = +opt.strip || 0;
          this.processUmask = opt.noChmod ? 0 : process.umask();
          this.umask = typeof opt.umask === "number" ? opt.umask : this.processUmask;
          this.dmode = opt.dmode || 511 & ~this.umask;
          this.fmode = opt.fmode || 438 & ~this.umask;
          this.on("entry", (entry) => this[ONENTRY](entry));
        }
        // a bad or damaged archive is a warning for Parser, but an error
        // when extracting.  Mark those errors as unrecoverable, because
        // the Unpack contract cannot be met.
        warn(code, msg, data = {}) {
          if (code === "TAR_BAD_ARCHIVE" || code === "TAR_ABORT")
            data.recoverable = false;
          return super.warn(code, msg, data);
        }
        [MAYBECLOSE]() {
          if (this[ENDED] && this[PENDING] === 0) {
            this.emit("prefinish");
            this.emit("finish");
            this.emit("end");
            this.emit("close");
          }
        }
        [CHECKPATH](entry) {
          if (this.strip) {
            const parts = normPath(entry.path).split("/");
            if (parts.length < this.strip)
              return false;
            entry.path = parts.slice(this.strip).join("/");
            if (entry.type === "Link") {
              const linkparts = normPath(entry.linkpath).split("/");
              if (linkparts.length >= this.strip)
                entry.linkpath = linkparts.slice(this.strip).join("/");
              else
                return false;
            }
          }
          if (!this.preservePaths) {
            const p = normPath(entry.path);
            const parts = p.split("/");
            if (parts.includes("..") || isWindows && /^[a-z]:\.\.$/i.test(parts[0])) {
              this.warn("TAR_ENTRY_ERROR", `path contains '..'`, {
                entry,
                path: p
              });
              return false;
            }
            const [root, stripped] = stripAbsolutePath(p);
            if (root) {
              entry.path = stripped;
              this.warn("TAR_ENTRY_INFO", `stripping ${root} from absolute path`, {
                entry,
                path: p
              });
            }
          }
          if (path.isAbsolute(entry.path))
            entry.absolute = normPath(path.resolve(entry.path));
          else
            entry.absolute = normPath(path.resolve(this.cwd, entry.path));
          if (!this.preservePaths && entry.absolute.indexOf(this.cwd + "/") !== 0 && entry.absolute !== this.cwd) {
            this.warn("TAR_ENTRY_ERROR", "path escaped extraction target", {
              entry,
              path: normPath(entry.path),
              resolvedPath: entry.absolute,
              cwd: this.cwd
            });
            return false;
          }
          if (entry.absolute === this.cwd && entry.type !== "Directory" && entry.type !== "GNUDumpDir")
            return false;
          if (this.win32) {
            const { root: aRoot } = path.win32.parse(entry.absolute);
            entry.absolute = aRoot + wc.encode(entry.absolute.substr(aRoot.length));
            const { root: pRoot } = path.win32.parse(entry.path);
            entry.path = pRoot + wc.encode(entry.path.substr(pRoot.length));
          }
          return true;
        }
        [ONENTRY](entry) {
          if (!this[CHECKPATH](entry))
            return entry.resume();
          assert.equal(typeof entry.absolute, "string");
          switch (entry.type) {
            case "Directory":
            case "GNUDumpDir":
              if (entry.mode)
                entry.mode = entry.mode | 448;
            case "File":
            case "OldFile":
            case "ContiguousFile":
            case "Link":
            case "SymbolicLink":
              return this[CHECKFS](entry);
            case "CharacterDevice":
            case "BlockDevice":
            case "FIFO":
            default:
              return this[UNSUPPORTED](entry);
          }
        }
        [ONERROR](er, entry) {
          if (er.name === "CwdError")
            this.emit("error", er);
          else {
            this.warn("TAR_ENTRY_ERROR", er, { entry });
            this[UNPEND]();
            entry.resume();
          }
        }
        [MKDIR](dir, mode, cb) {
          mkdir(normPath(dir), {
            uid: this.uid,
            gid: this.gid,
            processUid: this.processUid,
            processGid: this.processGid,
            umask: this.processUmask,
            preserve: this.preservePaths,
            unlink: this.unlink,
            cache: this.dirCache,
            cwd: this.cwd,
            mode,
            noChmod: this.noChmod
          }, cb);
        }
        [DOCHOWN](entry) {
          return this.forceChown || this.preserveOwner && (typeof entry.uid === "number" && entry.uid !== this.processUid || typeof entry.gid === "number" && entry.gid !== this.processGid) || (typeof this.uid === "number" && this.uid !== this.processUid || typeof this.gid === "number" && this.gid !== this.processGid);
        }
        [UID](entry) {
          return uint32(this.uid, entry.uid, this.processUid);
        }
        [GID](entry) {
          return uint32(this.gid, entry.gid, this.processGid);
        }
        [FILE](entry, fullyDone) {
          const mode = entry.mode & 4095 || this.fmode;
          const stream = new fsm.WriteStream(entry.absolute, {
            flags: getFlag(entry.size),
            mode,
            autoClose: false
          });
          stream.on("error", (er) => {
            if (stream.fd)
              fs.close(stream.fd, () => {
              });
            stream.write = () => true;
            this[ONERROR](er, entry);
            fullyDone();
          });
          let actions = 1;
          const done = (er) => {
            if (er) {
              if (stream.fd)
                fs.close(stream.fd, () => {
                });
              this[ONERROR](er, entry);
              fullyDone();
              return;
            }
            if (--actions === 0) {
              fs.close(stream.fd, (er2) => {
                if (er2)
                  this[ONERROR](er2, entry);
                else
                  this[UNPEND]();
                fullyDone();
              });
            }
          };
          stream.on("finish", (_) => {
            const abs = entry.absolute;
            const fd = stream.fd;
            if (entry.mtime && !this.noMtime) {
              actions++;
              const atime = entry.atime || /* @__PURE__ */ new Date();
              const mtime = entry.mtime;
              fs.futimes(fd, atime, mtime, (er) => er ? fs.utimes(abs, atime, mtime, (er2) => done(er2 && er)) : done());
            }
            if (this[DOCHOWN](entry)) {
              actions++;
              const uid = this[UID](entry);
              const gid = this[GID](entry);
              fs.fchown(fd, uid, gid, (er) => er ? fs.chown(abs, uid, gid, (er2) => done(er2 && er)) : done());
            }
            done();
          });
          const tx = this.transform ? this.transform(entry) || entry : entry;
          if (tx !== entry) {
            tx.on("error", (er) => {
              this[ONERROR](er, entry);
              fullyDone();
            });
            entry.pipe(tx);
          }
          tx.pipe(stream);
        }
        [DIRECTORY](entry, fullyDone) {
          const mode = entry.mode & 4095 || this.dmode;
          this[MKDIR](entry.absolute, mode, (er) => {
            if (er) {
              this[ONERROR](er, entry);
              fullyDone();
              return;
            }
            let actions = 1;
            const done = (_) => {
              if (--actions === 0) {
                fullyDone();
                this[UNPEND]();
                entry.resume();
              }
            };
            if (entry.mtime && !this.noMtime) {
              actions++;
              fs.utimes(entry.absolute, entry.atime || /* @__PURE__ */ new Date(), entry.mtime, done);
            }
            if (this[DOCHOWN](entry)) {
              actions++;
              fs.chown(entry.absolute, this[UID](entry), this[GID](entry), done);
            }
            done();
          });
        }
        [UNSUPPORTED](entry) {
          entry.unsupported = true;
          this.warn(
            "TAR_ENTRY_UNSUPPORTED",
            `unsupported entry type: ${entry.type}`,
            { entry }
          );
          entry.resume();
        }
        [SYMLINK](entry, done) {
          this[LINK](entry, entry.linkpath, "symlink", done);
        }
        [HARDLINK](entry, done) {
          const linkpath = normPath(path.resolve(this.cwd, entry.linkpath));
          this[LINK](entry, linkpath, "link", done);
        }
        [PEND]() {
          this[PENDING]++;
        }
        [UNPEND]() {
          this[PENDING]--;
          this[MAYBECLOSE]();
        }
        [SKIP](entry) {
          this[UNPEND]();
          entry.resume();
        }
        // Check if we can reuse an existing filesystem entry safely and
        // overwrite it, rather than unlinking and recreating
        // Windows doesn't report a useful nlink, so we just never reuse entries
        [ISREUSABLE](entry, st) {
          return entry.type === "File" && !this.unlink && st.isFile() && st.nlink <= 1 && !isWindows;
        }
        // check if a thing is there, and if so, try to clobber it
        [CHECKFS](entry) {
          this[PEND]();
          const paths = [entry.path];
          if (entry.linkpath)
            paths.push(entry.linkpath);
          this.reservations.reserve(paths, (done) => this[CHECKFS2](entry, done));
        }
        [PRUNECACHE](entry) {
          if (entry.type === "SymbolicLink")
            dropCache(this.dirCache);
          else if (entry.type !== "Directory")
            pruneCache(this.dirCache, entry.absolute);
        }
        [CHECKFS2](entry, fullyDone) {
          this[PRUNECACHE](entry);
          const done = (er) => {
            this[PRUNECACHE](entry);
            fullyDone(er);
          };
          const checkCwd = () => {
            this[MKDIR](this.cwd, this.dmode, (er) => {
              if (er) {
                this[ONERROR](er, entry);
                done();
                return;
              }
              this[CHECKED_CWD] = true;
              start();
            });
          };
          const start = () => {
            if (entry.absolute !== this.cwd) {
              const parent = normPath(path.dirname(entry.absolute));
              if (parent !== this.cwd) {
                return this[MKDIR](parent, this.dmode, (er) => {
                  if (er) {
                    this[ONERROR](er, entry);
                    done();
                    return;
                  }
                  afterMakeParent();
                });
              }
            }
            afterMakeParent();
          };
          const afterMakeParent = () => {
            fs.lstat(entry.absolute, (lstatEr, st) => {
              if (st && (this.keep || this.newer && st.mtime > entry.mtime)) {
                this[SKIP](entry);
                done();
                return;
              }
              if (lstatEr || this[ISREUSABLE](entry, st))
                return this[MAKEFS](null, entry, done);
              if (st.isDirectory()) {
                if (entry.type === "Directory") {
                  const needChmod = !this.noChmod && entry.mode && (st.mode & 4095) !== entry.mode;
                  const afterChmod = (er) => this[MAKEFS](er, entry, done);
                  if (!needChmod)
                    return afterChmod();
                  return fs.chmod(entry.absolute, entry.mode, afterChmod);
                }
                if (entry.absolute !== this.cwd) {
                  return fs.rmdir(entry.absolute, (er) => this[MAKEFS](er, entry, done));
                }
              }
              if (entry.absolute === this.cwd)
                return this[MAKEFS](null, entry, done);
              unlinkFile(entry.absolute, (er) => this[MAKEFS](er, entry, done));
            });
          };
          if (this[CHECKED_CWD])
            start();
          else
            checkCwd();
        }
        [MAKEFS](er, entry, done) {
          if (er) {
            this[ONERROR](er, entry);
            done();
            return;
          }
          switch (entry.type) {
            case "File":
            case "OldFile":
            case "ContiguousFile":
              return this[FILE](entry, done);
            case "Link":
              return this[HARDLINK](entry, done);
            case "SymbolicLink":
              return this[SYMLINK](entry, done);
            case "Directory":
            case "GNUDumpDir":
              return this[DIRECTORY](entry, done);
          }
        }
        [LINK](entry, linkpath, link, done) {
          fs[link](linkpath, entry.absolute, (er) => {
            if (er)
              this[ONERROR](er, entry);
            else {
              this[UNPEND]();
              entry.resume();
            }
            done();
          });
        }
      };
      var callSync = (fn) => {
        try {
          return [null, fn()];
        } catch (er) {
          return [er, null];
        }
      };
      var UnpackSync = class extends Unpack {
        [MAKEFS](er, entry) {
          return super[MAKEFS](er, entry, () => {
          });
        }
        [CHECKFS](entry) {
          this[PRUNECACHE](entry);
          if (!this[CHECKED_CWD]) {
            const er2 = this[MKDIR](this.cwd, this.dmode);
            if (er2)
              return this[ONERROR](er2, entry);
            this[CHECKED_CWD] = true;
          }
          if (entry.absolute !== this.cwd) {
            const parent = normPath(path.dirname(entry.absolute));
            if (parent !== this.cwd) {
              const mkParent = this[MKDIR](parent, this.dmode);
              if (mkParent)
                return this[ONERROR](mkParent, entry);
            }
          }
          const [lstatEr, st] = callSync(() => fs.lstatSync(entry.absolute));
          if (st && (this.keep || this.newer && st.mtime > entry.mtime))
            return this[SKIP](entry);
          if (lstatEr || this[ISREUSABLE](entry, st))
            return this[MAKEFS](null, entry);
          if (st.isDirectory()) {
            if (entry.type === "Directory") {
              const needChmod = !this.noChmod && entry.mode && (st.mode & 4095) !== entry.mode;
              const [er3] = needChmod ? callSync(() => {
                fs.chmodSync(entry.absolute, entry.mode);
              }) : [];
              return this[MAKEFS](er3, entry);
            }
            const [er2] = callSync(() => fs.rmdirSync(entry.absolute));
            this[MAKEFS](er2, entry);
          }
          const [er] = entry.absolute === this.cwd ? [] : callSync(() => unlinkFileSync(entry.absolute));
          this[MAKEFS](er, entry);
        }
        [FILE](entry, done) {
          const mode = entry.mode & 4095 || this.fmode;
          const oner = (er) => {
            let closeError;
            try {
              fs.closeSync(fd);
            } catch (e) {
              closeError = e;
            }
            if (er || closeError)
              this[ONERROR](er || closeError, entry);
            done();
          };
          let fd;
          try {
            fd = fs.openSync(entry.absolute, getFlag(entry.size), mode);
          } catch (er) {
            return oner(er);
          }
          const tx = this.transform ? this.transform(entry) || entry : entry;
          if (tx !== entry) {
            tx.on("error", (er) => this[ONERROR](er, entry));
            entry.pipe(tx);
          }
          tx.on("data", (chunk) => {
            try {
              fs.writeSync(fd, chunk, 0, chunk.length);
            } catch (er) {
              oner(er);
            }
          });
          tx.on("end", (_) => {
            let er = null;
            if (entry.mtime && !this.noMtime) {
              const atime = entry.atime || /* @__PURE__ */ new Date();
              const mtime = entry.mtime;
              try {
                fs.futimesSync(fd, atime, mtime);
              } catch (futimeser) {
                try {
                  fs.utimesSync(entry.absolute, atime, mtime);
                } catch (utimeser) {
                  er = futimeser;
                }
              }
            }
            if (this[DOCHOWN](entry)) {
              const uid = this[UID](entry);
              const gid = this[GID](entry);
              try {
                fs.fchownSync(fd, uid, gid);
              } catch (fchowner) {
                try {
                  fs.chownSync(entry.absolute, uid, gid);
                } catch (chowner) {
                  er = er || fchowner;
                }
              }
            }
            oner(er);
          });
        }
        [DIRECTORY](entry, done) {
          const mode = entry.mode & 4095 || this.dmode;
          const er = this[MKDIR](entry.absolute, mode);
          if (er) {
            this[ONERROR](er, entry);
            done();
            return;
          }
          if (entry.mtime && !this.noMtime) {
            try {
              fs.utimesSync(entry.absolute, entry.atime || /* @__PURE__ */ new Date(), entry.mtime);
            } catch (er2) {
            }
          }
          if (this[DOCHOWN](entry)) {
            try {
              fs.chownSync(entry.absolute, this[UID](entry), this[GID](entry));
            } catch (er2) {
            }
          }
          done();
          entry.resume();
        }
        [MKDIR](dir, mode) {
          try {
            return mkdir.sync(normPath(dir), {
              uid: this.uid,
              gid: this.gid,
              processUid: this.processUid,
              processGid: this.processGid,
              umask: this.processUmask,
              preserve: this.preservePaths,
              unlink: this.unlink,
              cache: this.dirCache,
              cwd: this.cwd,
              mode
            });
          } catch (er) {
            return er;
          }
        }
        [LINK](entry, linkpath, link, done) {
          try {
            fs[link + "Sync"](linkpath, entry.absolute);
            done();
            entry.resume();
          } catch (er) {
            return this[ONERROR](er, entry);
          }
        }
      };
      Unpack.Sync = UnpackSync;
      module22.exports = Unpack;
    }
  });
  var require_extract = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/lib/extract.js"(exports2, module22) {
      "use strict";
      var hlo = require_high_level_opt();
      var Unpack = require_unpack();
      var fs = require2("fs");
      var fsm = require_fs_minipass();
      var path = require2("path");
      var stripSlash = require_strip_trailing_slashes();
      module22.exports = (opt_, files, cb) => {
        if (typeof opt_ === "function")
          cb = opt_, files = null, opt_ = {};
        else if (Array.isArray(opt_))
          files = opt_, opt_ = {};
        if (typeof files === "function")
          cb = files, files = null;
        if (!files)
          files = [];
        else
          files = Array.from(files);
        const opt = hlo(opt_);
        if (opt.sync && typeof cb === "function")
          throw new TypeError("callback not supported for sync tar functions");
        if (!opt.file && typeof cb === "function")
          throw new TypeError("callback only supported with file option");
        if (files.length)
          filesFilter(opt, files);
        return opt.file && opt.sync ? extractFileSync(opt) : opt.file ? extractFile(opt, cb) : opt.sync ? extractSync(opt) : extract(opt);
      };
      var filesFilter = (opt, files) => {
        const map = new Map(files.map((f) => [stripSlash(f), true]));
        const filter = opt.filter;
        const mapHas = (file, r) => {
          const root = r || path.parse(file).root || ".";
          const ret = file === root ? false : map.has(file) ? map.get(file) : mapHas(path.dirname(file), root);
          map.set(file, ret);
          return ret;
        };
        opt.filter = filter ? (file, entry) => filter(file, entry) && mapHas(stripSlash(file)) : (file) => mapHas(stripSlash(file));
      };
      var extractFileSync = (opt) => {
        const u = new Unpack.Sync(opt);
        const file = opt.file;
        const stat = fs.statSync(file);
        const readSize = opt.maxReadSize || 16 * 1024 * 1024;
        const stream = new fsm.ReadStreamSync(file, {
          readSize,
          size: stat.size
        });
        stream.pipe(u);
      };
      var extractFile = (opt, cb) => {
        const u = new Unpack(opt);
        const readSize = opt.maxReadSize || 16 * 1024 * 1024;
        const file = opt.file;
        const p = new Promise((resolve, reject) => {
          u.on("error", reject);
          u.on("close", resolve);
          fs.stat(file, (er, stat) => {
            if (er)
              reject(er);
            else {
              const stream = new fsm.ReadStream(file, {
                readSize,
                size: stat.size
              });
              stream.on("error", reject);
              stream.pipe(u);
            }
          });
        });
        return cb ? p.then(cb, cb) : p;
      };
      var extractSync = (opt) => new Unpack.Sync(opt);
      var extract = (opt) => new Unpack(opt);
    }
  });
  var require_tar = __commonJS({
    "../../../../../../home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/node_modules/.pnpm/tar@6.1.11/node_modules/tar/index.js"(exports2) {
      "use strict";
      exports2.c = exports2.create = require_create();
      exports2.r = exports2.replace = require_replace();
      exports2.t = exports2.list = require_list();
      exports2.u = exports2.update = require_update();
      exports2.x = exports2.extract = require_extract();
      exports2.Pack = require_pack();
      exports2.Unpack = require_unpack();
      exports2.Parse = require_parse();
      exports2.ReadEntry = require_read_entry();
      exports2.WriteEntry = require_write_entry();
      exports2.Header = require_header();
      exports2.Pax = require_pax();
      exports2.types = require_types();
    }
  });
  Object.defineProperty($$NPMRequires, "axios@0.27.2", {
    get() {
      return require_axios2();
    }
  });
  Object.defineProperty($$NPMRequires, "tar@6.1.11", {
    get() {
      return require_tar();
    }
  });
  /*! Bundled license information:
  
  mime-db/index.js:
    (*!
     * mime-db
     * Copyright(c) 2014 Jonathan Ong
     * Copyright(c) 2015-2022 Douglas Christopher Wilson
     * MIT Licensed
     *)
  
  mime-types/index.js:
    (*!
     * mime-types
     * Copyright(c) 2014 Jonathan Ong
     * Copyright(c) 2015 Douglas Christopher Wilson
     * MIT Licensed
     *)
  */
};
function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, { value, enumerable: true, configurable: true, writable: true });
  } else {
    obj[key] = value;
  }
  return obj;
}
function $$$createKModule(filename, extensions) {
  let globalFilename = filename;
  class Module {
    constructor(filename2) {
      _defineProperty(this, "exports", {});
      this.filename = filename2;
    }
  }
  class KModuleLoader {
    constructor(filename2) {
      this.$filename = filename2;
    }
    get global() {
      if (KModuleLoader.$global === void 0) {
        try {
          KModuleLoader.$global = global;
        } catch (e) {
          KModuleLoader.$global = window;
        }
      }
      return KModuleLoader.$global;
    }
    addFiles(files) {
      for (let id in files) {
        KModuleLoader.$Files[id] = files[id];
      }
      return this;
    }
    addVirtualFile(path, filedata) {
      if (this.global.kawix) {
        return this.global.kawix.addVirtualFile(path, filedata);
      }
      if (typeof filedata == "function") {
        filedata = filedata();
      }
    }
    require() {
      return this.$require(arguments);
    }
    $customImportInfo(request, module2) {
      let func = KModuleLoader.$Files[request];
      if (typeof func == "function") {
        let self = this;
        let load = function() {
          if (!module2)
            module2 = new Module(request);
          let params = {
            global: self.global,
            Buffer: self.global.Buffer,
            module: module2,
            exports: module2.exports,
            KModule: new KModuleLoader(request),
            require: self.require.bind(self),
            asyncRequire: self.import.bind(self)
          };
          func(params);
          KModuleLoader.$cache[request] = module2;
          return KModuleLoader.$cache[request].exports;
        };
        return {
          mode: "custom",
          load
        };
      }
    }
    $require(originalArgs, module2) {
      let request = originalArgs[0];
      if (KModuleLoader.$cache[request]) {
        return KModuleLoader.$cache[request].exports;
      }
      if (KModuleLoader.$Files[request]) {
        let info = this.$customImportInfo(request, module2);
        if (info && info.load) {
          try {
            return info.load();
          } catch (e) {
            console.error("[packed] Failed to load packed module:", request, e);
          }
        }
      }
      try {
        return require(request);
      } catch (e) {
      }
      if (this.global.kawix) {
        return this.global.kawix.import.apply(this.global.kawix, originalArgs);
      }
      throw new Error("Module: " + request + " not found");
    }
    getData(name) {
      if (this.global.kawix) {
        let u = this.global.kawix.getData(this.$filename, name);
        if (u === void 0) {
          u = this.global.kawix.getData(globalFilename, name);
        }
        return u;
      }
    }
    import(request) {
      try {
        let file = request;
        if (file.startsWith("file://")) {
          file = file.substring(7);
        }
        if (KModuleLoader.$Files[file]) {
          return this.$require([file]);
        }
      } catch (e) {
      }
      if (this.global.kawix) {
        return this.global.kawix.import.apply(this.global.kawix, arguments);
      }
      throw new Error("Module: " + request + " not found");
    }
  }
  _defineProperty(KModuleLoader, "$Files", {});
  _defineProperty(KModuleLoader, "$cache", {});
  _defineProperty(KModuleLoader, "$global", void 0);
  var loader = new KModuleLoader(filename);
  if (loader.global.kawix && loader.global.kawix.customImportInfo) {
    loader.global.kawix.customImportInfo.push(function(request) {
      let res = loader.$customImportInfo(request, null);
      let exts = [].concat(extensions);
      while (res === void 0) {
        let ext = exts.shift();
        if (!ext)
          break;
        res = loader.$customImportInfo(request + ext, null);
      }
      return res;
    });
  }
  return loader;
}
var $$module = null;
try {
  $$module = module;
} catch (e) {
}
var $$KModule = $$$createKModule($$filename, [".ts", ".kwb", ".kwc", ".js"]).addFiles($$Files);
$$NPMRequires = {};
$$KModule.require("/home/ubuntu/.kawi/pnpm-packages/cada819bd5a6bdfa71d014632a529653/$compiled.ts").default;
$$KModule.$require(["/data/files/projects/Kode/kwruntime/installer/src/mod.ts"], $$module);
